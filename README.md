README
======

ExamSuite allows the user to create and print bubble tests, and then grade the scanned in filled-out tests and track their scores.   

What's New
----------
Check out the presentation on each versions' changes and new features:  

- [Slideshow for Version 0.2(a)](https://docs.google.com/presentation/d/1oU-Eh82-vvkHvDzkIGruGvXIwIy5vlqsGAPFYpQXjQU/edit?usp=sharing)  
- [Slideshow for Version 0.2d](https://docs.google.com/presentation/d/1NU3BPBvfpjpILdSkvOrKqKtNNQHszGCrsjtqolWm6mQ/edit?usp=sharing)  


Assumed Terms
-------------

These are the terms ExamSuite uses and what they are:  

- **Form** - This is the collection of objects that you modify and position to define the printed __form__.  
- **Answer Key** - Like it sounds, the answer key is a collection of ordered answers that define what the correct answers are for each question.  
- **Scan** - This is the image that was scanned in, and it handles finding the objects defined in the associated __form__ with the provided image.  
- **Markup Engine** - This holds / handles the markup done to the Scans after we've graded it.  Its useful for defining what and how you want to print on the graded scan.  
- **Student** - This is a convince structure to hold a reusable First, Last, and Group Name.  These are sortable and filterable when selecting which student to assign a scan to.  
- **Exam** - An Exam is a collection of all of the above into a single persistent object.  An Exam can hold:
	* Only 1 Form, and a Form can hold only 1 Markup Engine  
	* Only 1 Answer Key  
	* Many Students / Scans   
 
## Work Flow ##

The assumed workflow a teacher would employ while using this software is this:  

1. Design a **Form** using the Form Editor.  Either a reusable generic bubble test, or a specific test with question text and bubble on the same page (_multipage forms support is planned_)  
1. Create an **Exam**, select the **Form** from step 1 and Create a new **Answer Key**  
1. Print copies of the **Form** and give them to the Students.  Collect them, Scan them.  
1. Add a **Scan** to the **Exam**, select the **Student** and, if indicated, review any answers that have a low confidence score.  Save the **Scan** to the **Exam**.  
1. Afer all **Scans** are added and review, perform a _Markup All Tests_, which is mark all tests added to the _Gradebook_ and present them to you in a single PDF for printing.  
1. Hand them back to the students. Don't allow the student to argue grades with you, say, 'Dude, a _**computer** graded it_. FILL IN YOUR BUBBLES, MAN!'  
1. Review **Exam** Analytics and decide if the test was well designed or not.  It was. You had a computer help you _and it was awesome_.  
1. Relax, have a beer; that was a lot of hard work.  Think to yourself, _how do I get the computer to do more of my work so I can skip to this part?_  

## Installation ##

I'm kidding, there isn't any.  On the left of the page, click _Downloads_ and select the latest version.  This is a Zip, unzip it to a folder.  Now _**ExamSuite**_ is living on your computer.  Start it by launching 'ExamSuite.exe' and try it out, see if you like it.

## Planned Features ##

1. Number 1 goal is to get the loading and grading of scans as reliable and autonomous as possible.  This is the slowest part of the Workflow so it is the focus.  
1. UI Design.  Its terrible, I know.  But it will be better.  It may possibly move to WPF.  
1. Multipage Forms.  This is a long way out, I haven't really started implementing it yet.  

## Help, I have issues ##

Click on _Issues_ on the left bar and post your problems / questions there.  This way everyone can see them and, if it turns out to be a bug, it can get fixes.
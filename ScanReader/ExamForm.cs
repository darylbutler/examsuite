﻿using ScanReader.ExamObjects;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.ComponentModel;

namespace ScanReader
{
	public class ExamForm
	{
		// -- Constants ---------------------------------------------------------------------------
		public static int Margin { get { return 20; } }
		public static int SquareSize { get { return 16; } }
		public static int SmallSquareSize { get { return SquareSize / 4; } }
		public static int CornerLength { get { return Margin + (SquareSize / 2); } }
		public static int Width { get { return 600; } }
		public static int Height { get { return 800; } }		

		// -- Properties --------------------------------------------------------------------------
		[Category("General"), Description("Form's Name.  This must be unique when we save.")]
		public string Name { get; set; }     // Identifier
		[Category("General"), Description("Form's Author.  For Convenience Only.")]
		public string Author { get; set; }
		[Category("General"), Description("Form's Subjet, if it has one.  For Convenience Only.")]
		public string Subject { get; set; }
		[Category("General"), Description("Form's Comments.  For Convenience.")]
		public string Comments { get; set; }

		[Category("Markup"), Description("Form's Markup Settings.  Handles how the scans of this form are drawn on to indicate information.  Useful when printing graded scans for return to students or record keeping.")]
		public ExamMarkup MarkupEngine
		{
			get
			{
				if (_markupEngine == null)
					_markupEngine = new ExamMarkup();   // Load default engine
				return _markupEngine;
			}
			set
			{
				_markupEngine = value;
			}
		}
		private ExamMarkup _markupEngine;
		[Category("Markup"), Description("If true, mark up this form's questions are randomly correct and incorrect so the Markup setitngs can be visualized.  This setting is not saved and is used only for editing."), ReadOnly(true)]
		public bool PreviewMarkedUp
		{
			get
			{
				return _previewMarkedUp;
			}

			set
			{
				_previewMarkedUp = value;

				if (value)
					SetupMarkedUpPreview();
				else
					_previewExam = null;
			}
		}
		private bool _previewMarkedUp;
		private Exam _previewExam;

		// -- Collections -------------------------------------------------------------------------
		[Browsable(false)]
		public List<ExamObject> Objects;
		[Browsable(false)]
		public List<ExamQuestion> Questions
		{
			get
			{
				// Convience function
				// 2016-08-29 -- Found When grading, questions were in order of creation, not assigned number.  
				//      Changed ExamForm.Questions helper function to order questions by number so the indexes in the 
				//      array correspond to the assinged number (Creation order doens't matter)
				return (from obj in Objects where obj is ExamQuestion orderby ((ExamQuestion)obj).Number ascending select obj as ExamQuestion).ToList();
			}
		}
		[Browsable(false)]
		public ExamMarkupRect StudentNameLocation
		{
			get
			{
				var loc = (from obj in Objects where (obj is ExamMarkupRect && obj.Name == "StudentNameLocation") select obj as ExamMarkupRect).First();

				if (loc == null)
				{
					// Create it and return it
					loc = new ExamMarkupRect()
					{
						Name = "StudentNameLocation"
					};

				}

				loc.Position = MarkupEngine.StudentNameLocation.Location;
				loc.Size = MarkupEngine.StudentNameLocation.Size;

				return loc;
			}
		}
		[Browsable(false)]
		public ExamMarkupRect StudentScoreLocation
		{
			get
			{
				var loc = (from obj in Objects where (obj is ExamMarkupRect && obj.Name == "StudentScoreLocation") select obj as ExamMarkupRect).First();

				if (loc == null)
				{
					// Create it and return it
					loc = new ExamMarkupRect()
					{
						Name = "StudentScoreLocation"
					};

				}

				loc.Position = MarkupEngine.ScoreLocation.Location;
				loc.Size = MarkupEngine.ScoreLocation.Size;

				return loc;
			}
		}

		// -- Constructors ------------------------------------------------------------------------
		public ExamForm(string name)
		{
			Name = name;
			Objects = new List<ExamObject>()
			{
				new ExamMarkupRect()
				{
					Name = "StudentNameLocation"
				},
				new ExamMarkupRect()
				{
					Name = "StudentScoreLocation"
				}
			};
			MarkupEngine = new ExamMarkup();
			PreviewMarkedUp = false;
		}
		public ExamForm()
		{
			Name = string.Empty;
			Objects = new List<ExamObject>()
			{
				new ExamMarkupRect()
				{
					Name = "StudentNameLocation"
				},
				new ExamMarkupRect()
				{
					Name = "StudentScoreLocation"
				}
			};
			MarkupEngine = new ExamMarkup();
			PreviewMarkedUp = false;
		}

		// -- Drawing -----------------------------------------------------------------------------
		public Bitmap GetBitmap(bool drawBackground = false)
		{
			if (PreviewMarkedUp)
				PreviewMarkedUp = false;

			Bitmap bmp = new Bitmap(600, 800);
			using (Graphics g = Graphics.FromImage(bmp))
			{
				Draw(g, drawBackground);
			}
			return bmp;
		}
		public void Draw(Graphics g, bool drawBackground = true)
		{
			if (drawBackground)
			{
				g.FillRectangle(Brushes.LightGray, 1, 1, Width - 1, Height - 1);
				g.FillRectangle(Brushes.White, Margin / 2, Margin / 2, Width - Margin, Height - Margin);
			}

			// Draw Corners
			DrawCorners(g);

			// Let Object draw with style (Smoooooooth)
			var smooth = g.SmoothingMode;
			var hinting = g.TextRenderingHint;
			g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
			g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;

			// Allow our objects to draw themselves
			foreach (ExamObject obj in Objects)
				obj.Draw(g);

			// Draw form as marked up if marked as so
			if (PreviewMarkedUp)
				MarkupEngine.MarkUpScan(g, _previewExam, _previewExam.Students.First());
			
			// Return Draw style to default
			g.SmoothingMode = smooth;
			g.TextRenderingHint = hinting;
		}

		// -- Utility -----------------------------------------------------------------------------
		private void DrawCorners_old(Bitmap bmp, ref Graphics cnvs)
		{
			Pen pen = new Pen(Color.Black, 4);

			// Draw top Left corner
			// bits == 1000
			cnvs.DrawRectangle(pen,
								Margin + (pen.Width / 2),
								Margin + (pen.Width / 2),
								SquareSize,
								SquareSize
			);
			cnvs.FillRectangle(Brushes.Black,
								Margin + pen.Width,
								Margin + pen.Width,
								SmallSquareSize,
								SmallSquareSize
			);


			// Top Right
			// bits == 0100
			cnvs.DrawRectangle(pen,
								bmp.Width - (Margin + SquareSize + (pen.Width / 2)),
								Margin + (pen.Width / 2),
								SquareSize,
								SquareSize
			);
			cnvs.FillRectangle(Brushes.Black,
								bmp.Width - (Margin + pen.Width + SmallSquareSize),
								Margin + pen.Width,
								SmallSquareSize,
								SmallSquareSize
			);


			// Bottom Left
			// bits == 0010
			cnvs.DrawRectangle(pen,
								Margin + (pen.Width / 2),
								bmp.Height - (Margin + SquareSize + (pen.Width / 2)),
								SquareSize,
								SquareSize
			);
			cnvs.FillRectangle(Brushes.Black,
								Margin + pen.Width,
								bmp.Height - (Margin + pen.Width + SmallSquareSize),
								SmallSquareSize,
								SmallSquareSize
			);


			// Bottom Right
			// bits == 0001
			cnvs.DrawRectangle(pen,
								bmp.Width - (Margin + SquareSize + (pen.Width / 2)),
								bmp.Height - (Margin + SquareSize + (pen.Width / 2)),
								SquareSize,
								SquareSize
			);
			cnvs.FillRectangle(Brushes.Black,
								bmp.Width - (Margin + pen.Width + SmallSquareSize),
								bmp.Height - (Margin + pen.Width + SmallSquareSize),
								SmallSquareSize,
								SmallSquareSize
			);
		}
		private static void DrawCorners(Graphics cnvs)
		{           
			// Top Left Corner
			DrawACorner(new Point(Margin, Margin), cnvs);
			DrawACornerBit(new Point(Margin + SmallSquareSize, Margin + SmallSquareSize), cnvs);

			// Top Right Corner
			DrawACorner(new Point(Width - Margin - SquareSize, Margin), cnvs);
			DrawACornerBit(new Point(Width - Margin - (SmallSquareSize * 2), Margin + SmallSquareSize), cnvs);

			// Bottom Right Corner
			DrawACorner(new Point(Width - Margin - SquareSize, Height - Margin - SquareSize), cnvs);
			DrawACornerBit(new Point(Width - Margin - (SmallSquareSize * 2), Height - Margin - (SmallSquareSize * 2)), cnvs);

			// Bottom Left Corner
			DrawACorner(new Point(Margin, Height - Margin - SquareSize), cnvs);
			DrawACornerBit(new Point(Margin + SmallSquareSize, Height - Margin - (SmallSquareSize * 2)), cnvs);
		}
		private static void DrawACorner(Point Origin, Graphics cnvs)
		{
			Pen pen = new Pen(Color.Black, 1);
			for (int i = 0; i < SmallSquareSize; ++i)
			{
				// Bounding Rectangle
				cnvs.DrawRectangle(pen,
								  Origin.X + i,
								  Origin.Y + i,
								  SquareSize - (i * 2) - pen.Width,
								  SquareSize - (i * 2) - pen.Width
				);
			}            
		}
		private static void DrawACornerBit(Point Origin, Graphics cnvs)
		{
			// Corner Bit
			cnvs.FillRectangle(Brushes.Black,
								Origin.X,
								Origin.Y,
								SmallSquareSize,
								SmallSquareSize
			);
		}
		public static Bitmap GetCornerGlyphTopLeft()
		{
			Bitmap bmp = new Bitmap(SquareSize, SquareSize);
			Graphics g = Graphics.FromImage(bmp);

			DrawACorner(new Point(0, 0), g);
			DrawACornerBit(new Point(SmallSquareSize, SmallSquareSize), g);

			g.Dispose();

			return bmp;
		}
		public static Bitmap GetCornerGlyphTopRight()
		{
			Bitmap bmp = new Bitmap(SquareSize, SquareSize);
			Graphics g = Graphics.FromImage(bmp);

			DrawACorner(new Point(0, 0), g);
			DrawACornerBit(new Point(SmallSquareSize * 2, SmallSquareSize), g);

			g.Dispose();

			return bmp;
		}
		public static Bitmap GetCornerGlyphBottomLeft()
		{
			Bitmap bmp = new Bitmap(SquareSize, SquareSize);
			Graphics g = Graphics.FromImage(bmp);

			DrawACorner(new Point(0, 0), g);
			DrawACornerBit(new Point(SmallSquareSize, SmallSquareSize * 2), g);

			g.Dispose();

			return bmp;
		}
		public static Bitmap GetCornerGlyphBottomRight()
		{
			Bitmap bmp = new Bitmap(SquareSize, SquareSize);
			Graphics g = Graphics.FromImage(bmp);

			DrawACorner(new Point(0, 0), g);
			DrawACornerBit(new Point(SmallSquareSize * 2, SmallSquareSize * 2), g);

			g.Dispose();

			return bmp;
		}

		// -- Preivew Marked Up -------------------------------------------------------------------
		private void SetupMarkedUpPreview()
		{
			// Create a fake exam, student, and answers
			Exam exm = new Exam();

			// We're the form
			exm.Form = this;

			// Random Number generator
			Random random = new Random();

			// -- Create the student
			Student student = new Student("Suzie", "McTestsALOT");
			student.Score = new ExamScore();

			// Answer Key -- Add Random Answers to key and student
			exm.AnswerKey.Answers = new List<List<bool>>();
			for (int x = 0; x < Questions.Count; x++)
			{
				// Generate Random Answer
				Answer ansKey = new Answer(random.Next(5));
				exm.AnswerKey.Answers.Add(ansKey.Data.ToList());

				// Determine if student got answer correct
				bool correct = random.Next(1) == 1;

				Answer stdAns;
				if (correct)
					stdAns = ansKey;
				else
					stdAns = new Answer(random.Next(5));

				student.Score.Answers.Add(stdAns);

				// Determine if actually correct
				student.Score.QuestionCorrect.Add(ansKey.GetAsInt() == stdAns.GetAsInt());
			}

			// Add the student
			exm.Students.Add(student);

			// Save
			_previewExam = exm;
		}

		// -- XML Saving / Loading ---------------------------------------------------------------- 
		public void SaveToFile(string path)
		{
			string xml = SaveToXml();

			var stream = File.Create(path);
			using (StreamWriter writer = new StreamWriter(stream))
				writer.Write(xml);
			if (stream != null)
				stream.Dispose();
		}
		public string SaveToXml()
		{
			// Create the XML and return it
			StringBuilder xmlOut = new StringBuilder();
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.Indent = true;
			settings.IndentChars = "    ";
			settings.OmitXmlDeclaration = true;
			using (XmlWriter xml = XmlWriter.Create(xmlOut, settings))
			{
				xml.WriteStartElement("ExamForm");

				// Properties
				xml.WriteElementString("Name", Name);
				xml.WriteElementString("Author", Author);
				xml.WriteElementString("Subject", Subject);
				xml.WriteElementString("Comments", Comments);
				xml.WriteRaw(MarkupEngine.SaveToXml());

				// Exam Objects
				xml.WriteStartElement("Objects");

				foreach (ExamObject obj in Objects)
					xml.WriteRaw(obj.SaveToXml());

				xml.WriteEndElement();  // Objects

				// Done
				xml.WriteEndElement(); // ExamForm
				xml.WriteEndDocument();
			}

			XDocument doc = XDocument.Parse(xmlOut.ToString());
			return doc.ToString();
		}
		public static ExamForm LoadFromFile(string path)
		{
			string xml = File.ReadAllText(path);
			return LoadFromXml(xml);
		}
		public static ExamForm LoadFromXml(string xmlStr)
		{
			ExamForm form = new ExamForm();
			string outerXml;
			using (XmlReader xml = XmlReader.Create(new StringReader(xmlStr)))
			{
				while (xml.Read())
				{
					if (xml.IsStartElement())
					{
						outerXml = string.Empty;
						switch (xml.Name)
						{
							case "Name":
								if (xml.Read())
									form.Name = xml.Value.Trim();
								break;
							case "Author":
								if (xml.Read())
									form.Author = xml.Value.Trim();
								break;
							case "Subject":
								if (xml.Read())
									form.Subject = xml.Value.Trim();
								break;
							case "Comments":
								if (xml.Read())
									form.Comments = xml.Value.Trim();
								break;
							case "ExamMarkup":
								outerXml = xml.ReadOuterXml();
								form.MarkupEngine = ExamMarkup.LoadFromXml(outerXml);
								break;

							// Objects     
							case "ExamLine":
									outerXml = xml.ReadOuterXml();
									form.Objects.Add(ExamLine.LoadFromXml(outerXml));
								break;
							case "ExamPicture":
									outerXml = xml.ReadOuterXml();
									form.Objects.Add(ExamPicture.LoadFromXml(outerXml));
								break;
							case "ExamQuestion":
									outerXml = xml.ReadOuterXml();
									form.Objects.Add(ExamQuestion.LoadFromXml(outerXml));
								break;
							case "ExamText":
									outerXml = xml.ReadOuterXml();
									form.Objects.Add(ExamText.LoadFromXml(outerXml));
								break;
							case "ExamWrittenEntry":
									outerXml = xml.ReadOuterXml();
									form.Objects.Add(ExamWrittenEntry.LoadFromXml(outerXml));
								break;                                
						}
					}
				}
			}

			return form;
		}
	}
}

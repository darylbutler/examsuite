﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace ScanReader
{
    public class AnswerKey
    {
        // Todo Make this all better
        public List<List<bool>> Answers { get; set; }
        public string Name { get; set; }

        public AnswerKey()
        {
            Answers = new List<List<bool>>();
        }
        public void Save(string file)
        {
            XmlSerializer Format = new XmlSerializer(typeof(AnswerKey));
            Stream fStream = new FileStream(file, FileMode.Create, FileAccess.Write, FileShare.None);
            Format.Serialize(fStream, this);
            fStream.Close();
        }
        public string SaveToXml()
        {
            XmlSerializer Format = new XmlSerializer(typeof(AnswerKey));
            StringBuilder xmlOut = new StringBuilder();
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = "    ";
            settings.OmitXmlDeclaration = true;
            using (XmlWriter xml = XmlWriter.Create(xmlOut, settings))
            {
                Format.Serialize(xml, this);
            }
            return xmlOut.ToString();
        }
        public static AnswerKey LoadFromXml(string xmlStr)
        {
            XmlSerializer Format = new XmlSerializer(typeof(AnswerKey));
            AnswerKey key;
            using (XmlReader xml = XmlReader.Create(new StringReader(xmlStr)))
            {
                key = (AnswerKey)Format.Deserialize(xml);
            }
            return key;
        }
        public static AnswerKey LoadFromFile(string filename)
        {
            XmlSerializer Format = new XmlSerializer(typeof(AnswerKey));
            Stream fStream = File.OpenRead(filename);
            AnswerKey key = (AnswerKey)Format.Deserialize(fStream);
            fStream.Close();
            return key;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ScanReader
{
	public enum StudentScanState
	{
		NotLoaded				= 0,	// Default State
		Loading					= 1,	// Curently Loading
		LoadingError			= 5,	// Scan.LoadImage errored
		LoadedButLowConfidence	= 10,	// Image was loaded and graded but some bubbles have low confidence and should be reviewed
		LoadedOkay				= 20	// Nothing Wrong		
	}

	// Students take exams and belong to Classes
	public class Student : IDisposable, ICloneable
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }       
		public string FullName { get { return FirstName + ' ' + LastName; } }
		public string ProperName {  get { return LastName + ", " + FirstName; } }
		public string GroupName { get; set; }
		public ExamScan Scan { get; set; }
		public ExamScore Score { get; set; }
		public StudentScanState ScanState;
		public StudentScanState CurrentScanState
		{
			get
			{
				return ScanState;
			}
		}

		public Student()
		{
			Score = new ExamScore();
		}
		public Student(string fname, string lname)
		{
			FirstName = fname;
			LastName = lname;
			Score = new ExamScore();
		}
		public Student(string fname, string lname, string gname)
		{
			FirstName = fname;
			LastName = lname;
			GroupName = gname;
			Score = new ExamScore();
		}

		/// Method to Load Scan async 
		public StudentScanState LoadFromScanedImage(Exam exam, System.Drawing.Bitmap img)
		{
			// -- Reset our structures so we can load them with new values
			Score = new ExamScore();
			Scan = new ExamScan(img);

			// -- Load the Image		
			ScanState = StudentScanState.Loading;
			Scan.LoadImage();

			// -- If there were errors, we exit
			if (!Scan.Loaded || Scan.LoadedError)
			{
				ScanState = StudentScanState.LoadingError;
				return StudentScanState.LoadingError;
			}

			// -- Grade the Exam
			for (int i = 0; i < exam.AnswerKey.Answers.Count; i++)
			{
				// -- Find the Student Answer
				Answer ans = Scan.FindAnswer(exam.Form.Questions[i]);
				ans.Number = exam.Form.Questions[i].Number;

				// --  ...and save it
				Score.Answers.Add(ans);

				// -- And Grade it (and save it)
				Score.QuestionCorrect.Add(ans.IsEqual(exam.AnswerKey.Answers[i]));
			}

			// -- We're loaded, so check the confidences to return the state
			if (Score.GetLowestConfidence() < Exam.MinimumConfidenceForExamAnswer)
			{
				ScanState = StudentScanState.LoadedButLowConfidence;
				return StudentScanState.LoadedButLowConfidence;
			}

			// -- We're living the dream right here
			ScanState = StudentScanState.LoadedOkay;
			return StudentScanState.LoadedOkay;
		}	
		


		public string GetXML(string imgPath)
		{
			// Create the XML and return it
			StringBuilder xmlOut = new StringBuilder();
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.Indent = true;
			settings.IndentChars = "    ";
			settings.OmitXmlDeclaration = true;
			using (XmlWriter xml = XmlWriter.Create(xmlOut, settings))
			{
				xml.WriteStartElement("Student");
				xml.WriteElementString("FirstName", FirstName);
				xml.WriteElementString("LastName", LastName);
				xml.WriteElementString("GroupName", GroupName);

				// Exam Scan
				string path = string.Format(@"{0}\{1}{2}.png", imgPath, LastName, FirstName);
				string examScanXml;
				if (Scan == null)
					examScanXml = @"<ExamScan>None</ExamScan>";
				else
					examScanXml = Scan.SaveToXml(path);
				xml.WriteRaw(examScanXml);

				// Exam Score
				string examScoreXml;
				if (Score == null)
					examScoreXml = @"<ExamScore>None</ExamScore>";
				else
					examScoreXml = Score.SaveToXml();
				xml.WriteRaw(examScoreXml);

				xml.WriteEndElement();
			}

			return xmlOut.ToString();
		}
		public static Student LoadFromXml(string xmlStr)
		{
			Student s = new Student();

			using (XmlReader xml = XmlReader.Create(new StringReader(xmlStr)))
			{
				while (xml.Read())
				{
					if (xml.IsStartElement())
					{
						switch (xml.Name)
						{
							case "FirstName":
								if (xml.Read())
									s.FirstName = xml.Value.Trim();
								break;
							case "LastName":
								if (xml.Read())
									s.LastName = xml.Value.Trim();
								break;
							case "GroupName":
								if (xml.Read())
									s.GroupName = xml.Value.Trim();
								break;
							case "ExamScan":
								string esXml = xml.ReadOuterXml();
								if (esXml == @"<ExamScan>None</ExamScan>")
									s.Scan = null;
								else
								{
									s.Scan = ExamScan.LoadFromXml(esXml);
								}
								break;
							case "ExamScore":
								string scoreXml = xml.ReadOuterXml();
								if (scoreXml == @"<ExamScore>None</ExamScore>")
									s.Score = new ExamScore();
								else
								{
									s.Score = ExamScore.LoadFromXml(scoreXml);
								}
								break;
						}
					}
				}
			}
				
			return s;
		}

		public void Dispose()
		{
			this.Scan.Dispose();
		}
		public object Clone()
		{
			Student s = (Student)this.MemberwiseClone();
			s.Scan = this.Scan.DeepCopy();
			s.Score = (ExamScore)this.Score.Clone();
			return s;
		}
	}
}

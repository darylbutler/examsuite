﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ScanReader
{
	public class Answer
	{
		[XmlIgnore]
		public bool[] Data;         // This is how we store the data
		[XmlIgnore]
		public double[] Confidence; // How confident we are that answer is true
		public int Number;			// Question Number we correspond to	
		public int NumberOfQuestions { get { return 5; } }

		[XmlElement("Data")]
		public string xmlDataString
		{
			get
			{
				return GetAsString();
			}
			set
			{
				Set(value);
			}
		}
		[XmlElement("Confidence")]
		public string xmlConfString
		{
			get
			{
				return string.Format("{0:F2},{1:F2},{2:F2},{3:F2},{4:F2}", Confidence[0], Confidence[1], Confidence[2], Confidence[3], Confidence[4]);
			}
			set
			{
				Confidence = new double[5] { 0d, 0d, 0d, 0d, 0d };
				string[] values = value.Split(',');

				for (int i = 0; i < values.Count(); i++)
				{
					Confidence[i] = double.Parse(values[i]);
				}
			}
		}

		public Answer()
		{
			Data = new bool[5] { false, false, false, false, false };
			Confidence = new double[5] { 0d, 0d, 0d, 0d, 0d };
		}
		public Answer(bool[] _ans)
		{
			Data = new bool[5] { false, false, false, false, false };
			Confidence = new double[5] { 0d, 0d, 0d, 0d, 0d };

			for (int i = 0; i < _ans.Count(); i++)
			{
				if (_ans[i])
				{
					Data[i] = true;
					Confidence[i] = 1d;
				}
			}
		}
		public Answer (List<bool> _ans)
		{
			Data = new bool[5] { false, false, false, false, false };
			Confidence = new double[5] { 0d, 0d, 0d, 0d, 0d };

			for (int i = 0; i < _ans.Count; i++)
			{
				if (_ans[i])
				{
					Data[i] = true;
					Confidence[i] = 1d;
				}
			}
		}
		public Answer(string s)
		{
			Data = new bool[5] { false, false, false, false, false };
			Confidence = new double[5] { 0d, 0d, 0d, 0d, 0d };
			this.Set(s);
		}
		public Answer(int i)
		{
			Data = new bool[5] { false, false, false, false, false };
			Confidence = new double[5] { 0d, 0d, 0d, 0d, 0d };
			this.Set(i);
		}
		
		public bool this[int i]
		{
			get { return Data[i]; }
		}

		// -- Setters --
		// Format of ans is 'A' or 'B' etc...
		public void Set(string ans)
		{
			switch (ans.ToUpper())
			{
				case "A":
					Data = new bool[5] { true, false, false, false, false };
					Confidence = new double[5] { 1d, 0d, 0d, 0d, 0d };
					break;
				case "B":
					Data = new bool[5] { false, true, false, false, false };
					Confidence = new double[5] { 0d, 1d, 0d, 0d, 0d };
					break;
				case "C":
					Data = new bool[5] { false, false, true, false, false };
					Confidence = new double[5] { 0d, 0d, 1d, 0d, 0d };
					break;
				case "D":
					Data = new bool[5] { false, false, false, true, false };
					Confidence = new double[5] { 0d, 0d, 0d, 1d, 0d };
					break;
				case "E":
					Data = new bool[5] { false, false, false, false, true };
					Confidence = new double[5] { 0d, 0d, 0d, 0d, 1d };
					break;
				default:
					Data = new bool[5] { false, false, false, false, false };
					Confidence = new double[5] { 0d, 0d, 0d, 0d, 0d };
					break;
			}
		}
		// Format of ans is 0 or 1 etc...
		public void Set(int ans)
		{
			switch (ans)
			{
				case 0:
					Data = new bool[5] { true, false, false, false, false };
					Confidence = new double[5] { 1d, 0d, 0d, 0d, 0d };
					break;
				case 1:
					Data = new bool[5] { false, true, false, false, false };
					Confidence = new double[5] { 0d, 1d, 0d, 0d, 0d };
					break;
				case 2:
					Data = new bool[5] { false, false, true, false, false };
					Confidence = new double[5] { 0d, 0d, 1d, 0d, 0d };
					break;
				case 3:
					Data = new bool[5] { false, false, false, true, false };
					Confidence = new double[5] { 0d, 0d, 0d, 1d, 0d };
					break;
				case 4:
					Data = new bool[5] { false, false, false, false, true };
					Confidence = new double[5] { 0d, 0d, 0d, 0d, 1d };
					break;
				default:
					Data = new bool[5] { false, false, false, false, false };
					Confidence = new double[5] { 0d, 0d, 0d, 0d, 0d };
					break;
			}
		}

		// Set answer and confidence in one call
		public void Update(int position, bool marked, double confidence)
		{
			if (position >= 0 && position <= 4)
			{
				Data[position] = marked;
				Confidence[position] = confidence;
			}
		}

		// -- Getters --
		public int GetAsInt()
		{
			if (Data[0])
				return 0;
			if (Data[1])
				return 1;
			if (Data[2])
				return 2;
			if (Data[3])
				return 3;
			if (Data[4])
				return 4;
			return -1;
		}
		public string GetAsString()
		{
			if (Data[0])
				return "A";
			if (Data[1])
				return "B";
			if (Data[2])
				return "C";
			if (Data[3])
				return "D";
			if (Data[4])
				return "E";
			return "?";
		}
		public double GetConfidence()
		{
			// New Algorithm - Return both
			double confSplit = 0.5d; // Amount of confidence comes from bubble pixel filled ratio

			// calc average conf
			double sum = 0;
			foreach (double d in Confidence)	// Sum all but the selected Answer's Confidence
				sum += (d == getSelectedConfidence()) ? 0.0d : d;
			double avgConf = sum / (NumberOfQuestions - 1);	// Minus the uncounted selected answer confidence

			// Figure how far we are above that
			double amountAboveAverage = getSelectedConfidence() - avgConf;

			// TODO make this a parameter
			double scaledAmtAbveAvg = (amountAboveAverage > 0.4d) ? (1 - confSplit) : amountAboveAverage * (1 - confSplit);
			double scaledBubbleConf = getSelectedConfidence() * confSplit;

			// This is the result.  Ensure it doesn't go over 100% for display
			double conf = scaledAmtAbveAvg + scaledBubbleConf;
			return Math.Max(0, Math.Min(conf, 1.0d));
		}
		private double getSelectedConfidence()
		{
			if (Data[0])
				return Confidence[0];
			if (Data[1])
				return Confidence[1];
			if (Data[2])
				return Confidence[2];
			if (Data[3])
				return Confidence[3];
			if (Data[4])
				return Confidence[4];
			return 0d;
		}
		public override string ToString()
		{
			return GetAsString();
		}
		public bool IsBlank()
		{
			return !(Data[0] || Data[1] || Data[2] || Data[3] || Data[4]);
		}
		public bool IsEqual(List<bool> key)
		{
			return  key[0] == Data[0] &&
					key[1] == Data[1] &&
					key[2] == Data[2] &&
					key[3] == Data[3] &&
					key[4] == Data[4];
		}

	}
}

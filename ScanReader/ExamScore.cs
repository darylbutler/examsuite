﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Linq;
using System.Xml.Serialization;

namespace ScanReader
{
	public class ExamScore : ICloneable
	{
		public List<Answer> Answers { get; set; }           // The answer the student marked per question
		public List<bool>   QuestionCorrect { get; set; }   // If that question was correct

		public ExamScore()
		{
			Answers = new List<Answer>();
			QuestionCorrect = new List<bool>();
		}

		public double GetLowestConfidence()
		{
			if (Answers == null)
				return -1.0d;

			return Answers.Select(a => a.GetConfidence()).Min();
			
		}
		public int NumberOfQuestionsTotal { get { return Answers.Count; } }
		public int NumberOfQuestionsAnsweredCorrectly { get { return (from b in QuestionCorrect where b == true select b).Count(); } }
		public double PercentScore { get { return ((double)NumberOfQuestionsAnsweredCorrectly / (double)NumberOfQuestionsTotal) * 100.0d; } }
		public string PercentScoreString { get { return string.Format("{0:F1}%", PercentScore); } }

		public string SaveToXml()
		{
			XmlSerializer Format = new XmlSerializer(typeof(ExamScore));
			StringBuilder xmlOut = new StringBuilder();
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.Indent = true;
			settings.IndentChars = "    ";
			settings.OmitXmlDeclaration = true;
			using (XmlWriter xml = XmlWriter.Create(xmlOut, settings))
			{
				Format.Serialize(xml, this);
			}
			return xmlOut.ToString();
		}
		public static ExamScore LoadFromXml(string xmlStr)
		{
			XmlSerializer Format = new XmlSerializer(typeof(ExamScore));
			ExamScore es;
			using (XmlReader xml = XmlReader.Create(new StringReader(xmlStr)))
			{
				es = (ExamScore)Format.Deserialize(xml);
			}
			return es;
		}

		public object Clone()
		{
			ExamScore es = (ExamScore)this.MemberwiseClone();
			es.Answers = new List<Answer>(this.Answers);
			es.QuestionCorrect = new List<bool>(this.QuestionCorrect);
			return es;
		}
	}
}

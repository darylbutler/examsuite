﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace ScanReader
{
	// This class represents the exam object.  It holds all the parts of an exam in one location.  It facilitates how
	// we remember all the different graded tests so we can compute a test-wide score (e.x., All students missed question 2, etc..)
	//
	// We use this class to save all the other parts as 1 unit
	public class Exam
	{
		public static double MinimumConfidenceForExamAnswer = 0.7d;		// Options set by Alpha Project

		// The form we print out and hand to students
		public ExamForm Form { get; set; }
		// The key we use to grade that form
		public AnswerKey AnswerKey { get; set; }
		// The student object, each of which holds an ExamScan and ExamScore
		public List<Student> Students { get; set; }

		public DateTime CreatedDate { get; set; }
		public string Name { get; set; }            // Exam Name 'Final'
		public string Subject { get; set; }         // Exam Subject, ex 'Biology'
		public string Description { get; set; }     // Exam Description 'New Style'
		public string Author { get; set; }          // Exam's Author, who made the test

		// Constructor
		public Exam()
		{
			Form = new ExamForm();
			AnswerKey = new AnswerKey();
			Students = new List<Student>();
			Name = string.Empty;
			CreatedDate = DateTime.Now;
		}
		public Exam(ExamForm form, AnswerKey key)
		{
			Form = form;
			AnswerKey = key;
			Students = new List<Student>();
			CreatedDate = DateTime.Now;
			Name = string.Empty;
		}
		public void SaveToFile(string path)
		{
			path = path + @"\" + Name;
			Directory.CreateDirectory(path);

			string xml = SaveToXml(path);

			var stream = File.Create(path + string.Format(@"\{0}.exm", Name));
			using (StreamWriter writer = new StreamWriter(stream))
				writer.Write(xml);

			if (stream != null)
				stream.Dispose();
		}
		public string SaveToXml(string path)
		{           
			// Create the XML and return it
			StringBuilder xmlOut = new StringBuilder();
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.Indent = true;
			settings.IndentChars = "    ";
			settings.OmitXmlDeclaration = true;
			using (XmlWriter xml = XmlWriter.Create(xmlOut, settings))
			{
				xml.WriteStartElement("Exam");
				
				xml.WriteElementString("CreatedDate", CreatedDate.ToShortDateString());
				xml.WriteElementString("Name", Name);
				xml.WriteElementString("Subject", Subject);
				xml.WriteElementString("Description", Description);
				xml.WriteElementString("Author", Author);


				// Definition
				xml.WriteComment("Start ExamForm");                
				xml.WriteRaw("\n");
				xml.WriteRaw(Form.SaveToXml());
				xml.WriteRaw("\n");

				// AnswerKey
				xml.WriteComment("Start Answer Key");
				xml.WriteRaw("\n");
				xml.WriteRaw(AnswerKey.SaveToXml());
				xml.WriteRaw("\n");


				// Students
				xml.WriteComment("Start Students");
				xml.WriteStartElement("Students");
				foreach (Student s in Students)
					xml.WriteRaw(s.GetXML(path));

				xml.WriteEndElement();

				// Done
				xml.WriteEndElement();
				xml.WriteEndDocument();
			}

			XDocument doc = XDocument.Parse(xmlOut.ToString());
			return doc.ToString();
			

		}
		public static Exam LoadFromFile(string path)
		{
			string xml = File.ReadAllText(path);
			return LoadFromXml(xml);
		}
		public static Exam LoadFromXml(string xmlStr)
		{
			Exam exam = new Exam();
			using (XmlReader xml = XmlReader.Create(new StringReader(xmlStr)))
			{
				while (xml.Read())
				{
					if (xml.IsStartElement())
					{
						switch (xml.Name)
						{
							case "CreatedDate":
								if (xml.Read())
									exam.CreatedDate = DateTime.Parse(xml.Value.Trim());
								break;
							case "Name":
								if (xml.Read())
									exam.Name = xml.Value.Trim();
								break;
							case "Subject":
								if (xml.Read())
									exam.Subject = xml.Value.Trim();
								break;
							case "Description":
								if (xml.Read())
									exam.Description = xml.Value.Trim();
								break;
							case "Author":
								if (xml.Read())
									exam.Author = xml.Value.Trim();
								break;

							case "ExamForm":
								exam.Form = ExamForm.LoadFromXml(xml.ReadOuterXml());
								break;
							case "AnswerKey":
								exam.AnswerKey = AnswerKey.LoadFromXml(xml.ReadOuterXml());
								break;
							case "Students":
								exam.Students = new List<Student>();
								xml.ReadToDescendant("Student");
								while (xml.LocalName == "Student")
								{                                    
									exam.Students.Add(Student.LoadFromXml(xml.ReadOuterXml()));
									xml.ReadToFollowing("Student");
								}
								break;
						}
					}
				}
			}

			return exam;
		}


	}
}

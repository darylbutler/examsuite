﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ScanReader.ExamObjects
{
	public enum BubbleTypes
	{
		Rectangles,
		SquareSegmented

	}
	[DefaultProperty("Number")]
	public class ExamQuestion : ExamObject
	{
		#region ---- Properties ----
		[Category("General Settings"),
		 Description("Number of bubbles to draw in the row."), 
		 DefaultValue(5)]
		public int NumberOfBubbles { get; set; }

		[Category("General Settings"),
		 Description("Number these bubbles belong to.  For Example, 1 if these bubbles are for question #1.  This is used to ensure proper ordering."),
		 DefaultValue(0)]
		public int Number { get; set; }

		[Category("General Settings"),
		 Description("The algorithm used to draw the bubbles."),
		 DefaultValue(BubbleTypes.SquareSegmented)]
		public BubbleTypes BubbleType { get; set; }

		[Category("General Settings"),
		Description("How far apart the bubbles are drawn, in pixels."),
		DefaultValue(2)]
		public int BubbleSpacing { get; set; }

		[Category("General Settings"),
		 Description("The thickness of the lines used to draw the bubbles.  The effect of the property may change depending on the BubbleType value."),
		 DefaultValue(1)]
		public int LineThickness { get; set; }

		[Category("General Settings"),
		 Description("If the BubbleStyle is SquareSegmented, should we draw the wings (the 'ears' on the lines between the bubble space)."),
		 DefaultValue(true)]
		public bool DrawWings { get; set; }

		[Category("General Settings"),
		 Description("Color of the lines used to draw the bubbles.")]
		public Color LineColor { get; set; }



		[Category("Bubble Letter Settings"),
		 Description("Should we draw the letters inside the bubble areas that indicate the Alpha value of the space?"),
		 DefaultValue(true)]
		public bool DrawBubbleLetter { get; set; }

		[Category("Bubble Letter Settings"),
		 Description("Font used to draw the Letters.")]
		public Font BubbleLetterFont { get; set; }

		[Category("Bubble Letter Settings"),
		 Description("The color used to draw the Letters.")]
		public Color BubbleLetterColor { get; set; }

		[Category("Bubble Letter Settings"),
		 Description("The transparancy applied to the color before the Letters are drawn"),
		 DefaultValue(77)]
		public int BubbleLetterAlpha { get; set; }

		[Category("Bubble Letter Settings"),
		 Description("Horizontal Offset for Letter positioning"),
		 DefaultValue(3)]
		public int BubbleLetterOffsetX { get; set; }

		[Category("Bubble Letter Settings"),
		 Description("Vertical Offset for Letter positioning"),
		 DefaultValue(0)]
		public int BubbleLetterOffsetY { get; set; }


		[Category("Bubble Number Settings"),
		 Description("Should we draw the Number assigned to this bubble row?"),
		 DefaultValue(true)]
		public bool DrawNumber { get; set; }

		[Category("Bubble Number Settings"),
		 Description("Space between the bubble area and the top left of the drawn number."),
		 DefaultValue(4)]
		public int NumberPadding { get; set; }

		[Category("Bubble Number Settings"),
		 Description("The Font used to draw the Number.")]
		public Font NumberFont { get; set; }

		[Category("Bubble Number Settings"),
		 Description("The color used to draw the Number.")]
		public Color NumberColor { get; set; }
		#endregion

		[Browsable(false)]
		public int BubbleWidth
		{
			get
			{
				 return (Size.Width - BubbleSpacing * (NumberOfBubbles - 1)) / NumberOfBubbles;
			}
		}

		public ExamQuestion()
		{
			Size = new Size(75, 12);

			BubbleType = BubbleTypes.SquareSegmented;
			//BubbleType = BubbleTypes.Rectangles;
			BubbleSpacing = 2;
			NumberOfBubbles = 5;
			Number = 0;
			LineThickness = 1;
			LineColor = Color.Black;
			DrawWings = true;

			DrawBubbleLetter = true;
			BubbleLetterFont = new Font(FontFamily.GenericMonospace, 8, FontStyle.Bold);
			BubbleLetterColor = LineColor;
			BubbleLetterAlpha = 77;            
			BubbleLetterOffsetX = 3;
			BubbleLetterOffsetY = 0;

			DrawNumber = true;
			NumberFont = new Font(FontFamily.GenericMonospace, 13);
			NumberColor = Color.MidnightBlue;
			NumberPadding = 4;
		}
		public ExamQuestion(ExamQuestion q)
		{
			this.Size = q.Size;

			this.BubbleType = q.BubbleType;
			this.BubbleSpacing = q.BubbleSpacing;
			this.NumberOfBubbles = q.NumberOfBubbles;
			this.Number = q.Number + 1;
			this.LineThickness = q.LineThickness;
			this.LineColor = q.LineColor;
			this.DrawWings = q.DrawWings;

			this.DrawBubbleLetter = q.DrawBubbleLetter;
			this.BubbleLetterFont = q.BubbleLetterFont;
			this.BubbleLetterColor = q.BubbleLetterColor;
			this.BubbleLetterAlpha = q.BubbleLetterAlpha;
			this.BubbleLetterOffsetX = q.BubbleLetterOffsetX;
			this.BubbleLetterOffsetY = q.BubbleLetterOffsetY;

			this.DrawNumber = q.DrawNumber;
			this.NumberFont = q.NumberFont;
			this.NumberColor = q.NumberColor;
			this.NumberPadding = q.NumberPadding;
		}
		public override void Draw(Graphics g)
		{
			if (BubbleType == BubbleTypes.Rectangles)
				drawRectangles(g);
			else if (BubbleType == BubbleTypes.SquareSegmented)
				drawSegmented(g);
		}

		private void drawRectangles(Graphics g)
		{
			// -- Draw Bubbles
			for (int i = 0; i < NumberOfBubbles; i++)
			{
				Rectangle rect = new Rectangle(Position.X + (i * (BubbleWidth + BubbleSpacing)),
													Position.Y,
													BubbleWidth,
													Size.Height);

				g.DrawRectangle(new Pen(LineColor, LineThickness), rect);

				if (DrawBubbleLetter)
				{
					g.DrawString(
						BubbleNumberToLetter(i), 
						BubbleLetterFont, 
						new SolidBrush(Color.FromArgb(BubbleLetterAlpha, BubbleLetterColor)),
						rect.X + BubbleLetterOffsetX, 
						rect.Y + BubbleLetterOffsetY);
				}
			}

			// -- Draw Number
			drawNumber(g);
		}
		private void drawSegmented(Graphics g)
		{
			// Calculate Centering OffsetY (There is no Offset)
			int BubbleWidth = (Size.Width - ((NumberOfBubbles + 1) * LineThickness)) / NumberOfBubbles;

			// -- Draw Bubbles
			for (int i = 0; i < NumberOfBubbles + 1; i++)
			{
				Pen pen = new Pen(LineColor, LineThickness);

				int x = (LineThickness + BubbleWidth + 1) * i;

				g.DrawLine(pen, Position.X + x, Position.Y, Position.X + x, Position.Y + Size.Height);

				
				if (DrawBubbleLetter && i < NumberOfBubbles)
				{
					g.DrawString(
						BubbleNumberToLetter(i),
						BubbleLetterFont,
						new SolidBrush(Color.FromArgb(BubbleLetterAlpha, BubbleLetterColor)),
						Position.X + x + BubbleLetterOffsetX,
						Position.Y + BubbleLetterOffsetY);
				}
				
			}

			// -- Draw Wings on first and last lines
			if (DrawWings)
			{
				Pen pen = new Pen(LineColor, LineThickness);
				// Top Left
				g.DrawLine(pen, Position.X, Position.Y, Position.X + (0.33f * (float)BubbleWidth), Position.Y);
				// Bottom Left
				g.DrawLine(pen, Position.X, Position.Y + Size.Height, Position.X + (0.33f * (float)BubbleWidth), Position.Y + Size.Height);
				// Top Right
				g.DrawLine(pen, Position.X + ((LineThickness + BubbleWidth + 1) * NumberOfBubbles), Position.Y, Position.X + ((LineThickness + BubbleWidth + 1) * NumberOfBubbles) - (0.33f * (float)BubbleWidth), Position.Y);
				// Bottom Right
				g.DrawLine(pen, Position.X + ((LineThickness + BubbleWidth + 1) * NumberOfBubbles), Position.Y + Size.Height, Position.X + ((LineThickness + BubbleWidth + 1) * NumberOfBubbles) - (0.33f * (float)BubbleWidth), Position.Y + Size.Height);
			}

			// -- Draw Number
			drawNumber(g);
		}
		private void drawNumber(Graphics g)
		{
			if (DrawNumber)
			{
				RectangleF numberRect = GetNumberLocation(g);

				//g.DrawRectangle(Pens.BurlyWood, new Rectangle((int)numberRect.X, (int)numberRect.Y, (int)numberRect.Width, (int)numberRect.Height));
				StringFormat frmt = new StringFormat();
				frmt.LineAlignment = StringAlignment.Near;
				frmt.Alignment = StringAlignment.Far;
				g.DrawString(
						Number.ToString(),
						NumberFont,
						new SolidBrush(NumberColor),
						numberRect, 
						frmt);
			}
		}
		public RectangleF GetNumberLocation(Graphics g)
		{
			var Length = g.MeasureString(Number.ToString(), NumberFont);
			float offsetY = Size.Height - Length.Height;

			RectangleF rect = new RectangleF(
					Position.X - (Length.Width + NumberPadding),
					Position.Y + (offsetY * 0.5f),
					Length.Width,
					Length.Height
					);
			return rect;
		}
		// Returns an image of us --  we use this because we can better estimate our size
		public Bitmap GetImageOfInstance()
		{
			// Create sized image
			Bitmap bmp = new Bitmap(Size.Width + 1, Size.Height + 1);
			Graphics g = Graphics.FromImage(bmp);

			// To get the question to draw on the image, change the position of the bubble and restore it later
			Point oldPos = Position;
			Position = new Point(0, 0);
			this.Draw(g);
			Position = oldPos;
			g.Dispose();

			return bmp;
		}
		public Bitmap GetImageOfInstanceWithNumber()
		{
			Bitmap bmp = new Bitmap((int)(Size.Width * NumberFont.SizeInPoints), (int)(Size.Height * NumberFont.SizeInPoints));
			Graphics g = Graphics.FromImage(bmp);
			
			// Length of Number Text
			var Length = g.MeasureString(Number.ToString(), NumberFont);

			int totalWidth = (int)( Length.Width +      // Size of NumberText
									NumberPadding +     // Padding between Bubbles and NumberText
									Size.Width);        // Width of Bubbles

			int totalHeight = Math.Max(Size.Height, (int)Length.Height);

			// Create sized image
			bmp = new Bitmap(totalWidth, totalHeight);
			g = Graphics.FromImage(bmp);

			// To get the question to draw on the image, change the position of the bubble and restore it later
			Point oldPos = Position;
			Position = new Point(totalWidth - Size.Width - 2, 2);
			this.Draw(g);
			Position = oldPos;
			
			g.Dispose();

			return bmp;
		}
		private string BubbleNumberToLetter(int i)
		{
			switch (i)
			{
				case 0:
					return "A";
				case 1:
					return "B";
				case 2:
					return "C";
				case 3:
					return "D";
				case 4:
					return "E";
				case 5:
					return "F";
				case 6:
					return "G";
			}
			return "?";
		}



		public override string SaveToXml()
		{
			// Create the XML and return it
			StringBuilder xmlOut = new StringBuilder();
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.Indent = true;
			settings.IndentChars = "    ";
			settings.OmitXmlDeclaration = true;
			TypeConverter FontConverter = TypeDescriptor.GetConverter(typeof(Font));

			using (XmlWriter xml = XmlWriter.Create(xmlOut, settings))
			{
				// -- Start ---------------------------------------------------
				xml.WriteStartElement("ExamQuestion");

				// Write base
				WriteBaseToXml(xml);

				// -- Properties --
				xml.WriteStartElement("Properties");

				xml.WriteElementString("NumberOfBubbles", XmlConvert.ToString(NumberOfBubbles));
				xml.WriteElementString("Number", XmlConvert.ToString(Number));
				xml.WriteElementString("BubbleType", Enum.GetName(typeof(BubbleTypes), BubbleType));
				xml.WriteElementString("BubbleSpacing", XmlConvert.ToString(BubbleSpacing));
				xml.WriteElementString("LineThickness", XmlConvert.ToString(LineThickness));
				xml.WriteElementString("DrawWings", XmlConvert.ToString(DrawWings));
				xml.WriteElementString("LineColor", ColorTranslator.ToHtml(LineColor));

				xml.WriteElementString("DrawBubbleLetter", XmlConvert.ToString(DrawBubbleLetter));
				xml.WriteElementString("BubbleLetterFont", FontConverter.ConvertToString(BubbleLetterFont));
				xml.WriteElementString("BubbleLetterColor", ColorTranslator.ToHtml(BubbleLetterColor));
				xml.WriteElementString("BubbleLetterAlpha", XmlConvert.ToString(BubbleLetterAlpha));
				xml.WriteElementString("BubbleLetterOffsetX", XmlConvert.ToString(BubbleLetterOffsetX));
				xml.WriteElementString("BubbleLetterOffsetY", XmlConvert.ToString(BubbleLetterOffsetY));

				xml.WriteElementString("DrawNumber", XmlConvert.ToString(DrawNumber));
				xml.WriteElementString("NumberPadding", XmlConvert.ToString(NumberPadding));
				xml.WriteElementString("NumberFont", FontConverter.ConvertToString(NumberFont));
				xml.WriteElementString("NumberColor", ColorTranslator.ToHtml(NumberColor));

				xml.WriteEndElement(); // Properties

				// Done
				xml.WriteEndElement(); // ExamLine

				//xml.Close();
			}
			return xmlOut.ToString();
		}
		public static ExamQuestion LoadFromXml(string xmlStr)
		{
			ExamQuestion q = new ExamQuestion();
			TypeConverter FontConverter = TypeDescriptor.GetConverter(typeof(Font));
			using (XmlReader xml = XmlReader.Create(new StringReader(xmlStr)))
			{
				while (xml.Read())
				{
					if (xml.IsStartElement())
					{
						switch (xml.Name)
						{
							case "Name":
								if (xml.Read())
									q.Name = xml.Value;
								break;
							case "Size":
								if (xml.ReadToFollowing("Width") && xml.Read())
									q.Size = new Size (XmlConvert.ToInt32(xml.Value), q.Size.Height);
								if (xml.ReadToFollowing("Height") && xml.Read())
									q.Size = new Size(q.Size.Width, XmlConvert.ToInt32(xml.Value));
								break;
							case "Position":
								if (xml.Read())
								{
									if (xml.ReadToFollowing("X") && xml.Read())
										q.Position = new Point(XmlConvert.ToInt32(xml.Value), q.Position.Y);
									if (xml.ReadToFollowing("Y") && xml.Read())
										q.Position = new Point(q.Position.X, XmlConvert.ToInt32(xml.Value));
								}
								break;
							case "NumberOfBubbles":
								if (xml.Read())
									q.NumberOfBubbles = XmlConvert.ToInt32(xml.Value);
								break;
							case "Number":
								if (xml.Read())
									q.Number = XmlConvert.ToInt32(xml.Value);
								break;
							case "BubbleType":
								if (xml.Read())
								{
									BubbleTypes bt = BubbleTypes.Rectangles;
									Enum.TryParse(xml.Value, out bt);
									q.BubbleType = bt;
								}
								break;
							case "BubbleSpacing":
								if (xml.Read())
									q.BubbleSpacing = XmlConvert.ToInt32(xml.Value);
								break;
							case "LineThickness":
								if (xml.Read())
									q.LineThickness = XmlConvert.ToInt32(xml.Value);
								break;
							case "DrawWings":
								if (xml.Read())
									q.DrawWings = XmlConvert.ToBoolean(xml.Value);
								break;
							case "LineColor":
								if (xml.Read())
									q.LineColor = ColorTranslator.FromHtml(xml.Value);
								break;
							case "DrawBubbleLetter":
								if (xml.Read())
									q.DrawBubbleLetter = XmlConvert.ToBoolean(xml.Value);
								break;
							case "BubbleLetterFont":
								if (xml.Read())
									q.BubbleLetterFont = (Font) FontConverter.ConvertFromString(xml.Value);
								break;
							case "BubbleLetterColor":
								if (xml.Read())
									q.BubbleLetterColor = ColorTranslator.FromHtml(xml.Value);
								break;
							case "BubbleLetterAlpha":
								if (xml.Read())
									q.BubbleLetterAlpha = XmlConvert.ToInt32(xml.Value);
								break;
							case "BubbleLetterOffsetX":
								if (xml.Read())
									q.BubbleLetterOffsetX = XmlConvert.ToInt32(xml.Value);
								break;
							case "BubbleLetterOffsetY":
								if (xml.Read())
									q.BubbleLetterOffsetY = XmlConvert.ToInt32(xml.Value);
								break;
							case "DrawNumber":
								if (xml.Read())
									q.DrawNumber = XmlConvert.ToBoolean(xml.Value);
								break;
							case "NumberPadding":
								if (xml.Read())
									q.NumberPadding = XmlConvert.ToInt32(xml.Value);
								break;
							case "NumberFont":
								if (xml.Read())
									q.NumberFont = (Font)FontConverter.ConvertFromString(xml.Value);
								break;
							case "NumberColor":
								if (xml.Read())
									q.NumberColor = ColorTranslator.FromHtml(xml.Value);
								break;
						}
					}
				}
			}

			return q;
		}
	}
}

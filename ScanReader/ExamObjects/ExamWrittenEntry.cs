﻿using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Text;
using System.Xml;

namespace ScanReader.ExamObjects
{
	[DefaultProperty("Text")]
	public class ExamWrittenEntry : ExamObject
	{
		#region ---- Properties ----
		[Category("Text Settings"),
		 Description("The string of the prompt to draw."),
		 DefaultValue("<Prompt Text>:")]
		public string Text { get; set; }

		[Category("Text Settings"),
		 Description("The font used to draw the Text.")]
		public Font Font { get; set; }

		[Category("Text Settings"),
		 Description("The color used to draw the Text.")]
		public Color TextColor { get; set; }

		[Category("Text Settings"),
		 Description("The padding / distance between the Text and the Line drawn."),
		 DefaultValue(-4)]
		public int PaddingBeforeLine { get; set; }


		[Category("Line Settings"),
		 Description("The Thickness of the input line to draw."),
		 DefaultValue(2)]
		public int LineThickness { get; set; }

		[Category("Line Settings"),
		 Description("The Distance from the bottom of the object to draw the input line."),
		 DefaultValue(-4)]
		public int LineOffset { get; set; }

		[Category("Line Settings"),
		 Description("The color used to draw the input line.")]
		public Color LineColor { get; set; }
		#endregion

		public ExamWrittenEntry()
		{
			Text = "<Prompt Text>:";
			Font = new Font(FontFamily.GenericSansSerif, 12.0f);
			TextColor = Color.Black;
			PaddingBeforeLine = -4;
			LineThickness = 2;
			LineOffset = -2;
			LineColor = Color.Black;
		}
		public ExamWrittenEntry(ExamWrittenEntry e)
		{
			this.Text = e.Text;
			this.Font = e.Font;
			this.TextColor = e.TextColor;
			this.PaddingBeforeLine = e.PaddingBeforeLine;
			this.LineThickness = e.LineThickness;
			this.LineOffset = e.LineOffset;
			this.LineColor = e.LineColor;
		}

		public override void Draw(Graphics g)
		{
			// Draw text at top left
			var textLength = g.MeasureString(Text, Font);
			g.DrawString(Text, Font, new SolidBrush(TextColor), Position);

			// Ensure Width and Height of this string
			if (Size.Width < textLength.Width)
				Size = new Size((int)textLength.Width + 50, Size.Height);
			if (Size.Height < textLength.Height)
				Size = new Size(Size.Width, (int)textLength.Height);

			// Draw the Line
			Pen linePen = new Pen(LineColor, LineThickness);
			g.DrawLine(linePen,
				Position.X + textLength.Width + PaddingBeforeLine,
				Position.Y + textLength.Height + LineOffset,
				Position.X + Size.Width,
				Position.Y + textLength.Height + LineOffset
				);
		}

		// Save
		public override string SaveToXml()
		{
			// Create the XML and return it
			StringBuilder xmlOut = new StringBuilder();
			TypeConverter FontConverter = TypeDescriptor.GetConverter(typeof(Font));
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.Indent = true;
			settings.IndentChars = "    ";
			settings.OmitXmlDeclaration = true;

			XmlWriter xml = XmlWriter.Create(xmlOut, settings);

			// -- Start ---------------------------------------------------
			xml.WriteStartElement("ExamWrittenEntry");

			// Write base
			WriteBaseToXml(xml);

			// Color
			xml.WriteStartElement("Properties");

			xml.WriteElementString("Text", Text);
			xml.WriteElementString("Font", FontConverter.ConvertToString(Font));
			xml.WriteElementString("TextColor", ColorTranslator.ToHtml(TextColor));
			xml.WriteElementString("PaddingBeforeLine", XmlConvert.ToString(PaddingBeforeLine));
			xml.WriteElementString("LineThickness", XmlConvert.ToString(LineThickness));
			xml.WriteElementString("LineOffset", XmlConvert.ToString(LineOffset));
			xml.WriteElementString("LineColor", ColorTranslator.ToHtml(LineColor));

			xml.WriteEndElement(); // Properties

			// Done
			xml.WriteEndElement(); // ExamLine

			xml.Close();

			return xmlOut.ToString();
		}
		public static ExamWrittenEntry LoadFromXml(string xmlStr)
		{
			ExamWrittenEntry entry = new ExamWrittenEntry();
			TypeConverter FontConverter = TypeDescriptor.GetConverter(typeof(Font));
			using (XmlReader xml = XmlReader.Create(new StringReader(xmlStr)))
			{
				while (xml.Read())
				{
					if (xml.IsStartElement())
					{
						switch (xml.Name)
						{
							case "Name":
								if (xml.Read())
									entry.Name = xml.Value;
								break;
							case "Size":
								if (xml.ReadToFollowing("Width") && xml.Read())
									entry.Size = new Size(XmlConvert.ToInt32(xml.Value), entry.Size.Height);
								if (xml.ReadToFollowing("Height") && xml.Read())
									entry.Size = new Size(entry.Size.Width, XmlConvert.ToInt32(xml.Value));
								break;
							case "Position":
								if (xml.Read())
								{
									if (xml.ReadToFollowing("X") && xml.Read())
										entry.Position = new Point(XmlConvert.ToInt32(xml.Value), entry.Position.Y);
									if (xml.ReadToFollowing("Y") && xml.Read())
										entry.Position = new Point(entry.Position.X, XmlConvert.ToInt32(xml.Value));
								}
								break;
							case "Text":
								if (xml.Read())
									entry.Text = xml.Value;
								break;                            
							case "Font":
								if (xml.Read())
									entry.Font = (Font)FontConverter.ConvertFromString(xml.Value);
								break;
							case "TextColor":
								if (xml.Read())
									entry.TextColor = ColorTranslator.FromHtml(xml.Value);
								break;
							case "PaddingBeforeLine":
								if (xml.Read())
									entry.PaddingBeforeLine = XmlConvert.ToInt32(xml.Value);
								break;
							case "LineThickness":
								if (xml.Read())
									entry.LineThickness = XmlConvert.ToInt32(xml.Value);
								break;
							case "LineOffset":
								if (xml.Read())
									entry.LineOffset = XmlConvert.ToInt32(xml.Value);
								break;
							case "LineColor":
								if (xml.Read())
									entry.LineColor = ColorTranslator.FromHtml(xml.Value);
								break;
						}
					}
				}
			}

			return entry;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ScanReader.ExamObjects
{
	[DefaultProperty("Text")]
	public class ExamText : ExamObject
	{
		#region ---- Properties ----
		[Category("Text Settings"),
		 Description("The string to draw."),
		 DefaultValue("")]
		public string Text { get; set; }     // Text to display

		[Category("Text Settings"),
		Description("The font used to draw the Text.")]
		public Font Font { get; set; }       // Font of Text

		[Category("Text Settings"),
		 Description("The color used to draw the Text.")]
		public Color Color { get; set; }

		[Category("Text Settings"),
		 Description("The Horizontal Alignment of the Text drawn inside this object."),
		 DefaultValue(StringAlignment.Near)]
		public StringAlignment HorizontalAlignment { get; set; }

		[Category("Text Settings"),
		 Description("The Vertical Alignment of the Text drawn inside this object."),
		 DefaultValue(StringAlignment.Near)]
		public StringAlignment VerticalAlignment { get; set; }
		#endregion

		// Setup defaults
		public ExamText()
		{
			Size = new Size(150, 20);
			//Type = ExamTypes.Text;
			Text = "<Insert Some Text>";
			Font = new Font(FontFamily.GenericSansSerif, 12.0f);
			Color = Color.Black;
			HorizontalAlignment = StringAlignment.Near;
			VerticalAlignment = StringAlignment.Near;
		}
		public ExamText(ExamText et)
		{
			this.Size = et.Size;
			this.Text = et.Text;
			this.Font = et.Font;
			this.Color = et.Color;
			this.HorizontalAlignment = et.HorizontalAlignment;
			this.VerticalAlignment = et.VerticalAlignment;
		}

		// Draw the Label
		public override void Draw(Graphics g)
		{
			StringFormat format = new StringFormat();
			format.Alignment = HorizontalAlignment;
			format.LineAlignment = VerticalAlignment;
			Brush ForegroundBrush = new SolidBrush(Color);
			g.DrawString(Text, Font, ForegroundBrush, Location, format);
		}


		// Save
		public override string SaveToXml()
		{
			// Create the XML and return it
			StringBuilder xmlOut = new StringBuilder();
			TypeConverter FontConverter = TypeDescriptor.GetConverter(typeof(Font));
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.Indent = true;
			settings.IndentChars = "    ";
			settings.OmitXmlDeclaration = true;

			XmlWriter xml = XmlWriter.Create(xmlOut, settings);

			// -- Start ---------------------------------------------------
			xml.WriteStartElement("ExamText");

			// Write base
			WriteBaseToXml(xml);

			// Color
			xml.WriteStartElement("Properties");

			xml.WriteElementString("Text", Text);
			xml.WriteElementString("Font", FontConverter.ConvertToString(Font));
			xml.WriteElementString("Color", ColorTranslator.ToHtml(Color));
			xml.WriteElementString("HorizontalAlignment", Enum.GetName(typeof(StringAlignment), HorizontalAlignment));
			xml.WriteElementString("VerticalAlignment", Enum.GetName(typeof(StringAlignment), VerticalAlignment));

			xml.WriteEndElement(); // Properties

			// Done
			xml.WriteEndElement(); // ExamLine

			xml.Close();

			return xmlOut.ToString();
		}
		public static ExamText LoadFromXml(string xmlStr)
		{
			ExamText text = new ExamText();
			TypeConverter FontConverter = TypeDescriptor.GetConverter(typeof(Font));
			using (XmlReader xml = XmlReader.Create(new StringReader(xmlStr)))
			{
				while (xml.Read())
				{
					if (xml.IsStartElement())
					{
						switch (xml.Name)
						{
							case "Name":
								if (xml.Read())
									text.Name = xml.Value;
								break;
							case "Size":
								if (xml.ReadToFollowing("Width") && xml.Read())
									text.Size = new Size(XmlConvert.ToInt32(xml.Value), text.Size.Height);
								if (xml.ReadToFollowing("Height") && xml.Read())
									text.Size = new Size(text.Size.Width, XmlConvert.ToInt32(xml.Value));
								break;
							case "Position":
								if (xml.Read())
								{
									if (xml.ReadToFollowing("X") && xml.Read())
										text.Position = new Point(XmlConvert.ToInt32(xml.Value), text.Position.Y);
									if (xml.ReadToFollowing("Y") && xml.Read())
										text.Position = new Point(text.Position.X, XmlConvert.ToInt32(xml.Value));
								}
								break;
							case "Text":
								if (xml.Read())
									text.Text = xml.Value;
								break;
							case "Color":
								if (xml.Read())
									text.Color = ColorTranslator.FromHtml(xml.Value);
								break;
							case "Font":
								if (xml.Read())
									text.Font = (Font)FontConverter.ConvertFromString(xml.Value);
								break;
							case "HorizontalAlignment":
								if (xml.Read())
								{
									StringAlignment al = StringAlignment.Near;
									Enum.TryParse(xml.Value, out al);
									text.HorizontalAlignment = al;
								}                                    
								break;
							case "VerticalAlignment":
								if (xml.Read())
								{
									StringAlignment al = StringAlignment.Near;
									Enum.TryParse(xml.Value, out al);
									text.VerticalAlignment = al;
								}
								break;
						}
					}
				}
			}

			return text;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace ScanReader.ExamObjects
{
	[DefaultProperty("Color")]
	public class ExamLine : ExamObject
	{
		#region ---- Properties ----
		[Category("Object Settings"),
		 Description("Color of the Line")]
		public Color Color { get; set; }
		#endregion

		public ExamLine()
		{
			Size = new Size(250, 2);
			Color = Color.CornflowerBlue;
		}
		public ExamLine(ExamLine line)
		{
			this.Size = line.Size;
			this.Color = line.Color;
		}

		// Draw the Line
		public override void Draw(Graphics g)
		{            
			Brush ForegroundBrush = new SolidBrush(Color);
			g.FillRectangle(ForegroundBrush, Location);
		}

		public override string SaveToXml()
		{
			// Create the XML and return it
			StringBuilder xmlOut = new StringBuilder();
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.Indent = true;
			settings.IndentChars = "    ";
			settings.OmitXmlDeclaration = true;

			XmlWriter xml = XmlWriter.Create(xmlOut, settings);

			// -- Start ---------------------------------------------------
			xml.WriteStartElement("ExamLine");

			// Write base
			WriteBaseToXml(xml);

			// Color
			xml.WriteStartElement("Properties");
			xml.WriteElementString("Color", ColorTranslator.ToHtml(Color));
			xml.WriteEndElement(); // Properties

			// Done
			xml.WriteEndElement(); // ExamLine

			xml.Close();
			
			return xmlOut.ToString();
		}
		public static ExamLine LoadFromXml(string xmlStr)
		{
			ExamLine line = new ExamLine();
			using (XmlReader xml = XmlReader.Create(new StringReader(xmlStr)))
			{
				while (xml.Read())
				{
					if (xml.IsStartElement())
					{
						switch (xml.Name)
						{
							case "Name":
								if (xml.Read())
									line.Name = xml.Value;
								break;
							case "Size":
								if (xml.ReadToFollowing("Width") && xml.Read())
									line.Size = new Size(XmlConvert.ToInt32(xml.Value), line.Size.Height);
								if (xml.ReadToFollowing("Height") && xml.Read())
									line.Size = new Size(line.Size.Width, XmlConvert.ToInt32(xml.Value));
								break;
							case "Position":
								if (xml.Read())
								{
									if (xml.ReadToFollowing("X") && xml.Read())
										line.Position = new Point(XmlConvert.ToInt32(xml.Value), line.Position.Y);
									if (xml.ReadToFollowing("Y") && xml.Read())
										line.Position = new Point(line.Position.X, XmlConvert.ToInt32(xml.Value));
								}
								break;
							case "Color":
								if (xml.Read())
									line.Color = ColorTranslator.FromHtml(xml.Value);
								break;
						}
					}
				}
			}

			return line;
		}

	}
}

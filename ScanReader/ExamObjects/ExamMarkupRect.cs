﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScanReader.ExamObjects
{
	public class ExamMarkupRect : ExamObject
	{
		// Used only for FormEditing
		// We only use the position and Size
		public override void Draw(Graphics g)
		{
			// Do nothing
		}

		public override string SaveToXml()
		{
			return string.Empty;
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ScanReader.ExamObjects
{
	public enum ExamTypes
	{
		Text,
		Line,
		Picture,
		WrittenEntry,
		QuestionEntry
	}
	[DefaultProperty("Name")]
	public abstract class ExamObject
	{
		[Category("Base Settings"), Description("X and Y Coordinates of the Object in the Form.")]
		public Point Position { get; set; }

		[Category("Base Settings"), Description("The Width and Height of the Object.")]
		public Size Size { get; set; }

		[Category("Base Settings"),
			Description("Name of this object.  Used only when editing to ease selection and management of objects.  Isn't used when drawing or laying out form when printing.")]
		public string Name { get; set; }


		[Browsable(false)]
		public Rectangle Location { get { return new Rectangle(Position, Size); } }

		// The Magic Method
		public abstract void Draw(Graphics g);

		// Load and Save
		public abstract string SaveToXml();
		protected void WriteBaseToXml(XmlWriter xml)
		{
			// Base
			xml.WriteStartElement("Base");

			// Name
			xml.WriteElementString("Name", Name);

			// Size
			xml.WriteStartElement("Size");
			xml.WriteElementString("Width", XmlConvert.ToString(Size.Width));
			xml.WriteElementString("Height", XmlConvert.ToString(Size.Height));
			xml.WriteEndElement();  // Size

			// Position
			xml.WriteStartElement("Position");
			xml.WriteElementString("X", XmlConvert.ToString(Position.X));
			xml.WriteElementString("Y", XmlConvert.ToString(Position.Y));
			xml.WriteEndElement();  // Position

			xml.WriteEndElement();  // Base
		}
	}
}

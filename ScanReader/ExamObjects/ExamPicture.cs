﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ScanReader.ExamObjects
{
	[DefaultProperty("Image")]
	public class ExamPicture : ExamObject
	{
		#region ---- Properties ----
		[Category("Image Settings"),
		 Description("If true, then draw a border around this objet."),
		 DefaultValue(true)]
		public bool ShowBorder { get; set; }

		[Category("Image Settings"),
		 Description("If ShowBorder is true, this is the color of the border drawn")]
		public Color BorderColor { get; set; }

		[Category("Image Settings"),
		 Description("If ShowBorder is true, this is the thickness of the border drawn"),
		 DefaultValue(1)]
		public int BorderThickness { get; set; }

		[Category("Image Settings"),
		 Description("The Image to Show")]
		public Bitmap Image
		{
			get
			{
				return _image;
			}
			set
			{
				if (value != null)
				{
					// Convert incoming image to png and store it in a memory stream
					ImageStream = new MemoryStream();
					value.Save(ImageStream, System.Drawing.Imaging.ImageFormat.Png);
					_image = (Bitmap)Bitmap.FromStream(ImageStream);
					value.Dispose();
				}
			}
		}
		#endregion


		protected Bitmap _image;
		public MemoryStream ImageStream;    // When Image is loaded from a memory stream, save the stream so it can be saved				

		public ExamPicture()
		{
			Image = null;
			ImageStream = null;
			ShowBorder = true;
			BorderColor = Color.LightSteelBlue;
			BorderThickness = 1;
			Size = new Size(100, 100);
		}
		public ExamPicture(ExamPicture pic)
		{
			Image = pic.Image;
			ShowBorder = pic.ShowBorder;
			BorderColor = pic.BorderColor;
			BorderThickness = pic.BorderThickness;
			Size = pic.Size;
		}
		public override void Draw(Graphics g)
		{
			// Draw Image
			if (Image != null)
				g.DrawImage(Image, Location);

			if (ShowBorder)
			{
				Pen pen = new Pen(BorderColor);
				pen.Width = BorderThickness;
				RectangleF rect = new RectangleF(
					(float)Location.X - ((float)BorderThickness / 2.0f),
					(float)Location.Y - ((float)BorderThickness / 2.0f),
					(float)Location.Width + BorderThickness,
					(float)Location.Height + BorderThickness);
				g.DrawRectangle(pen, rect.X, rect.Y, rect.Width, rect.Height);
			}
		}

		// Save
		public override string SaveToXml()
		{
			// Create the XML and return it
			StringBuilder xmlOut = new StringBuilder();
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.Indent = true;
			settings.IndentChars = "    ";
			settings.OmitXmlDeclaration = true;

			XmlWriter xml = XmlWriter.Create(xmlOut, settings);

			// -- Start ---------------------------------------------------
			xml.WriteStartElement("ExamPicture");

			// Write base
			WriteBaseToXml(xml);

			// Color
			xml.WriteStartElement("Properties");

			if (ImageStream != null)
				xml.WriteElementString("Picture", Convert.ToBase64String(ImageStream.ToArray()));

			xml.WriteElementString("ShowBorder", XmlConvert.ToString(ShowBorder));
			xml.WriteElementString("BorderColor", ColorTranslator.ToHtml(BorderColor));
			xml.WriteElementString("BorderThickness", XmlConvert.ToString(BorderThickness));
			
			
			xml.WriteEndElement(); // Properties

			// Done
			xml.WriteEndElement(); // ExamPicture

			xml.Close();

			return xmlOut.ToString();
		}
		public static ExamPicture LoadFromXml(string xmlStr)
		{
			ExamPicture pic = new ExamPicture();
			using (XmlReader xml = XmlReader.Create(new StringReader(xmlStr)))
			{
				while (xml.Read())
				{
					if (xml.IsStartElement())
					{
						switch (xml.Name)
						{
							case "Name":
								if (xml.Read())
									pic.Name = xml.Value;
								break;
							case "Size":
								if (xml.ReadToFollowing("Width") && xml.Read())
									pic.Size = new Size(XmlConvert.ToInt32(xml.Value), pic.Size.Height);
								if (xml.ReadToFollowing("Height") && xml.Read())
									pic.Size = new Size(pic.Size.Width, XmlConvert.ToInt32(xml.Value));
								break;
							case "Position":
								if (xml.Read())
								{
									if (xml.ReadToFollowing("X") && xml.Read())
										pic.Position = new Point(XmlConvert.ToInt32(xml.Value), pic.Position.Y);
									if (xml.ReadToFollowing("Y") && xml.Read())
										pic.Position = new Point(pic.Position.X, XmlConvert.ToInt32(xml.Value));
								}
								break;
							case "ShowBorder":
								if (xml.Read())
									pic.ShowBorder = XmlConvert.ToBoolean(xml.Value);
								break;
							case "BorderColor":
								if (xml.Read())
									pic.BorderColor = ColorTranslator.FromHtml(xml.Value);
								break;
							case "BorderThickness":
								if (xml.Read())
									pic.BorderThickness = XmlConvert.ToInt32(xml.Value);
								break;
							case "Picture":
								if (xml.Read())
								{                                    
									var ms = new MemoryStream(Convert.FromBase64String(xml.Value));
									pic.Image = new Bitmap(ms);
									ms.Dispose();
									
								}
								break;
						}
					}
				}
			}

			return pic;
		}
	}
}

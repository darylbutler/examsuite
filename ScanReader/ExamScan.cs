﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;
using System.Xml;
using System.IO;
using ScanReader.ExamObjects;
using AForge.Imaging;
using AForge.Imaging.Filters;
using System.Xml.Serialization;

namespace ScanReader
{
	// This class is the representation of the scan of a test.  It aligns, scales, and deskews an image when loaded.  It can then be called to check
	// what answer is selected at which position.
	public sealed class ExamScan : IDisposable, ICloneable
	{
		// Constants
		const int MinLineWidth = 3;							// 3 dark pixels in a row is an edge, otherwise an artifact (Red herring)
		const int ImageSearchDivision = 10;					// For find corners.  Lower = larger corner search area. 25 would be perfect for an image with 0 skew.  Lower is more error tolerance
		const float MaxAllowSkewAngle = 0.2f;				// Skew angle must deskew to below this angle (in degrees)
		const float MaxAllowScaleDelta = 0.1f;              // Scale factors must scale to with +/- this ratio
		const byte DarkByte = 0;							// Byte-Value of a Dark Pixel
		const byte NotDarkByte = 255;						// Byte-Value of all other Pixels

		#region Error Settings
		const string ErrorNone = "No Error";
		const string ErrorImageNotLoaded = "Image hasn't been loaded yet.";
		const string ErrorCornerNotFoundTopLeft = "Could not find Top Left Corner.";
		const string ErrorCornerNotFoundTopRight = "Could not find Top Right Corner.";
		const string ErrorCornerNotFoundBottomLeft = "Could not find Bottom Left Corner.";
		const string ErrorCornerNotFoundBottomRight = "Could not find Bottom Right Corner.";
		const string ErrorMaxSkewAngle = "Skew angle too large and could not be corrected";
		const string ErrorMaxScaleDelta = "Scale delta too large and could not be corrected";
		#endregion

		// Options
		public float OptCornerPatternMatchRatio = 0.40f;		// Ratio of confidence for Corners matching
		public float OptIsDarkRatio = 0.72f;					// Ratio of Brightness to signal a dark pixel
		public int OptFindAnswersHistogramPasses = -1;			// Number of rows checked (From center row) in the bubble area.  -1 indicates all rows
		public float OptBlankBubbleFillRatio = 0.5f;			// Bubbles must be at least 1 * ? filled
		public bool OptSelectOnlyOneAnswer = true;              // If false, allow for detection of more than 1 answer
		public bool OptFindBubbleLocation = true;				// If True, use edge detection to more accurately find the bubble row

		public int OffsetX, OffsetY;							// Test
		public Bitmap OriginalImage { get; private set; }		// Passed in Image source
		public Bitmap Image                                     // Our Modified image
		{
			get {
				return image?.ToManagedImage();
			}
			private set
			{
				if (value != null)
				{				
					image?.Dispose();
					image = UnmanagedImage.FromManagedImage(value);
				}
			}

		}               
		private UnmanagedImage image;
		public bool Loaded { get; private set; }                // True once the image has been Scaled and Deskewed
		public bool LoadedError { get; private set; }			// True is an error was set during LoadImage(), don't try to find any answers because confidence will be near 0
		public string ErrorStr { get; private set; }            // Error message

		public double SkewAngle { get; private set; } = 0;      // Skew angle, found in FindSkewAngle()
		public double ScaleX { get; private set; } = -1;        // Horizontal Scale factor, found in FindScale()
		public double ScaleY { get; private set; } = -1;		// Vertical Scale factor, found in FindScale()
		public Point  TopLeftCorner { get; set; }				// Point of the top left corner found, found in FindCorners()
		public Point  TopRightCorner { get; set; }				// Point of the top right corner found, found in FindCorners()
		public Point  BottomLeftCorner { get; set; }			// Point of the bottom left corner found, found in FindCorners()
		public Point  BottomRightCorner { get; set; }			// Point of the bottom right corner found, found in FindCorners()

		public List<Rectangle> QuestionLocations;               // Found locations of each question

		private string ImagePath;
		internal const bool debuging = false;
		internal const string debugPath = "debug\\BubbleRowImages";
		
		// Only allow construction with an image passed in
		public ExamScan(Bitmap img)
		{
			OriginalImage = img;
			Loaded = false;
			LoadedError = false;
			ErrorStr = ErrorImageNotLoaded;
			QuestionLocations = new List<Rectangle>();
		}

		#region Load Image -----
		/// <summary>
		/// Loads the <paramref name="image"/> structure from the <paramref name="OriginalImage"/> property.  This method handles converting the 
		/// <paramref name="OriginalImage"/> into a suitable format that this ExamScan can use to find Answers
		/// </summary>
		/// <param name="DebugToStep">This parameter is used to step through the loading process.  If it is called with 0 (Default), then the entire loading process is 
		/// performed.  Otherwise, The loading proceeds to the specified step then returns early.  Viewing the <paramref name="image"/> structure will be useful in 
		/// determining where a problem is occuring</param>
		public void LoadImage(int DebugToStep = 0)
		{
			Loaded = false;
			LoadedError = false;
			ErrorStr = ErrorImageNotLoaded;

			// Get rid of the istance of Image is it exists because we will recreate it here
			if (image != null)
				image.Dispose();

			image = UnmanagedImage.FromManagedImage(OriginalImage);

			// -- Size the image to a workable size so the algorithms don't take so long,
			// And binarify the image while we're at it, so we're 8bpp
			BinarifyAndResize(ref image);
			if (System.Diagnostics.Debugger.IsAttached) debug_saveImage(Image, ".\\step1_binarifyAndSizeimage.png");
			if (DebugToStep == 1) return;	// Debug To Step 1

			// -- Deskew the image
			Deskew(ref image);
			if (System.Diagnostics.Debugger.IsAttached) debug_saveImage(Image, ".\\step2_deskewImage.png");
			if (DebugToStep == 2) return;   // Debug To Step 2

			// Check the Skew Angle to make sure we're where we need to be
			if (!CheckSkewAngle())
			{
				ThrowLoadingError(ErrorMaxSkewAngle);
				return;
			}			

			// -- Remove Quiet Edges
			RemoveQuietEdges(ref image);
			if (System.Diagnostics.Debugger.IsAttached) debug_saveImage(Image, ".\\step3_removeQuietEdges.png");
			if (DebugToStep == 3) return;   // Debug To Step 3

			// -- Descale
			Descale(ref image);
			if (System.Diagnostics.Debugger.IsAttached) debug_saveImage(Image, ".\\step4_descaleImage.png");
			if (DebugToStep == 4) return;   // Debug To Step 4	   6

			// -- Binarify Again
			Binarify(ref image);
			if (System.Diagnostics.Debugger.IsAttached) debug_saveImage(Image, ".\\step5_binarifyImage.png");
			if (DebugToStep == 5) return;   // Debug To Step 5

			// -- Restore missing information by filling in non dark pixels surrounded by dark pixels
			RestoreInformation(image);
			if (System.Diagnostics.Debugger.IsAttached) debug_saveImage(Image, ".\\step6_restoreInformation.png");
			if (DebugToStep == 6) return;   // Debug To Step 6

			// Find the Scale of the cropped image
			SetScale(image);

			// Find the Corners again since the image changed
			FindCorners(image);
			SetScale(image);

			// After all correction has been done, check for scale errors
			if (!CheckScaleRatio())
			{
				ThrowLoadingError(ErrorMaxScaleDelta);
				return;
			}

			Loaded = true;
			if (!LoadedError)
				ErrorStr = ErrorNone;

			//NOTE: Moved to save
			//OriginalImage.Dispose();
			//OriginalImage = Image;
		}
		#endregion

		private void debug_saveImage(Bitmap img, string file)
		{
			// We're debugging, we don't care
			try
			{
				img.Save(file);
				Console.WriteLine("Saved filed: {0}", file);
			}
			catch (Exception ex)
			{
				Console.Error.WriteLine("Write image failed!  Trying to write: {0}\nError Was: {1}", file, ex.Message);
			}
		}
		#region Throw Loading Error
		/// <summary>
		/// Report loading Errors
		/// </summary>
		/// <param name="errorstr"></param>
		private void ThrowLoadingError(string errorstr)
		{
			Loaded = false;
			LoadedError = true;
			ErrorStr = errorstr;
		}
		#endregion	   
		#region Pixel is Dark -----
		// Returns true when a pixel is not white (Changed now that we use greyscale)
		private bool IsDark(byte r)
		{
			// Single byte, that's easy
			return r < (255 * OptIsDarkRatio);
		}
		private bool IsDark(byte r, byte g, byte b)
		{
			// Fast Optomization for brightness approximation
			// Formula: Y = 0.375 R + 0.5 G + 0.125 B
			// Found at: http://stackoverflow.com/questions/596216/formula-to-determine-brightness-of-rgb-color 
			//int brightness = (r << 1 + r + g << 2 + b) >> 3;
			float brightness = (r + r + b + g + g + g) / 6.0f;

			return brightness < (255 * OptIsDarkRatio);
		}
		private bool IsDark(byte a, byte r, byte g, byte b)
		{
			return IsDark(r, g, b) && a > (255 * OptIsDarkRatio);
		}
		#endregion

		// -- Image Loading Functions
		#region -- Binarify
		/// <summary>
		/// Converts an image to greyscale using our is dark ratio.  If a pixel is found
		/// brighter than our darkness ratio, we'll put the pixel in our output image as fill white
		/// otherwise we'll mark it as full dark (0)
		/// This means that there are only two possible pixel values, 255 (Not Dark) and 0 (Dark)
		/// </summary>
		/// <param name="bmp">Image to convert.  Must be in a 8 bpp format.</param>
		/// <returns>Converted 8bpp Bitmap NOTE original is unchanged and should be released.</returns>
		public unsafe void Binarify(ref UnmanagedImage srcImg)
		{
			// Determine Bit Depth
			int Depth = Bitmap.GetPixelFormatSize(srcImg.PixelFormat);
			if (Depth != 8 && Depth != 24 && Depth != 32)
			{
				throw new ArgumentException("Only 8, 24 and 32 bpp images are supported.");
			}

			// Return image
			UnmanagedImage outData = UnmanagedImage.Create(srcImg.Width, srcImg.Height, PixelFormat.Format8bppIndexed);

			// Offsets for Image Pointers
			int offsetS = srcImg.Stride - (srcImg.Width * (Depth / 8));
			int offsetO = outData.Stride - outData.Width;

			// Pointers to both images
			byte* src = (byte*)srcImg.ImageData.ToPointer();    // Create pointers pointing to location of first byte
			byte* dst = (byte*)outData.ImageData.ToPointer();

			// Loop use variables
			byte b = 0, g = 0, r = 0, a = 0;
			int width = srcImg.Width;
			int height = srcImg.Height;

			for (int y = 0; y < height; y++)
			{
				for (int x = 0; x < width; x++, dst++)
				{
					bool pixelIsDark = false;

					switch (Depth)
					{
						case 8:
							// 1 byte per pixel
							pixelIsDark = IsDark(*src);
							src++;
							break;
						case 24:
							// 3 bytes per pixel
							b = *(src + 0);
							g = *(src + 1);
							r = *(src + 2);
							pixelIsDark = IsDark(r, g, b);
							src += 3;
							break;
						case 32:
							// 4 bytes per pixel
							b = *(src + 0);
							g = *(src + 1);
							r = *(src + 2);
							a = *(src + 3);
							pixelIsDark = IsDark(a, r, g, b);
							src += 4;
							break;
					}

					*dst = pixelIsDark ? DarkByte : NotDarkByte;
				}
				src += offsetS;
				dst += offsetO;
			}

			// cleanup and return
			if (srcImg != null)
				srcImg.Dispose();
			srcImg = outData;			
		}
		#endregion
		#region -- Resize and Binarify
		/// <summary>
		/// This function combines 3 functions:	Greyscaling the image, Binarifying the image, and resizing the image to a 
		/// size that is acceptable to load, since huge images take time to iterate through
		/// 
		/// The returned image will be Binarified: All pixels above the OptIsDarkRatio will be 100% dark, while all other will be 0% dark
		/// The returned image will be 1/6th the input size, if it is above 1000 width or height
		/// </summary>
		/// <param name="SourceImage">Input Image</param>
		/// <returns>Resized and Binarified Image</returns>
		public unsafe void BinarifyAndResize(ref UnmanagedImage sourceData)
		{
			// ----- Determine Bit Depth
			int Depth = Bitmap.GetPixelFormatSize(sourceData.PixelFormat) / 8;
			if (Depth != 1 && Depth != 3 && Depth != 4)
			{
				throw new ArgumentException("Only 8, 24 and 32 bpp images are supported.");
			}

			// ----- Determine Output Image's size
			int outWidth;
			int outHeight;

			// Don't make an already small image smaller
			if (sourceData.Width <= 1000 && sourceData.Height <= 1000)
			{
				outWidth = sourceData.Width;
				outHeight = sourceData.Height;
			}
			else
			{
				outWidth = sourceData.Width / 6;
				outHeight = sourceData.Height / 6;
			}
			UnmanagedImage outData = UnmanagedImage.Create(outWidth, outHeight, PixelFormat.Format8bppIndexed);		

			// Pointers to both images
			byte* sPtr = (byte*)sourceData.ImageData.ToPointer();    // Create pointers pointing to location of first byte
			byte* dPtr = (byte*)outData.ImageData.ToPointer();

			// Loop use variables
			byte b = 0, g = 0, r = 0, a = 0;
			int width = sourceData.Width;
			int height = sourceData.Height;

			// The # of pixels in the source image for 1 pixel in the dest image
			int xPixLen = (sourceData.Width / outWidth);
			int yPixLen = (sourceData.Height / outHeight);

			// Offsets for Image Pointers
			int offsetS = ((yPixLen * sourceData.Stride) - (outWidth * xPixLen * Depth));
			int offsetO = outData.Stride - outData.Width;

			int ptrStride = sourceData.Stride - (xPixLen * Depth);

			// For every pixel in the output image
			for (int y = 0; y < outHeight; y++)
			{
				for (int x = 0; x < outWidth; x++, dPtr++)
				{
					// Search the pixel area in the src image for dark pixels						
					bool pixelIsDark = false;

					// Search the next source block to analyze
					byte* ptr = sPtr;
					for (int iy = 0; iy < yPixLen; iy++)
					{
						for (int ix = 0; ix < xPixLen; ix++)
						{
							switch (Depth)
							{
								case 1:
									pixelIsDark = IsDark(*ptr);
									break;
								case 3:
									b = *(ptr + 0);
									g = *(ptr + 1);
									r = *(ptr + 2);
									pixelIsDark = IsDark(r, g, b);
									break;
								case 4:
									b = *(ptr + 0);
									g = *(ptr + 1);
									r = *(ptr + 2);
									a = *(ptr + 3);
									pixelIsDark = IsDark(a, r, g, b);
									break;
							}
							// Move to next Search Col
							ptr += Depth;

							// Fast Exit once a dark pixel is found
							if (pixelIsDark)
								break;
						}
						// Fast Exit once a dark pixel is found
						if (pixelIsDark)
							break;

						// Move to next Search Row
						ptr += ptrStride;
					}

					// Set the destination pixel
					*dPtr = pixelIsDark ? DarkByte : NotDarkByte;

					// Move to next X
					sPtr += (xPixLen * Depth);
				}
				// Move to next Y
				sPtr += offsetS;
				dPtr += offsetO;
			}

			// Cleanup
			sourceData.Dispose();

			// Return the output image
			sourceData = outData;
		}
		public unsafe Bitmap BinarifyAndResize(Bitmap SourceImage)
		{
			UnmanagedImage img = UnmanagedImage.FromManagedImage(SourceImage);
			BinarifyAndResize(ref img);
			return img.ToManagedImage();
		}
		#endregion
		#region -- Restore Information
		/// <summary>
		/// Loops through an image <paramref name="maxRepeats"/> times until the image isn't changed.
		/// For every NOT DARK pixel found, see if atleast <paramref name="sides"/> pixels above, below,
		/// to the left and to the right are dark.  If so, then mark this pixel as dark.
		/// </summary>
		/// <param name="data">BitmapData of image to process</param>
		/// <param name="sides">Number of sides that need to be dark to mark undark pixels as dark</param>
		/// <param name="maxRepeats">Maximum repeats (If each iteration changes a pixel)</param>
		public unsafe void RestoreInformation(UnmanagedImage data, int sides = 3, int maxRepeats = 50)
		{
			// Format checking
			if (data.PixelFormat != PixelFormat.Format8bppIndexed)
				throw new ArgumentException("Image not in 8bpp format", nameof(data));

			int width = data.Width;
			int height = data.Height;
			int offset = data.Stride - width;

			// Loop until we do not find a pixel to change, or we loop a maximum number of times
			int loopCnt = 0;
			bool pixelWasChanged = true;
			while (pixelWasChanged && loopCnt < maxRepeats)
			{
				// Reset Pointer to Image Start
				byte* ptr = (byte*)data.ImageData.ToPointer();
				// Reset Pixel was changed flag
				pixelWasChanged = false;

				// Loop for every pixel
				for (int y = 0; y < height; ++y)
				{
					for (int x = 0; x < width; ++x, ++ptr)
					{
						// If this pixel is dark, do nothing
						// also, assume a binarified image
						if (*ptr > DarkByte)
						{
							// pixel is light, check the perimiter pixel to see if they are dark

							// Count of dark pixels around this not dark pixel
							int darkPerimiterCnt = 0;

							// Above Pixel
							if (y != 0 && *(ptr - data.Stride) == DarkByte)
								++darkPerimiterCnt;

							// Bottom Pixel
							if (y != (height - 1) && *(ptr + data.Stride) == DarkByte)
								++darkPerimiterCnt;

							// Last Pixel
							if (x != 0 && *(ptr - 1) == DarkByte)
								++darkPerimiterCnt;

							// Next Pixel
							if (x != (width - 1) && *(ptr + 1) == DarkByte)
								++darkPerimiterCnt;

							// If we've exceeded the required count of perimeter darks, Darken this pixel
							if (darkPerimiterCnt >= sides)
							{
								*ptr = DarkByte;
								pixelWasChanged = true;
							}
						}
					}
					ptr += offset;
				}
				++loopCnt;
			}
		}
		// Helper for Analyze Image in GradeScanForm
		public Bitmap RestoreInformation(Bitmap bmp)
		{
			UnmanagedImage img = UnmanagedImage.FromManagedImage(bmp);
			RestoreInformation(img);
			return img.ToManagedImage();
		}
		#endregion
		#region -- Deskew Image
		/// <summary>
		/// Checks the skew angle of <paramref name="img"/> and writes it to SkewAngle
		/// If the skew angle is more than 0.2 degrees, returns an image rotated by the opposite amount
		/// otherwise returns <paramref name="img"/>.
		/// </summary>
		/// <param name="img">The Image to Measure the Skew of</param>
		/// <returns>The 'Unskewed' image</returns>
		public void Deskew(ref UnmanagedImage img)
		{
			// Find the Skew Angle
			DocumentSkewChecker SkewChecker = new DocumentSkewChecker();
			SkewAngle = SkewChecker.GetSkewAngle(img);

			// No need to deskew, we're not this accurate, so do nothing to the imagedata
			if (Math.Abs(SkewAngle) <= 0.1d)
				return;

			// create rotation filter
			RotateBilinear rotationFilter = new RotateBilinear(-SkewAngle);
			rotationFilter.FillColor = Color.White;

			// rotate image applying the filter
			UnmanagedImage bmp = rotationFilter.Apply(img);

			// Recheck the Skew Angle
			SkewAngle = SkewChecker.GetSkewAngle(bmp);

			img.Dispose();
			img = bmp;
		}
		public Bitmap Deskew(Bitmap img)
		{
			UnmanagedImage bmp = UnmanagedImage.FromManagedImage(img);
			Deskew(ref bmp);
			return bmp.ToManagedImage();
		}
		#endregion
		#region -- Find Empty Areas
		public unsafe Rectangle FindEmptyAreas(UnmanagedImage bmp, bool VerticalCheck = false, int VertMinLength = MinLineWidth, int HorzMinLength = MinLineWidth)
		{
			if (bmp.PixelFormat != PixelFormat.Format8bppIndexed)
				throw new ArgumentException("Image must be in 8bpp format!", nameof(bmp));

			int lowestX = bmp.Width,
				lowestY = bmp.Height,
				highestX = -1,
				highestY = -1;

			int offset = bmp.Stride - bmp.Width;
			byte* ptr = (byte*)bmp.ImageData.ToPointer();   // Create pointer pointing to location of first byte
			int width = bmp.Width,
				height = bmp.Height;


			for (int y = 0; y < height; y++)
			{
				int darkCount = 0;
				for (int x = 0; x < width; x++, ptr++)
				{
					byte b = *ptr;

					// We've found a dark pixel
					if (IsDark(b))
					{
						// Check Vertical -----------------------------------------------------
						int vMinLen = VertMinLength;
						int vertCnt = 0;    // Dont count start pixel yet

						if (VerticalCheck)
						{
							// Don't count this pixel if it doesn't have atleast a certain amount of pixels
							// above or below it (Thick lines, not independant pixels)

							// Try above
							for (int i = -vMinLen; i < vMinLen; i++)
							{
								// Skip values out of range
								if ((y + i) < 0 || (y + i) >= height)
									continue;

								byte a = *(ptr + (bmp.Stride * i));

								if (IsDark(a))
									vertCnt++;
								else
									vertCnt = 0;

								if (vertCnt >= vMinLen)
									break;
							}
						}

						// If the vertical count passes, then we can count the dark pixel
						if (vertCnt >= vMinLen || !VerticalCheck)
							darkCount++;
						else
							darkCount = 0;
					}
					else
						darkCount = 0;

					if (darkCount >= HorzMinLength)
					{
						int lowX = x - (darkCount - 1);

						// Check for lowest
						if (lowX < lowestX) lowestX = lowX;
						if (y < lowestY) lowestY = y;

						// Check for Highest
						if (x > highestX) highestX = x;
						if (y > highestY) highestY = y;
					}
				}
				ptr += offset;
			}

			// Return the found area as the bubble area
			return new Rectangle(lowestX, lowestY, (highestX - lowestX) + 1, (highestY - lowestY) + 1);
		}
		#endregion
		#region -- Remove Quiet Edges
		/// <summary>
		/// Using an edge detector, find the smallest and largest x's and y's and crop the
		/// input image to that area and return it																			  
		/// </summary>
		/// <param name="inImg">Bitmap to Process</param>
		/// <returns></returns>
		public void RemoveQuietEdges(ref UnmanagedImage img)
		{
			int minSquareEdgeLength = ExamForm.SquareSize / 2;
			Rectangle rect = FindEmptyAreas(img, true, minSquareEdgeLength, minSquareEdgeLength);

			Crop filter = new Crop(rect);
			UnmanagedImage bmp = filter.Apply(img);

			if (img != null)
				img.Dispose();

			img = bmp;
		}
		#endregion
		#region -- Descale
		public unsafe void Descale(ref UnmanagedImage img)
		{
			// Descale the image into temporary Image
			int newWidth = 600 - (ExamForm.Margin * 2);
			int newHeight = 800 - (ExamForm.Margin * 2);
			ResizeBicubic resize = new ResizeBicubic(newWidth, newHeight);

			// New Image
			UnmanagedImage scaledImg = resize.Apply(img);

			int nOffsetX = OffsetX + ExamForm.Margin;
			int nOffsetY = OffsetY + ExamForm.Margin;

			UnmanagedImage newImg = UnmanagedImage.Create(
				scaledImg.Width + ((nOffsetX) * 2),
				scaledImg.Height + ((nOffsetY) * 2),
				PixelFormat.Format8bppIndexed);

			// Copy data into new image
			int height = scaledImg.Height;
			int width = scaledImg.Width;

			// Pointers to data
			int sStride = scaledImg.Stride;
			byte* sPtr = (byte*)scaledImg.ImageData.ToPointer();  // Take all of source
			int dStride = newImg.Stride;
			byte* dPtr = (byte*)newImg.ImageData.ToPointer() + (nOffsetY * newImg.Stride) + nOffsetX;   // Pointer to offset location

			// Fill Unset areas with undark pixels
			AForge.SystemTools.SetUnmanagedMemory(newImg.ImageData, NotDarkByte, newImg.Height * newImg.Stride);

			// Copy the data, row by row
			for (int y = 0; y < height; y++)
			{
				// Copy the data
				AForge.SystemTools.CopyUnmanagedMemory(dPtr, sPtr, width);
				sPtr += sStride;
				dPtr += dStride;
			}

			// Cleanup
			if (scaledImg != null)
				scaledImg.Dispose();
			if (img != null)
				img.Dispose();

			// Return
			img = newImg;
		}
		#endregion
		#region -- Find Corners
		public void FindCorners(UnmanagedImage original)
		{
			// This is the match ratio we must be above to find the corner.  We may need to make this an Option            
			ExhaustiveTemplateMatching tm = new ExhaustiveTemplateMatching(OptCornerPatternMatchRatio);
			TemplateMatch[] matchings;

			// Start to find the top left corner
			int searchWidth = original.Width / ImageSearchDivision;
			int searchHeight = original.Height / ImageSearchDivision;

			// Loop through the 4 corners
			for (int loopX = 0; loopX < 4; loopX++)
			{
				Rectangle corner;
				UnmanagedImage glyph;

				switch (loopX)
				{
					case 0: // Find Top Left Corner
						corner = new Rectangle(0, 0, searchWidth, searchHeight);
						glyph = UnmanagedImage.FromManagedImage(ExamForm.GetCornerGlyphTopLeft());
						break;
					case 1: // Find Top Right Corner
						corner = new Rectangle(original.Width - searchWidth, 0, searchWidth, searchHeight);
						glyph = UnmanagedImage.FromManagedImage(ExamForm.GetCornerGlyphTopRight());
						break;
					case 2: // Find Bottom Left Corner
						corner = new Rectangle(0, original.Height - searchHeight, searchWidth, searchHeight);
						glyph = UnmanagedImage.FromManagedImage(ExamForm.GetCornerGlyphBottomLeft());
						break;
					default:
					case 3: // Find Bottom Right Corner
						corner = new Rectangle(original.Width - searchWidth, original.Height - searchHeight, searchWidth, searchHeight);
						glyph = UnmanagedImage.FromManagedImage(ExamForm.GetCornerGlyphBottomRight());
						break;
				}

				// Convert gylph to 8bpp
				Binarify(ref glyph);

				// Perform the check
				matchings = tm.ProcessImage(original, glyph, corner);

				// If found, set the corner
				if (matchings.Count() > 0)
				{
					Point p = new Point(
						matchings.FirstOrDefault().Rectangle.X + (matchings.FirstOrDefault().Rectangle.Width / 2),
						matchings.FirstOrDefault().Rectangle.Y + (matchings.FirstOrDefault().Rectangle.Height / 2));


					switch (loopX)
					{
						case 0:
							TopLeftCorner = p;
							break;
						case 1:
							TopRightCorner = p;
							break;
						case 2:
							BottomLeftCorner = p;
							break;
						case 3:
							BottomRightCorner = p;
							break;
					}
				}
			}
		}
		public bool CheckIfCornersWereFound()
		{
			// Check if corners were found
			ErrorStr = string.Empty;

			if (TopLeftCorner.X < 0 || TopLeftCorner.Y < 0)
			{
				if (ErrorStr == string.Empty)
					ErrorStr = ErrorCornerNotFoundTopLeft;
				else
					ErrorStr = string.Format("{0}{1}{2}", ErrorStr, Environment.NewLine, ErrorCornerNotFoundTopLeft);
			}
			if (TopRightCorner.X < 0 || TopRightCorner.Y < 0)
			{
				if (ErrorStr == string.Empty)
					ErrorStr = ErrorCornerNotFoundTopRight;
				else
					ErrorStr = string.Format("{0}{1}{2}", ErrorStr, Environment.NewLine, ErrorCornerNotFoundTopRight);
			}
			if (BottomLeftCorner.X < 0 || BottomLeftCorner.Y < 0)
			{
				if (ErrorStr == string.Empty)
					ErrorStr = ErrorCornerNotFoundBottomLeft;
				else
					ErrorStr = string.Format("{0}{1}{2}", ErrorStr, Environment.NewLine, ErrorCornerNotFoundBottomLeft);
			}
			if (BottomRightCorner.X < 0 || BottomRightCorner.Y < 0)
			{
				if (ErrorStr == string.Empty)
					ErrorStr = ErrorCornerNotFoundBottomRight;
				else
					ErrorStr = string.Format("{0}{1}{2}", ErrorStr, Environment.NewLine, ErrorCornerNotFoundBottomRight);
			}

			return ErrorStr == string.Empty;
		}
		public void ResetCorners()
		{
			TopLeftCorner = TopRightCorner = BottomLeftCorner = BottomRightCorner = new Point(-1, -1);
		}
		#endregion

		#region Check Skew Angle
		/// <summary>
		/// Bounds checking for Max Allowable Skew Angle Error
		/// </summary>
		/// <returns></returns>
		public bool CheckSkewAngle()
		{
			return Math.Abs(SkewAngle) >= 0 &&
				Math.Abs(SkewAngle) < MaxAllowSkewAngle;
		}
		#endregion
		#region Check Scale
		public void SetScale(UnmanagedImage img)
		{
			var scale = FindScale(img);
			ScaleX = scale.X;
			ScaleY = scale.Y;
		}
		/// <summary>
		/// Finds the Scale of the Image
		/// </summary>
		public PointF FindScale(UnmanagedImage img)
		{
			float ScaleHeight = 800 - (ExamForm.Margin * 2);    // Reference distance between corners vertically
			float ScaleWidth = 600 - (ExamForm.Margin * 2);     // Reference distacne between corners horizontally

			// We assume the incoming image is larger than us
			// Scaling a smaller image up would be losing information  
			float scalex = img.Width / ScaleWidth;
			float scaley = img.Height / ScaleHeight;
			return new PointF(scalex, scaley);
		}
		// Checks the instance's ScaleX and Y if they are sane values
		private bool ScaleRatioIsSane()
		{
			return (ScaleX >= 0 && ScaleY >= 0);
		}
		/// <summary>
		/// Errors bounds checking of Scale Ratio.
		/// To be called AFTER image has been scaled
		/// </summary>
		public bool CheckScaleRatio()
		{
			return ((1f + MaxAllowScaleDelta) >= ScaleX && (1f - MaxAllowScaleDelta) <= ScaleX &&
					(1f + MaxAllowScaleDelta) >= ScaleY && (1f - MaxAllowScaleDelta) <= ScaleY &&
					ScaleRatioIsSane());
		}
		#endregion
		
		#region Grading -----
		#region FindAnswer
		/// <summary>
		/// Find the darkened answer in an ExamQuesiton's area
		/// </summary>
		/// <param name="question">Exam Question to search for (We'll use its designed location)</param>
		/// <returns>Answer struct containing the dark squares and confidences</returns>				 
		public unsafe Answer FindAnswer(ExamQuestion question)
		{
			// Exit if not loaded
			if (!Loaded && Image == null || Image == null || LoadedError)
				throw new Exception("Find Answer Called before scan image was loaded!");

			// Answer to return
			Answer ans = new Answer();

			// Find the question's position and save it
			Rectangle pos = question.Location;
			if (OptFindBubbleLocation)
			{
				Rectangle? accuratePos = DetermineBubbleExactLocation(question);
				pos = accuratePos.GetValueOrDefault(question.Location);

				// Check if Rectangle is okay
				if (pos.X < 0 || pos.Y < 0 ||
					pos.Width > (1.5f * question.Size.Width) ||
					pos.Width < (0.5f * question.Size.Width) ||
					pos.Height > (1.5f * question.Size.Height) ||
					pos.Height < (0.5f * question.Size.Height))
					pos = question.Location;    // if rectangle is too far out of range, just use the question's location
			}

			// Store this in the array
			if (QuestionLocations.Count <= question.Number)
			{
				for (int i = 0; i <= (question.Number - QuestionLocations.Count) + 1; ++i)
					QuestionLocations.Add(new Rectangle());
			}
			QuestionLocations[question.Number] = pos;

			// Create a bitmap that is only the bubbles
			//Bitmap bubbleRow = new Bitmap(pos.Width, pos.Height);
			//using (Graphics g = Graphics.FromImage(bubbleRow))
			//	g.DrawImage(Image, new Rectangle(0, 0, bubbleRow.Width, bubbleRow.Height), pos, GraphicsUnit.Pixel);
			//bubbleRow = FormatImageAsGreyscale(bubbleRow);
			Crop crp = new Crop(pos);
			UnmanagedImage bubbleRow = crp.Apply(image);

			// Debug!
			if (System.Diagnostics.Debugger.IsAttached) debugSaveBubbleImage(bubbleRow.ToManagedImage(),
				string.Format("{0}\\question{1}-Found.png", debugPath, question.Number));

			// Create a histogram for the bubble area (If there are 5 bubbles, then there are 5 areas to check)
			int[] histogram = CreateHistogram(bubbleRow, question.NumberOfBubbles);

			// Determine number of pixels scanned to determine the ratio of bubble pixels that are dark
			int bubblePixels;
			if (OptFindAnswersHistogramPasses < 0)  // All Pixels
				bubblePixels = (bubbleRow.Width * bubbleRow.Height) / question.NumberOfBubbles;
			else
				bubblePixels = (bubbleRow.Width * OptFindAnswersHistogramPasses) / question.NumberOfBubbles;

			// bubblePixels is a denomator, so ensure it is above 0
			bubblePixels = Math.Max(1, bubblePixels);

			// Add answers to return array if they are darker than the OptionFillRatio
			for (int loopX = 0; loopX < histogram.Count(); loopX++)
			{
				int i = histogram[loopX];

				bool fullyFilled = ((float)i / (float)bubblePixels) >= OptBlankBubbleFillRatio;
				ans.Update(loopX, fullyFilled, (double)i / (double)bubblePixels);                
			}

			// Prevent multiple answers if we don't allow them
			if (OptSelectOnlyOneAnswer && (from a in ans.Data where a == true select a).Count() > 1)
			{
				int highestConfidence = Convert.ToInt32(ans.Confidence.Max());
				int highestIndex = ans.Confidence.ToList().FindIndex(x => x == highestConfidence);

				ans = new Answer();
				ans.Update(highestIndex, true, highestConfidence);
			}

			// Return the marked answers
			return ans;
		}
		private void debugSaveBubbleImage(Bitmap bmp, string name)
		{
			if (!Directory.Exists(debugPath))
				Directory.CreateDirectory(debugPath);
			bmp.Save(name);
		}
		#endregion
		#region Bubble Locating
		/// <summary>
		/// In this method, we will attempt to find the answer at the area specified by the question to account for
		/// variations in scaling because of crappy images
		/// </summary>
		/// <param name="q">Exam Question to find</param>
		/// <returns>Rectangle of exact location if found.</returns>
		public Rectangle? DetermineBubbleExactLocation(ExamQuestion q)
		{
			// Search area
			Rectangle rect = q.Location;
			int growSizeX = 6, growSizeY = 6;
			rect.Inflate(growSizeX, growSizeY);

			// Search Image
			//Bitmap search = new Bitmap(rect.Width, rect.Height);
			//using (Graphics g = Graphics.FromImage(search))
			//	g.DrawImage(Image, new Rectangle(0, 0, search.Width, search.Height), rect, GraphicsUnit.Pixel);
			Crop crp = new Crop(rect);
			UnmanagedImage search = crp.Apply(image);

			// Debug
			if (System.Diagnostics.Debugger.IsAttached) debugSaveBubbleImage(search.ToManagedImage(),
				string.Format("{0}\\question{1}-Search.png", debugPath, q.Number));

			// New Algorithm
			Rectangle found = FindEmptyAreas(search, false, 1, 1);
			found.Offset(rect.X, rect.Y);

			// Check for oversize	CHECK MOVED to FindAnswer caller

			if (search != null)
				search.Dispose();

			return found;
		}		
		#endregion
		#region Histogram
		/// <summary>
		/// Returns a count for the number of dark pixels found supplied area 
		/// OptFindAnswersHistogramPasses changes how many rows we will count
		/// </summary>
		/// <param name="area">Bitmap of Bubble Location to search</param>
		/// <param name="NumberOfBubbles">Number of bubbles (a, b, c, d, e) in the search area</param>
		/// <returns>array of dark pixel counts, with array count = input NumberOfBubbles</returns>
		private unsafe int[] CreateHistogram(UnmanagedImage area, int NumberOfBubbles)
		{
			// When we find a dark pixel, we can put it in the histogram by X / bubbleSize == Bubble Index (A, B, C, ...)
			double bubbleSize = (double)area.Width / NumberOfBubbles;

			// The empty histogram
			int[] hist = new int[NumberOfBubbles];

			// Decide the start row and end row for searching based on OptFindAnswersHistogramPasses
			int passes = OptFindAnswersHistogramPasses;
			int startRow = -1, endRow = -1;
			if (passes < 0) // All rows
			{
				startRow = 0;
				endRow = area.Height;
			}
			else
			{
				int middleRow = area.Height / 2;
				startRow = Math.Max(0, middleRow - (passes / 2));
				endRow = Math.Min(area.Height, startRow + passes);
			}

			// start
			byte* ptr = (byte*)area.ImageData.ToPointer() + (startRow * area.Stride);
			int offset = area.Stride - area.Width;

			for (int y = startRow; y < endRow; y++)
			{
				for (int x = 0; x < area.Width; x++, ptr++)
				{
					byte b = *ptr;

					if (IsDark(b))
					{
						int bubbleColumn = (int)Math.Floor(x / bubbleSize);
						hist[bubbleColumn]++;
					}
				}
				ptr += offset;
			}

			return hist;
		}
		#endregion
		#endregion

		#region Loading / Unloading From Memory (Implemented for Mass-Adding Scans to Exams)
		private bool Unloaded = false;	// When true, we've disposed of our Original Image to save Memory
		public void UnloadMemory()
		{
			Unloaded = true;
			OriginalImage?.Dispose();
		}
		public void ReloadForEditing(string path)
		{
			if (!File.Exists(path))
				throw new ArgumentException("Original Image Doesn't Exist at the specified Path!");

			OriginalImage = new Bitmap(path);
			Unloaded = false;
		}
		#endregion

		#region Loading from / Saving to XML -----
		public string SaveToXml(string path) 
		{
			// Create the XML and return it
			StringBuilder xmlOut = new StringBuilder();			
			var emptyNamepsaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.OmitXmlDeclaration = true;

			using (XmlWriter xml = XmlWriter.Create(xmlOut, settings))
			{
				xml.WriteStartElement("ExamScan");

				// If we're saving, then we're done with the original Image so let it go
				OriginalImage.Dispose();
				OriginalImage = Image;
				
				// Save ImageStream to xml
				var ImageStream = new MemoryStream();
				Image.Save(ImageStream, ImageFormat.Png);
				xml.WriteElementString("ImageData", Convert.ToBase64String(ImageStream.ToArray()));

				// Offsets
				xml.WriteElementString("OffsetX", XmlConvert.ToString(OffsetX));
				xml.WriteElementString("OffsetY", XmlConvert.ToString(OffsetY));

				// Exact Question Positions array
				xml.WriteStartElement("QuestionLocations");
				foreach (Rectangle r in QuestionLocations)
				{
					xml.WriteStartElement("Location");
					xml.WriteAttributeString("X", XmlConvert.ToString(r.X));
					xml.WriteAttributeString("Y", XmlConvert.ToString(r.Y));
					xml.WriteAttributeString("Width", XmlConvert.ToString(r.Width));
					xml.WriteAttributeString("Height", XmlConvert.ToString(r.Height));
					xml.WriteEndElement();
				}
				xml.WriteEndElement();

				// Options
				xml.WriteStartElement("Options");
				xml.WriteElementString("OptIsDarkRatio", XmlConvert.ToString(OptIsDarkRatio));
				xml.WriteElementString("OptFindAnswersHistogramPasses", XmlConvert.ToString(OptFindAnswersHistogramPasses));
				xml.WriteElementString("OptBlankBubbleFillRatio", XmlConvert.ToString(OptBlankBubbleFillRatio));
				xml.WriteElementString("OptSelectOnlyOneAnswer", XmlConvert.ToString(OptSelectOnlyOneAnswer));
				xml.WriteEndElement();

				// Done
				xml.WriteEndElement();
			}

			return xmlOut.ToString();
		}
		public static ExamScan LoadFromXml(string xmlStr)
		{
			ExamScan es = new ExamScan(null);

			using (XmlReader xml = XmlReader.Create(new StringReader(xmlStr)))
			{
				while (xml.Read())
				{
					if (xml.IsStartElement())
					{
						switch (xml.Name)
						{
							case "ImagePath":
								// Old Path
								if (xml.Read() && es.Image == null && es.OriginalImage == null)
								{
									// Set to both images because we do not need to reload the image, but we should be able to if we need to
									es.OriginalImage = new Bitmap(xml.Value.Trim());
									es.Image = es.OriginalImage;
									es.ImagePath = xml.Value.Trim();
								}
								break;
							case "ImageData":
								// New Path
								if (xml.Read())
								{
									var ms = new MemoryStream(Convert.FromBase64String(xml.Value));
									es.Image = new Bitmap(ms);
									es.OriginalImage = new Bitmap(ms);
									ms.Dispose();									
								}
								break;

							case "OffsetX":
								if (xml.Read())
									es.OffsetX = XmlConvert.ToInt32(xml.Value);
								break;
							case "OffsetY":
								if (xml.Read())
									es.OffsetY = XmlConvert.ToInt32(xml.Value);
								break;
							case "QuestionLocations":
								var reader = xml.ReadSubtree();
								es.QuestionLocations = new List<Rectangle>();

								while (reader.Read())
								{
									if (reader.IsStartElement())
									{
										if (xml.Name == "Location")
										{
											Rectangle rect = new Rectangle();
											xml.MoveToFirstAttribute();
											rect.X = XmlConvert.ToInt32(xml.GetAttribute("X"));
											rect.Y = XmlConvert.ToInt32(xml.GetAttribute("Y"));
											rect.Width = XmlConvert.ToInt32(xml.GetAttribute("Width"));
											rect.Height = XmlConvert.ToInt32(xml.GetAttribute("Height"));
											es.QuestionLocations.Add(rect);
										}
									}
								}

								break;

							case "OptIsDarkRatio":
								if (xml.Read())
									es.OptIsDarkRatio = (float)XmlConvert.ToDouble(xml.Value);
								break;
							case "OptFindAnswersHistogramPasses":
								if (xml.Read())
									es.OptFindAnswersHistogramPasses = XmlConvert.ToInt32(xml.Value);
								break;
							case "OptBlankBubbleFillRatio":
								if (xml.Read())
									es.OptBlankBubbleFillRatio = (float)XmlConvert.ToDouble(xml.Value);
								break;
							case "OptSelectOnlyOneAnswer":
								if (xml.Read())
									es.OptSelectOnlyOneAnswer = XmlConvert.ToBoolean(xml.Value);
								break;
						}
					}
				}

			}
			// Set Loaded to true since we're loading from a saved Scan.  
			es.Loaded = true;
			return es;
		}
		#endregion

		#region IDisposable Support
		private bool disposedValue = false; // To detect redundant calls

		void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					OriginalImage?.Dispose();
					image?.Dispose();

					if (!string.IsNullOrEmpty(ImagePath))
					{
						try
						{
							File.Delete(ImagePath);
							ImagePath = string.Empty;
						}
						// Ignore any exceptions, this is an old path -- We don't care if we actually delete, it will be saved in the XML
						catch { }
					}
				}

				disposedValue = true;
			}
		}

		// This code added to correctly implement the disposable pattern.
		public void Dispose()
		{
			// Do not change this code. Put cleanup code in Dispose(bool disposing) above.
			Dispose(true);
		}
		#endregion
		#region	IClonable Support
		public ExamScan DeepCopy()
		{
			ExamScan scan = (ExamScan)this.MemberwiseClone();

			// Create the Image copies
			scan.image = this.image.Clone();
			if (!Unloaded)
				scan.OriginalImage = (Bitmap)this.OriginalImage?.Clone();	// Original Image may be null because of unloading
			// Copy the List
			scan.QuestionLocations = new List<Rectangle>(this.QuestionLocations);

			return scan;
		}
		public object Clone()
		{
			return this.DeepCopy();
		}
		#endregion


		#region -- Old / testing function
		/// <summary>
		/// Search the image to find how to crop the uneeded whitespace around a bubble row
		/// </summary>
		/// <param name="search">Bitmap of area to search</param>
		/// <returns>Rectangle of cropped area</returns>
		private unsafe Rectangle _old_FindBubblePositionExactInSearchArea(Bitmap search)
		{
			BitmapData sourceData = null;
			int lowestX = search.Width,
				lowestY = search.Height,
				highestX = -1,
				highestY = -1;

			try
			{
				sourceData = search.LockBits(new Rectangle(0, 0, search.Width, search.Height), ImageLockMode.ReadOnly, Image.PixelFormat);

				for (int y = 0; y < search.Height; y++)
				{
					byte* sourceRow = (byte*)sourceData.Scan0 + (y * sourceData.Stride);

					for (int x = 0; x < search.Width; x++)
					{
						byte b = sourceRow[x];

						// We've found a dark pixel
						if (IsDark(b))
						{
							// Check for lowest
							if (x < lowestX) lowestX = x;
							if (y < lowestY) lowestY = y;

							// Check for Highest
							if (x > highestX) highestX = x;
							if (y > highestY) highestY = y;
						}
					}
				}
			}
			finally
			{
				if (sourceData != null)
					search.UnlockBits(sourceData);
			}

			// Return the found area as the bubble area
			return new Rectangle(lowestX, lowestY, highestX - lowestX, highestY - lowestY);
		}
		#endregion



	}
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;
using ScanReader.ExamObjects;

namespace ScanReader
{
	// This class handles marking up an ExamForm
	public class ExamMarkup
	{
		#region Properties ------------------------------------------------------------------------
		// -- Options -----------------------------------------------------------------------------
		[Category("Options"), Description("Smoothing Mode used while drawing.")]
		public System.Drawing.Drawing2D.SmoothingMode SmoothingMode { get; set; }

		[Category("Options"), Description("The Hinting used to draw text.")]
		public System.Drawing.Text.TextRenderingHint TextHinting { get; set; }

		// -- Name --------------------------------------------------------------------------------
		[Category("Name"), Description("True if we should draw the student's name when the test is marked up."), DefaultValue(true)]
		public bool PrintStudentName { get; set; }

		[Category("Name"), Description("Where to Print the Student's Name.  Pick a quiet area so no information is obscured when drawn.")]
		public Rectangle StudentNameLocation { get; set; }

		[XmlIgnore, Category("Name"), Description("Font used to draw the Student's Name.")]
		public Font StudentNameFont { get { return XmlStudentNameFont.Font; } set { XmlStudentNameFont = new sFont(value); } }

		[Browsable(false)]
		public sFont XmlStudentNameFont { get; set; }

		[XmlIgnore, Category("Name"), Description("Color used to draw the Student's Name.")]
		public Color StudentNameColor { get { return XmlStudentNameColor.Color; } set { XmlStudentNameColor = new sColor(value); } }

		[Browsable(false)]
		public sColor XmlStudentNameColor { get; set; }

		// -- Score -------------------------------------------------------------------------------
		[Category("Score"), Description("True if we should draw the student's score when the test is marked up.")]
		public bool PrintStudentScore { get; set; }

		[Category("Score"), Description("Where to Draw the Student's Score.")]
		public Rectangle ScoreLocation { get; set; }

		[XmlIgnore, Category("Score"), Description("Font used to draw the Student's Score.")]
		public Font ScoreFont { get { return XmlScoreFont.Font; } set { XmlScoreFont = new sFont(value); } }

		[Browsable(false)]
		public sFont XmlScoreFont { get; set; }

		[XmlIgnore, Category("Score"), Description("Color used to draw the Student's Score.")]
		public Color ScoreColor { get { return XmlScoreColor.Color; } set { XmlScoreColor = new sColor(value); } }

		[Browsable(false)]
		public sColor XmlScoreColor { get; set; }

		[Category("Score"), Description("Number of Decimal Places to draw of the Score.  Will be rounded."), DefaultValue(2)]
		public int ScoreDecimalPlaces { get; set; }

		[Category("Score"), Description("If true, also print the # of question answered correctly / total # of question (ie: 5 / 10)."), DefaultValue(2)]
		public bool ScoreShowCorrectAndTotalCount { get; set; }

		// -- Mark --------------------------------------------------------------------------------
		[Category("Marking"), Description("If true, Mark Correct Answers with a Checkmark."), DefaultValue(true)]
		public bool MarkCorrectAnswers { get; set; }

		[XmlIgnore, Category("Marking"), Description("The Color used for drawing the Correct Answer Checkmark.")]
		public Color MarkCorrectAnswerColor { get { return XmlMarkCorrectAnswerColor.Color; } set { XmlMarkCorrectAnswerColor = new sColor(value); } }

		[Browsable(false)]
		public sColor XmlMarkCorrectAnswerColor { get; set; }

		[Category("Marking"), Description("The thickness to draw the lines for the Correct Answer Marks."), DefaultValue(1.0f)]
		public float MarkCorrectAnswerLineThickness { get; set; }

		[Category("Marking"), Description("If true, Mark Incorrect Answer with an X."), DefaultValue(true)]
		public bool MarkIncorrectAnswers { get; set; }

		[XmlIgnore, Category("Marking"), Description("The Color used for drawing the Incorrect Answer X.")]
		public Color MarkIncorrectAnswerColor { get { return XmlMarkIncorrectAnswerColor.Color; } set { XmlMarkIncorrectAnswerColor = new sColor(value); } }

		[Browsable(false)]
		public sColor XmlMarkIncorrectAnswerColor { get; set; }

		[Category("Marking"), Description("The thickness to draw the lines for the Incorrect Answer Marks."), DefaultValue(1.0f)]
		public float MarkIncorrectAnswerLineThickness { get; set; }

		[Category("Marking"), Description("User defined horizontal offset to align the location of the Marks."), DefaultValue(0)]
		public int MarkOffsetX { get; set; }

		[Category("Marking"), Description("User defined vertical offset to align the location of the Marks."), DefaultValue(0)]
		public int MarkOffsetY { get; set; }

		[Category("Marking"), Description("User defined horizontal growth to make the Marks wider."), DefaultValue(0)]
		public int MarkGrowthX { get; set; }

		[Category("Marking"), Description("User defined vertical growth to make the Marks taller."), DefaultValue(0)]
		public int MarkGrowthY { get; set; }

		// -- Bubble Boxes ------------------------------------------------------------------------
		[Category("Bubble Boxes"), Description("Draw a box around the correct answer bubble if the question was answered correctly."), DefaultValue(false)]
		public bool BoxCorrectBubbleIfCorrect { get; set; }

		[Category("Bubble Boxes"), Description("Draw a box around the correct answer bubble if the question was NOT answered correctly."), DefaultValue(true)]
		public bool BoxCorrectBubbleIfIncorrect { get; set; }

		[XmlIgnore, Category("Bubble Boxes"), Description("Color used when drawing a box around a correct answer bubble.")]
		public Color BoxCorrectColor { get { return XmlBoxCorrectColor.Color; } set { XmlBoxCorrectColor = new sColor(value); } }

		[Browsable(false)]
		public sColor XmlBoxCorrectColor { get; set; }

		[Category("Bubble Boxes"), Description("The thickness of the lines used to draw the box around a correct answer bubble."), DefaultValue(1.0f)]
		public float BoxCorrectLineThickness { get; set; }

		[Category("Bubble Boxes"), Description("Draw a box around the incorrectly answered bubble."), DefaultValue(true)]
		public bool BoxIncorrectBubble { get; set; }

		[XmlIgnore, Category("Bubble Boxes"), Description("Color used when drawing a box around an incorrectly answered bubble.")]
		public Color BoxIncorrectColor { get { return XmlBoxIncorrectColor.Color; } set { XmlBoxIncorrectColor = new sColor(value); } }

		[Browsable(false)]
		public sColor XmlBoxIncorrectColor { get; set; }

		[Category("Bubble Boxes"), Description("The thickness of the lines used to draw the box around an incorrectly answered bubble."), DefaultValue(1.0f)]
		public float BoxIncorrectLineThickness { get; set; }

		//[Category("Bubble Boxes"), Description("")]
		#endregion

		#region Constructor -----------------------------------------------------------------------
		public ExamMarkup()
		{
			SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
			TextHinting = System.Drawing.Text.TextRenderingHint.AntiAlias;

			StudentNameLocation = new Rectangle(
				ExamForm.Margin * 2, 
				ExamForm.Margin,
				200,
				ExamForm.SquareSize
				);

			ScoreLocation = new Rectangle(
				StudentNameLocation.X + StudentNameLocation.Width + 100,
				ExamForm.Margin,
				200,
				ExamForm.SquareSize
				);

			PrintStudentName = true;
			StudentNameFont = new Font("Calibri", ExamForm.SquareSize, FontStyle.Bold, GraphicsUnit.Pixel);
			StudentNameColor = Color.CornflowerBlue;

			PrintStudentScore = true;
			ScoreDecimalPlaces = 2;
			ScoreShowCorrectAndTotalCount = true;
			ScoreFont = new Font("Calibri", ExamForm.SquareSize * 2, FontStyle.Bold, GraphicsUnit.Pixel);
			ScoreColor = Color.CornflowerBlue;

			BoxCorrectBubbleIfCorrect = false;
			BoxCorrectBubbleIfIncorrect = true;
			BoxCorrectColor = Color.Green;
			BoxCorrectLineThickness = 1.0f;

			BoxIncorrectBubble = true;
			BoxIncorrectColor = Color.Red;
			BoxIncorrectLineThickness = 1.0f;

			MarkCorrectAnswers = true;
			MarkCorrectAnswerColor = Color.Green;
			MarkCorrectAnswerLineThickness = 2.0f;

			MarkGrowthX = -2;
			MarkGrowthY = -2;

			MarkIncorrectAnswers = true;
			MarkIncorrectAnswerColor = Color.Red;
			MarkIncorrectAnswerLineThickness = 2.0f;

			MarkOffsetX = 0;
			MarkOffsetY = -2;
		}
		#endregion

		#region Drawing ---------------------------------------------------------------------------
		public void MarkUpScan(Graphics g, Exam exam, Student student)
		{
			// -- Remember the Original Smoothing Mode and Text Rendering Hints so we can restore them after we draw
			System.Drawing.Drawing2D.SmoothingMode orig_mode = g.SmoothingMode;
			System.Drawing.Text.TextRenderingHint orig_hint = g.TextRenderingHint;
			g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
			g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;

			// -- Draw Name and Score
			printStudentNameAndScore(g, exam, student);

			// -- Perform Marking
			markAnswersIfCorrect(g, exam, student);

			// -- Draw Boxes
			boxDrawBubbleBoxes(g, exam, student);

			// -- Restore the Smoothing Mode and Text Rendering Hints before we return
			g.SmoothingMode = orig_mode;
			g.TextRenderingHint = orig_hint;
		}	 
		public void DrawCorrectAnswer(Graphics g, Student s, ExamQuestion q, Answer ans)
		{
			// Allows External class to box an answer
			boxDrawAnswer(g, s, q, ans, BoxCorrectColor, BoxCorrectLineThickness);
		}
		public void DrawIncorrectAnswer(Graphics g, Student s, ExamQuestion q, Answer ans)
		{
			// Allows External class to box an answer
			boxDrawAnswer(g, s, q, ans, BoxIncorrectColor, BoxIncorrectLineThickness);
		}
		public void MarkAnswer(Graphics g, Exam e, Student s, int QuestionNumber)
		{
			// Allow outside marking of answer in this style
			// We use ForceMark = true to override options in this instance
			markAnswerIfCorrect(g, e, s, QuestionNumber, true);
		}
		private void markAnswersIfCorrect(Graphics g, Exam exam, Student student)
		{
			// Draw marks over the Question Numbers
			if (!this.MarkCorrectAnswers && !this.MarkIncorrectAnswers)
				return;	// No Marking			
			
			for (int i = 0; i < student.Score.QuestionCorrect.Count; ++i)
			{
				markAnswerIfCorrect(g, exam, student, i);
			}

			
		}
		private void markAnswerIfCorrect(Graphics g, Exam exam, Student student, int QuestionNumber, bool ForceMark = false)
		{
			// Assign User-Defined Offsets
			int offsetX = MarkOffsetX, offsetY = MarkOffsetY;

			// Get the Drawn Location of the Number relative to the bubble location
			RectangleF rect = exam.Form.Questions[QuestionNumber].GetNumberLocation(g);


			// Calculate a new offset, based on where the scanned image's Bubble were actually found
			if (student.Scan != null && student.Scan.Loaded && student.Scan.QuestionLocations.Count >= exam.Form.Questions.Count)
			{
				offsetX += student.Scan.QuestionLocations[QuestionNumber + 1].X - exam.Form.Questions[QuestionNumber].Location.X;
				offsetY += student.Scan.QuestionLocations[QuestionNumber + 1].Y - exam.Form.Questions[QuestionNumber].Location.Y;
			}

			// Perform the Location offset and Growth
			rect.Offset(offsetX, offsetY);
			rect.Inflate(MarkGrowthX, MarkGrowthY);

			if (student.Score.QuestionCorrect[QuestionNumber] && (this.MarkCorrectAnswers || ForceMark))
			{
				// Draw Check over Correct Answer
				Pen bP = new Pen(MarkCorrectAnswerColor, MarkCorrectAnswerLineThickness);

				g.DrawLine(bP,
					rect.X,
					rect.Y + (0.3f * rect.Height),
					rect.X + (0.35f * rect.Width),
					rect.Y + (0.6f * rect.Height));
				g.DrawLine(bP,
					rect.X + (0.35f * rect.Width),
					rect.Y + (0.6f * rect.Height),
					rect.Right,
					rect.Y);
			}
			else if (this.MarkIncorrectAnswers || ForceMark)
			{
				// Draw X over Incorrect Answer
				Pen bP = new Pen(MarkIncorrectAnswerColor, MarkIncorrectAnswerLineThickness);

				g.DrawLine(bP, rect.X, rect.Y, rect.Right, rect.Bottom);
				g.DrawLine(bP, rect.Right, rect.Y, rect.X, rect.Bottom);
			}
		}
		private void printStudentNameAndScore(Graphics g, Exam exam, Student student)
		{
			// Draw student's Name and their Score
			StringFormat frmt = new StringFormat(StringFormatFlags.NoClip);

			// -- Mark Name
			if (PrintStudentName)
			{
				g.DrawString(student.FullName, StudentNameFont, new SolidBrush(StudentNameColor), StudentNameLocation, frmt);
			}

			// -- Mark Score
			if (PrintStudentScore)
			{
				float total = exam.AnswerKey.Answers.Count;
				float correct = (from q in student.Score.QuestionCorrect
								 where q == true
								 select q).Count();
				float percent = (correct / total) * 100.0f;
				string score = string.Format("{0}%", percent.ToString(string.Format("n{0}", ScoreDecimalPlaces)));

				if (ScoreShowCorrectAndTotalCount)
					score = string.Format("{0}     {1} / {2}", score, correct, total);										

				g.DrawString(score, ScoreFont, new SolidBrush(ScoreColor), ScoreLocation, frmt);
			}
		}
		private void boxDrawBubbleBoxes(Graphics g, Exam exam, Student student)
		{
			// Draw the boxes around the bubbles
			if (!BoxCorrectBubbleIfCorrect && !BoxCorrectBubbleIfIncorrect && !BoxIncorrectBubble)
				return;	// No Boxing selected

			for (int i = 0; i < exam.AnswerKey.Answers.Count; ++i)
			{
				if (student.Score.QuestionCorrect[i] && BoxCorrectBubbleIfCorrect)
				{
					Answer correctAnswer = new Answer(exam.AnswerKey.Answers[i]);
					boxDrawAnswer(g, student, exam.Form.Questions[i], correctAnswer, BoxCorrectColor, BoxCorrectLineThickness);
				}
				else if (!student.Score.QuestionCorrect[i])
				{
					Answer studentAnswer = student.Score.Answers[i];
					Answer correctAnswer = new Answer(exam.AnswerKey.Answers[i]);

					if (this.BoxIncorrectBubble)
						boxDrawAnswer(g, student, exam.Form.Questions[i], studentAnswer, BoxIncorrectColor, BoxIncorrectLineThickness);
					if (this.BoxCorrectBubbleIfIncorrect)
						boxDrawAnswer(g, student, exam.Form.Questions[i], correctAnswer, BoxCorrectColor, BoxCorrectLineThickness);
				}
			}
		}
		private void boxDrawAnswer(Graphics g, Student student, ExamQuestion q, Answer answer, Color markColor, float lineThickness)
		{

			// -- Draw the box
			Pen bP = new Pen(markColor, lineThickness);

			Rectangle BubbleLocation;
			if (student.Scan != null && student.Scan.QuestionLocations != null && student.Scan.QuestionLocations.Count >= q.Number)
				BubbleLocation = student.Scan.QuestionLocations[q.Number];
			else
				BubbleLocation = q.Location;

			float bubbleWidth = ((float)BubbleLocation.Width / q.NumberOfBubbles);
			float x = BubbleLocation.X + (answer.GetAsInt() * bubbleWidth);

			RectangleF r = new RectangleF(
				Math.Max(0, x), Math.Max(0, BubbleLocation.Y),
				bubbleWidth, BubbleLocation.Height);

			g.DrawRectangle(bP, r.X, r.Y, r.Width, r.Height);

			//if (answer.GetAsInt() > -1)
			//{
				//Font font = new Font("Calibri", 11);
				//PointF point = new PointF(x, student.Scan.QuestionLocations[q.Number].Y - 16);
				//g.DrawString(answer.GetAsString(), font, new SolidBrush(markColor), point);
			//}
		}
		#endregion

		#region XML Saving / Loading --------------------------------------------------------------
		public string SaveToXml()
		{
			XmlSerializer Format = new XmlSerializer(typeof(ExamMarkup));
			StringBuilder xmlOut = new StringBuilder();
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.Indent = true;
			settings.IndentChars = "    ";
			settings.OmitXmlDeclaration = true;
			using (XmlWriter xml = XmlWriter.Create(xmlOut, settings))
			{
				Format.Serialize(xml, this);
			}
			return xmlOut.ToString();
		}
		public static ExamMarkup LoadFromXml(string xmlStr)
		{
			XmlSerializer Format = new XmlSerializer(typeof(ExamMarkup));
			ExamMarkup em;
			using (XmlReader xml = XmlReader.Create(new StringReader(xmlStr)))
			{
				em = (ExamMarkup)Format.Deserialize(xml);
			}
			return em;
		}
		#endregion
	}

	[Serializable]
	public class sFont
	{
		public sFont()
		{
			this.Font = null;
		}

		public sFont(Font font)
		{
			this.Font = font;
		}

		[XmlIgnore]
		public Font Font { get; set; }

		[XmlElement("Font")]
		public string FontString
		{
			get
			{
				if (this.Font != null)
				{
					TypeConverter converter = TypeDescriptor.GetConverter(typeof(Font));

					return converter.ConvertToString(this.Font);
				}
				else return null;
			}
			set
			{
				TypeConverter converter = TypeDescriptor.GetConverter(typeof(Font));

				this.Font = (Font)converter.ConvertFromString(value);
			}
		}
	}

	[Serializable]
	public class sColor
	{
		public sColor()
		{
			this.Color = Color.AliceBlue;
		}

		public sColor(Color color)
		{
			this.Color = color;
		}

		[XmlIgnore]
		public Color Color { get; set; }

		[XmlElement("Color")]
		public string ColorString
		{
			get
			{
				if (this.Color != null)
				{
					return ColorTranslator.ToHtml(this.Color);
				}
				else return null;
			}
			set
			{
				this.Color = ColorTranslator.FromHtml(value);
			}
		}
	}


}

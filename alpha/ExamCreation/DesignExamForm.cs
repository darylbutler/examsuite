﻿using ScanReader;
using ScanReader.ExamObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace alpha.ExamCreation
{
	

	public partial class DesignExamForm : Form
	{	
		// -- Members -----------------------------------------------------------------------------
		
		// Misc
		bool IgnoreEvents;      // True when we are programattically changing UI values so no *Changed events will be raised

		// File Generation
		bool Modified;          // True if the file has been Modified
		string FileName;        // Path if this file has been saved

		// The FileEditor Component
		FormEditBox FormEditor;

		// -- Constructor -------------------------------------------------------------------------
		public DesignExamForm()
		{
			InitializeComponent();
			FormEditor = new FormEditBox();
			FormEditor.Size = new Size(600, 800);
			FormEditor.Location = new Point(4, 25);			
			FormEditor.OnSelectionChanged += FormSelectedObjectsChanged;
			FormEditor.OnFormChanged += FormChanged;
			FormEditor.OnNewItem += FillObjectNamesBox;
			FormEditor.Click += (Object, EventArgs) => { FormEditor.Focus(); };
			FormEditor.OnMouseModeChanged += UpdateModeLabel;
			FormEditor.OnMousePositionUpdate += (x, y) => {
				CursorPositionLabel.Text = string.Format("Cursor ({0:D3}, {1:D3})", x, y);
			};
			FormEditor.OnDraggingUpdate += (x, y) => {
				DraggingLabel.Text = string.Format("X {0} {1:D3}, Y {2} {3:D3}", (x > 0 ? "+" : "-"), Math.Abs(x), (y > 0 ? "+" : "-"), Math.Abs(y));
				UpdateDraggingLabel();
			};
			FormEditor.OnSelectionAreaUpdate += (x1, y1, x2, y2) =>	{
				DraggingLabel.Text = string.Format("Bounds: ({0:D3}, {1:D3}) -> ({2:D3}, {3:D3})", x1, y1, x2, y2);
				UpdateDraggingLabel();
			};
			FormEditor.OnResizeDragUpdate += (x, y) => {
				string xLabel = "Width:" + (x > 0 ? " -" : " +") + Math.Abs(x).ToString("D3");
				string yLabel = "Height:" + (y > 0 ? " -" : " +") + Math.Abs(y).ToString("D3");
				DraggingLabel.Text = string.Format("{0}, {1}", xLabel, yLabel);
				UpdateDraggingLabel();
			};
			this.Controls.Add(FormEditor);									

			// Start with a new Exam
			FileName = string.Empty;
			Modified = false;
			FormEditor.Form = new ExamForm("<Untitled Form>");
			IgnoreEvents = false;

			// Draw before exiting
			FormEditor.SelectObject(null);
			UpdateModeLabel();			
			SetAppTitle();
		}

		// -- Update UI ---------------------------------------------------------------------------
		public string GetAppTitle()
		{
			return string.Format("ExamForm Editor - {0}", FormEditor.Form.Name);
		}
		public void UpdateModeLabel()
		{
			string mode = string.Empty;
			if (FormEditor.DraggingMode != DraggingModes.None)
			{
				switch (FormEditor.DraggingMode)
				{
					case DraggingModes.Moving:
						mode = "Moving";
						break;
					case DraggingModes.Resizing:
						mode = "Resizing";
						break;
					case DraggingModes.Selecting:
						mode = "Selecting";
						break;
				}
			}
			else
			{
				switch (FormEditor.MouseMode)
				{
					case MouseModes.Moving:
						mode = "Move";
						break;
					case MouseModes.Placing:
						mode = "Place";
						break;
					default:
						mode = "Select";
						break;
				}
			}
			ModeLabel.Text = string.Format("Mode: {0}", mode);
		}
		private void FormChanged()
		{
			// If the form was changed, load the changes
			FormSelectedObjectsChanged();   
			propertyGrid.Refresh();

			// Do the normal stuff
			Modified = true;
			SetAppTitle();
		}
		private void SetAppTitle()
		{
			this.Text = string.Format("{0}{1}", GetAppTitle(), Modified ? "*" : "");
		}
		private void FillObjectNamesBox(ExamObject obj)
		{
			IgnoreEvents = true;
			// Populate Box
			SelectByNameBox.Text = string.Empty;
			SelectByNameBox.Items.Clear();

			var ary = from e in FormEditor.Form.Objects where !(e is ExamMarkupRect) select e.Name;
			SelectByNameBox.Items.AddRange(ary.ToArray());

			// Select currently selected item if one
			if (FormEditor.SelectedObject != null)
				SelectByNameBox.SelectedItem = FormEditor.SelectedObject.Name;

			IgnoreEvents = false;
		}
		private void UpdateDraggingLabel()
		{
			switch (FormEditor.DraggingMode)
			{
				case DraggingModes.None:
					DraggingLabel.Image = null;
					DraggingLabel.Text = string.Empty;
					break;
				case DraggingModes.Resizing:
					DraggingLabel.Image = Properties.Resources.arrow_inout;
					break;
				case DraggingModes.Moving:
					DraggingLabel.Image = Properties.Resources.shape_move_backwards;
					break;
				case DraggingModes.Selecting:
					DraggingLabel.Image = Properties.Resources.shape_group;
					break;
			}
		}
		private void MarkUpModeBtn_Click(object sender, EventArgs e)
		{
			if (FormEditor.Form.PreviewMarkedUp)
				ExitMarkupEditMode();
			else
				EnterMarkupEditMode();
		}
		private void EnterMarkupEditMode()
		{
			// Change button
			MarkupEditModeBtn.Text = "Exit Edit Markup Mode";

			// Clear any selections
			FormEditor.SelectedObjects.Clear();

			// Select the markup engine for editing
			propertyGrid.SelectedObject = FormEditor.Form.MarkupEngine;

			// Generate the random exam stuff and tell the form to draw them
			FormEditor.Form.PreviewMarkedUp = true;

			// Redraw
			FormEditor.Invalidate();
		}
		private void ExitMarkupEditMode()
		{
			// Change button
			MarkupEditModeBtn.Text = "Enter Edit Markup Mode";

			// Turn off Markup Preview on Form
			FormEditor.Form.PreviewMarkedUp = false;

			// Select the form for editing
			FormEditor.SelectObject(null);
		}

		// -- Canvas Mouse Work -------------------------------------------------------------------		
		private void MouseModeToSelection()
		{
			FormEditor.MouseMode = MouseModes.Selection;			
			UpdateModeLabel();
		}
		private void MouseModeToMoving()
		{
			FormEditor.MouseMode = MouseModes.Moving;
			UpdateModeLabel();
		}
		private void MouseModeToPlacing()
		{
			FormEditor.MouseMode = MouseModes.Placing;
			UpdateModeLabel();
		}
		private void SelectionModeBtn_Click(object sender, EventArgs e)
		{
			if (SelectionModeBtn.Text == "Move Object (or RClick)")
			{
				MouseModeToMoving();
				SelectionModeBtn.Text = "Select Object(or LClick)";
			}
			else
			{
				SelectionModeBtn.Text = "Move Object (or RClick)";
				MouseModeToSelection();
			}
		}

		// -- Placing -----------------------------------------------------------------------------
		private void InsertLabelBtn_Click(object sender, EventArgs e)
		{
			MouseModeToPlacing();
			FormEditor.PlacingMode = ExamTypes.Text;
		}
		private void InsertLineBtn_Click(object sender, EventArgs e)
		{
			MouseModeToPlacing();
			FormEditor.PlacingMode = ExamTypes.Line;
		}
		private void InsertPictureBtn_Click(object sender, EventArgs e)
		{
			MouseModeToPlacing();
			FormEditor.PlacingMode = ExamTypes.Picture;
		}
		private void InsertWrittenEntryBtn_Click(object sender, EventArgs e)
		{
			MouseModeToPlacing();
			FormEditor.PlacingMode = ExamTypes.WrittenEntry;
		}
		private void InsertQuestionEntryBtn_Click(object sender, EventArgs e)
		{
			MouseModeToPlacing();
			FormEditor.PlacingMode = ExamTypes.QuestionEntry;
		}

		// -- Selection ---------------------------------------------------------------------------
		private void SelectByNameBox_SelectionChangeCommitted(object sender, EventArgs e)
		{
			if (IgnoreEvents)
				return;

			var objs = (from o in FormEditor.Form.Objects where o.Name == SelectByNameBox.Text select o);

			if (objs.Count() > 0)
				FormEditor.SelectObject(objs.First());
		}
		private void SelectByNameBox_KeyDown(object sender, KeyEventArgs e)
		{
			if (IgnoreEvents)
				return;

			if (e.KeyCode == Keys.Enter)
			{
				var objs = (from o in FormEditor.Form.Objects where o.Name == SelectByNameBox.Text select o);

				if (objs.Count() > 0)
					FormEditor.SelectObject(objs.First());
			}
		}
		private void FormSelectedObjectsChanged()
		{
			// Don't update or select anything if we are previewing the markup engine
			if (FormEditor.Form.PreviewMarkedUp)
				return;

			IgnoreEvents = true;

			// Change State based on selection
			if (FormEditor.SelectedObject == null)
			{
				GroupPanel.Visible = false;
				PositioningPanel.Visible = true;
				PropertiesPanel.Visible = true;

				SelectionModeBtn.Text = "Move Object (or RClick)";
				MouseModeToSelection();
				CopySelectedBtn.Enabled = false;
				ObjectPositionX.Value = 0;
				ObjectPositionY.Value = 0;
				ObjectSizeWidth.Value = 0;
				ObjectSizeHeight.Value = 0;
				ObjectPositionX.Enabled = false;
				ObjectPositionY.Enabled = false;
				ObjectSizeWidth.Enabled = false;
				ObjectSizeHeight.Enabled = false;
				ObjectNameBox.Text = "<No Objects Selected>";
				ObjectNameBox.Enabled = false;

				SelectByNameBox.SelectedIndex = -1;
				SelectByNameBox.Text = "";				

				// Assign Property Grid
				propertyGrid.SelectedObject = FormEditor.Form;  // Allow editing form when no object selected
			}
			else
			{
				GroupPanel.Visible = FormEditor.SelectedMultiple;  // Only show when more than 1 selected
				PositioningPanel.Visible = true;
				PropertiesPanel.Visible = true;
				CopySelectedBtn.Enabled = true;
				ObjectPositionX.Value = FormEditor.SelectedObject.Position.X;
				ObjectPositionY.Value = FormEditor.SelectedObject.Position.Y;
				ObjectSizeWidth.Value = FormEditor.SelectedObject.Size.Width;
				ObjectSizeHeight.Value = FormEditor.SelectedObject.Size.Height;
				ObjectPositionX.Enabled = !FormEditor.SelectedMultiple;
				ObjectPositionY.Enabled = !FormEditor.SelectedMultiple;
				ObjectSizeWidth.Enabled = !FormEditor.SelectedMultiple;
				ObjectSizeHeight.Enabled = !FormEditor.SelectedMultiple;

				if (FormEditor.SelectedMultiple)
				{
					SelectByNameBox.Text = string.Format("<{0} Objects Selected>", FormEditor.SelectedObjects.Count);
					ObjectNameBox.Text = string.Format("<{0} Objects Selected>", FormEditor.SelectedObjects.Count);
					ObjectNameBox.Enabled = false;
				}
				else
				{
					SelectByNameBox.SelectedItem = FormEditor.SelectedObject.Name;
					ObjectNameBox.Text = FormEditor.SelectedObject.Name;
					ObjectNameBox.Enabled = true;
				}

				// Assign Property Grid
				propertyGrid.SelectedObjects = FormEditor.SelectedObjects.ToArray();
			}

			IgnoreEvents = false;
		}

		// -- File New / Open / Save --------------------------------------------------------------
		private void SaveExamBtn_Click(object sender, EventArgs e)
		{
			if (FileName == string.Empty)
			{
				SaveFileDialog dialog = new SaveFileDialog();
				dialog.Filter = "Exam Form Files (*.efm)|*.efm|All Files (*.*)|*.*";
				dialog.InitialDirectory = @"Forms\";
				if (dialog.ShowDialog() == DialogResult.OK)
				{
					FileName = dialog.FileName;
				}
			}

			FormEditor.Form.SaveToFile(FileName);
			Modified = false;
			SetAppTitle();
		}
		private bool SaveIfModified()
		{
			if (Modified)
			{
				var result = MessageBox.Show("Form was modified and the changes haven't been saved.\nDo you want to save them first?",
					"Unsaved Changes!", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
				if (result == DialogResult.Cancel)
					return false;
				if (result == DialogResult.Yes)
					SaveExamBtn_Click(null, null);
			}
			return true;
		}
		private void NewExamBtn_Click(object sender, EventArgs e)
		{
			if (!SaveIfModified())
				return;

			FileName = string.Empty;
			FormEditor.Form = new ExamForm();
			Modified = false;
			SetAppTitle();
			FillObjectNamesBox(null);
			FormEditor.SelectObject(null);

			FormEditor.Invalidate();
		}		
		private void OpenExamBtn_Click(object sender, EventArgs e)
		{
			if (!SaveIfModified())
				return;

			// Open new
			OpenFileDialog dialog = new OpenFileDialog();
			dialog.Filter = "Exam Form Files (*.efm)|*.efm|All Files (*.*)|*.*";
			dialog.InitialDirectory = @"Forms\";
			if (dialog.ShowDialog() == DialogResult.OK)
			{
				FileName = dialog.FileName;
				FormEditor.Form = ExamForm.LoadFromFile(FileName);
				Modified = false;
				SetAppTitle();
				FillObjectNamesBox(null);
				FormEditor.SelectObject(null);
			}

			FormEditor.Invalidate();
		}
		private void ExportToImageBtn_Click(object sender, EventArgs e)
		{
			// Disable Markup Mode if its on
			if (FormEditor.Form.PreviewMarkedUp)
				ExitMarkupEditMode();

			// Create and Save the iamge
			SaveFileDialog dlg = new SaveFileDialog();
			dlg.Filter = "PNGs YO|*.png";

			if (dlg.ShowDialog() == DialogResult.OK)
			{
				//Bitmap frm = new Bitmap(600, 800);
				//using (Graphics g = Graphics.FromImage(frm))
				//	FormEditor.Form.Draw(g, true);
				Bitmap frm = FormEditor.Form.GetBitmap(false);
				frm.Save(dlg.FileName);
				frm.Dispose();
			}
		}
		private void PrintFormBtn_Click(object sender, EventArgs e)
		{
			if (FormEditor.Form == null) return;

			// Create a document  for printing
			var document = new iTextSharp.text.Document(iTextSharp.text.PageSize.LETTER, 10, 10, 10, 10);

			string fileName = "ToPrint.pdf";
			var output = new FileStream(fileName, FileMode.Create);
			var writer = iTextSharp.text.pdf.PdfWriter.GetInstance(document, output);
			document.Open();

			iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(FormEditor.Form.GetBitmap(false), System.Drawing.Imaging.ImageFormat.Png);
			img.ScaleToFit(document.PageSize.Width - 20, document.PageSize.Height - 20);
			img.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
			document.Add(img);

			output.Flush();
			document.Close();
			writer.Close();

			// Open the file afterwards
			System.Diagnostics.Process.Start(fileName);
		}

		// -- Object Manipulation -----------------------------------------------------------------
		private void CenterXBtn_Click(object sender, EventArgs e)
		{
			FormEditor.CenterSelectedByX();
		}
		private void CenterYBtn_Click(object sender, EventArgs e)
		{
			FormEditor.CenterSelectedByY();
		}
		private void AlignXBtn_Click(object sender, EventArgs e)
		{
			FormEditor.AlignSelectedXs(PadXCheck.Checked, (int)PadXSpacing.Value);
		}
		private void AlignYBtn_Click(object sender, EventArgs e)
		{
			FormEditor.AlignSelectedYs(PadYCheck.Checked, (int)PadYSpacing.Value);
		}
		private void CopySelectedBtn_Click(object sender, EventArgs e)
		{
			FormEditor.CopySelectedObject();
		}
		private void propertyGrid_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
		{
			if (e.ChangedItem.Label == "Name")
			{
				IgnoreEvents = true;
				ObjectNameBox.Text = e.ChangedItem.Value.ToString();
				IgnoreEvents = false;
			}
			FormChanged();

			FormEditor.Invalidate();
		}
		private void ObjectNameBox_TextChanged(object sender, EventArgs e)
		{
			if (IgnoreEvents || FormEditor.SelectedObject == null)
				return;

			FormEditor.SelectedObject.Name = ObjectNameBox.Text;
			propertyGrid.Refresh();
			FormChanged();
		}
		private void ObjectGeneric_ValueChanged(object sender, EventArgs e)
		{
			if (FormEditor.SelectedObject == null || FormEditor.DraggingMode != DraggingModes.None || IgnoreEvents) return;

			FormEditor.SelectedObject.Position = new Point((int)ObjectPositionX.Value, (int)ObjectPositionY.Value);
			FormEditor.SelectedObject.Size = new Size((int)ObjectSizeWidth.Value, (int)ObjectSizeHeight.Value);

			//UpdateCanvas();
			FormChanged();
		}

		
	}
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ScanReader;
using ScanReader.ExamObjects;

namespace alpha.ExamCreation
{
	public enum MouseModes
	{
		Selection,      // Normal
		Placing,
		Moving
	}
	public enum DraggingModes
	{
		None,
		Moving,     // Moving Objects
		Resizing,   // Resizing Object by drag hangles
		Selecting   // Draw bounding rectangle for Group-Selection
	}
	public enum ResizeModes
	{
		None,
		TopLeft,
		TopRight,
		BottomLeft,
		BottomRight,
		MiddleLeft,
		MiddleRight,
		TopMiddle,
		BottomMiddle
	}
	public struct DraggingObject
	{
		public int ArrayIndex; // Where object is in the original array
		public Point DraggingStartPosition; // Where the object was when dragging was started
	}

	public partial class FormEditBox : Control
	{
		// -- Constants ---------------------------------------------------------------------------
		private const int MinimumObjectSize = 4;

		// -- Main Property -----------------------------------------------------------------------------
		public ExamForm Form { get; set; }

		// -- Selection ---------------------------------------------------------------------------------
		public List<ExamObject> SelectedObjects;
		public List<Rectangle> ResizeHandles;
		public ExamObject SelectedObject { get { return SelectedObjects.FirstOrDefault(); } }
		public bool SelectedMultiple { get { return SelectedObjects.Count > 1; } }

		// -- Mouse Work --------------------------------------------------------------------------------		
		public MouseModes MouseMode                                 // What user is doing with Mouse
		{
			get { return mousemode; }
			set
			{
				mousemode = value;
				OnMouseModeChanged?.Invoke();
			}
		}
		public ExamTypes PlacingMode { get; set; }					// What user is doing with Mouse with context

		// -- Dragging ----------------------------------------------------------------------------------
		public Point MouseDraggingOrigin { get; private set; }      // Point where Dragging has Begun
		List<DraggingObject> DraggingObjects;						// Data needed for dragging
		Point DraggingSelectionEndPoint;							// Bounding Point we will use for group selection
		bool DraggingStartedWithShift;                              // True if shift was depressed when dragging started		
		public DraggingModes DraggingMode                           // What are we dragging
		{
			get { return draggingmode; }
			private set
			{
				draggingmode = value;
				OnDraggingModeChanged?.Invoke();
			}
		}		
		public ResizeModes ResizeMode                               // Which corner of the objet we are resizing
		{
			get { return resizemodes; }
			private set
			{
				resizemodes = value;
				OnResizeModeChanged?.Invoke();
			}
		}
		Rectangle ResizeStartingLocation;       // Position and Size of Object before we start resizing

		// -- Behind Values
		private DraggingModes draggingmode;
		private ResizeModes resizemodes;
		private MouseModes mousemode;

		// -- Events ------------------------------------------------------------------------------------
		public delegate void OnChangedEvent();
		public delegate void OnMousePositionChangedEvent(int x, int y);
		public delegate void OnSelectionRegionChangedEvent(int x1, int y1, int x2, int y2);
		public delegate void OnNewItemEvent(ExamObject o);
		public OnChangedEvent OnMouseModeChanged;				// Occurs when the Mouse Mode updates		
		public OnChangedEvent OnDraggingModeChanged;			// Same
		public OnChangedEvent OnResizeModeChanged;				// Same
		public OnChangedEvent OnFormChanged;					// Occurs when the form is changed in a way that would require saving
		public OnChangedEvent OnSelectionChanged;               // Occurs when the SelectedObjects array has been changed
		public OnChangedEvent OnSelectedObjectChanged;			// Occurs when we modify one of the SelectedObjects
		public OnNewItemEvent OnNewItem;                        // Occurs when a new items is added to the form
		public OnMousePositionChangedEvent OnMousePositionUpdate;   // Allows for monitoring of the mouse location
		public OnMousePositionChangedEvent OnDraggingUpdate;        // Allows for monitoring of the dragging magnitude
		public OnSelectionRegionChangedEvent OnSelectionAreaUpdate; // Allows for monitoring of the selection bounding area
		public OnMousePositionChangedEvent OnResizeDragUpdate;		// Allows for monitoring of resizing mid-drag

		// -- Constructor -------------------------------------------------------------------------------
		public FormEditBox()
		{
			InitializeComponent();
			this.DoubleBuffered = true;
			SelectedObjects		= new List<ExamObject>();
			DraggingObjects		= new List<DraggingObject>();
			ResizeHandles		= new List<Rectangle>();
			MouseMode			= MouseModes.Selection;
			DraggingMode		= DraggingModes.None;
			ResizeMode			= ResizeModes.None;

			this.Paint			+= FormEditBox_Paint;
			this.MouseDown		+= nMouseDown;
			this.MouseMove		+= nMouseMove;
			this.MouseUp		+= nMouseUp;
		}
		protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
		{
			if (keyData == Keys.Delete)
			{
				DeleteKeyPressed();
				return true;
			}
			else
				return base.ProcessCmdKey(ref msg, keyData);
		}

		// -- Painting ----------------------------------------------------------------------------------
		private void FormEditBox_Paint(object sender, PaintEventArgs e)
		{
			// Draw the border
			Rectangle borderRectangle = this.ClientRectangle;
			ControlPaint.DrawBorder3D(e.Graphics, borderRectangle,
				Border3DStyle.Flat);

			// Draw the form
			Form.Draw(e.Graphics);

			// Draw the Name and Score draggable / resizable areas
			DrawNameAndScoreAreas(e.Graphics);

			// Draw Selected Objects
			DrawSelectedObjects(e.Graphics);

			// Draw Selection Area
			DrawDraggingSelectionArea(e.Graphics);
		}
		private void DrawSelectedObjects(Graphics g)
		{
			if (SelectedObject == null || DraggingMode != DraggingModes.None) return;

			Pen pen = new Pen(Color.BlueViolet, 1.0f);
			// Draw each object
			foreach (ExamObject obj in SelectedObjects)
				g.DrawRectangle(pen, obj.Location);

			if (!SelectedMultiple)
			{
				// Draw 8 resize hangles
				CreateResizeHandlesForObject(SelectedObject);

				// Draw each handle
				g.FillRectangles(Brushes.BlueViolet, ResizeHandles.ToArray());
			}
		}
		private void DrawDraggingSelectionArea(Graphics g)
		{
			if (DraggingMode != DraggingModes.Selecting)
				return;

			Rectangle rect = GetRectangleFromTwoPoints(MouseDraggingOrigin, DraggingSelectionEndPoint);
			var brush = new SolidBrush(Color.FromArgb(40, Color.Gray));

			g.DrawRectangle(new Pen(Brushes.Gray, 2.0f), rect);
			g.FillRectangle(brush, rect);
		}
		private void CreateResizeHandlesForObject(ExamObject selectedObject)
		{
			ResizeHandles.Clear();

			int handleSize = 7; // pixels per handle square

			int lowX = SelectedObject.Location.X - (handleSize / 2),
				lowY = SelectedObject.Location.Y - (handleSize / 2),
				midX = lowX + (SelectedObject.Size.Width / 2),
				midY = lowY + (SelectedObject.Size.Height / 2),
				bigX = lowX + SelectedObject.Size.Width,
				bigY = lowY + SelectedObject.Size.Height;

			// 0 - Top Left
			ResizeHandles.Add(new Rectangle(lowX, lowY, handleSize, handleSize));

			// 1 - Top Middle
			ResizeHandles.Add(new Rectangle(midX, lowY, handleSize, handleSize));

			// 2 - Top Right
			ResizeHandles.Add(new Rectangle(bigX, lowY, handleSize, handleSize));

			// 3 - Middle Left
			ResizeHandles.Add(new Rectangle(bigX, midY, handleSize, handleSize));

			// 4 - Middle Right
			ResizeHandles.Add(new Rectangle(lowX, midY, handleSize, handleSize));

			// 5 - Bottom Left
			ResizeHandles.Add(new Rectangle(lowX, bigY, handleSize, handleSize));

			// 6 - Bottom Middle
			ResizeHandles.Add(new Rectangle(midX, bigY, handleSize, handleSize));

			// 7 - Bottom Right
			ResizeHandles.Add(new Rectangle(bigX, bigY, handleSize, handleSize));
		}
		private void DrawNameAndScoreAreas(Graphics g)
		{
			if (Form.PreviewMarkedUp)
			{
				if (Form.MarkupEngine.PrintStudentName)
					g.DrawRectangle(new Pen(Color.CornflowerBlue, 2.0f), Form.MarkupEngine.StudentNameLocation);
				if (Form.MarkupEngine.PrintStudentScore)
					g.DrawRectangle(new Pen(Color.DarkKhaki, 2.0f), Form.MarkupEngine.ScoreLocation);
			}
		}

		// -- Mouse Mode --------------------------------------------------------------------------------
		private void UpdateCanvasCursor()
		{
			switch (MouseMode)
			{
				case MouseModes.Selection:
					Cursor = Cursors.Default;
					break;
				case MouseModes.Placing:
					Cursor = Cursors.UpArrow;
					break;
				case MouseModes.Moving:
					Cursor = Cursors.SizeAll;
					break;
			}
		}

		// -- Moving / Dragging / Resizing --------------------------------------------------------
		private void nMouseDown(object sender, MouseEventArgs e)
		{
			

			// If we're placing something, then place and then exit
			if (MouseMode == MouseModes.Placing && !Form.PreviewMarkedUp)
			{
				PlaceObjectAtMouseLocation();
			}
			else
			{
				ExamObject obj = GetObjectAt(new Point(e.X, e.Y));

				// If we're not placing, then we're either
				//	- Clicking on an object to select it
				//	- Dragging to select multiple items				
				//	- Selecting a resize point on an already selected item			
				if ((MouseMode == MouseModes.Moving && e.Button == MouseButtons.Left) || // If we're in the moving mode, only allow moving
					e.Button == MouseButtons.Right)                                      // Or if we rclick (MovingMode Shortcut)					
				{
					// If we've clicked on nothing, then there is nothing to drag
					if (obj == null) return;

					// If we've clicked on something we haven't selected yet, select it first
					if (SelectedObject == null || !SelectedObjects.Contains(obj))
						SelectObject(obj);

					// Drag them
					if (!Form.PreviewMarkedUp || (Form.PreviewMarkedUp && obj is ExamMarkupRect))
					StartDraggingForMovement();
				}
				else
				{
					var rMode = GetResizeModesFromLocation(e.X, e.Y);
					if (!SelectedMultiple && SelectedObject != null &&  // Only 1 object selected
						rMode != ResizeModes.None)
					{
						DraggingMode = DraggingModes.Resizing;
						ResizeMode = rMode;
						StartDraggingForResizing();
					}
					else if (obj != null)  // Nothing currently and we've clicked on an Object
					{
						if (ModifierKeys == Keys.Shift)
							SelectObject(obj, true);        // (SelectMultiple) Add to Selection
						else
							SelectObject(obj, false);       // (SingleSelection) Change Selection
					}
					else if (obj == null && !Form.PreviewMarkedUp) // We've clicked nowhere, prepare to drag selection
					{
						StartDraggingForSelection();
					}
				}
			}

			// Update Mode Label
			//UpdateModeLabel();
		}
		private void nMouseMove(object sender, MouseEventArgs e)
		{
			// Update Cursor Position Label
			OnMousePositionUpdate?.Invoke(e.X, e.Y);

			if (DraggingMode == DraggingModes.Moving)
			{
				int DeltaX = MouseDraggingOrigin.X - e.X,
					DeltaY = MouseDraggingOrigin.Y - e.Y;

				// Update Label to show Dragging Magnitude
				OnDraggingUpdate?.Invoke(DeltaX, DeltaY);

				foreach (DraggingObject dObj in DraggingObjects)
				{
					int x = dObj.DraggingStartPosition.X - DeltaX;
					int y = dObj.DraggingStartPosition.Y - DeltaY;

					// Do not restrict object positions to Margins if they are the StudentName or Score positions - They live in the margins
					if (!Form.PreviewMarkedUp)
					{
						// Ceiling X
						if (x < ExamForm.Margin + ExamForm.SquareSize)
							x = ExamForm.Margin + ExamForm.SquareSize;
						else if (x + SelectedObjects[dObj.ArrayIndex].Size.Width > 600 - (ExamForm.Margin + ExamForm.SquareSize))
							x = 600 - SelectedObjects[dObj.ArrayIndex].Size.Width - (ExamForm.Margin + ExamForm.SquareSize);

						// Ceiling Y
						if (y < ExamForm.Margin + ExamForm.SquareSize)
							y = ExamForm.Margin + ExamForm.SquareSize;
						else if (y + SelectedObjects[dObj.ArrayIndex].Size.Height > 800 - (ExamForm.Margin + ExamForm.SquareSize))
							y = 800 - SelectedObjects[dObj.ArrayIndex].Size.Height - (ExamForm.Margin + ExamForm.SquareSize);
					}
					else
					{
						// Ceiling X
						if (x < 0)
							x = 0;
						else if (x + SelectedObjects[dObj.ArrayIndex].Size.Width > 600)
							x = 600 - SelectedObjects[dObj.ArrayIndex].Size.Width;

						// Ceiling Y
						if (y < 0)
							y = 0;
						else if (y + SelectedObjects[dObj.ArrayIndex].Size.Height > 800)
							y = 800 - SelectedObjects[dObj.ArrayIndex].Size.Height;
					}

					// Assign the new point to the 
					SelectedObjects[dObj.ArrayIndex].Position = new Point(x, y);

					// Special Case for Preview Markup
					if (Form.PreviewMarkedUp)
					{
						if (SelectedObjects[dObj.ArrayIndex].Name == "StudentNameLocation")
							Form.MarkupEngine.StudentNameLocation = new Rectangle(x, y, Form.MarkupEngine.StudentNameLocation.Width, Form.MarkupEngine.StudentNameLocation.Height);
						else if (SelectedObjects[dObj.ArrayIndex].Name == "StudentScoreLocation")
							Form.MarkupEngine.ScoreLocation = new Rectangle(x, y, Form.MarkupEngine.ScoreLocation.Width, Form.MarkupEngine.ScoreLocation.Height);
					}
				}

				// Redraw
				Invalidate();
			}
			else if (DraggingMode == DraggingModes.Selecting)
			{
				// Update dragging area
				int DeltaX = MouseDraggingOrigin.X - e.X,
					DeltaY = MouseDraggingOrigin.Y - e.Y;

				DraggingSelectionEndPoint.X = e.X;
				DraggingSelectionEndPoint.Y = e.Y;

				// Update Dragging Label				
				OnSelectionAreaUpdate?.Invoke(MouseDraggingOrigin.X, MouseDraggingOrigin.Y, e.X, e.Y);

				// Draw the area
				Invalidate();
			}
			else if (ResizeMode == ResizeModes.None)
			{
				// Change the mouse cursor if over a reisze handle
				switch (GetResizeModesFromLocation(e.X, e.Y))
				{
					case ResizeModes.TopLeft:
					case ResizeModes.BottomRight:
						Cursor = Cursors.SizeNWSE;
						break;
					case ResizeModes.TopRight:
					case ResizeModes.BottomLeft:
						Cursor = Cursors.SizeNESW;
						break;
					case ResizeModes.TopMiddle:
					case ResizeModes.BottomMiddle:
						Cursor = Cursors.SizeNS;
						break;
					case ResizeModes.MiddleLeft:
					case ResizeModes.MiddleRight:
						Cursor = Cursors.SizeWE;
						break;
					default:
						UpdateCanvasCursor();
						break;
				}
			}
			else if (ResizeMode != ResizeModes.None)
			{
				int minimum = ExamForm.Margin + ExamForm.SquareSize;
				int mouseX = Math.Min(Math.Max(minimum, e.X), this.Width - minimum),
					mouseY = Math.Min(Math.Max(minimum, e.Y), this.Height - minimum);

				int DeltaX = MouseDraggingOrigin.X - mouseX,
					DeltaY = MouseDraggingOrigin.Y - mouseY;

				// Dummy Rectangle
				Rectangle r = ResizeStartingLocation;
				int width = 0, height = 0, offsetX = 0, offsetY = 0;

				// Resize the dummy rectangle
				switch (ResizeMode)
				{
					case ResizeModes.TopLeft:
						width = r.Width + DeltaX;
						height = r.Height + DeltaY;
						offsetX = -DeltaX;
						offsetY = -DeltaY;
						break;
					case ResizeModes.TopRight:
						width = r.Width - DeltaX;
						height = r.Height + DeltaY;
						offsetY = -DeltaY;
						break;
					case ResizeModes.BottomLeft:
						width = r.Width + DeltaX;
						height = r.Height - DeltaY;
						offsetX = -DeltaX;
						break;
					case ResizeModes.BottomRight:
						width = r.Width - DeltaX;
						height = r.Height - DeltaY;
						break;
					case ResizeModes.MiddleLeft:
						width = r.Width + DeltaX;
						height = r.Height;
						offsetX = -DeltaX;
						break;
					case ResizeModes.MiddleRight:
						width = r.Width - DeltaX;
						height = r.Height;
						break;
					case ResizeModes.TopMiddle:
						width = r.Width;
						height = r.Height + DeltaY;
						offsetY = -DeltaY;
						break;
					case ResizeModes.BottomMiddle:
						width = r.Width;
						height = r.Height - DeltaY;
						break;
				}

				// Check bounds
				if (width > MinimumObjectSize)
				{
					r.Width = width;
					r.Offset(offsetX, 0);
				}
				else
				{
					r.Width = SelectedObject.Size.Width;
					r.Location = new Point(SelectedObject.Location.X, r.Location.Y);
				}
				if (height > MinimumObjectSize)
				{
					r.Height = height;
					r.Offset(0, offsetY);
				}
				else
				{
					r.Height = SelectedObject.Size.Height;
					r.Location = new Point(r.Location.X, SelectedObject.Location.Y);
				}

				// Update DraggingLabel
				int dx = ResizeStartingLocation.Size.Width - r.Size.Width;
				int dy = ResizeStartingLocation.Size.Height - r.Size.Height;
				OnResizeDragUpdate?.Invoke(dx, dy);

				// Assign the location
				SelectedObject.Position = r.Location;
				SelectedObject.Size = r.Size;

				// Special Case for Markup
				if (Form.PreviewMarkedUp)
				{
					if (SelectedObject.Name == "StudentNameLocation")
						Form.MarkupEngine.StudentNameLocation = new Rectangle(r.Location, r.Size);
					else if (SelectedObject.Name == "StudentScoreLocation")
						Form.MarkupEngine.ScoreLocation = new Rectangle(r.Location, r.Size);
				}

				// Draw the area
				Invalidate();
			}
		}
		private void nMouseUp(object sender, MouseEventArgs e)
		{
			if (DraggingMode == DraggingModes.Selecting)
			{
				if (DraggingSelectionEndPoint == Point.Empty)
				{
					//if (!SelectedMultiple)
					SelectObject(null, false);
				}
				else
				{
					// Get the objects in the selection area
					Rectangle DraggingSelectionArea = GetRectangleFromTwoPoints(MouseDraggingOrigin, DraggingSelectionEndPoint);
					var objs = GetObjectsIn(DraggingSelectionArea);
					SelectObjects(objs, DraggingStartedWithShift || (ModifierKeys == Keys.Shift));
				}
				DraggingSelectionEndPoint = Point.Empty;
			}
			else if (DraggingMode == DraggingModes.Resizing || DraggingMode == DraggingModes.Moving)
				OnFormChanged?.Invoke();

			// Always reset the Dragging mode
			DraggingMode = DraggingModes.None;
			// Reset the Resizing mode
			ResizeMode = ResizeModes.None;
			// Get rid of any old drawn areas
			Invalidate();
			// Update Mode Label
			OnMouseModeChanged?.Invoke();
			OnDraggingUpdate?.Invoke(0, 0);
			OnSelectionAreaUpdate?.Invoke(0, 0, 0, 0);
			OnResizeDragUpdate?.Invoke(0, 0);
		}

		// Helpers
		private void PlaceObjectAtMouseLocation()
		{
			// Mouse Down after an Insert Button was clicked
			ExamObject obj = null;
			switch (PlacingMode)
			{
				case ExamTypes.Text:
					obj = new ExamText();
					break;
				case ExamTypes.Line:
					obj = new ExamLine();
					break;
				case ExamTypes.Picture:
					obj = new ExamPicture();
					break;
				case ExamTypes.WrittenEntry:
					obj = new ExamWrittenEntry();
					break;
				case ExamTypes.QuestionEntry:
					obj = new ExamQuestion();
					break;
			}

			// If we're placing something
			if (obj != null)
			{
				obj.Name = GenerateGenericName(obj);
				obj.Position = GetMouseOnCanvasPosition();
				Form.Objects.Add(obj);

				SelectObject(obj);
				Invalidate();
				OnNewItem?.Invoke(obj);
			}
			// Always reset mouse mode
			MouseMode = MouseModes.Selection;
			OnFormChanged?.Invoke();
		}
		ResizeModes GetResizeModesFromLocation(int x, int y)
		{
			// Return true is mouse is currently over a resize handle
			if (ResizeHandles.Count < 8)
				return ResizeModes.None;

			// Top Left
			if (ResizeHandles[0].Contains(x, y)) return ResizeModes.TopLeft;
			if (ResizeHandles[1].Contains(x, y)) return ResizeModes.TopMiddle;
			if (ResizeHandles[2].Contains(x, y)) return ResizeModes.TopRight;
			if (ResizeHandles[3].Contains(x, y)) return ResizeModes.MiddleRight;
			if (ResizeHandles[4].Contains(x, y)) return ResizeModes.MiddleLeft;
			if (ResizeHandles[5].Contains(x, y)) return ResizeModes.BottomLeft;
			if (ResizeHandles[6].Contains(x, y)) return ResizeModes.BottomMiddle;
			if (ResizeHandles[7].Contains(x, y)) return ResizeModes.BottomRight;

			return ResizeModes.None;
		}
		private void StartDraggingForMovement()
		{
			DraggingObjects.Clear();

			MouseDraggingOrigin = GetMouseOnCanvasPosition();
			for (int i = 0; i < SelectedObjects.Count; i++)
			{
				DraggingObject dObj = new DraggingObject();
				dObj.ArrayIndex = i;
				dObj.DraggingStartPosition = SelectedObjects[i].Position;
				DraggingObjects.Add(dObj);
			}
			DraggingMode = DraggingModes.Moving;

			OnFormChanged?.Invoke();
		}
		private void StartDraggingForSelection()
		{
			MouseDraggingOrigin = GetMouseOnCanvasPosition();
			DraggingStartedWithShift = (ModifierKeys == Keys.Shift);
			DraggingMode = DraggingModes.Selecting;
		}
		private void StartDraggingForResizing()
		{
			ResizeStartingLocation = SelectedObject.Location;
			MouseDraggingOrigin = GetMouseOnCanvasPosition();
			OnFormChanged?.Invoke();
		}
		public static Rectangle GetRectangleFromTwoPoints(Point p1, Point p2)
		{
			int top = Math.Min(p1.Y, p2.Y);
			int bottom = Math.Max(p1.Y, p2.Y);
			int left = Math.Min(p1.X, p2.X);
			int right = Math.Max(p1.X, p2.X);

			Rectangle rect = Rectangle.FromLTRB(left, top, right, bottom);

			return rect;
		}
		private Point GetMouseOnCanvasPosition()
		{
			return this.PointToClient(MousePosition);
		}

		// -- Generate Object Names ---------------------------------------------------------------------
		private string GenerateGenericName(ExamObject obj)
		{
			string ret = string.Empty;

			if (obj is ExamText)
				ret = "Label";
			else if (obj is ExamLine)
				ret = "Line";
			else if (obj is ExamPicture)
				ret = "Picture";
			else if (obj is ExamWrittenEntry)
				ret = "Entry";
			else if (obj is ExamQuestion)
				ret = "Question";
			else
				ret = "Unknown";

			return (GenerateGenericName(ret));
		}
		private string GenerateGenericName(string name)
		{
			for (int i = 0; i < Form.Objects.Count + 1; ++i)
			{
				string tempName = string.Concat(name, i + 1);
				if ((from e in Form.Objects where e.Name == tempName select e).Count() == 0)
					return tempName;
			}

			return name;
		}

		// -- Selection ---------------------------------------------------------------------------------
		private ExamObject GetObjectAt(Point p)
		{
			if (Form.PreviewMarkedUp)
			{
				if (Form.StudentNameLocation.Location.Contains(p))
					return Form.StudentNameLocation;
				else if ((Form.StudentScoreLocation.Location.Contains(p)))
					return Form.StudentScoreLocation;
			}

			// return an object at a position or return null for no object found
			foreach (ExamObject obj in Form.Objects)
			{
				if (obj.Location.Contains(p) && !(obj is ExamMarkupRect))
					return obj;
			}
			return null;
		}
		private List<ExamObject> GetObjectsIn(Rectangle rect)
		{
			// Returns all objects that are inside the supplied rect
			List<ExamObject> objs = new List<ExamObject>();

			foreach (ExamObject obj in Form.Objects)
			{
				if (obj.Location.IntersectsWith(rect) && !(obj is ExamMarkupRect))
					objs.Add(obj);
			}
			return objs;
		}
		public void SelectObjects(List<ExamObject> objs, bool AddToSelection = false)
		{
			// If we are in Preview markup mode, don't select anything
			if (Form.PreviewMarkedUp && objs.Count == 1 && !(objs[0] is ExamMarkupRect))
				return;

			// Get rid of old resize handles
			ResizeHandles.Clear();

			// Load
			if (!AddToSelection)
				SelectedObjects.Clear();            // Don't clear selection if we're selecting multiple
			if (objs.Count > 0)
			{
				foreach (var obj in objs)
				{
					if (SelectedObjects.Contains(obj))        // Allow shift to remove from selection
						SelectedObjects.Remove(obj);
					else
						SelectedObjects.Add(obj);
				}
			}

			// Generate Event
			OnSelectionChanged?.Invoke();

			// Draw the selected item
			Invalidate();
		}
		public void SelectObject(ExamObject obj, bool AddToSelection = false)
		{
			List<ExamObject> lst = new List<ExamObject>();
			if (obj != null)
				lst.Add(obj);

			SelectObjects(lst, AddToSelection);
		}

		// -- Object Manipulation -----------------------------------------------------------------------
		private void DeleteKeyPressed()
		{
			if (MouseMode == MouseModes.Selection && SelectedObject != null && !(SelectedObject is ExamMarkupRect))
			{
				foreach (var obj in SelectedObjects)
					Form.Objects.Remove(obj);
				SelectedObjects.Clear();

				Invalidate();
				OnNewItem?.Invoke(null);
				OnSelectionChanged?.Invoke();
			}
		}

		public void CenterSelectedByX()
		{
			if (SelectedObject == null) return;

			foreach (var obj in SelectedObjects)
				obj.Position = new Point((this.Width / 2) - (obj.Size.Width / 2), obj.Position.Y);

			// Notify form was changed
			OnFormChanged?.Invoke();
			Invalidate();
		}
		public void CenterSelectedByY()
		{
			if (SelectedObject == null) return;

			foreach (var obj in SelectedObjects)
				obj.Position = new Point(obj.Position.X, (this.Height / 2) - (obj.Size.Height / 2));

			// Notify form was changed
			OnFormChanged?.Invoke();
			Invalidate();
		}
		public void AlignSelectedXs(bool WithSpacing, int Spacing = 0)
		{
			if (!SelectedMultiple)
				return;

			int x = SelectedObject.Location.X;
			int y = SelectedObject.Location.Y;

			foreach (ExamObject obj in SelectedObjects)
			{
				if (WithSpacing)
				{
					if (y + obj.Size.Height > this.Height)
					{
						// Offscreen, wrap controls
						y = SelectedObject.Location.Y;  // Reset y to first y
						x += obj.Size.Width + Spacing;   // Move x over
					}

					obj.Position = new Point(x, y);
					y += obj.Size.Height + Spacing;
				}
				else
					obj.Position = new Point(x, obj.Position.Y);
			}

			// Notify form was changed
			OnFormChanged?.Invoke();
			Invalidate();
		}
		public void AlignSelectedYs(bool WithSpacing, int Spacing = 0)
		{
			if (!SelectedMultiple)
				return;

			int x = SelectedObject.Location.X;
			int y = SelectedObject.Location.Y;

			foreach (ExamObject obj in SelectedObjects)
			{
				if (WithSpacing)
				{
					if (x + obj.Size.Width > this.Width)
					{
						// Offscreen, wrap controls
						x = SelectedObject.Location.X;  // Reset x to first x
						y += obj.Size.Height + Spacing;   // Move y over
					}

					obj.Position = new Point(x, y);
					x += obj.Size.Width + Spacing;
				}
				else
					obj.Position = new Point(obj.Position.X, y);
			}

			// Notify form was changed
			OnFormChanged?.Invoke();
			Invalidate();
		}
		public void CopySelectedObject()
		{
			if (SelectedObject == null) return;

			ExamObject obj = null;
			if (SelectedObject is ExamLine)
				obj = new ExamLine(SelectedObject as ExamLine);
			if (SelectedObject is ExamPicture)
				obj = new ExamPicture(SelectedObject as ExamPicture);
			if (SelectedObject is ExamQuestion)
				obj = new ExamQuestion(SelectedObject as ExamQuestion);
			if (SelectedObject is ExamText)
				obj = new ExamText(SelectedObject as ExamText);
			if (SelectedObject is ExamWrittenEntry)
				obj = new ExamWrittenEntry(SelectedObject as ExamWrittenEntry);

			if (obj != null)
			{
				obj.Name = GenerateGenericName(SelectedObject.Name + "_COPY");
				obj.Position = new Point(SelectedObject.Position.X + obj.Size.Width, SelectedObject.Position.Y + obj.Size.Height);
				Form.Objects.Add(obj);

				SelectObject(obj);
				Invalidate();
				OnNewItem?.Invoke(obj);
				OnFormChanged?.Invoke();
			}
		}


	}
}

﻿namespace alpha.ExamCreation
{
    partial class DesignExamForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DesignExamForm));
			this.panel1 = new System.Windows.Forms.Panel();
			this.PropertiesPanel = new System.Windows.Forms.GroupBox();
			this.propertyGrid = new System.Windows.Forms.PropertyGrid();
			this.PositioningPanel = new System.Windows.Forms.GroupBox();
			this.ObjectNameBox = new System.Windows.Forms.TextBox();
			this.label35 = new System.Windows.Forms.Label();
			this.CenterYBtn = new System.Windows.Forms.Button();
			this.CenterXBtn = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.ObjectSizeHeight = new System.Windows.Forms.NumericUpDown();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.ObjectSizeWidth = new System.Windows.Forms.NumericUpDown();
			this.ObjectPositionY = new System.Windows.Forms.NumericUpDown();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.ObjectPositionX = new System.Windows.Forms.NumericUpDown();
			this.GroupPanel = new System.Windows.Forms.GroupBox();
			this.PadXSpacing = new System.Windows.Forms.NumericUpDown();
			this.PadXCheck = new System.Windows.Forms.CheckBox();
			this.AlignYBtn = new System.Windows.Forms.Button();
			this.PadYSpacing = new System.Windows.Forms.NumericUpDown();
			this.PadYCheck = new System.Windows.Forms.CheckBox();
			this.AlignXBtn = new System.Windows.Forms.Button();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.InsertQuestionBtn = new System.Windows.Forms.Button();
			this.InsertStudentEntryBtn = new System.Windows.Forms.Button();
			this.InsertPictureBtn = new System.Windows.Forms.Button();
			this.InsertLineBtn = new System.Windows.Forms.Button();
			this.InsertLabelBtn = new System.Windows.Forms.Button();
			this.CopySelectedBtn = new System.Windows.Forms.Button();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.SelectByNameBox = new System.Windows.Forms.ComboBox();
			this.label31 = new System.Windows.Forms.Label();
			this.SelectionModeBtn = new System.Windows.Forms.Button();
			this.MarkupEditModeBtn = new System.Windows.Forms.Button();
			this.colorDialog = new System.Windows.Forms.ColorDialog();
			this.fontDialog = new System.Windows.Forms.FontDialog();
			this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
			this.toolStrip1 = new System.Windows.Forms.ToolStrip();
			this.NewExamBtn = new System.Windows.Forms.ToolStripButton();
			this.OpenExamBtn = new System.Windows.Forms.ToolStripButton();
			this.SaveExamBtn = new System.Windows.Forms.ToolStripButton();
			this.SaveAsBtn = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.PrintFormBtn = new System.Windows.Forms.ToolStripButton();
			this.ExportToImageBtn = new System.Windows.Forms.ToolStripButton();
			this.examSaveDialog = new System.Windows.Forms.SaveFileDialog();
			this.examOpenDialog = new System.Windows.Forms.OpenFileDialog();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.ModeLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.CursorPositionLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.DraggingLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.panel1.SuspendLayout();
			this.PropertiesPanel.SuspendLayout();
			this.PositioningPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.ObjectSizeHeight)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ObjectSizeWidth)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ObjectPositionY)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ObjectPositionX)).BeginInit();
			this.GroupPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.PadXSpacing)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.PadYSpacing)).BeginInit();
			this.groupBox3.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.toolStrip1.SuspendLayout();
			this.statusStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.panel1.Controls.Add(this.PropertiesPanel);
			this.panel1.Controls.Add(this.PositioningPanel);
			this.panel1.Controls.Add(this.GroupPanel);
			this.panel1.Controls.Add(this.groupBox3);
			this.panel1.Controls.Add(this.groupBox2);
			this.panel1.Location = new System.Drawing.Point(622, 25);
			this.panel1.Name = "panel1";
			this.panel1.Padding = new System.Windows.Forms.Padding(0, 0, 4, 0);
			this.panel1.Size = new System.Drawing.Size(326, 831);
			this.panel1.TabIndex = 3;
			// 
			// PropertiesPanel
			// 
			this.PropertiesPanel.Controls.Add(this.propertyGrid);
			this.PropertiesPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.PropertiesPanel.Location = new System.Drawing.Point(0, 325);
			this.PropertiesPanel.Name = "PropertiesPanel";
			this.PropertiesPanel.Size = new System.Drawing.Size(322, 506);
			this.PropertiesPanel.TabIndex = 13;
			this.PropertiesPanel.TabStop = false;
			this.PropertiesPanel.Text = "Properties";
			// 
			// propertyGrid
			// 
			this.propertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.propertyGrid.Location = new System.Drawing.Point(3, 16);
			this.propertyGrid.Name = "propertyGrid";
			this.propertyGrid.Size = new System.Drawing.Size(316, 487);
			this.propertyGrid.TabIndex = 6;
			this.propertyGrid.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this.propertyGrid_PropertyValueChanged);
			// 
			// PositioningPanel
			// 
			this.PositioningPanel.Controls.Add(this.ObjectNameBox);
			this.PositioningPanel.Controls.Add(this.label35);
			this.PositioningPanel.Controls.Add(this.CenterYBtn);
			this.PositioningPanel.Controls.Add(this.CenterXBtn);
			this.PositioningPanel.Controls.Add(this.label6);
			this.PositioningPanel.Controls.Add(this.label5);
			this.PositioningPanel.Controls.Add(this.ObjectSizeHeight);
			this.PositioningPanel.Controls.Add(this.label3);
			this.PositioningPanel.Controls.Add(this.label4);
			this.PositioningPanel.Controls.Add(this.ObjectSizeWidth);
			this.PositioningPanel.Controls.Add(this.ObjectPositionY);
			this.PositioningPanel.Controls.Add(this.label2);
			this.PositioningPanel.Controls.Add(this.label1);
			this.PositioningPanel.Controls.Add(this.ObjectPositionX);
			this.PositioningPanel.Dock = System.Windows.Forms.DockStyle.Top;
			this.PositioningPanel.Location = new System.Drawing.Point(0, 241);
			this.PositioningPanel.Name = "PositioningPanel";
			this.PositioningPanel.Size = new System.Drawing.Size(322, 84);
			this.PositioningPanel.TabIndex = 12;
			this.PositioningPanel.TabStop = false;
			this.PositioningPanel.Text = "General";
			// 
			// ObjectNameBox
			// 
			this.ObjectNameBox.Location = new System.Drawing.Point(61, 13);
			this.ObjectNameBox.Name = "ObjectNameBox";
			this.ObjectNameBox.Size = new System.Drawing.Size(257, 20);
			this.ObjectNameBox.TabIndex = 23;
			this.ObjectNameBox.TextChanged += new System.EventHandler(this.ObjectNameBox_TextChanged);
			// 
			// label35
			// 
			this.label35.AutoSize = true;
			this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label35.Location = new System.Drawing.Point(6, 16);
			this.label35.Name = "label35";
			this.label35.Size = new System.Drawing.Size(43, 13);
			this.label35.TabIndex = 22;
			this.label35.Text = "Name:";
			// 
			// CenterYBtn
			// 
			this.CenterYBtn.Location = new System.Drawing.Point(231, 57);
			this.CenterYBtn.Name = "CenterYBtn";
			this.CenterYBtn.Size = new System.Drawing.Size(88, 23);
			this.CenterYBtn.TabIndex = 21;
			this.CenterYBtn.Text = "Center Y";
			this.CenterYBtn.UseVisualStyleBackColor = true;
			this.CenterYBtn.Click += new System.EventHandler(this.CenterYBtn_Click);
			// 
			// CenterXBtn
			// 
			this.CenterXBtn.Location = new System.Drawing.Point(231, 35);
			this.CenterXBtn.Name = "CenterXBtn";
			this.CenterXBtn.Size = new System.Drawing.Size(88, 23);
			this.CenterXBtn.TabIndex = 20;
			this.CenterXBtn.Text = "Center X";
			this.CenterXBtn.UseVisualStyleBackColor = true;
			this.CenterXBtn.Click += new System.EventHandler(this.CenterXBtn_Click);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(56, 61);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(23, 13);
			this.label6.TabIndex = 19;
			this.label6.Text = "W:";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.Location = new System.Drawing.Point(59, 37);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(19, 13);
			this.label5.TabIndex = 18;
			this.label5.Text = "X:";
			this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// ObjectSizeHeight
			// 
			this.ObjectSizeHeight.Location = new System.Drawing.Point(168, 59);
			this.ObjectSizeHeight.Maximum = new decimal(new int[] {
            800,
            0,
            0,
            0});
			this.ObjectSizeHeight.Name = "ObjectSizeHeight";
			this.ObjectSizeHeight.Size = new System.Drawing.Size(57, 20);
			this.ObjectSizeHeight.TabIndex = 17;
			this.ObjectSizeHeight.ValueChanged += new System.EventHandler(this.ObjectGeneric_ValueChanged);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(145, 61);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(20, 13);
			this.label3.TabIndex = 16;
			this.label3.Text = "H:";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(6, 61);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(39, 13);
			this.label4.TabIndex = 15;
			this.label4.Text = "Size: ";
			// 
			// ObjectSizeWidth
			// 
			this.ObjectSizeWidth.Location = new System.Drawing.Point(82, 59);
			this.ObjectSizeWidth.Maximum = new decimal(new int[] {
            600,
            0,
            0,
            0});
			this.ObjectSizeWidth.Name = "ObjectSizeWidth";
			this.ObjectSizeWidth.Size = new System.Drawing.Size(57, 20);
			this.ObjectSizeWidth.TabIndex = 14;
			this.ObjectSizeWidth.ValueChanged += new System.EventHandler(this.ObjectGeneric_ValueChanged);
			// 
			// ObjectPositionY
			// 
			this.ObjectPositionY.Location = new System.Drawing.Point(168, 35);
			this.ObjectPositionY.Maximum = new decimal(new int[] {
            800,
            0,
            0,
            0});
			this.ObjectPositionY.Name = "ObjectPositionY";
			this.ObjectPositionY.Size = new System.Drawing.Size(57, 20);
			this.ObjectPositionY.TabIndex = 13;
			this.ObjectPositionY.ValueChanged += new System.EventHandler(this.ObjectGeneric_ValueChanged);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(145, 37);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(19, 13);
			this.label2.TabIndex = 12;
			this.label2.Text = "Y:";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(6, 37);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(56, 13);
			this.label1.TabIndex = 11;
			this.label1.Text = "Position:";
			// 
			// ObjectPositionX
			// 
			this.ObjectPositionX.Location = new System.Drawing.Point(82, 35);
			this.ObjectPositionX.Maximum = new decimal(new int[] {
            600,
            0,
            0,
            0});
			this.ObjectPositionX.Name = "ObjectPositionX";
			this.ObjectPositionX.Size = new System.Drawing.Size(57, 20);
			this.ObjectPositionX.TabIndex = 10;
			this.ObjectPositionX.ValueChanged += new System.EventHandler(this.ObjectGeneric_ValueChanged);
			// 
			// GroupPanel
			// 
			this.GroupPanel.Controls.Add(this.PadXSpacing);
			this.GroupPanel.Controls.Add(this.PadXCheck);
			this.GroupPanel.Controls.Add(this.AlignYBtn);
			this.GroupPanel.Controls.Add(this.PadYSpacing);
			this.GroupPanel.Controls.Add(this.PadYCheck);
			this.GroupPanel.Controls.Add(this.AlignXBtn);
			this.GroupPanel.Dock = System.Windows.Forms.DockStyle.Top;
			this.GroupPanel.Location = new System.Drawing.Point(0, 173);
			this.GroupPanel.Name = "GroupPanel";
			this.GroupPanel.Size = new System.Drawing.Size(322, 68);
			this.GroupPanel.TabIndex = 11;
			this.GroupPanel.TabStop = false;
			this.GroupPanel.Text = "Group Control";
			// 
			// PadXSpacing
			// 
			this.PadXSpacing.Location = new System.Drawing.Point(259, 44);
			this.PadXSpacing.Name = "PadXSpacing";
			this.PadXSpacing.Size = new System.Drawing.Size(59, 20);
			this.PadXSpacing.TabIndex = 21;
			this.PadXSpacing.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
			// 
			// PadXCheck
			// 
			this.PadXCheck.AutoSize = true;
			this.PadXCheck.Checked = true;
			this.PadXCheck.CheckState = System.Windows.Forms.CheckState.Checked;
			this.PadXCheck.Location = new System.Drawing.Point(110, 46);
			this.PadXCheck.Name = "PadXCheck";
			this.PadXCheck.Size = new System.Drawing.Size(147, 17);
			this.PadXCheck.TabIndex = 20;
			this.PadXCheck.Text = "And Pad X With Spacing:";
			this.PadXCheck.UseVisualStyleBackColor = true;
			// 
			// AlignYBtn
			// 
			this.AlignYBtn.Location = new System.Drawing.Point(3, 42);
			this.AlignYBtn.Name = "AlignYBtn";
			this.AlignYBtn.Size = new System.Drawing.Size(101, 23);
			this.AlignYBtn.TabIndex = 19;
			this.AlignYBtn.Text = "Align Y";
			this.AlignYBtn.UseVisualStyleBackColor = true;
			this.AlignYBtn.Click += new System.EventHandler(this.AlignYBtn_Click);
			// 
			// PadYSpacing
			// 
			this.PadYSpacing.Location = new System.Drawing.Point(259, 21);
			this.PadYSpacing.Name = "PadYSpacing";
			this.PadYSpacing.Size = new System.Drawing.Size(59, 20);
			this.PadYSpacing.TabIndex = 18;
			this.PadYSpacing.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
			// 
			// PadYCheck
			// 
			this.PadYCheck.AutoSize = true;
			this.PadYCheck.Checked = true;
			this.PadYCheck.CheckState = System.Windows.Forms.CheckState.Checked;
			this.PadYCheck.Location = new System.Drawing.Point(110, 23);
			this.PadYCheck.Name = "PadYCheck";
			this.PadYCheck.Size = new System.Drawing.Size(147, 17);
			this.PadYCheck.TabIndex = 16;
			this.PadYCheck.Text = "And Pad Y With Spacing:";
			this.PadYCheck.UseVisualStyleBackColor = true;
			// 
			// AlignXBtn
			// 
			this.AlignXBtn.Location = new System.Drawing.Point(3, 19);
			this.AlignXBtn.Name = "AlignXBtn";
			this.AlignXBtn.Size = new System.Drawing.Size(101, 23);
			this.AlignXBtn.TabIndex = 15;
			this.AlignXBtn.Text = "Align X";
			this.AlignXBtn.UseVisualStyleBackColor = true;
			this.AlignXBtn.Click += new System.EventHandler(this.AlignXBtn_Click);
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.InsertQuestionBtn);
			this.groupBox3.Controls.Add(this.InsertStudentEntryBtn);
			this.groupBox3.Controls.Add(this.InsertPictureBtn);
			this.groupBox3.Controls.Add(this.InsertLineBtn);
			this.groupBox3.Controls.Add(this.InsertLabelBtn);
			this.groupBox3.Controls.Add(this.CopySelectedBtn);
			this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
			this.groupBox3.Location = new System.Drawing.Point(0, 62);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(322, 111);
			this.groupBox3.TabIndex = 10;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Insert";
			// 
			// InsertQuestionBtn
			// 
			this.InsertQuestionBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.InsertQuestionBtn.Location = new System.Drawing.Point(3, 61);
			this.InsertQuestionBtn.Name = "InsertQuestionBtn";
			this.InsertQuestionBtn.Size = new System.Drawing.Size(316, 23);
			this.InsertQuestionBtn.TabIndex = 12;
			this.InsertQuestionBtn.Text = "Insert Question / Bubble Field";
			this.InsertQuestionBtn.UseVisualStyleBackColor = true;
			this.InsertQuestionBtn.Click += new System.EventHandler(this.InsertQuestionEntryBtn_Click);
			// 
			// InsertStudentEntryBtn
			// 
			this.InsertStudentEntryBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.InsertStudentEntryBtn.Location = new System.Drawing.Point(161, 37);
			this.InsertStudentEntryBtn.Name = "InsertStudentEntryBtn";
			this.InsertStudentEntryBtn.Size = new System.Drawing.Size(158, 23);
			this.InsertStudentEntryBtn.TabIndex = 11;
			this.InsertStudentEntryBtn.Text = "Insert Student Entry Field";
			this.InsertStudentEntryBtn.UseVisualStyleBackColor = true;
			this.InsertStudentEntryBtn.Click += new System.EventHandler(this.InsertWrittenEntryBtn_Click);
			// 
			// InsertPictureBtn
			// 
			this.InsertPictureBtn.Location = new System.Drawing.Point(3, 37);
			this.InsertPictureBtn.Name = "InsertPictureBtn";
			this.InsertPictureBtn.Size = new System.Drawing.Size(157, 23);
			this.InsertPictureBtn.TabIndex = 10;
			this.InsertPictureBtn.Text = "Insert Picture";
			this.InsertPictureBtn.UseVisualStyleBackColor = true;
			this.InsertPictureBtn.Click += new System.EventHandler(this.InsertPictureBtn_Click);
			// 
			// InsertLineBtn
			// 
			this.InsertLineBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.InsertLineBtn.Location = new System.Drawing.Point(161, 13);
			this.InsertLineBtn.Name = "InsertLineBtn";
			this.InsertLineBtn.Size = new System.Drawing.Size(158, 23);
			this.InsertLineBtn.TabIndex = 9;
			this.InsertLineBtn.Text = "Insert Line";
			this.InsertLineBtn.UseVisualStyleBackColor = true;
			this.InsertLineBtn.Click += new System.EventHandler(this.InsertLineBtn_Click);
			// 
			// InsertLabelBtn
			// 
			this.InsertLabelBtn.Location = new System.Drawing.Point(3, 13);
			this.InsertLabelBtn.Name = "InsertLabelBtn";
			this.InsertLabelBtn.Size = new System.Drawing.Size(157, 23);
			this.InsertLabelBtn.TabIndex = 8;
			this.InsertLabelBtn.Text = "Insert Label";
			this.InsertLabelBtn.UseVisualStyleBackColor = true;
			this.InsertLabelBtn.Click += new System.EventHandler(this.InsertLabelBtn_Click);
			// 
			// CopySelectedBtn
			// 
			this.CopySelectedBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.CopySelectedBtn.Location = new System.Drawing.Point(3, 85);
			this.CopySelectedBtn.Name = "CopySelectedBtn";
			this.CopySelectedBtn.Size = new System.Drawing.Size(316, 23);
			this.CopySelectedBtn.TabIndex = 14;
			this.CopySelectedBtn.Text = "(Copy Selected)";
			this.CopySelectedBtn.UseVisualStyleBackColor = true;
			this.CopySelectedBtn.Click += new System.EventHandler(this.CopySelectedBtn_Click);
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.SelectByNameBox);
			this.groupBox2.Controls.Add(this.label31);
			this.groupBox2.Controls.Add(this.SelectionModeBtn);
			this.groupBox2.Controls.Add(this.MarkupEditModeBtn);
			this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
			this.groupBox2.Location = new System.Drawing.Point(0, 0);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(322, 62);
			this.groupBox2.TabIndex = 9;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Select";
			// 
			// SelectByNameBox
			// 
			this.SelectByNameBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.SelectByNameBox.FormattingEnabled = true;
			this.SelectByNameBox.Location = new System.Drawing.Point(58, 13);
			this.SelectByNameBox.Name = "SelectByNameBox";
			this.SelectByNameBox.Size = new System.Drawing.Size(261, 21);
			this.SelectByNameBox.TabIndex = 14;
			this.SelectByNameBox.SelectedValueChanged += new System.EventHandler(this.SelectByNameBox_SelectionChangeCommitted);
			this.SelectByNameBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SelectByNameBox_KeyDown);
			// 
			// label31
			// 
			this.label31.AutoSize = true;
			this.label31.Dock = System.Windows.Forms.DockStyle.Left;
			this.label31.Location = new System.Drawing.Point(3, 16);
			this.label31.Name = "label31";
			this.label31.Size = new System.Drawing.Size(46, 13);
			this.label31.TabIndex = 13;
			this.label31.Text = "Objects:";
			// 
			// SelectionModeBtn
			// 
			this.SelectionModeBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.SelectionModeBtn.Location = new System.Drawing.Point(161, 35);
			this.SelectionModeBtn.Name = "SelectionModeBtn";
			this.SelectionModeBtn.Size = new System.Drawing.Size(158, 23);
			this.SelectionModeBtn.TabIndex = 12;
			this.SelectionModeBtn.Text = "Select Object (or LClick)";
			this.SelectionModeBtn.UseVisualStyleBackColor = true;
			this.SelectionModeBtn.Click += new System.EventHandler(this.SelectionModeBtn_Click);
			// 
			// MarkupEditModeBtn
			// 
			this.MarkupEditModeBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.MarkupEditModeBtn.Location = new System.Drawing.Point(3, 35);
			this.MarkupEditModeBtn.Name = "MarkupEditModeBtn";
			this.MarkupEditModeBtn.Size = new System.Drawing.Size(157, 23);
			this.MarkupEditModeBtn.TabIndex = 11;
			this.MarkupEditModeBtn.Text = "Enter Markup Edit Mode";
			this.MarkupEditModeBtn.UseVisualStyleBackColor = true;
			this.MarkupEditModeBtn.Click += new System.EventHandler(this.MarkUpModeBtn_Click);
			// 
			// openFileDialog
			// 
			this.openFileDialog.Filter = "All Image Formats|*.jpg;*.jpeg;*.png;*.bmp|All Files(*.*)|*.*";
			this.openFileDialog.Title = "Select Picture to Display";
			// 
			// toolStrip1
			// 
			this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NewExamBtn,
            this.OpenExamBtn,
            this.SaveExamBtn,
            this.SaveAsBtn,
            this.toolStripSeparator1,
            this.PrintFormBtn,
            this.ExportToImageBtn});
			this.toolStrip1.Location = new System.Drawing.Point(0, 0);
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.Size = new System.Drawing.Size(948, 25);
			this.toolStrip1.TabIndex = 5;
			this.toolStrip1.Text = "File";
			// 
			// NewExamBtn
			// 
			this.NewExamBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.NewExamBtn.Image = global::alpha.Properties.Resources.page;
			this.NewExamBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.NewExamBtn.Name = "NewExamBtn";
			this.NewExamBtn.Size = new System.Drawing.Size(23, 22);
			this.NewExamBtn.Text = "New Form";
			this.NewExamBtn.ToolTipText = "Create a New Form for Editing";
			this.NewExamBtn.Click += new System.EventHandler(this.NewExamBtn_Click);
			// 
			// OpenExamBtn
			// 
			this.OpenExamBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.OpenExamBtn.Image = global::alpha.Properties.Resources.folder;
			this.OpenExamBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.OpenExamBtn.Name = "OpenExamBtn";
			this.OpenExamBtn.Size = new System.Drawing.Size(23, 22);
			this.OpenExamBtn.Text = "Open Form";
			this.OpenExamBtn.ToolTipText = "Open an Existing Form...";
			this.OpenExamBtn.Click += new System.EventHandler(this.OpenExamBtn_Click);
			// 
			// SaveExamBtn
			// 
			this.SaveExamBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.SaveExamBtn.Image = global::alpha.Properties.Resources.disk;
			this.SaveExamBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.SaveExamBtn.Name = "SaveExamBtn";
			this.SaveExamBtn.Size = new System.Drawing.Size(23, 22);
			this.SaveExamBtn.Text = "Save Form";
			this.SaveExamBtn.ToolTipText = "Save the current Form";
			this.SaveExamBtn.Click += new System.EventHandler(this.SaveExamBtn_Click);
			// 
			// SaveAsBtn
			// 
			this.SaveAsBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.SaveAsBtn.Image = global::alpha.Properties.Resources.disk_multiple;
			this.SaveAsBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.SaveAsBtn.Name = "SaveAsBtn";
			this.SaveAsBtn.Size = new System.Drawing.Size(23, 22);
			this.SaveAsBtn.Text = "Save As...";
			this.SaveAsBtn.ToolTipText = "Save current Form to a new file...";
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
			// 
			// PrintFormBtn
			// 
			this.PrintFormBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.PrintFormBtn.Image = global::alpha.Properties.Resources.printer;
			this.PrintFormBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.PrintFormBtn.Name = "PrintFormBtn";
			this.PrintFormBtn.Size = new System.Drawing.Size(23, 22);
			this.PrintFormBtn.Text = "Print Form";
			this.PrintFormBtn.Click += new System.EventHandler(this.PrintFormBtn_Click);
			// 
			// ExportToImageBtn
			// 
			this.ExportToImageBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.ExportToImageBtn.Image = global::alpha.Properties.Resources.picture_go;
			this.ExportToImageBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.ExportToImageBtn.Name = "ExportToImageBtn";
			this.ExportToImageBtn.Size = new System.Drawing.Size(23, 22);
			this.ExportToImageBtn.Text = "Export As Image";
			this.ExportToImageBtn.Click += new System.EventHandler(this.ExportToImageBtn_Click);
			// 
			// examSaveDialog
			// 
			this.examSaveDialog.DefaultExt = "efm";
			this.examSaveDialog.Filter = "Exam Forms (*.efm)|*.efm|All Files (*.*)|*.*";
			this.examSaveDialog.Title = "Where to Save, WHERE TO SAVE!";
			// 
			// examOpenDialog
			// 
			this.examOpenDialog.FileName = "openFileDialog1";
			this.examOpenDialog.Filter = "Exam Forms (*.efm)|*.efm|All Files (*.*)|*.*";
			this.examOpenDialog.Title = "Select Form to Open...";
			// 
			// statusStrip1
			// 
			this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ModeLabel,
            this.CursorPositionLabel,
            this.DraggingLabel,
            this.toolStripStatusLabel1});
			this.statusStrip1.Location = new System.Drawing.Point(0, 832);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
			this.statusStrip1.Size = new System.Drawing.Size(948, 24);
			this.statusStrip1.TabIndex = 6;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// ModeLabel
			// 
			this.ModeLabel.AutoSize = false;
			this.ModeLabel.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
			this.ModeLabel.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
			this.ModeLabel.Image = global::alpha.Properties.Resources.shape_handles;
			this.ModeLabel.Name = "ModeLabel";
			this.ModeLabel.Size = new System.Drawing.Size(100, 19);
			this.ModeLabel.Text = "Mode:";
			this.ModeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// CursorPositionLabel
			// 
			this.CursorPositionLabel.AutoSize = false;
			this.CursorPositionLabel.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
			this.CursorPositionLabel.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
			this.CursorPositionLabel.Image = global::alpha.Properties.Resources.cursor;
			this.CursorPositionLabel.Name = "CursorPositionLabel";
			this.CursorPositionLabel.Size = new System.Drawing.Size(125, 19);
			this.CursorPositionLabel.Text = "Cursor (900, 900)";
			// 
			// DraggingLabel
			// 
			this.DraggingLabel.AutoSize = false;
			this.DraggingLabel.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
			this.DraggingLabel.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
			this.DraggingLabel.Name = "DraggingLabel";
			this.DraggingLabel.Size = new System.Drawing.Size(200, 19);
			// 
			// toolStripStatusLabel1
			// 
			this.toolStripStatusLabel1.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
			this.toolStripStatusLabel1.BorderStyle = System.Windows.Forms.Border3DStyle.Etched;
			this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
			this.toolStripStatusLabel1.Size = new System.Drawing.Size(508, 19);
			this.toolStripStatusLabel1.Spring = true;
			// 
			// DesignExamForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(948, 856);
			this.Controls.Add(this.statusStrip1);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.toolStrip1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MinimumSize = new System.Drawing.Size(800, 895);
			this.Name = "DesignExamForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "DesignExamForm";
			this.panel1.ResumeLayout(false);
			this.PropertiesPanel.ResumeLayout(false);
			this.PositioningPanel.ResumeLayout(false);
			this.PositioningPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.ObjectSizeHeight)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ObjectSizeWidth)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ObjectPositionY)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ObjectPositionX)).EndInit();
			this.GroupPanel.ResumeLayout(false);
			this.GroupPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.PadXSpacing)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.PadYSpacing)).EndInit();
			this.groupBox3.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.toolStrip1.ResumeLayout(false);
			this.toolStrip1.PerformLayout();
			this.statusStrip1.ResumeLayout(false);
			this.statusStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.FontDialog fontDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.GroupBox GroupPanel;
        private System.Windows.Forms.Button CopySelectedBtn;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button InsertQuestionBtn;
        private System.Windows.Forms.Button InsertStudentEntryBtn;
        private System.Windows.Forms.Button InsertPictureBtn;
        private System.Windows.Forms.Button InsertLineBtn;
        private System.Windows.Forms.Button InsertLabelBtn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox SelectByNameBox;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Button SelectionModeBtn;
        private System.Windows.Forms.Button MarkupEditModeBtn;
        private System.Windows.Forms.NumericUpDown PadYSpacing;
        private System.Windows.Forms.CheckBox PadYCheck;
        private System.Windows.Forms.Button AlignXBtn;
        private System.Windows.Forms.NumericUpDown PadXSpacing;
        private System.Windows.Forms.CheckBox PadXCheck;
        private System.Windows.Forms.Button AlignYBtn;
        private System.Windows.Forms.GroupBox PositioningPanel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown ObjectSizeHeight;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown ObjectSizeWidth;
        private System.Windows.Forms.NumericUpDown ObjectPositionY;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown ObjectPositionX;
        private System.Windows.Forms.Button CenterYBtn;
        private System.Windows.Forms.Button CenterXBtn;
        private System.Windows.Forms.GroupBox PropertiesPanel;
        private System.Windows.Forms.TextBox ObjectNameBox;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.ToolStripButton SaveExamBtn;
        private System.Windows.Forms.SaveFileDialog examSaveDialog;
        private System.Windows.Forms.ToolStripButton NewExamBtn;
        private System.Windows.Forms.ToolStripButton OpenExamBtn;
        private System.Windows.Forms.OpenFileDialog examOpenDialog;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton PrintFormBtn;
        private System.Windows.Forms.ToolStripButton ExportToImageBtn;
		private System.Windows.Forms.PropertyGrid propertyGrid;
		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.ToolStripStatusLabel CursorPositionLabel;
		private System.Windows.Forms.ToolStripStatusLabel DraggingLabel;
		private System.Windows.Forms.ToolStripStatusLabel ModeLabel;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.ToolStripButton SaveAsBtn;
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using alpha.Dialogs;

namespace alpha
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);

			// -- Clear the ToDoScans directory of left over scans
			if (!System.Diagnostics.Debugger.IsAttached)
				CleanUpToDoDirectory();

			// -- Set Exam Scan options
			ScanReader.Exam.MinimumConfidenceForExamAnswer = Properties.Settings.Default.MinimumConfidenceForExamAnswer;

			// -- Run the Application
			Application.Run(new ViewExamForm());
			
			// -- These are for Working on different forms 
			// -- Should always be commented on release
			//Application.Run(new ExamCreation.DesignExamForm());
			//Application.Run(new MainForm());
			//Application.Run(new Form1());

		}

		/// TODO Move to Static Database class 
		static void CleanUpToDoDirectory()
		{
			// Now, we should delete all files form the todoscans directory
			var files = System.IO.Directory.GetFiles(@"ToDoScans\");
			
			foreach (string file in files)
			{
				try
				{
					System.IO.File.Delete(file);
				}
				catch { /* Ignore Errors */ }
			}
		}
	}
}

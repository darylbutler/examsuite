﻿using ScanReader;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using alpha.Components;
using System.IO;
using System.Diagnostics;

namespace alpha
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();			
		}

		private void button2_Click(object sender, EventArgs e)
		{
			string file = @"debug\test4.pdf";

			List<Image> images = PdfImageExtractor.ReadPdf(file);

			if (images.Count > 0)
				ImgView.Image = images.First();
		}

		

	}
}

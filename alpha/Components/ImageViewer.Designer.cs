﻿namespace alpha.Components
{
    partial class ImageViewer
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.imageBox = new Cyotek.Windows.Forms.ImageBox();
			this.toolStrip1 = new System.Windows.Forms.ToolStrip();
			this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
			this.ModePanBtn = new System.Windows.Forms.ToolStripButton();
			this.ModeZoomBtn = new System.Windows.Forms.ToolStripButton();
			this.ModeSelectBtn = new System.Windows.Forms.ToolStripButton();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
			this.ZoomInBtn = new System.Windows.Forms.ToolStripButton();
			this.ZoomOutBtn = new System.Windows.Forms.ToolStripButton();
			this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
			this.ZoomLabel = new System.Windows.Forms.ToolStripLabel();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.ZoomDefaultBtn = new System.Windows.Forms.ToolStripButton();
			this.ZoomFitBtn = new System.Windows.Forms.ToolStripButton();
			this.toolStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// imageBox
			// 
			this.imageBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.imageBox.Location = new System.Drawing.Point(0, 27);
			this.imageBox.Margin = new System.Windows.Forms.Padding(4);
			this.imageBox.Name = "imageBox";
			this.imageBox.Size = new System.Drawing.Size(865, 844);
			this.imageBox.TabIndex = 0;
			// 
			// toolStrip1
			// 
			this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
			this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.ModePanBtn,
            this.ModeZoomBtn,
            this.ModeSelectBtn,
            this.toolStripSeparator1,
            this.toolStripLabel2,
            this.ZoomInBtn,
            this.ZoomOutBtn,
            this.toolStripLabel3,
            this.ZoomLabel,
            this.toolStripSeparator2,
            this.ZoomDefaultBtn,
            this.ZoomFitBtn});
			this.toolStrip1.Location = new System.Drawing.Point(0, 0);
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.Size = new System.Drawing.Size(865, 27);
			this.toolStrip1.TabIndex = 1;
			this.toolStrip1.Text = "toolStrip1";
			// 
			// toolStripLabel1
			// 
			this.toolStripLabel1.Name = "toolStripLabel1";
			this.toolStripLabel1.Size = new System.Drawing.Size(51, 24);
			this.toolStripLabel1.Text = "Mode:";
			// 
			// ModePanBtn
			// 
			this.ModePanBtn.Checked = true;
			this.ModePanBtn.CheckOnClick = true;
			this.ModePanBtn.CheckState = System.Windows.Forms.CheckState.Checked;
			this.ModePanBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.ModePanBtn.Image = global::alpha.Properties.Resources.cursor;
			this.ModePanBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.ModePanBtn.Name = "ModePanBtn";
			this.ModePanBtn.Size = new System.Drawing.Size(24, 24);
			this.ModePanBtn.Text = "Pan";
			this.ModePanBtn.ToolTipText = "Mouse Click to Pan";
			this.ModePanBtn.Click += new System.EventHandler(this.ModePanBtn_Click);
			// 
			// ModeZoomBtn
			// 
			this.ModeZoomBtn.CheckOnClick = true;
			this.ModeZoomBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.ModeZoomBtn.Image = global::alpha.Properties.Resources.zoom;
			this.ModeZoomBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.ModeZoomBtn.Name = "ModeZoomBtn";
			this.ModeZoomBtn.Size = new System.Drawing.Size(24, 24);
			this.ModeZoomBtn.Text = "Zoom";
			this.ModeZoomBtn.ToolTipText = "Click to Draw Region to Zoom To";
			this.ModeZoomBtn.Click += new System.EventHandler(this.ModeZoomBtn_Click);
			// 
			// ModeSelectBtn
			// 
			this.ModeSelectBtn.CheckOnClick = true;
			this.ModeSelectBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.ModeSelectBtn.Image = global::alpha.Properties.Resources.shape_handles;
			this.ModeSelectBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.ModeSelectBtn.Name = "ModeSelectBtn";
			this.ModeSelectBtn.Size = new System.Drawing.Size(24, 24);
			this.ModeSelectBtn.Text = "Select Area";
			this.ModeSelectBtn.ToolTipText = "Select an Area of the Image";
			this.ModeSelectBtn.Click += new System.EventHandler(this.ModeSelectBtn_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(6, 27);
			// 
			// toolStripLabel2
			// 
			this.toolStripLabel2.Name = "toolStripLabel2";
			this.toolStripLabel2.Size = new System.Drawing.Size(61, 24);
			this.toolStripLabel2.Text = "Actions:";
			// 
			// ZoomInBtn
			// 
			this.ZoomInBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.ZoomInBtn.Image = global::alpha.Properties.Resources.zoom_in;
			this.ZoomInBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.ZoomInBtn.Name = "ZoomInBtn";
			this.ZoomInBtn.Size = new System.Drawing.Size(24, 24);
			this.ZoomInBtn.Text = "Zoom In";
			this.ZoomInBtn.Click += new System.EventHandler(this.ZoomInBtn_Click);
			// 
			// ZoomOutBtn
			// 
			this.ZoomOutBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.ZoomOutBtn.Image = global::alpha.Properties.Resources.zoom_out;
			this.ZoomOutBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.ZoomOutBtn.Name = "ZoomOutBtn";
			this.ZoomOutBtn.Size = new System.Drawing.Size(24, 24);
			this.ZoomOutBtn.Text = "Zoom Out";
			this.ZoomOutBtn.Click += new System.EventHandler(this.ZoomOutBtn_Click);
			// 
			// toolStripLabel3
			// 
			this.toolStripLabel3.Name = "toolStripLabel3";
			this.toolStripLabel3.Size = new System.Drawing.Size(56, 24);
			this.toolStripLabel3.Text = "Zoom: ";
			// 
			// ZoomLabel
			// 
			this.ZoomLabel.AutoSize = false;
			this.ZoomLabel.Name = "ZoomLabel";
			this.ZoomLabel.Size = new System.Drawing.Size(50, 24);
			this.ZoomLabel.Text = "100%";
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(6, 27);
			// 
			// ZoomDefaultBtn
			// 
			this.ZoomDefaultBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.ZoomDefaultBtn.Image = global::alpha.Properties.Resources.picture_empty;
			this.ZoomDefaultBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.ZoomDefaultBtn.Name = "ZoomDefaultBtn";
			this.ZoomDefaultBtn.Size = new System.Drawing.Size(24, 24);
			this.ZoomDefaultBtn.Text = "Reset Zoom";
			this.ZoomDefaultBtn.ToolTipText = "Reset Zoom Level to Default";
			this.ZoomDefaultBtn.Click += new System.EventHandler(this.ZoomDefaultBtn_Click);
			// 
			// ZoomFitBtn
			// 
			this.ZoomFitBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.ZoomFitBtn.Image = global::alpha.Properties.Resources.arrow_out;
			this.ZoomFitBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.ZoomFitBtn.Name = "ZoomFitBtn";
			this.ZoomFitBtn.Size = new System.Drawing.Size(24, 24);
			this.ZoomFitBtn.Text = "Zoom to Fit";
			this.ZoomFitBtn.ToolTipText = "Adjust Zoom to Fit Image to View";
			this.ZoomFitBtn.Click += new System.EventHandler(this.ZoomFitBtn_Click);
			// 
			// ImageViewer
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.imageBox);
			this.Controls.Add(this.toolStrip1);
			this.Margin = new System.Windows.Forms.Padding(4);
			this.Name = "ImageViewer";
			this.Size = new System.Drawing.Size(865, 871);
			this.toolStrip1.ResumeLayout(false);
			this.toolStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private Cyotek.Windows.Forms.ImageBox imageBox;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripButton ModePanBtn;
        private System.Windows.Forms.ToolStripButton ModeZoomBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripButton ZoomInBtn;
        private System.Windows.Forms.ToolStripButton ZoomOutBtn;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripLabel ZoomLabel;
        private System.Windows.Forms.ToolStripButton ZoomDefaultBtn;
        private System.Windows.Forms.ToolStripButton ZoomFitBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton ModeSelectBtn;
    }
}

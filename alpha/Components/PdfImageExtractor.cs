﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp.text.pdf;

namespace alpha.Components
{
	class PdfImageExtractor
	{
		
		public static List<Image> ReadPdf(string filename)
		{
			List<Image> images = new List<Image>();
			PdfReader reader = new PdfReader(filename);

			for (int pageNumber = 1; pageNumber <= reader.NumberOfPages; pageNumber++)
			{
				PdfDictionary pg = reader.GetPageN(pageNumber);
				images.AddRange(GetImagesFromPdfDict(pg, reader));
			}


				/*

				if (obj != null && obj.IsStream())
				{
					PRStream stream = (PRStream)obj;

					bool IsXObject = stream.GetAsName(PdfName.TYPE)?.ToString() == "/XObject";
					bool IsSubTypeImage = stream.GetAsName(PdfName.SUBTYPE)?.ToString() == "/Image";

					if (IsXObject && IsSubTypeImage)
					{
						// Get the Filters
						PdfArray filters = stream.GetAsArray(PdfName.FILTER);
						bool isFlateDecode = false;

						if (filters != null)
							isFlateDecode = filters.Contains(PdfName.FLATEDECODE);

						// Grab the stream
						byte[] b;
						try
						{
							b = PdfReader.GetStreamBytes(stream);
						}
						catch (iTextSharp.text.exceptions.UnsupportedPdfException e)
						{
							b = PdfReader.GetStreamBytesRaw(stream);
						}							
						
						if (isFlateDecode)
						{
							b = PdfReader.FlateDecode(b);
						}
						iTextSharp.text.pdf.parser.PdfImageObject 
						MemoryStream ms = new MemoryStream(b);
						Image img = Image.FromStream(ms);

						if (img != null)
							images.Add(img);
					}
						int xrefIdx = ((PRIndirectReference)obj).Number;
						PdfObject pdfObj = reader.GetPdfObject(xrefIdx);
						PdfStream str = (PdfStream)(pdfObj);
						iTextSharp.text.pdf.parser.PdfImageObject pdfImage =
							new iTextSharp.text.pdf.parser.PdfImageObject((PRStream)str);
						images.Add(pdfImage.GetDrawingImage());
					}
				}
				
			}	
			*/
			return images;

		}
		private static IList<Image> GetImagesFromPdfDict(PdfDictionary page, PdfReader doc)
		{
			List<Image> images = new List<Image>();
			PdfDictionary res = (PdfDictionary)(PdfReader.GetPdfObject(page.Get(PdfName.RESOURCES)));
			PdfDictionary xobj = (PdfDictionary)(PdfReader.GetPdfObject(res.Get(PdfName.XOBJECT)));

			if (xobj != null)
			{
				foreach (PdfName name in xobj.Keys)
				{
					PdfObject obj = xobj.Get(name);
					if (obj.IsIndirect())
					{
						PdfDictionary tg = (PdfDictionary)(PdfReader.GetPdfObject(obj));
						PdfName subtype = (PdfName)(PdfReader.GetPdfObject(tg.Get(PdfName.SUBTYPE)));
						if (PdfName.IMAGE.Equals(subtype))
						{
							int xrefIdx = ((PRIndirectReference)obj).Number;
							PdfObject pdfObj = doc.GetPdfObject(xrefIdx);
							PdfStream str = (PdfStream)(pdfObj);

							iTextSharp.text.pdf.parser.PdfImageObject pdfImage =
								new iTextSharp.text.pdf.parser.PdfImageObject((PRStream)str);							
							Image img = pdfImage.GetDrawingImage();
							
							img.Save(string.Format("debug\\pdf_{0}.png", xrefIdx));

							images.Add(img);
						}
						else if (PdfName.FORM.Equals(subtype) || PdfName.GROUP.Equals(subtype))
						{
							images.AddRange(GetImagesFromPdfDict(tg, doc));
						}
					}
				}
			}

			return images;
		}
	}
}

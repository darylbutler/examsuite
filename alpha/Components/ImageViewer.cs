﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Cyotek.Windows.Forms;

namespace alpha.Components
{
    public partial class ImageViewer : UserControl
    {
        // Allow public access to ImageBox
        public ImageBox ImageBox { get { return imageBox; } }
        public Image Image { get { return imageBox.Image; } set { imageBox.Image = value; } }

        public ImageViewer()
        {
            InitializeComponent();

            imageBox.ZoomChanged += ImageBox_ZoomChanged;
        }

        private void ImageBox_ZoomChanged(object sender, EventArgs e)
        {
            ZoomLabel.Text = string.Format("{0}%", imageBox.Zoom);
        }

        // -- Mouse Click Mode --
        private void ModePanBtn_Click(object sender, EventArgs e)
        {
            ModeZoomBtn.CheckState = CheckState.Unchecked;
            ModeSelectBtn.CheckState = CheckState.Unchecked;

            imageBox.SelectionMode = ImageBoxSelectionMode.None;
        }
        private void ModeZoomBtn_Click(object sender, EventArgs e)
        {
            ModePanBtn.CheckState = CheckState.Unchecked;
            ModeSelectBtn.CheckState = CheckState.Unchecked;

            imageBox.SelectionMode = ImageBoxSelectionMode.Zoom;
        }
        private void ModeSelectBtn_Click(object sender, EventArgs e)
        {
            ModePanBtn.CheckState = CheckState.Unchecked;
            ModeZoomBtn.CheckState = CheckState.Unchecked;

            imageBox.SelectionMode = ImageBoxSelectionMode.Rectangle;
        }


        // -- Zoom
        private void ZoomInBtn_Click(object sender, EventArgs e)
        {
            imageBox.ZoomIn(true);
        }

        private void ZoomOutBtn_Click(object sender, EventArgs e)
        {
            imageBox.ZoomOut(true);
        }
        private void ZoomDefaultBtn_Click(object sender, EventArgs e)
        {
            imageBox.Zoom = 100;
        }
        private void ZoomFitBtn_Click(object sender, EventArgs e)
        {
            imageBox.ZoomToFit();
        }

        
    }
}

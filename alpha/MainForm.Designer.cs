﻿namespace alpha
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.groupBox6 = new System.Windows.Forms.GroupBox();
			this.EditStudentsBtn = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.groupBox5 = new System.Windows.Forms.GroupBox();
			this.ViewExamsBtn = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.ExtractFromPDFBtn = new System.Windows.Forms.Button();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.GradeMultipleBtn = new System.Windows.Forms.Button();
			this.DeleteBulkBtn = new System.Windows.Forms.Button();
			this.AddBulkBtn = new System.Windows.Forms.Button();
			this.GradeTheseListBox = new System.Windows.Forms.ListBox();
			this.GradeOneBtn = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.groupBox7 = new System.Windows.Forms.GroupBox();
			this.FormEditorBtn = new System.Windows.Forms.Button();
			this.label5 = new System.Windows.Forms.Label();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.NewExamBtn = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			this.groupBox6.SuspendLayout();
			this.groupBox5.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.groupBox4.SuspendLayout();
			this.groupBox7.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.groupBox6);
			this.groupBox1.Controls.Add(this.groupBox5);
			this.groupBox1.Controls.Add(this.groupBox3);
			this.groupBox1.Controls.Add(this.groupBox7);
			this.groupBox1.Controls.Add(this.groupBox2);
			this.groupBox1.Location = new System.Drawing.Point(0, 0);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(530, 704);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Step By Step:";
			// 
			// groupBox6
			// 
			this.groupBox6.Controls.Add(this.EditStudentsBtn);
			this.groupBox6.Controls.Add(this.label4);
			this.groupBox6.Dock = System.Windows.Forms.DockStyle.Top;
			this.groupBox6.Location = new System.Drawing.Point(3, 559);
			this.groupBox6.Name = "groupBox6";
			this.groupBox6.Size = new System.Drawing.Size(524, 121);
			this.groupBox6.TabIndex = 1;
			this.groupBox6.TabStop = false;
			this.groupBox6.Text = "Edit Preloaded Student List (optional) ";
			// 
			// EditStudentsBtn
			// 
			this.EditStudentsBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.EditStudentsBtn.Location = new System.Drawing.Point(6, 91);
			this.EditStudentsBtn.Name = "EditStudentsBtn";
			this.EditStudentsBtn.Size = new System.Drawing.Size(512, 23);
			this.EditStudentsBtn.TabIndex = 2;
			this.EditStudentsBtn.Text = "Edit Student List...";
			this.EditStudentsBtn.UseVisualStyleBackColor = true;
			this.EditStudentsBtn.Click += new System.EventHandler(this.EditStudentsBtn_Click);
			// 
			// label4
			// 
			this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label4.Location = new System.Drawing.Point(6, 16);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(512, 72);
			this.label4.TabIndex = 1;
			this.label4.Text = resources.GetString("label4.Text");
			// 
			// groupBox5
			// 
			this.groupBox5.Controls.Add(this.ViewExamsBtn);
			this.groupBox5.Controls.Add(this.label3);
			this.groupBox5.Dock = System.Windows.Forms.DockStyle.Top;
			this.groupBox5.Location = new System.Drawing.Point(3, 439);
			this.groupBox5.Name = "groupBox5";
			this.groupBox5.Size = new System.Drawing.Size(524, 120);
			this.groupBox5.TabIndex = 2;
			this.groupBox5.TabStop = false;
			this.groupBox5.Text = "View / Edit an Exam";
			// 
			// ViewExamsBtn
			// 
			this.ViewExamsBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ViewExamsBtn.Location = new System.Drawing.Point(6, 91);
			this.ViewExamsBtn.Name = "ViewExamsBtn";
			this.ViewExamsBtn.Size = new System.Drawing.Size(512, 23);
			this.ViewExamsBtn.TabIndex = 1;
			this.ViewExamsBtn.Text = "View Exam...";
			this.ViewExamsBtn.UseVisualStyleBackColor = true;
			this.ViewExamsBtn.Click += new System.EventHandler(this.ViewExamsBtn_Click);
			// 
			// label3
			// 
			this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.label3.Location = new System.Drawing.Point(6, 16);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(512, 72);
			this.label3.TabIndex = 0;
			this.label3.Text = resources.GetString("label3.Text");
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.ExtractFromPDFBtn);
			this.groupBox3.Controls.Add(this.groupBox4);
			this.groupBox3.Controls.Add(this.GradeOneBtn);
			this.groupBox3.Controls.Add(this.label2);
			this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
			this.groupBox3.Location = new System.Drawing.Point(3, 183);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(524, 256);
			this.groupBox3.TabIndex = 2;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Grade Scanned Tests";
			// 
			// ExtractFromPDFBtn
			// 
			this.ExtractFromPDFBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.ExtractFromPDFBtn.Location = new System.Drawing.Point(269, 221);
			this.ExtractFromPDFBtn.Name = "ExtractFromPDFBtn";
			this.ExtractFromPDFBtn.Size = new System.Drawing.Size(252, 23);
			this.ExtractFromPDFBtn.TabIndex = 3;
			this.ExtractFromPDFBtn.Text = "(or) Extract Images from PDF";
			this.ExtractFromPDFBtn.UseVisualStyleBackColor = true;
			this.ExtractFromPDFBtn.Click += new System.EventHandler(this.ExtractFromPDFBtn_Click);
			// 
			// groupBox4
			// 
			this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox4.Controls.Add(this.GradeMultipleBtn);
			this.groupBox4.Controls.Add(this.DeleteBulkBtn);
			this.groupBox4.Controls.Add(this.AddBulkBtn);
			this.groupBox4.Controls.Add(this.GradeTheseListBox);
			this.groupBox4.Location = new System.Drawing.Point(6, 65);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(512, 151);
			this.groupBox4.TabIndex = 2;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Bulk Add";
			// 
			// GradeMultipleBtn
			// 
			this.GradeMultipleBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.GradeMultipleBtn.Location = new System.Drawing.Point(6, 120);
			this.GradeMultipleBtn.Name = "GradeMultipleBtn";
			this.GradeMultipleBtn.Size = new System.Drawing.Size(500, 23);
			this.GradeMultipleBtn.TabIndex = 3;
			this.GradeMultipleBtn.Text = "Grade These...";
			this.GradeMultipleBtn.UseVisualStyleBackColor = true;
			this.GradeMultipleBtn.Click += new System.EventHandler(this.GradeMultipleBtn_Click);
			// 
			// DeleteBulkBtn
			// 
			this.DeleteBulkBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.DeleteBulkBtn.Location = new System.Drawing.Point(457, 48);
			this.DeleteBulkBtn.Name = "DeleteBulkBtn";
			this.DeleteBulkBtn.Size = new System.Drawing.Size(49, 23);
			this.DeleteBulkBtn.TabIndex = 2;
			this.DeleteBulkBtn.Text = "Del";
			this.DeleteBulkBtn.UseVisualStyleBackColor = true;
			this.DeleteBulkBtn.Click += new System.EventHandler(this.DeleteBulkBtn_Click);
			// 
			// AddBulkBtn
			// 
			this.AddBulkBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.AddBulkBtn.Location = new System.Drawing.Point(457, 19);
			this.AddBulkBtn.Name = "AddBulkBtn";
			this.AddBulkBtn.Size = new System.Drawing.Size(49, 23);
			this.AddBulkBtn.TabIndex = 1;
			this.AddBulkBtn.Text = "Add";
			this.AddBulkBtn.UseVisualStyleBackColor = true;
			this.AddBulkBtn.Click += new System.EventHandler(this.AddBulkBtn_Click);
			// 
			// GradeTheseListBox
			// 
			this.GradeTheseListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.GradeTheseListBox.FormattingEnabled = true;
			this.GradeTheseListBox.Location = new System.Drawing.Point(6, 19);
			this.GradeTheseListBox.Name = "GradeTheseListBox";
			this.GradeTheseListBox.Size = new System.Drawing.Size(445, 95);
			this.GradeTheseListBox.TabIndex = 0;
			this.GradeTheseListBox.SelectedIndexChanged += new System.EventHandler(this.GradeTheseListBox_SelectedIndexChanged);
			this.GradeTheseListBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GradeTheseListBox_KeyDown);
			// 
			// GradeOneBtn
			// 
			this.GradeOneBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.GradeOneBtn.Location = new System.Drawing.Point(3, 221);
			this.GradeOneBtn.Name = "GradeOneBtn";
			this.GradeOneBtn.Size = new System.Drawing.Size(250, 23);
			this.GradeOneBtn.TabIndex = 1;
			this.GradeOneBtn.Text = "(or) Select Single Scan to Grade...";
			this.GradeOneBtn.UseVisualStyleBackColor = true;
			this.GradeOneBtn.Click += new System.EventHandler(this.GradeOneBtn_Click);
			// 
			// label2
			// 
			this.label2.Dock = System.Windows.Forms.DockStyle.Top;
			this.label2.Location = new System.Drawing.Point(3, 16);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(518, 46);
			this.label2.TabIndex = 0;
			this.label2.Text = resources.GetString("label2.Text");
			// 
			// groupBox7
			// 
			this.groupBox7.Controls.Add(this.FormEditorBtn);
			this.groupBox7.Controls.Add(this.label5);
			this.groupBox7.Dock = System.Windows.Forms.DockStyle.Top;
			this.groupBox7.Location = new System.Drawing.Point(3, 92);
			this.groupBox7.Name = "groupBox7";
			this.groupBox7.Size = new System.Drawing.Size(524, 91);
			this.groupBox7.TabIndex = 1;
			this.groupBox7.TabStop = false;
			this.groupBox7.Text = "Create or Edit a Form / Answer Sheet (Optional)";
			// 
			// FormEditorBtn
			// 
			this.FormEditorBtn.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.FormEditorBtn.Location = new System.Drawing.Point(3, 65);
			this.FormEditorBtn.Margin = new System.Windows.Forms.Padding(6);
			this.FormEditorBtn.Name = "FormEditorBtn";
			this.FormEditorBtn.Size = new System.Drawing.Size(518, 23);
			this.FormEditorBtn.TabIndex = 1;
			this.FormEditorBtn.Text = "Create / Edit Form...";
			this.FormEditorBtn.UseVisualStyleBackColor = true;
			this.FormEditorBtn.Click += new System.EventHandler(this.FormEditorBtn_Click);
			// 
			// label5
			// 
			this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label5.Location = new System.Drawing.Point(3, 16);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(518, 72);
			this.label5.TabIndex = 0;
			this.label5.Text = "Here you can create or edit a form.  Note: This shit is Alpha Alpha stage, dawg. " +
    " Play around with it all you want but it\'s rough around the edges hardcore.";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.NewExamBtn);
			this.groupBox2.Controls.Add(this.label1);
			this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
			this.groupBox2.Location = new System.Drawing.Point(3, 16);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(524, 76);
			this.groupBox2.TabIndex = 0;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Create a New Exam";
			// 
			// NewExamBtn
			// 
			this.NewExamBtn.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.NewExamBtn.Location = new System.Drawing.Point(3, 50);
			this.NewExamBtn.Margin = new System.Windows.Forms.Padding(6);
			this.NewExamBtn.Name = "NewExamBtn";
			this.NewExamBtn.Size = new System.Drawing.Size(518, 23);
			this.NewExamBtn.TabIndex = 1;
			this.NewExamBtn.Text = "Create a New Exam...";
			this.NewExamBtn.UseVisualStyleBackColor = true;
			this.NewExamBtn.Click += new System.EventHandler(this.NewExamBtn_Click);
			// 
			// label1
			// 
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Location = new System.Drawing.Point(3, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(518, 57);
			this.label1.TabIndex = 0;
			this.label1.Text = "In this step, you name an exam, define which printed form it uses, and create its" +
    " answer key";
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(530, 716);
			this.Controls.Add(this.groupBox1);
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Test Suite";
			this.groupBox1.ResumeLayout(false);
			this.groupBox6.ResumeLayout(false);
			this.groupBox5.ResumeLayout(false);
			this.groupBox3.ResumeLayout(false);
			this.groupBox4.ResumeLayout(false);
			this.groupBox7.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button GradeOneBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button NewExamBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button GradeMultipleBtn;
        private System.Windows.Forms.Button DeleteBulkBtn;
        private System.Windows.Forms.Button AddBulkBtn;
        private System.Windows.Forms.ListBox GradeTheseListBox;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button ViewExamsBtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button EditStudentsBtn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button FormEditorBtn;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button ExtractFromPDFBtn;
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace alpha
{
	public class PreloadedStudent
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string FullName { get { return FirstName + ' ' + LastName; } }
		public string ProperName { get { return LastName + ", " + FirstName; } }
		public string GroupName { get; set; }

		public PreloadedStudent() { }
		public PreloadedStudent(string fname, string lname)
		{
			FirstName = fname;
			LastName = lname;
			GroupName = string.Empty;
		}
		public PreloadedStudent(string fname, string lname, string gname)
		{
			FirstName = fname;
			LastName = lname;
			GroupName = gname;
		}
	}
	public class StudentList
	{
		public List<PreloadedStudent> Students;

		public StudentList()
		{
			Students = new List<PreloadedStudent>();
		}

		public static StudentList Load(string path)
		{
			StudentList list = new StudentList();

			XmlSerializer Format = new XmlSerializer(typeof(StudentList));
			string xmlStr = File.ReadAllText(path);
			using (XmlReader xml = XmlReader.Create(new StringReader(xmlStr)))
			{
				list = (StudentList)Format.Deserialize(xml);
			}
			return list;
		}
		public void Save(string path)
		{
			XmlSerializer Format = new XmlSerializer(typeof(StudentList));
			StringBuilder xmlOut = new StringBuilder();
			XmlWriterSettings settings = new XmlWriterSettings();
			settings.Indent = true;
			settings.IndentChars = "    ";
			settings.OmitXmlDeclaration = true;
			using (XmlWriter xml = XmlWriter.Create(xmlOut, settings))
			{
				Format.Serialize(xml, this);
			}

			var stream = File.Create(path);
				using (StreamWriter writer = new StreamWriter(stream))
					writer.Write(xmlOut);
			if (stream != null)
				stream.Dispose();
		}
	}
}

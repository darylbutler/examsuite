﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using ScanReader;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace alpha.Dialogs
{
	public partial class ViewExamForm : Form
	{
		List<string> ExamList;
		Exam CurrentExam;

		public ViewExamForm()
		{
			InitializeComponent();			
			this.Text = GetAppTitle();

			LoadExams();			
		}
		public string GetAppTitle()
		{
			// Load Title
			var version = System.Reflection.Assembly.GetExecutingAssembly()
										   .GetName()
										   .Version;
			const string releaseVersion = "0_2c";
			return string.Format("Exam Suite {0} -- v{1}.{2}.{3}", releaseVersion, version.Major, version.Minor, version.Build);
		}
		private void LoadExams()
		{
			ExamList = Database.GetExamNames();
			FilterExamsBox_TextChanged(null, null);
			if (ExamList.Count > 0)
				ExamListBox.SelectedIndex = 0; // Select first exam
			else
				LoadExamBtn.Enabled = false;

			// Load last loaded exam if it exists
			string exm = Properties.Settings.Default.LastLoadedExam;
			if (!string.IsNullOrWhiteSpace(exm) && ExamList.Contains(exm))
			{
				ExamListBox.SelectedItem = exm;
				LoadExamBtn_Click(null, null);
			}
			else
			{
				// Take away Tab pages because no exam is loaded
				if (tabControl.TabPages.Contains(EditDetailsPage))
					tabControl.TabPages.Remove(EditDetailsPage);
				if (tabControl.TabPages.Contains(AnswerKeyPage))
					tabControl.TabPages.Remove(AnswerKeyPage);
				if (tabControl.TabPages.Contains(GradebookTab))
					tabControl.TabPages.Remove(GradebookTab);
				if (tabControl.TabPages.Contains(AnalyticsPage))
					tabControl.TabPages.Remove(AnalyticsPage);
			}
		}
		private void NewExamBtn_Click(object sender, EventArgs e)
		{
			var dlg = new NewExamForm();
			if (dlg.ShowDialog() == DialogResult.OK)
			{
				LoadExamBtn.Enabled = true;
				LoadExams();
				int index = -1;
				index = ExamList.FindIndex(x => x.StartsWith(dlg.CreatedExam.Name));

				if (index > -1)
				{
					ExamListBox.SelectedIndex = index;
					LoadExamBtn.Focus();
				}
			}
			
		}
		private void FilterExamsBox_TextChanged(object sender, EventArgs e)
		{
			List<string> filteredList = ExamList;
			if (FilterExamsBox.Text != string.Empty)
			{
				filteredList = (from exam in ExamList
								where exam.StartsWith(FilterExamsBox.Text, StringComparison.CurrentCultureIgnoreCase)
								select exam).ToList();
			}
			ExamListBox.Items.Clear();
			foreach (var ex in filteredList)
			{
				ExamListBox.Items.Add(ex);
			}
		}
		private void LoadExamBtn_Click(object sender, EventArgs e)
		{
			if (ExamListBox.SelectedIndex < 0) return;

			Exam exm = Database.GetExam(ExamListBox.SelectedItem.ToString());
			Properties.Settings.Default.LastLoadedExam = ExamListBox.SelectedItem.ToString();
			Properties.Settings.Default.Save();

			LoadExam(exm);
		}

		// Load UI for Selected Exam
		private void LoadExam(Exam exam)
		{
			if (exam == null) return;

			CurrentExam = exam;

			this.Text = string.Format("{0} -- Loaded Exam: {1}", GetAppTitle(), CurrentExam.Name);

			SaveExamBtn.Enabled = false;

			// Add Tab pages back
			if (!tabControl.TabPages.Contains(EditDetailsPage))
				tabControl.TabPages.Add(EditDetailsPage);
			if (!tabControl.TabPages.Contains(AnswerKeyPage))
				tabControl.TabPages.Add(AnswerKeyPage);
			if (!tabControl.TabPages.Contains(GradebookTab))
				tabControl.TabPages.Add(GradebookTab);
			if (!tabControl.TabPages.Contains(AnalyticsPage))
				tabControl.TabPages.Add(AnalyticsPage);


			LoadAnswerKey();
			BuildGradebook();
			LoadExamDetails();
			BuildAnalytics();

			tabControl.SelectedTab = GradebookTab;
		}
		private void ExamChanged()
		{
			SaveExamBtn.Enabled = true;
		}

		// -- Answer Key
		private void LoadAnswerKey()
		{
			AnswerKeyDataGrid.Rows.Clear();

			int i = 0;
			foreach (List<bool> question in CurrentExam.AnswerKey.Answers)
			{
				DataGridViewRow row = new DataGridViewRow();

				var cell = new DataGridViewTextBoxCell();
				cell.Value = ++i;
				row.Cells.Add(cell);

				var cell1 = new DataGridViewComboBoxCell();
				cell1.DataSource = new List<string>() { "A", "B", "C", "D", "E" };
				cell1.ValueType = typeof(string);
				cell1.Value = new Answer(question).GetAsString();
				row.Cells.Add(cell1);

				AnswerKeyDataGrid.Rows.Add(row);
			}
		}
		private void AnswerKeyDataGrid_CellValueChanged(object sender, DataGridViewCellEventArgs e)
		{
			ExamChanged();
		}
		private void AddQuestionToAnswerKey(object sender, EventArgs e)
		{
			DataGridViewRow row = new DataGridViewRow();

			var cell = new DataGridViewTextBoxCell();
			cell.Value = AnswerKeyDataGrid.Rows.Count + 1;
			row.Cells.Add(cell);

			var cell1 = new DataGridViewComboBoxCell();
			cell1.DataSource = new List<string>() { "A", "B", "C", "D", "E" };
			cell1.ValueType = typeof(string);
			row.Cells.Add(cell1);

			AnswerKeyDataGrid.Rows.Add(row);
			


			ExamChanged();
			AnswerKeyDataGrid.Focus();
		}
		private void RemoveQuestionToAnswerKey(object sender, EventArgs e)
		{
			if (AnswerKeyDataGrid.SelectedCells.Count < 1) return;
			AnswerKeyDataGrid.Rows.RemoveAt(AnswerKeyDataGrid.SelectedCells[0].RowIndex);
			ReorderAnswerKeyGrid();
			ExamChanged();
		}
		private void ReorderAnswerKeyGrid()
		{
			for (int i = 0; i < AnswerKeyDataGrid.Rows.Count; ++i)
			{
				AnswerKeyDataGrid.Rows[i].Cells[0].Value = i + 1;
			}
		}
		private void AnswerKeyDataGrid_KeyDown(object sender, KeyEventArgs e)
		{
			if (AnswerKeyDataGrid.SelectedCells.Count < 1) return;

			if (e.KeyCode == Keys.A)
				AnswerKeyDataGrid.Rows[AnswerKeyDataGrid.SelectedCells[0].RowIndex].Cells[1].Value = "A";
			else if (e.KeyCode == Keys.B)
				AnswerKeyDataGrid.Rows[AnswerKeyDataGrid.SelectedCells[0].RowIndex].Cells[1].Value = "B";
			else if (e.KeyCode == Keys.C)
				AnswerKeyDataGrid.Rows[AnswerKeyDataGrid.SelectedCells[0].RowIndex].Cells[1].Value = "C";
			else if (e.KeyCode == Keys.D)
				AnswerKeyDataGrid.Rows[AnswerKeyDataGrid.SelectedCells[0].RowIndex].Cells[1].Value = "D";
			else if (e.KeyCode == Keys.E)
				AnswerKeyDataGrid.Rows[AnswerKeyDataGrid.SelectedCells[0].RowIndex].Cells[1].Value = "E";
		}

		// -- Gradebook ---------------------------------------------------------------------------
		private void BuildGradebook()
		{
			GradebookGrid.Rows.Clear();

			foreach (var s in CurrentExam.Students)
			{
				DataGridViewRow row = new DataGridViewRow();

				// Last Name
				var cell1 = new DataGridViewTextBoxCell();
				cell1.Value = s.LastName;
				row.Cells.Add(cell1);

				// First Name
				var cell2 = new DataGridViewTextBoxCell();
				cell2.Value = s.FirstName;
				row.Cells.Add(cell2);

				// Class
				var cell3 = new DataGridViewTextBoxCell();
				cell3.Value = s.GroupName;
				row.Cells.Add(cell3);

				// # Correct
				var cell4 = new DataGridViewTextBoxCell();
				cell4.ValueType = typeof(int);
				int numberCorrect = (from q in s.Score.QuestionCorrect where q == true select q).Count();
				cell4.Value = numberCorrect;
				row.Cells.Add(cell4);

				// # Total
				var cell5 = new DataGridViewTextBoxCell();
				cell5.ValueType = typeof(int);
				int total = s.Score.QuestionCorrect.Count;
				cell5.Value = total;
				row.Cells.Add(cell5);

				// % Grade
				var cell6 = new DataGridViewTextBoxCell();
				float grade = (float) numberCorrect / (float)total;
				cell6.Value = string.Format("{0:F1}%", grade * 100.0f);
				row.Cells.Add(cell6);

				GradebookGrid.Rows.Add(row);
			}
		}
		// -- Print all marked up tests
		private void MarkAllTestsBtn_Click(object sender, EventArgs e)
		{
			// Create a multipage document with each students' marked up test for printing
			var document = new Document(PageSize.LETTER, 10, 10, 10, 10);
			// TODO move this to MemoryStream and let Database save it so it is consistient
			string fileName = string.Format("{0}{1}\\ToPrint.pdf", Database.ExamsPath, CurrentExam.Name);
			var output = new FileStream(fileName, FileMode.Create);
			var writer = PdfWriter.GetInstance(document, output);
			document.Open();

			foreach (Student s in CurrentExam.Students)
			{
				Bitmap bmp = new Bitmap(s.Scan.Image);
				using (Graphics g = Graphics.FromImage(bmp))
					CurrentExam.Form.MarkupEngine.MarkUpScan(g, CurrentExam, s);

				iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(bmp, System.Drawing.Imaging.ImageFormat.Png);
				img.ScaleToFit(document.PageSize.Width - 20, document.PageSize.Height - 20);
				img.Alignment = Element.ALIGN_CENTER;
				document.Add(img);
				document.NewPage();
			}
			output.Flush();
			document.Close();
			writer.Close();

			// Open the file afterwards
			System.Diagnostics.Process.Start(fileName);
		}
		// -- Edit Student Scan
		private void EditStudentScanBtn_Click(object sender, EventArgs e)
		{
			if (GradebookGrid.SelectedRows.Count < 1) return;

			Student s = CurrentExam.Students[GradebookGrid.SelectedCells[0].RowIndex];
			GradeExamsForm frm = new GradeExamsForm(CurrentExam, s);

			if (frm.ShowDialog() == DialogResult.OK)
			{
				// Save Exam
				CurrentExam.Students[GradebookGrid.SelectedCells[0].RowIndex] = frm.SelectedStudent;
				ExamChanged();
				// Reload gradebook
				BuildGradebook();
			}
			
		}
		// -- Delete Scan
		private void DeleteStudentBtn_Click(object sender, EventArgs e)
		{
			if (GradebookGrid.SelectedRows.Count < 1) return;

			if (MessageBox.Show("Are you sure you want to delete this Scan?\nAll Data pertaining to this scan will be deleted.", "Woah there!", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation) == DialogResult.Yes)
			{
				CurrentExam.Students[GradebookGrid.SelectedCells[0].RowIndex].Scan.Dispose();
				CurrentExam.Students.Remove(CurrentExam.Students[GradebookGrid.SelectedCells[0].RowIndex]);

				LoadExam(CurrentExam);
				ExamChanged();
			}
		}
		// -- Add Scans
		private void AddScanBtn_Click(object sender, EventArgs e)
		{
			using (AddScanToExamForm frm = new AddScanToExamForm(CurrentExam))
			{
				if (frm.ShowDialog() == DialogResult.OK &&
				frm.ScansToAdd != null && frm.ScansToAdd.Count > 0)
				{
					// Add scans to the Exam	 
					foreach (var s in frm.ScansToAdd)
						CurrentExam.Students.Add((Student)s.Clone());

					//CurrentExam.Students.AddRange(frm.ScansToAdd);
					// Load the UI from the Exam
					LoadExam(CurrentExam);
					ExamChanged();
				}
			}			
		}

		// -- Details
		private void LoadExamDetails()
		{
			NameEdit.Text = CurrentExam.Name;            
			NameEdit.ReadOnly = true;
			SubjectEdit.Text = CurrentExam.Subject;
			DescriptionEdit.Text = CurrentExam.Description;
			AuthorEdit.Text = CurrentExam.Author;

			// Update Save btn on Exam Change
			SubjectEdit.TextChanged += ExamDetailsChanged;
			DescriptionEdit.TextChanged += ExamDetailsChanged;
			AuthorEdit.TextChanged += ExamDetailsChanged;
		}
		private void ExamDetailsChanged(object sender, EventArgs e)
		{
			ExamChanged();
		}

		// -- Analytics
		private void BuildAnalytics()
		{
			AnaltyicsGrid.Rows.Clear();

			// Per Question Stats
			for (int i = 0; i < CurrentExam.AnswerKey.Answers.Count; ++i)
			{
				// Find the stats...
				float wrong = 0, blank = 0, correct = 0, total = 0, a = 0, b = 0, c = 0, d = 0, e = 0;

				// Scores
				foreach (var s in CurrentExam.Students)
				{
					if (s.Score.Answers.Count != CurrentExam.AnswerKey.Answers.Count) // Invalid student
						continue;		//Skip Student

					total++;

					if (s.Score.Answers[i].IsBlank())
						blank++;
					else if (s.Score.QuestionCorrect[i])
						correct++;
					else
						wrong++;

					if (s.Score.Answers[i][0])
						a++;
					if (s.Score.Answers[i][1])
						b++;
					if (s.Score.Answers[i][2])
						c++;
					if (s.Score.Answers[i][3])
						d++;
					if (s.Score.Answers[i][4])
						e++;
				}

				// Show the stats...
				DataGridViewRow row = new DataGridViewRow();

				// Question Number
				var cell1 = new DataGridViewTextBoxCell();
				cell1.Value = i + 1;
				row.Cells.Add(cell1);

				// % Wrong
				float wrongPer = wrong / total * 100.0f;
				var cell2 = new DataGridViewTextBoxCell();
				cell2.Value = string.Format("{0:F0}%", wrongPer);
				row.Cells.Add(cell2);

				// % Blank
				float blankPer = blank / total * 100.0f;
				var cell3 = new DataGridViewTextBoxCell();
				cell3.Value = string.Format("{0:F0}%", blankPer);
				row.Cells.Add(cell3);

				// % Correct
				float correctPer = correct / total * 100.0f;
				var cell4 = new DataGridViewTextBoxCell();
				cell4.Value = string.Format("{0:F0}%", correctPer);
				row.Cells.Add(cell4);

				// A%
				float aPer = a / total * 100.0f;
				var cell5 = new DataGridViewTextBoxCell();
				cell5.Value = string.Format("{0:F0}%", aPer);
				row.Cells.Add(cell5);
				// B%
				float bPer = b / total * 100.0f;
				var cell6 = new DataGridViewTextBoxCell();
				cell6.Value = string.Format("{0:F0}%", bPer);
				row.Cells.Add(cell6);
				// C%
				float cPer = c / total * 100.0f;
				var cell7 = new DataGridViewTextBoxCell();
				cell7.Value = string.Format("{0:F0}%", cPer);
				row.Cells.Add(cell7);
				// D%
				float dPer = d / total * 100.0f;
				var cell8 = new DataGridViewTextBoxCell();
				cell8.Value = string.Format("{0:F0}%", dPer);
				row.Cells.Add(cell8);
				// E%
				float ePer = e / total * 100.0f;
				var cell9 = new DataGridViewTextBoxCell();
				cell9.Value = string.Format("{0:F0}%", ePer);
				row.Cells.Add(cell9);


				AnaltyicsGrid.Rows.Add(row);
			}
		}

		// -- Save Exam
		private void SaveExamBtn_Click(object sender, EventArgs e)
		{
			CurrentExam.Subject = SubjectEdit.Text;
			CurrentExam.Description = DescriptionEdit.Text;
			CurrentExam.Author = AuthorEdit.Text;

			CurrentExam.AnswerKey = new AnswerKey();
			for (int i = 0; i < AnswerKeyDataGrid.Rows.Count; ++i)
			{
				CurrentExam.AnswerKey.Answers.Add(new Answer(AnswerKeyDataGrid.Rows[i].Cells[1].Value.ToString()).Data.ToList());
			}

			CurrentExam.SaveToFile("Exams");
			SaveExamBtn.Enabled = false;
		}

		// -- Edit Students
		private void EditStudentsBtn_Click(object sender, EventArgs e)
		{
			new Dialogs.EditStudentsForm().ShowDialog();
		}

		// -- Launch Form Editor
		private void FormEditorBtn_Click(object sender, EventArgs e)
		{
			new ExamCreation.DesignExamForm().ShowDialog();
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using ScanReader;

namespace alpha.Dialogs
{
	public partial class NewExamForm : Form
	{
		public Exam CreatedExam;

		public NewExamForm()
		{
			InitializeComponent();
			LoadExamForms();
		}

		private void LoadExamForms()
		{
			ExamFormListBox.Items.Clear();
			ExamFormListBox.Items.AddRange(Database.GetFormNames().ToArray());
		}
		private void RemoveQuestionBtn_Click(object sender, EventArgs e)
		{
			if (AnswerKeyDataGrid.SelectedCells.Count < 1) return;
			AnswerKeyDataGrid.Rows.RemoveAt(AnswerKeyDataGrid.SelectedCells[0].RowIndex);
			ReorderAnswerKeyGrid();
		}
		private DataGridViewRow createNewAnswerKeyRow()
		{
			DataGridViewRow row = new DataGridViewRow();

			var cell = new DataGridViewTextBoxCell();
			cell.Value = AnswerKeyDataGrid.Rows.Count + 1;
			row.Cells.Add(cell);

			var cell1 = new DataGridViewComboBoxCell();
			cell1.DataSource = new List<string>() { "A", "B", "C", "D", "E" };
			cell1.ValueType = typeof(string);
			cell1.Value = string.Empty;
			row.Cells.Add(cell1);

			return row;
		}
		private void AddQuestionBtn_Click(object sender, EventArgs e)
		{		
			// Add new row
			AnswerKeyDataGrid.Rows.Add(createNewAnswerKeyRow());
			// Deselect any selected cells
			if (AnswerKeyDataGrid.SelectedCells.Count > 0)
				AnswerKeyDataGrid.SelectedCells[0].Selected = false;
			// Select the last cell
			AnswerKeyDataGrid.Rows[AnswerKeyDataGrid.RowCount - 1].Cells[0].Selected = true;
			// Scroll to the last cell
			AnswerKeyDataGrid.FirstDisplayedScrollingRowIndex = AnswerKeyDataGrid.RowCount - 1;
			// Give the grid focus to accept keyboard input
			AnswerKeyDataGrid.Focus();
		}
		private void AnswerKeyDataGrid_KeyDown(object sender, KeyEventArgs e)
		{
			if (AnswerKeyDataGrid.SelectedCells.Count < 1) return;

			if (e.KeyCode == Keys.A)
				AnswerKeyDataGrid.Rows[AnswerKeyDataGrid.SelectedCells[0].RowIndex].Cells[1].Value = "A";
			else if (e.KeyCode == Keys.B)
				AnswerKeyDataGrid.Rows[AnswerKeyDataGrid.SelectedCells[0].RowIndex].Cells[1].Value = "B";
			else if (e.KeyCode == Keys.C)
				AnswerKeyDataGrid.Rows[AnswerKeyDataGrid.SelectedCells[0].RowIndex].Cells[1].Value = "C";
			else if (e.KeyCode == Keys.D)
				AnswerKeyDataGrid.Rows[AnswerKeyDataGrid.SelectedCells[0].RowIndex].Cells[1].Value = "D";
			else if (e.KeyCode == Keys.E)
				AnswerKeyDataGrid.Rows[AnswerKeyDataGrid.SelectedCells[0].RowIndex].Cells[1].Value = "E";
		}
		private void ReorderAnswerKeyGrid()
		{
			for (int i = 0; i < AnswerKeyDataGrid.Rows.Count; ++i)
			{
				AnswerKeyDataGrid.Rows[i].Cells[0].Value = i + 1;
			}
		}
		private bool AnswerKeyIsEmpty()
		{
			for (int i = 0; i < AnswerKeyDataGrid.Rows.Count; ++i)
			{
				if (!string.IsNullOrWhiteSpace(AnswerKeyDataGrid.Rows[i].Cells[1].Value.ToString()))
					return false;
			}
			return true;
		}
		private bool AnswerKeyHasBlankAnswers()
		{
			for (int i = 0; i < AnswerKeyDataGrid.Rows.Count; ++i)
			{
				if (string.IsNullOrWhiteSpace(AnswerKeyDataGrid.Rows[i].Cells[1].Value.ToString()))
					return true;
			}
			return false;
		}
		private void DoneBtn_Click(object sender, EventArgs e)
		{
			// Check to see if we can save
			if (ExamFormListBox.SelectedIndex < 0)
			{
				MessageBox.Show("You must select an Exam Definition before you can save!");
				return;
			}
			if (NameEdit.Text == string.Empty)
			{
				MessageBox.Show("Exam must have a name!");
				return;
			}
			if (Directory.Exists("Exams\\" + NameEdit.Text))
			{
				MessageBox.Show("An Exam with this Name already exists!");				
				return;
			}
			if (AnswerKeyHasBlankAnswers())
			{
				MessageBox.Show("There cannot be blank answers in the Answer Key!");
				return;
			}

			// Save
			Exam exam = new Exam();

			exam.Name = NameEdit.Text;
			exam.Subject = SubjectEdit.Text;
			exam.Description = DescriptionEdit.Text;
			exam.Author = AuthorEdit.Text;
			exam.CreatedDate = DateTime.Now;

			ExamForm form = Database.GetForm(ExamFormListBox.SelectedItem.ToString());
			exam.Form = form;

			exam.AnswerKey = new AnswerKey();
			for (int i = 0; i < AnswerKeyDataGrid.Rows.Count; ++i)
			{
				exam.AnswerKey.Answers.Add(StringToAnswer(AnswerKeyDataGrid.Rows[i].Cells[1].Value.ToString()));
			}

			CreatedExam = exam;
			exam.SaveToFile("Exams");
			DialogResult = DialogResult.OK;
		}

		private List<bool> StringToAnswer(string ans)
		{
			List<bool> ret = new List<bool>() { false, false, false, false, false };

			switch (ans)
			{
				case "A":
					ret[0] = true;
					return ret;
				case "B":
					ret[1] = true;
					return ret;
				case "C":
					ret[2] = true;
					return ret;
				case "D":
					ret[3] = true;
					return ret;
				case "E":
					ret[4] = true;
					return ret;
			}
			return ret;
		}

		private void ExamFormListBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			//Selected a new ExamForm, autopopulate the Answer Key
			if (AnswerKeyIsEmpty())
			{
				// Generate the Answer Key from the Form
				AnswerKeyDataGrid.Rows.Clear();
				foreach (var q in Database.GetForm(ExamFormListBox.SelectedItem.ToString()).Questions)
					AnswerKeyDataGrid.Rows.Add(createNewAnswerKeyRow());
				AnswerKeyDataGrid.Focus();
			}
		}
	}
}

﻿namespace alpha.Dialogs
{
    partial class NewExamForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewExamForm));
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.AuthorEdit = new System.Windows.Forms.TextBox();
			this.DescriptionEdit = new System.Windows.Forms.TextBox();
			this.SubjectEdit = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.NameEdit = new System.Windows.Forms.TextBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.ExamFormListBox = new System.Windows.Forms.ListBox();
			this.AnswerKeyDataGrid = new System.Windows.Forms.DataGridView();
			this.Number = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Answer = new System.Windows.Forms.DataGridViewComboBoxColumn();
			this.AddQuestionBtn = new System.Windows.Forms.Button();
			this.RemoveQuestionBtn = new System.Windows.Forms.Button();
			this.label5 = new System.Windows.Forms.Label();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.CancelBtn = new System.Windows.Forms.Button();
			this.DoneBtn = new System.Windows.Forms.Button();
			this.groupBox1.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.AnswerKeyDataGrid)).BeginInit();
			this.groupBox3.SuspendLayout();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.tableLayoutPanel1);
			this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
			this.groupBox1.Location = new System.Drawing.Point(5, 5);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(569, 116);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Exam Details:";
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 2;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75F));
			this.tableLayoutPanel1.Controls.Add(this.AuthorEdit, 1, 3);
			this.tableLayoutPanel1.Controls.Add(this.DescriptionEdit, 1, 2);
			this.tableLayoutPanel1.Controls.Add(this.SubjectEdit, 1, 1);
			this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
			this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
			this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.NameEdit, 1, 0);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 5;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(563, 97);
			this.tableLayoutPanel1.TabIndex = 0;
			// 
			// AuthorEdit
			// 
			this.AuthorEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.AuthorEdit.Location = new System.Drawing.Point(143, 69);
			this.AuthorEdit.Name = "AuthorEdit";
			this.AuthorEdit.Size = new System.Drawing.Size(417, 20);
			this.AuthorEdit.TabIndex = 7;
			// 
			// DescriptionEdit
			// 
			this.DescriptionEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.DescriptionEdit.Location = new System.Drawing.Point(143, 47);
			this.DescriptionEdit.Name = "DescriptionEdit";
			this.DescriptionEdit.Size = new System.Drawing.Size(417, 20);
			this.DescriptionEdit.TabIndex = 6;
			// 
			// SubjectEdit
			// 
			this.SubjectEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.SubjectEdit.Location = new System.Drawing.Point(143, 25);
			this.SubjectEdit.Name = "SubjectEdit";
			this.SubjectEdit.Size = new System.Drawing.Size(417, 20);
			this.SubjectEdit.TabIndex = 5;
			// 
			// label4
			// 
			this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(3, 66);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(134, 22);
			this.label4.TabIndex = 3;
			this.label4.Text = "Author:";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label3
			// 
			this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(3, 44);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(134, 22);
			this.label3.TabIndex = 2;
			this.label3.Text = "Description:";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label2
			// 
			this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(3, 22);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(134, 22);
			this.label2.TabIndex = 1;
			this.label2.Text = "Subject:";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label1
			// 
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(3, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(134, 22);
			this.label1.TabIndex = 0;
			this.label1.Text = "Name:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// NameEdit
			// 
			this.NameEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.NameEdit.Location = new System.Drawing.Point(143, 3);
			this.NameEdit.Name = "NameEdit";
			this.NameEdit.Size = new System.Drawing.Size(417, 20);
			this.NameEdit.TabIndex = 4;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.ExamFormListBox);
			this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
			this.groupBox2.Location = new System.Drawing.Point(5, 121);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(569, 194);
			this.groupBox2.TabIndex = 1;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Select Exam Form (Printed Form):";
			// 
			// ExamFormListBox
			// 
			this.ExamFormListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ExamFormListBox.FormattingEnabled = true;
			this.ExamFormListBox.Location = new System.Drawing.Point(6, 19);
			this.ExamFormListBox.Name = "ExamFormListBox";
			this.ExamFormListBox.Size = new System.Drawing.Size(554, 160);
			this.ExamFormListBox.TabIndex = 0;
			this.ExamFormListBox.SelectedIndexChanged += new System.EventHandler(this.ExamFormListBox_SelectedIndexChanged);
			// 
			// AnswerKeyDataGrid
			// 
			this.AnswerKeyDataGrid.AllowUserToAddRows = false;
			this.AnswerKeyDataGrid.AllowUserToDeleteRows = false;
			this.AnswerKeyDataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.AnswerKeyDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.AnswerKeyDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.AnswerKeyDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Number,
            this.Answer});
			this.AnswerKeyDataGrid.Location = new System.Drawing.Point(6, 19);
			this.AnswerKeyDataGrid.Name = "AnswerKeyDataGrid";
			this.AnswerKeyDataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
			this.AnswerKeyDataGrid.Size = new System.Drawing.Size(554, 195);
			this.AnswerKeyDataGrid.TabIndex = 0;
			this.AnswerKeyDataGrid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AnswerKeyDataGrid_KeyDown);
			// 
			// Number
			// 
			this.Number.FillWeight = 50F;
			this.Number.HeaderText = "Number";
			this.Number.Name = "Number";
			this.Number.ReadOnly = true;
			// 
			// Answer
			// 
			this.Answer.HeaderText = "Answer";
			this.Answer.Name = "Answer";
			// 
			// AddQuestionBtn
			// 
			this.AddQuestionBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.AddQuestionBtn.Location = new System.Drawing.Point(6, 219);
			this.AddQuestionBtn.Name = "AddQuestionBtn";
			this.AddQuestionBtn.Size = new System.Drawing.Size(120, 23);
			this.AddQuestionBtn.TabIndex = 1;
			this.AddQuestionBtn.Text = "Add Question";
			this.AddQuestionBtn.UseVisualStyleBackColor = true;
			this.AddQuestionBtn.Click += new System.EventHandler(this.AddQuestionBtn_Click);
			// 
			// RemoveQuestionBtn
			// 
			this.RemoveQuestionBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.RemoveQuestionBtn.Location = new System.Drawing.Point(132, 219);
			this.RemoveQuestionBtn.Name = "RemoveQuestionBtn";
			this.RemoveQuestionBtn.Size = new System.Drawing.Size(120, 23);
			this.RemoveQuestionBtn.TabIndex = 2;
			this.RemoveQuestionBtn.Text = "Remove Question";
			this.RemoveQuestionBtn.UseVisualStyleBackColor = true;
			this.RemoveQuestionBtn.Click += new System.EventHandler(this.RemoveQuestionBtn_Click);
			// 
			// label5
			// 
			this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label5.Location = new System.Drawing.Point(261, 217);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(302, 29);
			this.label5.TabIndex = 3;
			this.label5.Text = "TIP: Select the row by clicking in the leftmost column then simply type the answe" +
    "r you want to choose";
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.AddQuestionBtn);
			this.groupBox3.Controls.Add(this.RemoveQuestionBtn);
			this.groupBox3.Controls.Add(this.label5);
			this.groupBox3.Controls.Add(this.AnswerKeyDataGrid);
			this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox3.Location = new System.Drawing.Point(5, 315);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(569, 248);
			this.groupBox3.TabIndex = 2;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Create an Answer Key:";
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.CancelBtn);
			this.panel1.Controls.Add(this.DoneBtn);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(5, 563);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(569, 29);
			this.panel1.TabIndex = 5;
			// 
			// CancelBtn
			// 
			this.CancelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.CancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.CancelBtn.Location = new System.Drawing.Point(383, 3);
			this.CancelBtn.Name = "CancelBtn";
			this.CancelBtn.Size = new System.Drawing.Size(75, 23);
			this.CancelBtn.TabIndex = 6;
			this.CancelBtn.Text = "Cancel";
			this.CancelBtn.UseVisualStyleBackColor = true;
			// 
			// DoneBtn
			// 
			this.DoneBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.DoneBtn.Location = new System.Drawing.Point(464, 3);
			this.DoneBtn.Name = "DoneBtn";
			this.DoneBtn.Size = new System.Drawing.Size(102, 23);
			this.DoneBtn.TabIndex = 5;
			this.DoneBtn.Text = "Save && Done";
			this.DoneBtn.UseVisualStyleBackColor = true;
			this.DoneBtn.Click += new System.EventHandler(this.DoneBtn_Click);
			// 
			// NewExamForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(579, 597);
			this.Controls.Add(this.groupBox3);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "NewExamForm";
			this.Padding = new System.Windows.Forms.Padding(5);
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Create a New Exam...";
			this.groupBox1.ResumeLayout(false);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.AnswerKeyDataGrid)).EndInit();
			this.groupBox3.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox AuthorEdit;
        private System.Windows.Forms.TextBox DescriptionEdit;
        private System.Windows.Forms.TextBox SubjectEdit;
        private System.Windows.Forms.TextBox NameEdit;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListBox ExamFormListBox;
        private System.Windows.Forms.DataGridView AnswerKeyDataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn Number;
        private System.Windows.Forms.DataGridViewComboBoxColumn Answer;
        private System.Windows.Forms.Button AddQuestionBtn;
        private System.Windows.Forms.Button RemoveQuestionBtn;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button CancelBtn;
        private System.Windows.Forms.Button DoneBtn;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ScanReader;

namespace alpha.Dialogs
{
    public partial class EditStudentsForm : Form
    {
        StudentList StudentsList;
        
        public EditStudentsForm()
        {
            InitializeComponent();
            StudentsList = Database.GetStudents();

            // Load the items
            StudentsGrid.Rows.Clear();
            foreach (var s in StudentsList.Students)
            {
                DataGridViewRow row = new DataGridViewRow();

                // Group Name
                var groupCell = new DataGridViewTextBoxCell();
                groupCell.Value = s.GroupName;
                row.Cells.Add(groupCell);

                // First Name
                var fnameCell = new DataGridViewTextBoxCell();
                fnameCell.Value = s.FirstName;
                row.Cells.Add(fnameCell);

                // First Name
                var lnameCell = new DataGridViewTextBoxCell();
                lnameCell.Value = s.LastName;
                row.Cells.Add(lnameCell);

                StudentsGrid.Rows.Add(row);
            }

        }

        private void SaveBtn_Click(object sender, EventArgs e)
        {
            // Rebuild StudentsList
            StudentsList.Students.Clear();
            for (int i = 0; i < StudentsGrid.Rows.Count; ++i)
            {
                if (StudentsGrid.Rows[i].Cells[0].Value != null &&
                    StudentsGrid.Rows[i].Cells[1].Value != null &&
                    StudentsGrid.Rows[i].Cells[2].Value != null &&
                    StudentsGrid.Rows[i].Cells[0].Value.ToString() != string.Empty &&
                    StudentsGrid.Rows[i].Cells[1].Value.ToString() != string.Empty &&
                    StudentsGrid.Rows[i].Cells[2].Value.ToString() != string.Empty)
                {
                    StudentsList.Students.Add(new PreloadedStudent(
                        StudentsGrid.Rows[i].Cells[1].Value.ToString(),
                        StudentsGrid.Rows[i].Cells[2].Value.ToString(),
                        StudentsGrid.Rows[i].Cells[0].Value.ToString()));
                }
            }

            // Save the Students List
            //StudentsList.Save("students.xml");
            Database.SaveStudents(StudentsList);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ScanReader;
using System.Drawing.Printing;
using ScanReader.ExamObjects;

namespace alpha
{
	public partial class GradeExamsForm : Form
	{
		public enum StudentSort
		{
			NameAscending,
			NameDescending,
			GroupAscending,
			GroupDescending
		}

		public ExamScan Scan;
		public Exam SelectedExam;
		public Student SelectedStudent;

		Bitmap TestImage;
		List<string> ExamList;
		StudentList Students;
		List<PreloadedStudent> FilteredStudents;
		StudentSort StudentSortType;
		int FocusedQuestion;

		public bool TestWasGradedAndSaved;
		bool EditMode;

		//public GradeExamsForm(string testImagePath)
		//{
		//	FocusedQuestion = -1;
		//	TestWasGradedAndSaved = false;
		//	InitializeComponent();            

		//	// Load the local image and setup image viewer
		//	TestImage = new Bitmap(testImagePath);         
		//	ImgView.Image = new Bitmap(TestImage);
		//	ImgView.ImageBox.ZoomToFit();
		//	Scan = new ExamScan(TestImage);

		//	// Load the data for the tabs
		//	loadExams();
		//	LoadStudents();
		//	LoadExamScanOptions();

		//	// Remove all tabs
		//	tabControl1.TabPages.Clear();

		//	// Set app title
		//	this.Text = string.Format("{0} -- Loading...", GetAppTitle());
		//}
		//public GradeExamsForm(string testImagePath, Exam exam)
		//{
		//	// Quicker Creation (NO Select Exam)
		//	FocusedQuestion = -1;
		//	TestWasGradedAndSaved = false;
		//	InitializeComponent();

		//	// Load the local image and setup image viewer
		//	SelectedExam = exam;
		//	TestImage = new Bitmap(testImagePath);
		//	ImgView.Image = new Bitmap(TestImage);
		//	ImgView.ImageBox.ZoomToFit();
		//	Scan = new ExamScan(TestImage);
		//	SelectedStudent = new Student();			

		//	// Load the data for the tabs
		//	LoadStudents();
		//	LoadExamScanOptions();

		//	// Remove all tabs
		//	tabControl1.TabPages.Clear();

		//	// Set app title
		//	this.Text = string.Format("{0} -- Loading...", GetAppTitle());
		//}
		public GradeExamsForm(Exam exm, Student student, bool editing = true)
		{
			// Editing an Existing Scan
			EditMode = editing;

			FocusedQuestion = -1;
			TestWasGradedAndSaved = true;
			InitializeComponent();

			// Load the local image and setup image viewer
			SelectedExam = exm;            
			SelectedStudent = student;
			Scan = student.Scan;

			// Setup UI values
			LoadExamScanOptions();  

			// Load Image display
			TestImage = student.Scan.Image;            
			ImgView.Image = new Bitmap(TestImage);
			ImgView.ImageBox.ZoomToFit();

			// Load the data for the tabs
			if (student.Scan.Loaded && !student.Scan.LoadedError)
			{
				DrawQuestionsAndScoreStudent();
				ShowAnswers();
				UpdateStudentGradeLabels();
				MarkUpTestBtn.Enabled = true;
				DoneBtn.Enabled = true;
			}
			else
				LoadImageBtn_Click(null, null);

			// Remove all tabs
			tabControl1.TabPages.Clear();

			// Set app title
			this.Text = string.Format("{0} -- Loading...", GetAppTitle());
		}
		public string GetAppTitle()
		{
			return string.Format("Scan and Grade Exam{0}", (SelectedExam!=null) ? string.Format(" -- {0}", SelectedExam.Name) : string.Empty);
		}
		private void GradeExamsForm_Load(object sender, EventArgs e)
		{
			this.Refresh();

			if (EditMode)
			{
				// If we are in edit mode, we do not want to reload the image
				tabControl1.TabPages.AddRange(new TabPage[] { CheckAnswersPage, FinishedPage });
				tabControl1.SelectedTab =  CheckAnswersPage;
			}
			else if (!EditMode && SelectedExam != null && SelectedStudent != null)
			{
				tabControl1.TabPages.AddRange(new TabPage[] { AnalyzeImagePage, CheckAnswersPage, FinishedPage });
				tabControl1.SelectedTab = Scan.Loaded ? CheckAnswersPage : AnalyzeImagePage;
			}
			else
			{
				tabControl1.TabPages.AddRange(new TabPage[] { SelectExamPage, SelectStudentPage, AnalyzeImagePage, CheckAnswersPage, FinishedPage });
				tabControl1.SelectedTab = SelectExamPage;
			}

			// For Debug Stepping
			AnalyzeImageBtnStep0.Tag = 0;
			AnalyzeImageBtnStep1.Tag = 1;
			AnalyzeImageBtnStep2.Tag = 2;
			AnalyzeImageBtnStep3.Tag = 3;
			AnalyzeImageBtnStep4.Tag = 4;
			AnalyzeImageBtnStep5.Tag = 5;
			AnalyzeImageBtnStep6.Tag = 6;

			// Allow us to Load the Image if we're not editing a saved scan
			LoadImageBtn.Enabled = !EditMode;

			// Loaded, reset Title
			this.Text = GetAppTitle();
		}
		private void ClearImage()
		{
			if (TestImage != null && (Scan == null || Scan.Image == null))
				ImgView.Image = new Bitmap(TestImage);
			else 
				ImgView.Image = new Bitmap(Scan.Image);
		}

		// -- Select Exam -------------------------------------------------------------------------
		private void loadExams()
		{
			ExamList = Database.GetExamNames();
			FilterExamsBox_TextChanged(null, null);
		}
		private void FilterExamsBox_TextChanged(object sender, EventArgs e)
		{
			List<string> filteredList = ExamList;
			if (FilterExamsBox.Text != string.Empty)
			{
				filteredList = (from exam in ExamList
								where exam.StartsWith(FilterExamsBox.Text, StringComparison.CurrentCultureIgnoreCase)
								select exam).ToList();
			}
			ExamListBox.Items.Clear();
			foreach (var ex in filteredList)
			{
				ExamListBox.Items.Add(ex);
			}
		}
		private void ExamListBox_SelectedValueChanged(object sender, EventArgs e)
		{
			// Load Newly selected Exam
			if (ExamListBox.SelectedItem == null) return;

			SelectedExam = Database.GetExam(ExamListBox.SelectedItem.ToString());
			ExamNameLabel.Text = SelectedExam.Name;

			// Exam Changed, reset Title
			this.Text = GetAppTitle();

			LoadImageBtn.Enabled = (SelectedExam != null && SelectedStudent != null);
			LoadStudents();
		}
		private void LoadExamScanOptions()
		{
			if (Scan == null) return;

			OptIsDarkRatio.Text = Scan.OptIsDarkRatio.ToString();
			OptFindAnswersHistogramPasses.Text = Scan.OptFindAnswersHistogramPasses.ToString();
			OptBlankBubbleFillRatio.Text = Scan.OptBlankBubbleFillRatio.ToString();
			OptSelectOnlyOneAnswer.Checked = Scan.OptSelectOnlyOneAnswer;
			OptCornerMatchingConfidence.Text = Scan.OptCornerPatternMatchRatio.ToString();
			OptFindBubbleLocation.Checked = Scan.OptFindBubbleLocation;
		}
		private void SaveExamScanOptions()
		{
			if (Scan == null) return;

			Scan.OptIsDarkRatio = float.Parse(OptIsDarkRatio.Text);
			Scan.OptFindAnswersHistogramPasses = int.Parse(OptFindAnswersHistogramPasses.Text);
			Scan.OptBlankBubbleFillRatio = float.Parse(OptBlankBubbleFillRatio.Text);
			Scan.OptSelectOnlyOneAnswer = OptSelectOnlyOneAnswer.Checked;
			Scan.OptCornerPatternMatchRatio = float.Parse(OptCornerMatchingConfidence.Text);
			Scan.OptFindBubbleLocation = OptFindBubbleLocation.Checked;
		}

		// -- Select Student ----------------------------------------------------------------------
		private void LoadStudents()
		{
			Students = Database.GetStudents();

			StudentFilterEdit_TextChanged(null, null);
		}
		private void StudentFilterEdit_TextChanged(object sender, EventArgs e)
		{
			if (Students == null) return;

			var students = from s in Students.Students select s;

			// Filter
			if (StudentFilterEdit.Text != string.Empty)
				students = from s in students
						   where    s.FirstName.StartsWith(StudentFilterEdit.Text, StringComparison.CurrentCultureIgnoreCase) || 
									s.LastName.StartsWith(StudentFilterEdit.Text, StringComparison.CurrentCultureIgnoreCase) || 
									s.GroupName.StartsWith(StudentFilterEdit.Text, StringComparison.CurrentCultureIgnoreCase)
						   select s;

			// Sort
			if (StudentSortType == StudentSort.GroupAscending)
				students = from s in students orderby s.GroupName ascending select s;
			else if (StudentSortType == StudentSort.GroupDescending)
				students = from s in students orderby s.GroupName descending select s;
			else if (StudentSortType == StudentSort.NameAscending)
				students = from s in students orderby s.LastName ascending select s;
			else
				students = from s in students orderby s.LastName descending select s;

			// Insert into Listbox
			StudentListBox.Items.Clear();
			foreach (var s in students)
			{
				bool studentExists = SelectedExam != null &&
					(from st in SelectedExam.Students
					 where st.FirstName == s.FirstName && st.LastName == s.LastName && st.GroupName == s.GroupName
					 select st).Count() > 0;
				StudentListBox.Items.Add(string.Format("{0}{1}, {2} in {3}", studentExists ? "*" : "", s.LastName, s.FirstName, s.GroupName));
			}
			FilteredStudents = students.ToList();
		}
		private void StudentListBox_SelectedValueChanged(object sender, EventArgs e)
		{
			if (Students == null || StudentListBox.SelectedIndex < 0) return;

			// Show warning for duplicate students
			DuplicateStudentWarningLabel.Visible = StudentListBox.SelectedItem != null && 
				StudentListBox.SelectedItem.ToString().StartsWith("*");

			// Update values anyway
			StudentFirstNameEdit.Text = FilteredStudents[StudentListBox.SelectedIndex].FirstName;
			StudentLastNameEdit.Text = FilteredStudents[StudentListBox.SelectedIndex].LastName;
			StudentClassEdit.Text = FilteredStudents[StudentListBox.SelectedIndex].GroupName;
			
		}
		private void StudentFirstNameEdit_TextChanged(object sender, EventArgs e)
		{
			if (StudentFirstNameEdit.Text != String.Empty &&
				StudentLastNameEdit.Text != String.Empty)
			{
				if (SelectedStudent == null)
					SelectedStudent = new Student(StudentFirstNameEdit.Text, StudentLastNameEdit.Text, StudentClassEdit.Text);
				else
				{
					SelectedStudent.FirstName = StudentFirstNameEdit.Text;
					SelectedStudent.LastName = StudentLastNameEdit.Text;
					SelectedStudent.GroupName = StudentClassEdit.Text;
				}
			}

			if (SelectedStudent != null)
				StudentNameLabel.Text = SelectedStudent.FullName;

			LoadImageBtn.Enabled = (SelectedExam != null && SelectedStudent != null);
		}
		private void SortStudentsByNameAscBtn_Click(object sender, EventArgs e)
		{
			StudentSortType = StudentSort.NameAscending;
			StudentFilterEdit_TextChanged(sender, e);
		}
		private void SortStudentsByNameDescBtn_Click(object sender, EventArgs e)
		{
			StudentSortType = StudentSort.NameDescending;
			StudentFilterEdit_TextChanged(sender, e);
		}
		private void SortStudentsByGroupAscBtn_Click(object sender, EventArgs e)
		{
			StudentSortType = StudentSort.GroupAscending;
			StudentFilterEdit_TextChanged(sender, e);
		}
		private void SortStudentsByGroupDescBtn_Click(object sender, EventArgs e)
		{
			StudentSortType = StudentSort.GroupDescending;
			StudentFilterEdit_TextChanged(sender, e);
		}
		private void StudentListBox_DoubleClick(object sender, EventArgs e)
		{
			// Student Value Changed already called, so shortcut to LoadImage
			StudentListBox.Enabled = false;
			LoadImageBtn_Click(null, null);
			StudentListBox.Enabled = true;
		}

		// -- Analyze / Load Scan -----------------------------------------------------------------
		private void LoadImageBtn_Click(object sender, EventArgs e)
		{
			if (EditMode)
				throw new Exception("LoadImage called when in EditMode!");

			// Load the Image
			SaveExamScanOptions();
			Scan.LoadImage();

			// Update Image to corrected image in Scan
			ClearImage();
				
			// Update Zoom to fit new smaller image
			ImgView.ImageBox.ZoomToFit();

			// Circle the found corners
			DrawCorners();

			// Update the Stats labels for data awareness
			UpdateStatsAfterScan();

			// Check for errors
			if (!Scan.Loaded || Scan.LoadedError)
			{
				// Show Analyze Image Page if we had problems
				if (!tabControl1.TabPages.Contains(AnalyzeImagePage))
					tabControl1.TabPages.Add(AnalyzeImagePage);
				tabControl1.SelectedTab = AnalyzeImagePage;
			}
			else
			{
				//if (EditMode)
				//{
				//	// Remove the Analyze Image page if we loaded correctly
				//	if (tabControl1.TabPages.Contains(AnalyzeImagePage))
				//		tabControl1.TabPages.Remove(AnalyzeImagePage);
				//}

				// Proceed with showing the student scores
				DrawQuestionsAndScoreStudent();
				ShowAnswers();
				UpdateStudentGradeLabels();

				// Allow the test to be marked up
				MarkUpTestBtn.Enabled = true;

				//if (!EditMode)
				//{
				//	// Decide where to go to next
				//	if (string.IsNullOrWhiteSpace(SelectedStudent.FirstName) || string.IsNullOrWhiteSpace(SelectedStudent.LastName))
				//	{
				//		// We still need to select a student, so go there
				//		tabControl1.SelectedTab = SelectStudentPage;
				//	}
				//	else
				//	{
				//		// Do we need to review the score?  Check if any score is below the threshold 
				//		foreach (Answer ans in SelectedStudent.Score.Answers)
				//		{
				//			if (ans.GetConfidence() < Properties.Settings.Default.MinimumConfidenceForExamAnswer)
				//			{
				//				// Show the Question Tab
				//				tabControl1.SelectedTab = CheckAnswersPage;
				//				return;
				//			}
				//		}

				//		// If we reach here, all Questions were pretty well figured out, so skip to Test Markup
				//		tabControl1.SelectedTab = FinishedPage;
				//		MarkUpTestBtn_Click(null, null);
				//	}
				//}
			}			
		}
		private void DrawCorners()
		{
			using (Graphics g = Graphics.FromImage(ImgView.Image))
			{
				Pen pen = new Pen(Brushes.MediumBlue, 1);

				int SquareSizeX = (int)(ExamForm.SquareSize * Scan.ScaleX);
				int SquareSizeY = (int)(ExamForm.SquareSize * Scan.ScaleY);
				int offsetX = SquareSizeX / 2;
				int offsetY = SquareSizeY / 2;
				g.DrawEllipse(pen, Scan.TopLeftCorner.X - offsetX, Scan.TopLeftCorner.Y - offsetY, 
					SquareSizeX, SquareSizeY);
				g.DrawEllipse(pen, Scan.TopRightCorner.X - offsetX, Scan.TopRightCorner.Y - offsetY,
					SquareSizeX, SquareSizeY);
				g.DrawEllipse(pen, Scan.BottomLeftCorner.X - offsetX, Scan.BottomLeftCorner.Y - offsetY,
					SquareSizeX, SquareSizeY);
				g.DrawEllipse(pen, Scan.BottomRightCorner.X - offsetX, Scan.BottomRightCorner.Y - offsetY,
					SquareSizeX, SquareSizeY);
			}
			ImgView.Invalidate();
		}
		private void UpdateStatsAfterScan()
		{
			// Display Stats
			LoadImageErrorLabel.Text = Scan.ErrorStr;
			StatTopLeftCorner.Text = String.Format("({0}, {1})", Scan.TopLeftCorner.X, Scan.TopLeftCorner.Y);
			StatTopRightCorner.Text = String.Format("({0}, {1})", Scan.TopRightCorner.X, Scan.TopRightCorner.Y);
			StatBottomLeftCorner.Text = String.Format("({0}, {1})", Scan.BottomLeftCorner.X, Scan.BottomLeftCorner.Y);
			StatBottomRightCorner.Text = String.Format("({0}, {1})", Scan.BottomRightCorner.X, Scan.BottomRightCorner.Y);
			StatSkewAngle.Text = string.Format("{0:F2} deg", Scan.SkewAngle);
			StatScaleX.Text = string.Format("1:{0:F2}", Scan.ScaleX);
			StatScaleY.Text = string.Format("1:{0:F2}", Scan.ScaleY);
		}
		private void AnalyzeImageBtn_DebugStep(object sender, EventArgs e)
		{
			if (sender is Button)
			{
				// --- Disable Buttons to Show Progress
				LoadImageBtn.Enabled = false;
				AnalyzeImageBtnStep0.Enabled = false;
				AnalyzeImageBtnStep1.Enabled = false;
				AnalyzeImageBtnStep2.Enabled = false;
				AnalyzeImageBtnStep3.Enabled = false;
				AnalyzeImageBtnStep4.Enabled = false;
				AnalyzeImageBtnStep5.Enabled = false;
				AnalyzeImageBtnStep6.Enabled = false;

				// --- Load any changed ExamScan Options
				SaveExamScanOptions();

				// --- Get the Step Number
				int stepNum = (int)(sender as Button).Tag;

				// --- Get rid of the Image we're showing right now
				ImgView.Image?.Dispose();

				// --- Execute the Step
				if (stepNum == 0)
				{
					// Just show the Original Image
					ImgView.Image = (Bitmap)Scan.OriginalImage.Clone();
				}
				else
				{
					// Debug to Step
					Scan.LoadImage(stepNum);

					// Show the new Image
					ImgView.Image = Scan.Image;

					// Update the Stat Labels, maybe something changed
					UpdateStatsAfterScan();
				}

				// --- Fit the new Image
				ImgView.ImageBox.ZoomToFit();

				// --- Re-enable Buttons to Show Progress
				LoadImageBtn.Enabled = true;
				AnalyzeImageBtnStep0.Enabled = true;
				AnalyzeImageBtnStep1.Enabled = true;
				AnalyzeImageBtnStep2.Enabled = true;
				AnalyzeImageBtnStep3.Enabled = true;
				AnalyzeImageBtnStep4.Enabled = true;
				AnalyzeImageBtnStep5.Enabled = true;
				AnalyzeImageBtnStep6.Enabled = true;

			}
		} 
		private void OIffsetYMinusBtn_Click(object sender, EventArgs e)
		{
			if (Scan.Loaded)
			{
				Scan.OffsetY--;

				Scan.LoadImage();
				ClearImage();
				UpdateStatsAfterScan();
				DrawQuestionsAndScoreStudent();
				ShowAnswers();
				UpdateStudentGradeLabels();
			}
		}
		private void OffsetYPlusBtn_Click(object sender, EventArgs e)
		{
			if (Scan.Loaded)
			{
				Scan.OffsetY++;

				Scan.LoadImage();
				ClearImage();
				UpdateStatsAfterScan();
				DrawQuestionsAndScoreStudent();
				ShowAnswers();
				UpdateStudentGradeLabels();
			}
		}
		private void OffsetXPlusBtn_Click(object sender, EventArgs e)
		{
			if (Scan.Loaded)
			{
				Scan.OffsetX++;

				Scan.LoadImage();
				ClearImage();
				UpdateStatsAfterScan();
				DrawQuestionsAndScoreStudent();
				ShowAnswers();
				UpdateStudentGradeLabels();
			}
		}
		private void OffsetXMinusBtn_Click(object sender, EventArgs e)
		{
			if (Scan.Loaded)
			{
				Scan.OffsetX--;

				Scan.LoadImage();
				ClearImage();
				UpdateStatsAfterScan();
				DrawQuestionsAndScoreStudent();
				ShowAnswers();
				UpdateStudentGradeLabels();
			}
		}
		private void DrawQuestionsAndScoreStudent()
		{
			Graphics g = Graphics.FromImage(ImgView.Image);

			SelectedStudent.Scan = Scan;
			if (!EditMode)
				SelectedStudent.Score.Answers = new List<Answer>();

			foreach (var q in SelectedExam.Form.Questions)
			{
				if (q.Number > SelectedExam.AnswerKey.Answers.Count)
					continue;

				Answer ans;
				if (!EditMode)
				{
					ans = Scan.FindAnswer(q);
					ans.Number = q.Number;
					SelectedStudent.Score.Answers.Add(ans);
				}
				else
					ans = SelectedStudent.Score.Answers[q.Number - 1];

				// Inidicate answer position
				int growSize = 0;
				Pen p = new Pen(Color.IndianRed, 1.0f);
				Rectangle rect = new Rectangle(
					Math.Max(0, Scan.QuestionLocations[q.Number].X - growSize),
					Math.Max(0, Scan.QuestionLocations[q.Number].Y - growSize),
					Math.Min(Scan.Image.Width, Scan.QuestionLocations[q.Number].Width) + growSize,
					Math.Min(Scan.Image.Height, Scan.QuestionLocations[q.Number].Height) + growSize);
				g.DrawRectangle(p, rect);

				// draw bubble answer                
				Pen bP = new Pen(Color.ForestGreen, 1f);
				for (int bubble = 0; bubble < q.NumberOfBubbles; bubble++)
				{
					if (ans[bubble])
					{
						float bubbleWidth = ((float)Scan.QuestionLocations[q.Number].Width / (float)q.NumberOfBubbles);
						float x = Scan.QuestionLocations[q.Number].X + (bubble * bubbleWidth);

						RectangleF r = new RectangleF(
							Math.Max(0, x), Math.Max(0, Scan.QuestionLocations[q.Number].Y),
							bubbleWidth, Scan.QuestionLocations[q.Number].Height);

						g.DrawRectangle(bP, r.X, r.Y, r.Width, r.Height);

						// Draw Answer
						Font font = new Font("Calibri", 11);
						PointF point = new PointF(x, Scan.QuestionLocations[q.Number].Y - 16);
						string Letter;

						switch (bubble)
						{
							case 0:
								Letter = "A";
								break;
							case 1:
								Letter = "B";
								break;
							case 2:
								Letter = "C";
								break;
							case 3:
								Letter = "D";
								break;
							case 4:
								Letter = "E";
								break;
							default:
								Letter = "?";
								break;
						}

						g.DrawString(Letter, font, new SolidBrush(Color.ForestGreen), point);
					}
				}
			}
			g.Dispose();
			ImgView.Refresh();
		}

		// -- Check Answers -----------------------------------------------------------------------
		private void ShowAnswers()
		{
			// Grade the test and Populate the answers grid on the Check Answers tab
			SelectedStudent.Score.QuestionCorrect.Clear();            

			if (SelectedStudent.Score.Answers.Count < SelectedExam.AnswerKey.Answers.Count)
			{
				// This shouldn't happen, so throw an error now.  We can fix it if it does happen in production
				throw new Exception("Not Enough Answers when grading the Exam!");
			}

			// Clear the grid
			StudentScoreGrid.Rows.Clear();

			for (int i = 0; i < SelectedExam.AnswerKey.Answers.Count; ++i)
			{
				var CorrectAns = SelectedExam.AnswerKey.Answers[i];
				var StudentAns = SelectedStudent.Score.Answers[i];
				SelectedStudent.Score.QuestionCorrect.Add(AnsIsCorrect(CorrectAns, StudentAns));

				DataGridViewRow row = new DataGridViewRow();

				// Highlight row if no answer found
				Color rowHighlight;
				if  (StudentAns.IsBlank() || 
					(from a in StudentAns.Data where a == true select a).Count() > 1 || 
					StudentAns.GetConfidence() < Properties.Settings.Default.MinimumConfidenceForExamAnswer)
					rowHighlight = Color.LightSalmon;
				else
					rowHighlight = Color.White;

				// Create Number Grid
				var cell = new DataGridViewTextBoxCell();
				cell.Value = StudentAns.Number.ToString();
				cell.Style.BackColor = rowHighlight;
				row.Cells.Add(cell);

				// Create Correct Answer Cell
				var cell1 = new DataGridViewTextBoxCell();
				cell1.Value = new Answer(CorrectAns).GetAsString();
				cell1.Style.BackColor = rowHighlight;
				row.Cells.Add(cell1);

				// Create Student Answer Cell
				var cell2 = new DataGridViewComboBoxCell();
				cell2.DataSource = new List<string>() { "A", "B", "C", "D", "E", "?" };
				cell2.ValueType = typeof(string);
				cell2.Value = StudentAns.GetAsString();
				cell2.Style.BackColor = rowHighlight;
				row.Cells.Add(cell2);

				// Create Confidence % cell
				var cell3 = new DataGridViewTextBoxCell();
				cell3.Value = string.Format("{0:F1}%", (StudentAns.GetConfidence() * 100.0d));
				cell3.Style.BackColor = rowHighlight;
				row.Cells.Add(cell3);

				StudentScoreGrid.Rows.Add(row);
			}
		}
		private void UpdateAnswerGrid()
		{
			for (int i = 0; i < StudentScoreGrid.Rows.Count; i++)
			{
				var CorrectAns = SelectedExam.AnswerKey.Answers[i];
				var StudentAns = SelectedStudent.Score.Answers[i];

				// Highlight row if no answer found
				Color rowHighlight;
				if (StudentAns.IsBlank() ||
					(from a in StudentAns.Data where a == true select a).Count() > 1 ||
					StudentAns.GetConfidence() < Properties.Settings.Default.MinimumConfidenceForExamAnswer)
					rowHighlight = Color.LightSalmon;
				else
					rowHighlight = StudentScoreGrid.DefaultCellStyle.BackColor;

				// 0 -> Number
				StudentScoreGrid.Rows[i].Cells[0].Value = StudentAns.Number.ToString();
				StudentScoreGrid.Rows[i].Cells[0].Style.BackColor = rowHighlight;

				// 1 -> Correct Answer
				StudentScoreGrid.Rows[i].Cells[1].Value = new Answer(CorrectAns).GetAsString();
				StudentScoreGrid.Rows[i].Cells[1].Style.BackColor = rowHighlight;

				// 2 -> Student Answer
				StudentScoreGrid.Rows[i].Cells[2].Value = StudentAns.GetAsString();
				StudentScoreGrid.Rows[i].Cells[2].Style.BackColor = rowHighlight;

				// 3 -> Answer Confidence
				StudentScoreGrid.Rows[i].Cells[3].Value = string.Format("{0:F1}%", (StudentAns.GetConfidence() * 100.0d));
				StudentScoreGrid.Rows[i].Cells[3].Style.BackColor = rowHighlight;
			}
		}
		private bool AnsIsCorrect(List<bool> key, Answer ans)
		{
			return  key[0] == ans[0] &&
					key[1] == ans[1] &&
					key[2] == ans[2] &&
					key[3] == ans[3] &&
					key[4] == ans[4];
		}
		private void UpdateStudentGradeLabels()
		{
			float total = SelectedExam.AnswerKey.Answers.Count;
			float correct = (from q in SelectedStudent.Score.QuestionCorrect
							 where q == true
							 select q).Count();

			float percent = (correct / total) * 100.0f;

			ScorePercentLabel.Text = string.Format("{0:F1}%", percent);
			ScoreNumberRightLabel.Text = string.Format("{0} / {1}", correct, total);
		}
		private void StudentScoreGrid_SelectionChanged(object sender, EventArgs e)
		{
			if (StudentScoreGrid.SelectedCells.Count > 0 && StudentScoreGrid.SelectedCells[0].RowIndex != FocusedQuestion)
			{
				FocusedQuestion = StudentScoreGrid.SelectedCells[0].RowIndex;
				ClearImage();

				float Expand = 40f;
				ImgView.ImageBox.ZoomToRegion(SelectedExam.Form.Questions[FocusedQuestion].Location.X - Expand,
					SelectedExam.Form.Questions[FocusedQuestion].Location.Y - Expand,
					SelectedExam.Form.Questions[FocusedQuestion].Location.Width + (Expand * 2),
					SelectedExam.Form.Questions[FocusedQuestion].Location.Height + (Expand * 2));

				
				// Draw the Student Answer
				Answer studentAnswer = new Answer(StudentScoreGrid.Rows[FocusedQuestion].Cells[2].Value.ToString());
				Answer correctAnswer = new Answer(StudentScoreGrid.Rows[FocusedQuestion].Cells[1].Value.ToString());
				bool correct = correctAnswer.GetAsInt() == studentAnswer.GetAsInt();

				using (Graphics g = Graphics.FromImage(ImgView.Image))
				{
					// Dont draw the answer if it is blank
					if (!studentAnswer.IsBlank())
						SelectedExam.Form.MarkupEngine.DrawIncorrectAnswer(g, SelectedStudent, SelectedExam.Form.Questions[FocusedQuestion], studentAnswer);
					
					// Always draw the correct answers
					SelectedExam.Form.MarkupEngine.DrawCorrectAnswer(g, SelectedStudent, SelectedExam.Form.Questions[FocusedQuestion], correctAnswer);

					// Mark the Number if right or wrong
					SelectedExam.Form.MarkupEngine.MarkAnswer(g, SelectedExam, SelectedStudent, SelectedExam.Form.Questions[FocusedQuestion].Number - 1);
				}
				ImgView.Refresh();
			}
		}
		private void StudentScoreGrid_KeyDown(object sender, KeyEventArgs e)
		{
			if (StudentScoreGrid.SelectedCells.Count < 1) return;

			if (e.KeyCode == Keys.A)
				StudentScoreGrid.Rows[StudentScoreGrid.SelectedCells[0].RowIndex].Cells[2].Value = "A";
			else if (e.KeyCode == Keys.B)
				StudentScoreGrid.Rows[StudentScoreGrid.SelectedCells[0].RowIndex].Cells[2].Value = "B";
			else if (e.KeyCode == Keys.C)
				StudentScoreGrid.Rows[StudentScoreGrid.SelectedCells[0].RowIndex].Cells[2].Value = "C";
			else if (e.KeyCode == Keys.D)
				StudentScoreGrid.Rows[StudentScoreGrid.SelectedCells[0].RowIndex].Cells[2].Value = "D";
			else if (e.KeyCode == Keys.E)
				StudentScoreGrid.Rows[StudentScoreGrid.SelectedCells[0].RowIndex].Cells[2].Value = "E";
		}
		private void StudentScoreGrid_CellValueChanged(object sender, DataGridViewCellEventArgs e)
		{
			if (FocusedQuestion > -1)
			{
				// Update Student Answer
				UpdateStudentAnswer(FocusedQuestion, StudentScoreGrid.Rows[FocusedQuestion].Cells[2].Value.ToString());
				UpdateStudentGradeLabels();
				UpdateAnswerGrid();

				ClearImage();

				// Draw the Student Answer
				Answer studentAnswer = new Answer(StudentScoreGrid.Rows[FocusedQuestion].Cells[2].Value.ToString());
				Answer correctAnswer = new Answer(StudentScoreGrid.Rows[FocusedQuestion].Cells[1].Value.ToString());

				using (Graphics g = Graphics.FromImage(ImgView.Image))
				{
					// Draw the correct answer last, so it will cover the student answer in green if correct
					SelectedExam.Form.MarkupEngine.DrawIncorrectAnswer(g, SelectedStudent, SelectedExam.Form.Questions[FocusedQuestion], studentAnswer);
					SelectedExam.Form.MarkupEngine.DrawCorrectAnswer(g, SelectedStudent, SelectedExam.Form.Questions[FocusedQuestion], correctAnswer);
					// Mark the Number if right or wrong
					SelectedExam.Form.MarkupEngine.MarkAnswer(g, SelectedExam, SelectedStudent, SelectedExam.Form.Questions[FocusedQuestion].Number - 1);
				}
				ImgView.Refresh();
			}
		}
		private void StudentScoreGrid_CurrentCellDirtyStateChanged(object sender, EventArgs e)
		{
			StudentScoreGrid.CommitEdit(DataGridViewDataErrorContexts.Commit);
		}
		private void UpdateStudentAnswer(int Question, string answer)
		{
			Answer ans = new Answer(answer);
			ans.Number = Question + 1;

			SelectedStudent.Score.Answers[Question] = ans;
			SelectedStudent.Score.QuestionCorrect[Question] = AnsIsCorrect(SelectedExam.AnswerKey.Answers[Question], SelectedStudent.Score.Answers[Question]);
		}

		// -- Finish and Save ---------------------------------------------------------------------
		private void MarkUpTestBtn_Click(object sender, EventArgs e)
		{
			// Check if Valid Student
			if (string.IsNullOrWhiteSpace(SelectedStudent.FirstName) || string.IsNullOrWhiteSpace(SelectedStudent.LastName))
			{
				MessageBox.Show("Student's First and Last Name cannot be empty to Mark Up Test.\nPlease select or enter a valid student", "Incorrect Student", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				tabControl1.SelectedTab = SelectStudentPage;
				return;
			}

			ClearImage();			

			if (SelectedExam.Form.MarkupEngine == null)
				SelectedExam.Form.MarkupEngine = new ExamMarkup();  // If no engine exists, load the default one

			// Perform the Markup with the Markup Engine
			using (Graphics g = Graphics.FromImage(ImgView.Image))
			{
				SelectedExam.Form.MarkupEngine.MarkUpScan(g, SelectedExam, SelectedStudent);
			}

			ImgView.Refresh();
			ImgView.ImageBox.ZoomToFit();

			PrintTestBtn.Enabled = true;
			SaveToExamBtn.Enabled = true;
		}
		private void PrintTestBtn_Click(object sender, EventArgs e)
		{
			// Create a document  for printing
			var document = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 25, 25, 25, 25);

			string fileName = "ToPrint.pdf";
			var output = new FileStream(fileName, FileMode.Create);
			var writer = iTextSharp.text.pdf.PdfWriter.GetInstance(document, output);
			document.Open();

			// Get the Scan
			Bitmap bmp = new Bitmap(Scan.Image);
			// Mark it up
			using (Graphics g = Graphics.FromImage(bmp))
				SelectedExam.Form.MarkupEngine.MarkUpScan(g, SelectedExam, SelectedStudent);

			iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(bmp, System.Drawing.Imaging.ImageFormat.Png);
			img.ScaleToFit(document.PageSize.Width - 10, document.PageSize.Height - 10);
			img.Alignment = iTextSharp.text.Element.ALIGN_CENTER;
			document.Add(img);

			output.Flush();
			document.Close();
			writer.Close();

			// Open the file afterwards
			System.Diagnostics.Process.Start(fileName);
		}
		private void SaveToExamBtn_Click(object sender, EventArgs e)
		{
			// Logic moved to DoneBtn_Click	
		}
		private void DoneBtn_Click(object sender, EventArgs e)
		{
			if (!EditMode)
			{
				// Check if Valid Student
				if (string.IsNullOrWhiteSpace(SelectedStudent.FirstName) || string.IsNullOrWhiteSpace(SelectedStudent.LastName))
				{
					MessageBox.Show("Student's First and Last Name cannot be empty to Mark Up Test.\nPlease select or enter a valid student", "Incorrect Student", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
					if (!tabControl1.TabPages.Contains(SelectStudentPage))
						tabControl1.TabPages.Add(SelectStudentPage);
					tabControl1.SelectedTab = SelectStudentPage;
					return;
				}

				// Save the Student to the Exam
				SelectedStudent.Scan = Scan;
				SelectedExam.Students.Add(SelectedStudent);

				// Save the Exam
				SelectedExam.SaveToFile("Exams");   // TODO move to Database            
			}

			// Allow close
			TestWasGradedAndSaved = true;
			DialogResult = DialogResult.OK;
			Close();
		}

		
	}
}

﻿namespace alpha
{
	partial class GradeExamsForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			// Do not release the scan			
			//if (Scan != null)
			//	Scan.Dispose();
			if (TestImage != null)
				TestImage.Dispose();

			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GradeExamsForm));
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.AnalyzeImageBtnStep2 = new System.Windows.Forms.Button();
			this.OptBlankBubbleFillRatio = new System.Windows.Forms.TextBox();
			this.OptFindAnswersHistogramPasses = new System.Windows.Forms.TextBox();
			this.OptIsDarkRatio = new System.Windows.Forms.TextBox();
			this.OptCornerMatchingConfidence = new System.Windows.Forms.TextBox();
			this.OptSelectOnlyOneAnswer = new System.Windows.Forms.CheckBox();
			this.AnalyzeImageBtnStep6 = new System.Windows.Forms.Button();
			this.AnalyzeImageBtnStep5 = new System.Windows.Forms.Button();
			this.AnalyzeImageBtnStep4 = new System.Windows.Forms.Button();
			this.AnalyzeImageBtnStep3 = new System.Windows.Forms.Button();
			this.AnalyzeImageBtnStep1 = new System.Windows.Forms.Button();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.SelectExamPage = new System.Windows.Forms.TabPage();
			this.panel2 = new System.Windows.Forms.Panel();
			this.FilterExamsBox = new System.Windows.Forms.TextBox();
			this.label17 = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.ExamNameLabel = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.ExamListBox = new System.Windows.Forms.ListBox();
			this.SelectStudentPage = new System.Windows.Forms.TabPage();
			this.DuplicateStudentWarningLabel = new System.Windows.Forms.Label();
			this.groupBox9 = new System.Windows.Forms.GroupBox();
			this.StudentListBox = new System.Windows.Forms.ListBox();
			this.SortStudentsByClassDescBtn = new System.Windows.Forms.Button();
			this.SortStudentsByNameDescBtn = new System.Windows.Forms.Button();
			this.SortStudentsByClassAscBtn = new System.Windows.Forms.Button();
			this.SortStudentsByNameAscBtn = new System.Windows.Forms.Button();
			this.StudentFilterEdit = new System.Windows.Forms.TextBox();
			this.label15 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label14 = new System.Windows.Forms.Label();
			this.StudentClassEdit = new System.Windows.Forms.TextBox();
			this.StudentLastNameEdit = new System.Windows.Forms.TextBox();
			this.StudentFirstNameEdit = new System.Windows.Forms.TextBox();
			this.label13 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.StudentNameLabel = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.AnalyzeImagePage = new System.Windows.Forms.TabPage();
			this.groupBox10 = new System.Windows.Forms.GroupBox();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.label21 = new System.Windows.Forms.Label();
			this.label20 = new System.Windows.Forms.Label();
			this.label19 = new System.Windows.Forms.Label();
			this.CornerMatchingConfidence = new System.Windows.Forms.Label();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.StatScaleY = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.StatScaleX = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.StatSkewAngle = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.StatBottomRightCorner = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.StatBottomLeftCorner = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.StatTopRightCorner = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.StatTopLeftCorner = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.LoadImageErrorLabel = new System.Windows.Forms.Label();
			this.LoadImageBtn = new System.Windows.Forms.Button();
			this.CheckAnswersPage = new System.Windows.Forms.TabPage();
			this.groupBox6 = new System.Windows.Forms.GroupBox();
			this.StudentScoreGrid = new System.Windows.Forms.DataGridView();
			this.QuestionNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.AnswerKey = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.StudentAnswer = new System.Windows.Forms.DataGridViewComboBoxColumn();
			this.Confidence = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.groupBox5 = new System.Windows.Forms.GroupBox();
			this.ScoreNumberRightLabel = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.ScorePercentLabel = new System.Windows.Forms.Label();
			this.FinishedPage = new System.Windows.Forms.TabPage();
			this.groupBox8 = new System.Windows.Forms.GroupBox();
			this.SaveToExamBtn = new System.Windows.Forms.Button();
			this.groupBox7 = new System.Windows.Forms.GroupBox();
			this.PrintTestBtn = new System.Windows.Forms.Button();
			this.MarkUpTestBtn = new System.Windows.Forms.Button();
			this.panel3 = new System.Windows.Forms.Panel();
			this.CancelBtn = new System.Windows.Forms.Button();
			this.DoneBtn = new System.Windows.Forms.Button();
			this.AnalyzeImageBtnStep0 = new System.Windows.Forms.Button();
			this.OptFindBubbleLocation = new System.Windows.Forms.CheckBox();
			this.ImgView = new alpha.Components.ImageViewer();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.SelectExamPage.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panel1.SuspendLayout();
			this.SelectStudentPage.SuspendLayout();
			this.groupBox9.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.AnalyzeImagePage.SuspendLayout();
			this.groupBox10.SuspendLayout();
			this.groupBox4.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.CheckAnswersPage.SuspendLayout();
			this.groupBox6.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.StudentScoreGrid)).BeginInit();
			this.groupBox5.SuspendLayout();
			this.FinishedPage.SuspendLayout();
			this.groupBox8.SuspendLayout();
			this.groupBox7.SuspendLayout();
			this.panel3.SuspendLayout();
			this.SuspendLayout();
			// 
			// AnalyzeImageBtnStep2
			// 
			this.AnalyzeImageBtnStep2.Dock = System.Windows.Forms.DockStyle.Top;
			this.AnalyzeImageBtnStep2.Location = new System.Drawing.Point(2, 61);
			this.AnalyzeImageBtnStep2.Name = "AnalyzeImageBtnStep2";
			this.AnalyzeImageBtnStep2.Size = new System.Drawing.Size(339, 23);
			this.AnalyzeImageBtnStep2.TabIndex = 2;
			this.AnalyzeImageBtnStep2.Tag = "2";
			this.AnalyzeImageBtnStep2.Text = "Step 2 - Deskew Image ";
			this.toolTip1.SetToolTip(this.AnalyzeImageBtnStep2, "Find any Skew Angle from the Scanning Process and Correct it, if it is severe eno" +
		"ugh.\\nThisStepCauses a Loss of Information.");
			this.AnalyzeImageBtnStep2.UseVisualStyleBackColor = true;
			this.AnalyzeImageBtnStep2.Click += new System.EventHandler(this.AnalyzeImageBtn_DebugStep);
			// 
			// OptBlankBubbleFillRatio
			// 
			this.OptBlankBubbleFillRatio.Location = new System.Drawing.Point(6, 94);
			this.OptBlankBubbleFillRatio.Name = "OptBlankBubbleFillRatio";
			this.OptBlankBubbleFillRatio.Size = new System.Drawing.Size(54, 20);
			this.OptBlankBubbleFillRatio.TabIndex = 7;
			this.toolTip1.SetToolTip(this.OptBlankBubbleFillRatio, "Bubbles must be at least 1/? filled");
			// 
			// OptFindAnswersHistogramPasses
			// 
			this.OptFindAnswersHistogramPasses.Location = new System.Drawing.Point(6, 68);
			this.OptFindAnswersHistogramPasses.Name = "OptFindAnswersHistogramPasses";
			this.OptFindAnswersHistogramPasses.Size = new System.Drawing.Size(54, 20);
			this.OptFindAnswersHistogramPasses.TabIndex = 5;
			this.toolTip1.SetToolTip(this.OptFindAnswersHistogramPasses, "Number of rows checked (From center row) in the bubble area.  -1 indicates all ro" +
		"ws");
			// 
			// OptIsDarkRatio
			// 
			this.OptIsDarkRatio.Location = new System.Drawing.Point(6, 42);
			this.OptIsDarkRatio.Name = "OptIsDarkRatio";
			this.OptIsDarkRatio.Size = new System.Drawing.Size(54, 20);
			this.OptIsDarkRatio.TabIndex = 3;
			this.toolTip1.SetToolTip(this.OptIsDarkRatio, "Ratio of Brightness to signal a dark pixel");
			// 
			// OptCornerMatchingConfidence
			// 
			this.OptCornerMatchingConfidence.Location = new System.Drawing.Point(6, 16);
			this.OptCornerMatchingConfidence.Name = "OptCornerMatchingConfidence";
			this.OptCornerMatchingConfidence.Size = new System.Drawing.Size(54, 20);
			this.OptCornerMatchingConfidence.TabIndex = 1;
			this.toolTip1.SetToolTip(this.OptCornerMatchingConfidence, "Ratio of confidence for Corners matching (Higher is more exact match)");
			// 
			// OptSelectOnlyOneAnswer
			// 
			this.OptSelectOnlyOneAnswer.AutoSize = true;
			this.OptSelectOnlyOneAnswer.Location = new System.Drawing.Point(6, 120);
			this.OptSelectOnlyOneAnswer.Name = "OptSelectOnlyOneAnswer";
			this.OptSelectOnlyOneAnswer.Size = new System.Drawing.Size(144, 17);
			this.OptSelectOnlyOneAnswer.TabIndex = 0;
			this.OptSelectOnlyOneAnswer.Text = "Limit to Only One Answer";
			this.toolTip1.SetToolTip(this.OptSelectOnlyOneAnswer, "If false, allow for detection of more than 1 answer");
			this.OptSelectOnlyOneAnswer.UseVisualStyleBackColor = true;
			// 
			// AnalyzeImageBtnStep6
			// 
			this.AnalyzeImageBtnStep6.Dock = System.Windows.Forms.DockStyle.Top;
			this.AnalyzeImageBtnStep6.Location = new System.Drawing.Point(2, 153);
			this.AnalyzeImageBtnStep6.Name = "AnalyzeImageBtnStep6";
			this.AnalyzeImageBtnStep6.Size = new System.Drawing.Size(339, 23);
			this.AnalyzeImageBtnStep6.TabIndex = 7;
			this.AnalyzeImageBtnStep6.Tag = "6";
			this.AnalyzeImageBtnStep6.Text = "Step 6 - Restore Informaiton";
			this.toolTip1.SetToolTip(this.AnalyzeImageBtnStep6, "This is the last step.  We\'ll find the corners and calculate if they\'re within to" +
		"lerance.  \r\nIf this step succeeds and all other parameters are okay, then we are" +
		" loaded.");
			this.AnalyzeImageBtnStep6.UseVisualStyleBackColor = true;
			this.AnalyzeImageBtnStep6.Click += new System.EventHandler(this.AnalyzeImageBtn_DebugStep);
			// 
			// AnalyzeImageBtnStep5
			// 
			this.AnalyzeImageBtnStep5.Dock = System.Windows.Forms.DockStyle.Top;
			this.AnalyzeImageBtnStep5.Location = new System.Drawing.Point(2, 130);
			this.AnalyzeImageBtnStep5.Name = "AnalyzeImageBtnStep5";
			this.AnalyzeImageBtnStep5.Size = new System.Drawing.Size(339, 23);
			this.AnalyzeImageBtnStep5.TabIndex = 5;
			this.AnalyzeImageBtnStep5.Tag = "5";
			this.AnalyzeImageBtnStep5.Text = "Step 5 - Binarify Image";
			this.toolTip1.SetToolTip(this.AnalyzeImageBtnStep5, "Make it clear which pixels count and which don\'t.  Any pixel below the IsDarkRati" +
		"o is replaced with Black and counts.  Otherwise, it is set to White.  There will" +
		" be no greys.");
			this.AnalyzeImageBtnStep5.UseVisualStyleBackColor = true;
			this.AnalyzeImageBtnStep5.Click += new System.EventHandler(this.AnalyzeImageBtn_DebugStep);
			// 
			// AnalyzeImageBtnStep4
			// 
			this.AnalyzeImageBtnStep4.Dock = System.Windows.Forms.DockStyle.Top;
			this.AnalyzeImageBtnStep4.Location = new System.Drawing.Point(2, 107);
			this.AnalyzeImageBtnStep4.Name = "AnalyzeImageBtnStep4";
			this.AnalyzeImageBtnStep4.Size = new System.Drawing.Size(339, 23);
			this.AnalyzeImageBtnStep4.TabIndex = 6;
			this.AnalyzeImageBtnStep4.Tag = "4";
			this.AnalyzeImageBtnStep4.Text = "Step 4 - Descale Image";
			this.toolTip1.SetToolTip(this.AnalyzeImageBtnStep4, "Resize the image to the same image it was before it was printed.  Also adds in th" +
		"e correct Margin spacing around the corners.\r\nThis operation causes a loss of in" +
		"formation.");
			this.AnalyzeImageBtnStep4.UseVisualStyleBackColor = true;
			this.AnalyzeImageBtnStep4.Click += new System.EventHandler(this.AnalyzeImageBtn_DebugStep);
			// 
			// AnalyzeImageBtnStep3
			// 
			this.AnalyzeImageBtnStep3.Dock = System.Windows.Forms.DockStyle.Top;
			this.AnalyzeImageBtnStep3.Location = new System.Drawing.Point(2, 84);
			this.AnalyzeImageBtnStep3.Name = "AnalyzeImageBtnStep3";
			this.AnalyzeImageBtnStep3.Size = new System.Drawing.Size(339, 23);
			this.AnalyzeImageBtnStep3.TabIndex = 3;
			this.AnalyzeImageBtnStep3.Tag = "3";
			this.AnalyzeImageBtnStep3.Text = "Step 3 - Remove Quiet Edges";
			this.toolTip1.SetToolTip(this.AnalyzeImageBtnStep3, resources.GetString("AnalyzeImageBtnStep3.ToolTip"));
			this.AnalyzeImageBtnStep3.UseVisualStyleBackColor = true;
			this.AnalyzeImageBtnStep3.Click += new System.EventHandler(this.AnalyzeImageBtn_DebugStep);
			// 
			// AnalyzeImageBtnStep1
			// 
			this.AnalyzeImageBtnStep1.Dock = System.Windows.Forms.DockStyle.Top;
			this.AnalyzeImageBtnStep1.Location = new System.Drawing.Point(2, 38);
			this.AnalyzeImageBtnStep1.Name = "AnalyzeImageBtnStep1";
			this.AnalyzeImageBtnStep1.Size = new System.Drawing.Size(339, 23);
			this.AnalyzeImageBtnStep1.TabIndex = 1;
			this.AnalyzeImageBtnStep1.Tag = "1";
			this.AnalyzeImageBtnStep1.Text = "Step 1 - Binarify And Resize Original Image";
			this.toolTip1.SetToolTip(this.AnalyzeImageBtnStep1, "Start by Making the Image smaller if it is really large, so everything goes faste" +
		"r");
			this.AnalyzeImageBtnStep1.UseVisualStyleBackColor = true;
			this.AnalyzeImageBtnStep1.Click += new System.EventHandler(this.AnalyzeImageBtn_DebugStep);
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.ImgView);
			this.splitContainer1.Panel1.Padding = new System.Windows.Forms.Padding(6);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.tabControl1);
			this.splitContainer1.Panel2.Controls.Add(this.panel3);
			this.splitContainer1.Panel2.Padding = new System.Windows.Forms.Padding(6);
			this.splitContainer1.Size = new System.Drawing.Size(1080, 849);
			this.splitContainer1.SplitterDistance = 688;
			this.splitContainer1.TabIndex = 3;
			// 
			// tabControl1
			// 
			this.tabControl1.Alignment = System.Windows.Forms.TabAlignment.Right;
			this.tabControl1.Controls.Add(this.SelectExamPage);
			this.tabControl1.Controls.Add(this.SelectStudentPage);
			this.tabControl1.Controls.Add(this.AnalyzeImagePage);
			this.tabControl1.Controls.Add(this.CheckAnswersPage);
			this.tabControl1.Controls.Add(this.FinishedPage);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.Location = new System.Drawing.Point(6, 6);
			this.tabControl1.Multiline = true;
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(376, 802);
			this.tabControl1.TabIndex = 3;
			// 
			// SelectExamPage
			// 
			this.SelectExamPage.BackColor = System.Drawing.SystemColors.Control;
			this.SelectExamPage.Controls.Add(this.panel2);
			this.SelectExamPage.Controls.Add(this.panel1);
			this.SelectExamPage.Controls.Add(this.ExamListBox);
			this.SelectExamPage.Location = new System.Drawing.Point(4, 4);
			this.SelectExamPage.Name = "SelectExamPage";
			this.SelectExamPage.Padding = new System.Windows.Forms.Padding(3);
			this.SelectExamPage.Size = new System.Drawing.Size(349, 794);
			this.SelectExamPage.TabIndex = 0;
			this.SelectExamPage.Text = "Select Exam";
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.FilterExamsBox);
			this.panel2.Controls.Add(this.label17);
			this.panel2.Location = new System.Drawing.Point(6, 48);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(326, 27);
			this.panel2.TabIndex = 4;
			// 
			// FilterExamsBox
			// 
			this.FilterExamsBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.FilterExamsBox.Location = new System.Drawing.Point(37, 3);
			this.FilterExamsBox.Name = "FilterExamsBox";
			this.FilterExamsBox.Size = new System.Drawing.Size(286, 20);
			this.FilterExamsBox.TabIndex = 3;
			this.FilterExamsBox.TextChanged += new System.EventHandler(this.FilterExamsBox_TextChanged);
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label17.Location = new System.Drawing.Point(2, 6);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(29, 13);
			this.label17.TabIndex = 2;
			this.label17.Text = "Filter";
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.ExamNameLabel);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Location = new System.Drawing.Point(6, 6);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(326, 36);
			this.panel1.TabIndex = 3;
			// 
			// ExamNameLabel
			// 
			this.ExamNameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.ExamNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ExamNameLabel.Location = new System.Drawing.Point(53, 6);
			this.ExamNameLabel.Name = "ExamNameLabel";
			this.ExamNameLabel.Size = new System.Drawing.Size(272, 23);
			this.ExamNameLabel.TabIndex = 3;
			this.ExamNameLabel.Text = "<None>";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(2, 6);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(51, 15);
			this.label1.TabIndex = 2;
			this.label1.Text = "Exam: ";
			// 
			// ExamListBox
			// 
			this.ExamListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
			| System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.ExamListBox.FormattingEnabled = true;
			this.ExamListBox.Location = new System.Drawing.Point(6, 78);
			this.ExamListBox.Name = "ExamListBox";
			this.ExamListBox.Size = new System.Drawing.Size(336, 667);
			this.ExamListBox.TabIndex = 2;
			this.ExamListBox.SelectedValueChanged += new System.EventHandler(this.ExamListBox_SelectedValueChanged);
			// 
			// SelectStudentPage
			// 
			this.SelectStudentPage.BackColor = System.Drawing.SystemColors.Control;
			this.SelectStudentPage.Controls.Add(this.DuplicateStudentWarningLabel);
			this.SelectStudentPage.Controls.Add(this.groupBox9);
			this.SelectStudentPage.Controls.Add(this.groupBox1);
			this.SelectStudentPage.Controls.Add(this.StudentNameLabel);
			this.SelectStudentPage.Controls.Add(this.label2);
			this.SelectStudentPage.Location = new System.Drawing.Point(4, 4);
			this.SelectStudentPage.Name = "SelectStudentPage";
			this.SelectStudentPage.Padding = new System.Windows.Forms.Padding(3);
			this.SelectStudentPage.Size = new System.Drawing.Size(349, 794);
			this.SelectStudentPage.TabIndex = 1;
			this.SelectStudentPage.Text = "Select Student";
			// 
			// DuplicateStudentWarningLabel
			// 
			this.DuplicateStudentWarningLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.DuplicateStudentWarningLabel.ForeColor = System.Drawing.Color.Red;
			this.DuplicateStudentWarningLabel.Location = new System.Drawing.Point(6, 29);
			this.DuplicateStudentWarningLabel.Name = "DuplicateStudentWarningLabel";
			this.DuplicateStudentWarningLabel.Size = new System.Drawing.Size(329, 50);
			this.DuplicateStudentWarningLabel.TabIndex = 4;
			this.DuplicateStudentWarningLabel.Text = "Warning! A Scan has already been entered for this student!";
			this.DuplicateStudentWarningLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.DuplicateStudentWarningLabel.Visible = false;
			// 
			// groupBox9
			// 
			this.groupBox9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox9.Controls.Add(this.StudentListBox);
			this.groupBox9.Controls.Add(this.SortStudentsByClassDescBtn);
			this.groupBox9.Controls.Add(this.SortStudentsByNameDescBtn);
			this.groupBox9.Controls.Add(this.SortStudentsByClassAscBtn);
			this.groupBox9.Controls.Add(this.SortStudentsByNameAscBtn);
			this.groupBox9.Controls.Add(this.StudentFilterEdit);
			this.groupBox9.Controls.Add(this.label15);
			this.groupBox9.Location = new System.Drawing.Point(9, 187);
			this.groupBox9.Name = "groupBox9";
			this.groupBox9.Size = new System.Drawing.Size(334, 636);
			this.groupBox9.TabIndex = 3;
			this.groupBox9.TabStop = false;
			this.groupBox9.Text = "Preloaded Students";
			// 
			// StudentListBox
			// 
			this.StudentListBox.FormattingEnabled = true;
			this.StudentListBox.Location = new System.Drawing.Point(9, 100);
			this.StudentListBox.Name = "StudentListBox";
			this.StudentListBox.Size = new System.Drawing.Size(306, 563);
			this.StudentListBox.TabIndex = 6;
			this.StudentListBox.SelectedValueChanged += new System.EventHandler(this.StudentListBox_SelectedValueChanged);
			this.StudentListBox.DoubleClick += new System.EventHandler(this.StudentListBox_DoubleClick);
			// 
			// SortStudentsByClassDescBtn
			// 
			this.SortStudentsByClassDescBtn.Location = new System.Drawing.Point(165, 72);
			this.SortStudentsByClassDescBtn.Name = "SortStudentsByClassDescBtn";
			this.SortStudentsByClassDescBtn.Size = new System.Drawing.Size(150, 23);
			this.SortStudentsByClassDescBtn.TabIndex = 5;
			this.SortStudentsByClassDescBtn.Text = "Sort By Class (Desc)";
			this.SortStudentsByClassDescBtn.UseVisualStyleBackColor = true;
			this.SortStudentsByClassDescBtn.Click += new System.EventHandler(this.SortStudentsByGroupDescBtn_Click);
			// 
			// SortStudentsByNameDescBtn
			// 
			this.SortStudentsByNameDescBtn.Location = new System.Drawing.Point(9, 71);
			this.SortStudentsByNameDescBtn.Name = "SortStudentsByNameDescBtn";
			this.SortStudentsByNameDescBtn.Size = new System.Drawing.Size(150, 23);
			this.SortStudentsByNameDescBtn.TabIndex = 4;
			this.SortStudentsByNameDescBtn.Text = "Sort By Name (Desc)";
			this.SortStudentsByNameDescBtn.UseVisualStyleBackColor = true;
			this.SortStudentsByNameDescBtn.Click += new System.EventHandler(this.SortStudentsByNameDescBtn_Click);
			// 
			// SortStudentsByClassAscBtn
			// 
			this.SortStudentsByClassAscBtn.Location = new System.Drawing.Point(165, 43);
			this.SortStudentsByClassAscBtn.Name = "SortStudentsByClassAscBtn";
			this.SortStudentsByClassAscBtn.Size = new System.Drawing.Size(150, 23);
			this.SortStudentsByClassAscBtn.TabIndex = 3;
			this.SortStudentsByClassAscBtn.Text = "Sort By Class (Asc)";
			this.SortStudentsByClassAscBtn.UseVisualStyleBackColor = true;
			this.SortStudentsByClassAscBtn.Click += new System.EventHandler(this.SortStudentsByGroupAscBtn_Click);
			// 
			// SortStudentsByNameAscBtn
			// 
			this.SortStudentsByNameAscBtn.Location = new System.Drawing.Point(9, 42);
			this.SortStudentsByNameAscBtn.Name = "SortStudentsByNameAscBtn";
			this.SortStudentsByNameAscBtn.Size = new System.Drawing.Size(150, 23);
			this.SortStudentsByNameAscBtn.TabIndex = 2;
			this.SortStudentsByNameAscBtn.Text = "Sort By Name (Asc)";
			this.SortStudentsByNameAscBtn.UseVisualStyleBackColor = true;
			this.SortStudentsByNameAscBtn.Click += new System.EventHandler(this.SortStudentsByNameAscBtn_Click);
			// 
			// StudentFilterEdit
			// 
			this.StudentFilterEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.StudentFilterEdit.Location = new System.Drawing.Point(87, 17);
			this.StudentFilterEdit.Name = "StudentFilterEdit";
			this.StudentFilterEdit.Size = new System.Drawing.Size(240, 20);
			this.StudentFilterEdit.TabIndex = 1;
			this.StudentFilterEdit.TextChanged += new System.EventHandler(this.StudentFilterEdit_TextChanged);
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Location = new System.Drawing.Point(3, 20);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(78, 13);
			this.label15.TabIndex = 0;
			this.label15.Text = "Filter By Name:";
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.label14);
			this.groupBox1.Controls.Add(this.StudentClassEdit);
			this.groupBox1.Controls.Add(this.StudentLastNameEdit);
			this.groupBox1.Controls.Add(this.StudentFirstNameEdit);
			this.groupBox1.Controls.Add(this.label13);
			this.groupBox1.Controls.Add(this.label11);
			this.groupBox1.Location = new System.Drawing.Point(9, 82);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(334, 99);
			this.groupBox1.TabIndex = 2;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Selected Student (Can Modify Manually)";
			// 
			// label14
			// 
			this.label14.Location = new System.Drawing.Point(6, 68);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(75, 13);
			this.label14.TabIndex = 5;
			this.label14.Text = "Class:";
			// 
			// StudentClassEdit
			// 
			this.StudentClassEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.StudentClassEdit.Location = new System.Drawing.Point(103, 65);
			this.StudentClassEdit.Name = "StudentClassEdit";
			this.StudentClassEdit.Size = new System.Drawing.Size(224, 20);
			this.StudentClassEdit.TabIndex = 4;
			this.StudentClassEdit.TextChanged += new System.EventHandler(this.StudentFirstNameEdit_TextChanged);
			// 
			// StudentLastNameEdit
			// 
			this.StudentLastNameEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.StudentLastNameEdit.Location = new System.Drawing.Point(103, 39);
			this.StudentLastNameEdit.Name = "StudentLastNameEdit";
			this.StudentLastNameEdit.Size = new System.Drawing.Size(224, 20);
			this.StudentLastNameEdit.TabIndex = 3;
			this.StudentLastNameEdit.TextChanged += new System.EventHandler(this.StudentFirstNameEdit_TextChanged);
			// 
			// StudentFirstNameEdit
			// 
			this.StudentFirstNameEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.StudentFirstNameEdit.Location = new System.Drawing.Point(103, 13);
			this.StudentFirstNameEdit.Name = "StudentFirstNameEdit";
			this.StudentFirstNameEdit.Size = new System.Drawing.Size(224, 20);
			this.StudentFirstNameEdit.TabIndex = 2;
			this.StudentFirstNameEdit.TextChanged += new System.EventHandler(this.StudentFirstNameEdit_TextChanged);
			// 
			// label13
			// 
			this.label13.Location = new System.Drawing.Point(6, 42);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(75, 13);
			this.label13.TabIndex = 1;
			this.label13.Text = "Last Name:";
			// 
			// label11
			// 
			this.label11.Location = new System.Drawing.Point(6, 16);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(75, 13);
			this.label11.TabIndex = 0;
			this.label11.Text = "First Name:";
			// 
			// StudentNameLabel
			// 
			this.StudentNameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.StudentNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.StudentNameLabel.Location = new System.Drawing.Point(99, 13);
			this.StudentNameLabel.Name = "StudentNameLabel";
			this.StudentNameLabel.Size = new System.Drawing.Size(244, 16);
			this.StudentNameLabel.TabIndex = 1;
			this.StudentNameLabel.Text = "<None>";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(6, 13);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(87, 13);
			this.label2.TabIndex = 0;
			this.label2.Text = "Sudent Name:";
			// 
			// AnalyzeImagePage
			// 
			this.AnalyzeImagePage.BackColor = System.Drawing.SystemColors.Control;
			this.AnalyzeImagePage.Controls.Add(this.groupBox10);
			this.AnalyzeImagePage.Controls.Add(this.groupBox4);
			this.AnalyzeImagePage.Controls.Add(this.groupBox2);
			this.AnalyzeImagePage.Controls.Add(this.groupBox3);
			this.AnalyzeImagePage.Controls.Add(this.LoadImageBtn);
			this.AnalyzeImagePage.Location = new System.Drawing.Point(4, 4);
			this.AnalyzeImagePage.Name = "AnalyzeImagePage";
			this.AnalyzeImagePage.Padding = new System.Windows.Forms.Padding(3);
			this.AnalyzeImagePage.Size = new System.Drawing.Size(349, 794);
			this.AnalyzeImagePage.TabIndex = 2;
			this.AnalyzeImagePage.Text = "Analyze Image";
			// 
			// groupBox10
			// 
			this.groupBox10.Controls.Add(this.AnalyzeImageBtnStep6);
			this.groupBox10.Controls.Add(this.AnalyzeImageBtnStep5);
			this.groupBox10.Controls.Add(this.AnalyzeImageBtnStep4);
			this.groupBox10.Controls.Add(this.AnalyzeImageBtnStep3);
			this.groupBox10.Controls.Add(this.AnalyzeImageBtnStep2);
			this.groupBox10.Controls.Add(this.AnalyzeImageBtnStep1);
			this.groupBox10.Controls.Add(this.AnalyzeImageBtnStep0);
			this.groupBox10.Dock = System.Windows.Forms.DockStyle.Top;
			this.groupBox10.Location = new System.Drawing.Point(3, 363);
			this.groupBox10.Margin = new System.Windows.Forms.Padding(2);
			this.groupBox10.Name = "groupBox10";
			this.groupBox10.Padding = new System.Windows.Forms.Padding(2);
			this.groupBox10.Size = new System.Drawing.Size(343, 180);
			this.groupBox10.TabIndex = 4;
			this.groupBox10.TabStop = false;
			this.groupBox10.Text = "Manually Step Through Loading Steps";
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add(this.OptFindBubbleLocation);
			this.groupBox4.Controls.Add(this.label21);
			this.groupBox4.Controls.Add(this.OptBlankBubbleFillRatio);
			this.groupBox4.Controls.Add(this.label20);
			this.groupBox4.Controls.Add(this.OptFindAnswersHistogramPasses);
			this.groupBox4.Controls.Add(this.label19);
			this.groupBox4.Controls.Add(this.OptIsDarkRatio);
			this.groupBox4.Controls.Add(this.CornerMatchingConfidence);
			this.groupBox4.Controls.Add(this.OptCornerMatchingConfidence);
			this.groupBox4.Controls.Add(this.OptSelectOnlyOneAnswer);
			this.groupBox4.Dock = System.Windows.Forms.DockStyle.Top;
			this.groupBox4.Location = new System.Drawing.Point(3, 199);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(343, 164);
			this.groupBox4.TabIndex = 3;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Options";
			// 
			// label21
			// 
			this.label21.AutoSize = true;
			this.label21.Location = new System.Drawing.Point(66, 97);
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size(113, 13);
			this.label21.TabIndex = 8;
			this.label21.Text = "Blank Bubble Fill Ratio";
			// 
			// label20
			// 
			this.label20.AutoSize = true;
			this.label20.Location = new System.Drawing.Point(66, 71);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(219, 13);
			this.label20.TabIndex = 6;
			this.label20.Text = "# of Passes for Find Answer (<1 == All Rows)";
			// 
			// label19
			// 
			this.label19.AutoSize = true;
			this.label19.Location = new System.Drawing.Point(66, 45);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(94, 13);
			this.label19.TabIndex = 4;
			this.label19.Text = "Pixel Is Dark Ratio";
			// 
			// CornerMatchingConfidence
			// 
			this.CornerMatchingConfidence.AutoSize = true;
			this.CornerMatchingConfidence.Location = new System.Drawing.Point(66, 19);
			this.CornerMatchingConfidence.Name = "CornerMatchingConfidence";
			this.CornerMatchingConfidence.Size = new System.Drawing.Size(142, 13);
			this.CornerMatchingConfidence.TabIndex = 2;
			this.CornerMatchingConfidence.Text = "Corner Matching Confidence";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.StatScaleY);
			this.groupBox2.Controls.Add(this.label12);
			this.groupBox2.Controls.Add(this.StatScaleX);
			this.groupBox2.Controls.Add(this.label9);
			this.groupBox2.Controls.Add(this.label7);
			this.groupBox2.Controls.Add(this.StatSkewAngle);
			this.groupBox2.Controls.Add(this.label5);
			this.groupBox2.Controls.Add(this.StatBottomRightCorner);
			this.groupBox2.Controls.Add(this.label10);
			this.groupBox2.Controls.Add(this.StatBottomLeftCorner);
			this.groupBox2.Controls.Add(this.label8);
			this.groupBox2.Controls.Add(this.StatTopRightCorner);
			this.groupBox2.Controls.Add(this.label6);
			this.groupBox2.Controls.Add(this.StatTopLeftCorner);
			this.groupBox2.Controls.Add(this.label4);
			this.groupBox2.Controls.Add(this.label3);
			this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
			this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.groupBox2.Location = new System.Drawing.Point(3, 86);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(343, 113);
			this.groupBox2.TabIndex = 1;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Stats";
			// 
			// StatScaleY
			// 
			this.StatScaleY.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.StatScaleY.Location = new System.Drawing.Point(204, 93);
			this.StatScaleY.Name = "StatScaleY";
			this.StatScaleY.Size = new System.Drawing.Size(75, 13);
			this.StatScaleY.TabIndex = 15;
			this.StatScaleY.Text = "1:1";
			// 
			// label12
			// 
			this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label12.Location = new System.Drawing.Point(168, 93);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(30, 13);
			this.label12.TabIndex = 14;
			this.label12.Text = "Y:";
			// 
			// StatScaleX
			// 
			this.StatScaleX.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.StatScaleX.Location = new System.Drawing.Point(204, 80);
			this.StatScaleX.Name = "StatScaleX";
			this.StatScaleX.Size = new System.Drawing.Size(75, 13);
			this.StatScaleX.TabIndex = 13;
			this.StatScaleX.Text = "1:1";
			// 
			// label9
			// 
			this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label9.Location = new System.Drawing.Point(168, 80);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(30, 13);
			this.label9.TabIndex = 12;
			this.label9.Text = "X:";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label7.Location = new System.Drawing.Point(168, 67);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(39, 13);
			this.label7.TabIndex = 11;
			this.label7.Text = "Scale";
			// 
			// StatSkewAngle
			// 
			this.StatSkewAngle.AutoSize = true;
			this.StatSkewAngle.Location = new System.Drawing.Point(6, 80);
			this.StatSkewAngle.Name = "StatSkewAngle";
			this.StatSkewAngle.Size = new System.Drawing.Size(14, 15);
			this.StatSkewAngle.TabIndex = 10;
			this.StatSkewAngle.Text = "0";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.Location = new System.Drawing.Point(6, 67);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(38, 13);
			this.label5.TabIndex = 9;
			this.label5.Text = "Skew";
			// 
			// StatBottomRightCorner
			// 
			this.StatBottomRightCorner.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.StatBottomRightCorner.Location = new System.Drawing.Point(249, 42);
			this.StatBottomRightCorner.Name = "StatBottomRightCorner";
			this.StatBottomRightCorner.Size = new System.Drawing.Size(75, 13);
			this.StatBottomRightCorner.TabIndex = 8;
			this.StatBottomRightCorner.Text = "(0, 0)";
			// 
			// label10
			// 
			this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label10.Location = new System.Drawing.Point(168, 42);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(75, 13);
			this.label10.TabIndex = 7;
			this.label10.Text = "Bottom Right:";
			// 
			// StatBottomLeftCorner
			// 
			this.StatBottomLeftCorner.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.StatBottomLeftCorner.Location = new System.Drawing.Point(87, 42);
			this.StatBottomLeftCorner.Name = "StatBottomLeftCorner";
			this.StatBottomLeftCorner.Size = new System.Drawing.Size(75, 13);
			this.StatBottomLeftCorner.TabIndex = 6;
			this.StatBottomLeftCorner.Text = "(0, 0)";
			// 
			// label8
			// 
			this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label8.Location = new System.Drawing.Point(6, 42);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(75, 13);
			this.label8.TabIndex = 5;
			this.label8.Text = "Bottom Left: ";
			// 
			// StatTopRightCorner
			// 
			this.StatTopRightCorner.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.StatTopRightCorner.Location = new System.Drawing.Point(249, 29);
			this.StatTopRightCorner.Name = "StatTopRightCorner";
			this.StatTopRightCorner.Size = new System.Drawing.Size(75, 13);
			this.StatTopRightCorner.TabIndex = 4;
			this.StatTopRightCorner.Text = "(0, 0)";
			// 
			// label6
			// 
			this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label6.Location = new System.Drawing.Point(168, 29);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(75, 13);
			this.label6.TabIndex = 3;
			this.label6.Text = "Top Right: ";
			// 
			// StatTopLeftCorner
			// 
			this.StatTopLeftCorner.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.StatTopLeftCorner.Location = new System.Drawing.Point(87, 29);
			this.StatTopLeftCorner.Name = "StatTopLeftCorner";
			this.StatTopLeftCorner.Size = new System.Drawing.Size(75, 13);
			this.StatTopLeftCorner.TabIndex = 2;
			this.StatTopLeftCorner.Text = "(0, 0)";
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(6, 29);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(75, 13);
			this.label4.TabIndex = 1;
			this.label4.Text = "Top Left: ";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(6, 16);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(54, 13);
			this.label3.TabIndex = 0;
			this.label3.Text = "Corners:";
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.LoadImageErrorLabel);
			this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
			this.groupBox3.Location = new System.Drawing.Point(3, 26);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(343, 60);
			this.groupBox3.TabIndex = 2;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Loading Error";
			// 
			// LoadImageErrorLabel
			// 
			this.LoadImageErrorLabel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.LoadImageErrorLabel.Location = new System.Drawing.Point(3, 16);
			this.LoadImageErrorLabel.Name = "LoadImageErrorLabel";
			this.LoadImageErrorLabel.Size = new System.Drawing.Size(337, 41);
			this.LoadImageErrorLabel.TabIndex = 0;
			// 
			// LoadImageBtn
			// 
			this.LoadImageBtn.Dock = System.Windows.Forms.DockStyle.Top;
			this.LoadImageBtn.Enabled = false;
			this.LoadImageBtn.Location = new System.Drawing.Point(3, 3);
			this.LoadImageBtn.Name = "LoadImageBtn";
			this.LoadImageBtn.Size = new System.Drawing.Size(343, 23);
			this.LoadImageBtn.TabIndex = 0;
			this.LoadImageBtn.Text = "Load Image";
			this.LoadImageBtn.UseVisualStyleBackColor = true;
			this.LoadImageBtn.Click += new System.EventHandler(this.LoadImageBtn_Click);
			// 
			// CheckAnswersPage
			// 
			this.CheckAnswersPage.BackColor = System.Drawing.SystemColors.Control;
			this.CheckAnswersPage.Controls.Add(this.groupBox6);
			this.CheckAnswersPage.Controls.Add(this.groupBox5);
			this.CheckAnswersPage.Location = new System.Drawing.Point(4, 4);
			this.CheckAnswersPage.Name = "CheckAnswersPage";
			this.CheckAnswersPage.Padding = new System.Windows.Forms.Padding(3);
			this.CheckAnswersPage.Size = new System.Drawing.Size(349, 794);
			this.CheckAnswersPage.TabIndex = 3;
			this.CheckAnswersPage.Text = "Check Answers";
			// 
			// groupBox6
			// 
			this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
			| System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox6.Controls.Add(this.StudentScoreGrid);
			this.groupBox6.Location = new System.Drawing.Point(6, 117);
			this.groupBox6.Name = "groupBox6";
			this.groupBox6.Size = new System.Drawing.Size(337, 670);
			this.groupBox6.TabIndex = 1;
			this.groupBox6.TabStop = false;
			this.groupBox6.Text = "Questions";
			// 
			// StudentScoreGrid
			// 
			this.StudentScoreGrid.AllowUserToAddRows = false;
			this.StudentScoreGrid.AllowUserToDeleteRows = false;
			this.StudentScoreGrid.AllowUserToResizeRows = false;
			this.StudentScoreGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.StudentScoreGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.StudentScoreGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
			this.QuestionNumber,
			this.AnswerKey,
			this.StudentAnswer,
			this.Confidence});
			this.StudentScoreGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.StudentScoreGrid.Location = new System.Drawing.Point(3, 16);
			this.StudentScoreGrid.MultiSelect = false;
			this.StudentScoreGrid.Name = "StudentScoreGrid";
			this.StudentScoreGrid.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.StudentScoreGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.StudentScoreGrid.ShowCellErrors = false;
			this.StudentScoreGrid.ShowCellToolTips = false;
			this.StudentScoreGrid.ShowEditingIcon = false;
			this.StudentScoreGrid.ShowRowErrors = false;
			this.StudentScoreGrid.Size = new System.Drawing.Size(331, 651);
			this.StudentScoreGrid.TabIndex = 0;
			this.StudentScoreGrid.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.StudentScoreGrid_CellValueChanged);
			this.StudentScoreGrid.CurrentCellDirtyStateChanged += new System.EventHandler(this.StudentScoreGrid_CurrentCellDirtyStateChanged);
			this.StudentScoreGrid.SelectionChanged += new System.EventHandler(this.StudentScoreGrid_SelectionChanged);
			this.StudentScoreGrid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.StudentScoreGrid_KeyDown);
			// 
			// QuestionNumber
			// 
			this.QuestionNumber.FillWeight = 15F;
			this.QuestionNumber.HeaderText = "#";
			this.QuestionNumber.Name = "QuestionNumber";
			this.QuestionNumber.ReadOnly = true;
			// 
			// AnswerKey
			// 
			this.AnswerKey.FillWeight = 25F;
			this.AnswerKey.HeaderText = "Key";
			this.AnswerKey.Name = "AnswerKey";
			this.AnswerKey.ReadOnly = true;
			// 
			// StudentAnswer
			// 
			this.StudentAnswer.FillWeight = 35F;
			this.StudentAnswer.HeaderText = "Student";
			this.StudentAnswer.Name = "StudentAnswer";
			this.StudentAnswer.ToolTipText = "The Answer the Scan read the student filled in";
			// 
			// Confidence
			// 
			this.Confidence.FillWeight = 25F;
			this.Confidence.HeaderText = "Conf %";
			this.Confidence.Name = "Confidence";
			this.Confidence.ReadOnly = true;
			// 
			// groupBox5
			// 
			this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox5.Controls.Add(this.ScoreNumberRightLabel);
			this.groupBox5.Controls.Add(this.label16);
			this.groupBox5.Controls.Add(this.ScorePercentLabel);
			this.groupBox5.Location = new System.Drawing.Point(6, 6);
			this.groupBox5.Name = "groupBox5";
			this.groupBox5.Size = new System.Drawing.Size(337, 105);
			this.groupBox5.TabIndex = 0;
			this.groupBox5.TabStop = false;
			this.groupBox5.Text = "Grade";
			// 
			// ScoreNumberRightLabel
			// 
			this.ScoreNumberRightLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.ScoreNumberRightLabel.Location = new System.Drawing.Point(121, 83);
			this.ScoreNumberRightLabel.Name = "ScoreNumberRightLabel";
			this.ScoreNumberRightLabel.Size = new System.Drawing.Size(209, 13);
			this.ScoreNumberRightLabel.TabIndex = 2;
			this.ScoreNumberRightLabel.Text = "0 / 0";
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label16.Location = new System.Drawing.Point(7, 83);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(108, 13);
			this.label16.TabIndex = 1;
			this.label16.Text = "# Right / Total #:";
			// 
			// ScorePercentLabel
			// 
			this.ScorePercentLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.ScorePercentLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ScorePercentLabel.Location = new System.Drawing.Point(6, 18);
			this.ScorePercentLabel.Name = "ScorePercentLabel";
			this.ScorePercentLabel.Size = new System.Drawing.Size(325, 45);
			this.ScorePercentLabel.TabIndex = 0;
			this.ScorePercentLabel.Text = "100%";
			this.ScorePercentLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// FinishedPage
			// 
			this.FinishedPage.BackColor = System.Drawing.SystemColors.Control;
			this.FinishedPage.Controls.Add(this.groupBox8);
			this.FinishedPage.Controls.Add(this.groupBox7);
			this.FinishedPage.Location = new System.Drawing.Point(4, 4);
			this.FinishedPage.Name = "FinishedPage";
			this.FinishedPage.Padding = new System.Windows.Forms.Padding(3);
			this.FinishedPage.Size = new System.Drawing.Size(349, 794);
			this.FinishedPage.TabIndex = 4;
			this.FinishedPage.Text = "Finished!";
			// 
			// groupBox8
			// 
			this.groupBox8.Controls.Add(this.SaveToExamBtn);
			this.groupBox8.Location = new System.Drawing.Point(6, 92);
			this.groupBox8.Name = "groupBox8";
			this.groupBox8.Size = new System.Drawing.Size(326, 49);
			this.groupBox8.TabIndex = 1;
			this.groupBox8.TabStop = false;
			this.groupBox8.Text = "And then finally...";
			this.groupBox8.Visible = false;
			// 
			// SaveToExamBtn
			// 
			this.SaveToExamBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.SaveToExamBtn.Enabled = false;
			this.SaveToExamBtn.Location = new System.Drawing.Point(6, 19);
			this.SaveToExamBtn.Name = "SaveToExamBtn";
			this.SaveToExamBtn.Size = new System.Drawing.Size(314, 23);
			this.SaveToExamBtn.TabIndex = 0;
			this.SaveToExamBtn.Text = "Save Test To Exam";
			this.SaveToExamBtn.UseVisualStyleBackColor = true;
			this.SaveToExamBtn.Click += new System.EventHandler(this.SaveToExamBtn_Click);
			// 
			// groupBox7
			// 
			this.groupBox7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox7.Controls.Add(this.PrintTestBtn);
			this.groupBox7.Controls.Add(this.MarkUpTestBtn);
			this.groupBox7.Location = new System.Drawing.Point(6, 6);
			this.groupBox7.Name = "groupBox7";
			this.groupBox7.Size = new System.Drawing.Size(337, 80);
			this.groupBox7.TabIndex = 0;
			this.groupBox7.TabStop = false;
			this.groupBox7.Text = "Mark Up Test and Print";
			// 
			// PrintTestBtn
			// 
			this.PrintTestBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.PrintTestBtn.Enabled = false;
			this.PrintTestBtn.Location = new System.Drawing.Point(6, 48);
			this.PrintTestBtn.Name = "PrintTestBtn";
			this.PrintTestBtn.Size = new System.Drawing.Size(325, 23);
			this.PrintTestBtn.TabIndex = 1;
			this.PrintTestBtn.Text = "Print Test (optional)";
			this.PrintTestBtn.UseVisualStyleBackColor = true;
			this.PrintTestBtn.Click += new System.EventHandler(this.PrintTestBtn_Click);
			// 
			// MarkUpTestBtn
			// 
			this.MarkUpTestBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
			| System.Windows.Forms.AnchorStyles.Right)));
			this.MarkUpTestBtn.Enabled = false;
			this.MarkUpTestBtn.Location = new System.Drawing.Point(6, 19);
			this.MarkUpTestBtn.Name = "MarkUpTestBtn";
			this.MarkUpTestBtn.Size = new System.Drawing.Size(325, 23);
			this.MarkUpTestBtn.TabIndex = 0;
			this.MarkUpTestBtn.Text = "Mark Up Test";
			this.MarkUpTestBtn.UseVisualStyleBackColor = true;
			this.MarkUpTestBtn.Click += new System.EventHandler(this.MarkUpTestBtn_Click);
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.CancelBtn);
			this.panel3.Controls.Add(this.DoneBtn);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel3.Location = new System.Drawing.Point(6, 808);
			this.panel3.Margin = new System.Windows.Forms.Padding(2);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(376, 35);
			this.panel3.TabIndex = 4;
			// 
			// CancelBtn
			// 
			this.CancelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.CancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.CancelBtn.Location = new System.Drawing.Point(217, 8);
			this.CancelBtn.Name = "CancelBtn";
			this.CancelBtn.Size = new System.Drawing.Size(75, 23);
			this.CancelBtn.TabIndex = 7;
			this.CancelBtn.Text = "Cancel";
			this.CancelBtn.UseVisualStyleBackColor = true;
			// 
			// DoneBtn
			// 
			this.DoneBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.DoneBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.DoneBtn.Enabled = false;
			this.DoneBtn.Location = new System.Drawing.Point(298, 8);
			this.DoneBtn.Name = "DoneBtn";
			this.DoneBtn.Size = new System.Drawing.Size(75, 23);
			this.DoneBtn.TabIndex = 6;
			this.DoneBtn.Text = "Done";
			this.DoneBtn.UseVisualStyleBackColor = true;
			this.DoneBtn.Click += new System.EventHandler(this.DoneBtn_Click);
			// 
			// AnalyzeImageBtnStep0
			// 
			this.AnalyzeImageBtnStep0.Dock = System.Windows.Forms.DockStyle.Top;
			this.AnalyzeImageBtnStep0.Location = new System.Drawing.Point(2, 15);
			this.AnalyzeImageBtnStep0.Name = "AnalyzeImageBtnStep0";
			this.AnalyzeImageBtnStep0.Size = new System.Drawing.Size(339, 23);
			this.AnalyzeImageBtnStep0.TabIndex = 8;
			this.AnalyzeImageBtnStep0.Tag = "1";
			this.AnalyzeImageBtnStep0.Text = "Step 0 - Original Image";
			this.toolTip1.SetToolTip(this.AnalyzeImageBtnStep0, "Start by Making the Image smaller if it is really large, so everything goes faste" +
		"r");
			this.AnalyzeImageBtnStep0.UseVisualStyleBackColor = true;
			this.AnalyzeImageBtnStep0.Click += new System.EventHandler(this.AnalyzeImageBtn_DebugStep);
			// 
			// OptFindBubbleLocation
			// 
			this.OptFindBubbleLocation.AutoSize = true;
			this.OptFindBubbleLocation.Location = new System.Drawing.Point(6, 143);
			this.OptFindBubbleLocation.Name = "OptFindBubbleLocation";
			this.OptFindBubbleLocation.Size = new System.Drawing.Size(201, 17);
			this.OptFindBubbleLocation.TabIndex = 9;
			this.OptFindBubbleLocation.Text = "Determine Relative Bubble Locations";
			this.toolTip1.SetToolTip(this.OptFindBubbleLocation, "If true, search for the bubbles instead of just using the defined bubble position" +
		"s");
			this.OptFindBubbleLocation.UseVisualStyleBackColor = true;
			// 
			// ImgView
			// 
			this.ImgView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ImgView.Image = null;
			this.ImgView.Location = new System.Drawing.Point(6, 6);
			this.ImgView.Margin = new System.Windows.Forms.Padding(4);
			this.ImgView.Name = "ImgView";
			this.ImgView.Size = new System.Drawing.Size(676, 837);
			this.ImgView.TabIndex = 1;
			// 
			// GradeExamsForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1080, 849);
			this.Controls.Add(this.splitContainer1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "GradeExamsForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Scan And Grade Exams";
			this.Shown += new System.EventHandler(this.GradeExamsForm_Load);
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.tabControl1.ResumeLayout(false);
			this.SelectExamPage.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.SelectStudentPage.ResumeLayout(false);
			this.SelectStudentPage.PerformLayout();
			this.groupBox9.ResumeLayout(false);
			this.groupBox9.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.AnalyzeImagePage.ResumeLayout(false);
			this.groupBox10.ResumeLayout(false);
			this.groupBox4.ResumeLayout(false);
			this.groupBox4.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.groupBox3.ResumeLayout(false);
			this.CheckAnswersPage.ResumeLayout(false);
			this.groupBox6.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.StudentScoreGrid)).EndInit();
			this.groupBox5.ResumeLayout(false);
			this.groupBox5.PerformLayout();
			this.FinishedPage.ResumeLayout(false);
			this.groupBox8.ResumeLayout(false);
			this.groupBox7.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.SplitContainer splitContainer1;
		private Components.ImageViewer ImgView;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage SelectExamPage;
		private System.Windows.Forms.ListBox ExamListBox;
		private System.Windows.Forms.TabPage SelectStudentPage;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.TextBox StudentClassEdit;
		private System.Windows.Forms.TextBox StudentLastNameEdit;
		private System.Windows.Forms.TextBox StudentFirstNameEdit;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label StudentNameLabel;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TabPage AnalyzeImagePage;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Label LoadImageErrorLabel;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Label StatScaleY;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label StatScaleX;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label StatSkewAngle;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label StatBottomRightCorner;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label StatBottomLeftCorner;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label StatTopRightCorner;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label StatTopLeftCorner;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button LoadImageBtn;
		private System.Windows.Forms.TabPage CheckAnswersPage;
		private System.Windows.Forms.TabPage FinishedPage;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.Label ScoreNumberRightLabel;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label ScorePercentLabel;
		private System.Windows.Forms.GroupBox groupBox6;
		private System.Windows.Forms.DataGridView StudentScoreGrid;
		private System.Windows.Forms.GroupBox groupBox7;
		private System.Windows.Forms.Button MarkUpTestBtn;
		private System.Windows.Forms.Button PrintTestBtn;
		private System.Windows.Forms.GroupBox groupBox8;
		private System.Windows.Forms.Button SaveToExamBtn;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.TextBox FilterExamsBox;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label ExamNameLabel;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.GroupBox groupBox9;
		private System.Windows.Forms.ListBox StudentListBox;
		private System.Windows.Forms.Button SortStudentsByClassDescBtn;
		private System.Windows.Forms.Button SortStudentsByNameDescBtn;
		private System.Windows.Forms.Button SortStudentsByClassAscBtn;
		private System.Windows.Forms.Button SortStudentsByNameAscBtn;
		private System.Windows.Forms.TextBox StudentFilterEdit;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.CheckBox OptSelectOnlyOneAnswer;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.TextBox OptFindAnswersHistogramPasses;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.TextBox OptIsDarkRatio;
		private System.Windows.Forms.Label CornerMatchingConfidence;
		private System.Windows.Forms.TextBox OptCornerMatchingConfidence;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.TextBox OptBlankBubbleFillRatio;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.Label DuplicateStudentWarningLabel;
		private System.Windows.Forms.DataGridViewTextBoxColumn QuestionNumber;
		private System.Windows.Forms.DataGridViewTextBoxColumn AnswerKey;
		private System.Windows.Forms.DataGridViewComboBoxColumn StudentAnswer;
		private System.Windows.Forms.DataGridViewTextBoxColumn Confidence;
		private System.Windows.Forms.GroupBox groupBox10;
		private System.Windows.Forms.Button AnalyzeImageBtnStep5;
		private System.Windows.Forms.Button AnalyzeImageBtnStep3;
		private System.Windows.Forms.Button AnalyzeImageBtnStep2;
		private System.Windows.Forms.Button AnalyzeImageBtnStep1;
		private System.Windows.Forms.Button AnalyzeImageBtnStep4;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Button CancelBtn;
		private System.Windows.Forms.Button DoneBtn;
		private System.Windows.Forms.Button AnalyzeImageBtnStep6;
		private System.Windows.Forms.Button AnalyzeImageBtnStep0;
		private System.Windows.Forms.CheckBox OptFindBubbleLocation;
	}
}
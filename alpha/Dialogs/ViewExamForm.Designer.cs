﻿namespace alpha.Dialogs
{
    partial class ViewExamForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewExamForm));
			this.tabControl = new System.Windows.Forms.TabControl();
			this.LoadExamPage = new System.Windows.Forms.TabPage();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.ExamListBox = new System.Windows.Forms.ListBox();
			this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
			this.NewExamBtn = new System.Windows.Forms.Button();
			this.LoadExamBtn = new System.Windows.Forms.Button();
			this.panel2 = new System.Windows.Forms.Panel();
			this.FilterExamsBox = new System.Windows.Forms.TextBox();
			this.label17 = new System.Windows.Forms.Label();
			this.EditDetailsPage = new System.Windows.Forms.TabPage();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.AuthorEdit = new System.Windows.Forms.TextBox();
			this.DescriptionEdit = new System.Windows.Forms.TextBox();
			this.SubjectEdit = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.NameEdit = new System.Windows.Forms.TextBox();
			this.AnswerKeyPage = new System.Windows.Forms.TabPage();
			this.groupBox5 = new System.Windows.Forms.GroupBox();
			this.label5 = new System.Windows.Forms.Label();
			this.RemoveQuestionBtn = new System.Windows.Forms.Button();
			this.AddQuestionBtn = new System.Windows.Forms.Button();
			this.AnswerKeyDataGrid = new System.Windows.Forms.DataGridView();
			this.Number = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Answer = new System.Windows.Forms.DataGridViewComboBoxColumn();
			this.GradebookTab = new System.Windows.Forms.TabPage();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.GradebookGrid = new System.Windows.Forms.DataGridView();
			this.LastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.FirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Group = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Correct = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Grade = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.panel1 = new System.Windows.Forms.Panel();
			this.AddScanBtn = new System.Windows.Forms.Button();
			this.DeleteStudentBtn = new System.Windows.Forms.Button();
			this.EditStudentScanBtn = new System.Windows.Forms.Button();
			this.MarkAllTestsBtn = new System.Windows.Forms.Button();
			this.AnalyticsPage = new System.Windows.Forms.TabPage();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.AnaltyicsGrid = new System.Windows.Forms.DataGridView();
			this.Question = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.TotalWrong = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.TotalBlank = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.TotalCorrect = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.AnsweredA = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.AnsweredB = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.AnsweredC = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.AnsweredD = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.AnsweredE = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.FormEditorBtn = new System.Windows.Forms.Button();
			this.EditStudentsBtn = new System.Windows.Forms.Button();
			this.SaveExamBtn = new System.Windows.Forms.Button();
			this.tabControl.SuspendLayout();
			this.LoadExamPage.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.tableLayoutPanel2.SuspendLayout();
			this.panel2.SuspendLayout();
			this.EditDetailsPage.SuspendLayout();
			this.groupBox4.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			this.AnswerKeyPage.SuspendLayout();
			this.groupBox5.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.AnswerKeyDataGrid)).BeginInit();
			this.GradebookTab.SuspendLayout();
			this.groupBox2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GradebookGrid)).BeginInit();
			this.panel1.SuspendLayout();
			this.AnalyticsPage.SuspendLayout();
			this.groupBox3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.AnaltyicsGrid)).BeginInit();
			this.SuspendLayout();
			// 
			// tabControl
			// 
			this.tabControl.Alignment = System.Windows.Forms.TabAlignment.Bottom;
			this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tabControl.Controls.Add(this.LoadExamPage);
			this.tabControl.Controls.Add(this.EditDetailsPage);
			this.tabControl.Controls.Add(this.AnswerKeyPage);
			this.tabControl.Controls.Add(this.GradebookTab);
			this.tabControl.Controls.Add(this.AnalyticsPage);
			this.tabControl.Location = new System.Drawing.Point(12, 12);
			this.tabControl.Name = "tabControl";
			this.tabControl.SelectedIndex = 0;
			this.tabControl.Size = new System.Drawing.Size(783, 587);
			this.tabControl.TabIndex = 0;
			// 
			// LoadExamPage
			// 
			this.LoadExamPage.Controls.Add(this.groupBox1);
			this.LoadExamPage.Location = new System.Drawing.Point(4, 4);
			this.LoadExamPage.Name = "LoadExamPage";
			this.LoadExamPage.Padding = new System.Windows.Forms.Padding(4);
			this.LoadExamPage.Size = new System.Drawing.Size(775, 561);
			this.LoadExamPage.TabIndex = 0;
			this.LoadExamPage.Text = "Load Exam";
			this.LoadExamPage.UseVisualStyleBackColor = true;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.ExamListBox);
			this.groupBox1.Controls.Add(this.tableLayoutPanel2);
			this.groupBox1.Controls.Add(this.panel2);
			this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox1.Location = new System.Drawing.Point(4, 4);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(767, 553);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Select Exam";
			// 
			// ExamListBox
			// 
			this.ExamListBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ExamListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ExamListBox.FormattingEnabled = true;
			this.ExamListBox.ItemHeight = 15;
			this.ExamListBox.Location = new System.Drawing.Point(3, 43);
			this.ExamListBox.Name = "ExamListBox";
			this.ExamListBox.Size = new System.Drawing.Size(761, 469);
			this.ExamListBox.TabIndex = 0;
			this.ExamListBox.DoubleClick += new System.EventHandler(this.LoadExamBtn_Click);
			// 
			// tableLayoutPanel2
			// 
			this.tableLayoutPanel2.ColumnCount = 2;
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel2.Controls.Add(this.NewExamBtn, 0, 0);
			this.tableLayoutPanel2.Controls.Add(this.LoadExamBtn, 0, 0);
			this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 512);
			this.tableLayoutPanel2.Name = "tableLayoutPanel2";
			this.tableLayoutPanel2.Padding = new System.Windows.Forms.Padding(4);
			this.tableLayoutPanel2.RowCount = 1;
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel2.Size = new System.Drawing.Size(761, 38);
			this.tableLayoutPanel2.TabIndex = 6;
			// 
			// NewExamBtn
			// 
			this.NewExamBtn.BackColor = System.Drawing.SystemColors.Control;
			this.NewExamBtn.Dock = System.Windows.Forms.DockStyle.Fill;
			this.NewExamBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.NewExamBtn.Image = global::alpha.Properties.Resources.page;
			this.NewExamBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.NewExamBtn.Location = new System.Drawing.Point(383, 7);
			this.NewExamBtn.Name = "NewExamBtn";
			this.NewExamBtn.Size = new System.Drawing.Size(371, 24);
			this.NewExamBtn.TabIndex = 6;
			this.NewExamBtn.Text = "New Exam...";
			this.NewExamBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.NewExamBtn.UseVisualStyleBackColor = false;
			this.NewExamBtn.Click += new System.EventHandler(this.NewExamBtn_Click);
			// 
			// LoadExamBtn
			// 
			this.LoadExamBtn.BackColor = System.Drawing.SystemColors.Control;
			this.LoadExamBtn.Dock = System.Windows.Forms.DockStyle.Fill;
			this.LoadExamBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.LoadExamBtn.Image = global::alpha.Properties.Resources.folder;
			this.LoadExamBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.LoadExamBtn.Location = new System.Drawing.Point(7, 7);
			this.LoadExamBtn.Name = "LoadExamBtn";
			this.LoadExamBtn.Size = new System.Drawing.Size(370, 24);
			this.LoadExamBtn.TabIndex = 5;
			this.LoadExamBtn.Text = "Load Exam";
			this.LoadExamBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.LoadExamBtn.UseVisualStyleBackColor = false;
			this.LoadExamBtn.Click += new System.EventHandler(this.LoadExamBtn_Click);
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.FilterExamsBox);
			this.panel2.Controls.Add(this.label17);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel2.Location = new System.Drawing.Point(3, 16);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(761, 27);
			this.panel2.TabIndex = 5;
			// 
			// FilterExamsBox
			// 
			this.FilterExamsBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.FilterExamsBox.Location = new System.Drawing.Point(37, 3);
			this.FilterExamsBox.Name = "FilterExamsBox";
			this.FilterExamsBox.Size = new System.Drawing.Size(721, 20);
			this.FilterExamsBox.TabIndex = 3;
			this.FilterExamsBox.TextChanged += new System.EventHandler(this.FilterExamsBox_TextChanged);
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label17.Location = new System.Drawing.Point(2, 6);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(29, 13);
			this.label17.TabIndex = 2;
			this.label17.Text = "Filter";
			// 
			// EditDetailsPage
			// 
			this.EditDetailsPage.Controls.Add(this.groupBox4);
			this.EditDetailsPage.Location = new System.Drawing.Point(4, 4);
			this.EditDetailsPage.Name = "EditDetailsPage";
			this.EditDetailsPage.Padding = new System.Windows.Forms.Padding(3);
			this.EditDetailsPage.Size = new System.Drawing.Size(775, 561);
			this.EditDetailsPage.TabIndex = 3;
			this.EditDetailsPage.Text = "Edit Details";
			this.EditDetailsPage.UseVisualStyleBackColor = true;
			// 
			// groupBox4
			// 
			this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox4.Controls.Add(this.tableLayoutPanel1);
			this.groupBox4.Location = new System.Drawing.Point(6, 3);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(763, 116);
			this.groupBox4.TabIndex = 1;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Exam Details:";
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tableLayoutPanel1.ColumnCount = 2;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75F));
			this.tableLayoutPanel1.Controls.Add(this.AuthorEdit, 1, 3);
			this.tableLayoutPanel1.Controls.Add(this.DescriptionEdit, 1, 2);
			this.tableLayoutPanel1.Controls.Add(this.SubjectEdit, 1, 1);
			this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
			this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
			this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.NameEdit, 1, 0);
			this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 19);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 5;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(751, 114);
			this.tableLayoutPanel1.TabIndex = 0;
			// 
			// AuthorEdit
			// 
			this.AuthorEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.AuthorEdit.Location = new System.Drawing.Point(190, 69);
			this.AuthorEdit.Name = "AuthorEdit";
			this.AuthorEdit.Size = new System.Drawing.Size(558, 20);
			this.AuthorEdit.TabIndex = 7;
			// 
			// DescriptionEdit
			// 
			this.DescriptionEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.DescriptionEdit.Location = new System.Drawing.Point(190, 47);
			this.DescriptionEdit.Name = "DescriptionEdit";
			this.DescriptionEdit.Size = new System.Drawing.Size(558, 20);
			this.DescriptionEdit.TabIndex = 6;
			// 
			// SubjectEdit
			// 
			this.SubjectEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.SubjectEdit.Location = new System.Drawing.Point(190, 25);
			this.SubjectEdit.Name = "SubjectEdit";
			this.SubjectEdit.Size = new System.Drawing.Size(558, 20);
			this.SubjectEdit.TabIndex = 5;
			// 
			// label4
			// 
			this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(3, 66);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(181, 22);
			this.label4.TabIndex = 3;
			this.label4.Text = "Author:";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label3
			// 
			this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(3, 44);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(181, 22);
			this.label3.TabIndex = 2;
			this.label3.Text = "Description:";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label2
			// 
			this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(3, 22);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(181, 22);
			this.label2.TabIndex = 1;
			this.label2.Text = "Subject:";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label1
			// 
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(3, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(181, 22);
			this.label1.TabIndex = 0;
			this.label1.Text = "Name:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// NameEdit
			// 
			this.NameEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.NameEdit.Location = new System.Drawing.Point(190, 3);
			this.NameEdit.Name = "NameEdit";
			this.NameEdit.Size = new System.Drawing.Size(558, 20);
			this.NameEdit.TabIndex = 4;
			// 
			// AnswerKeyPage
			// 
			this.AnswerKeyPage.Controls.Add(this.groupBox5);
			this.AnswerKeyPage.Location = new System.Drawing.Point(4, 4);
			this.AnswerKeyPage.Name = "AnswerKeyPage";
			this.AnswerKeyPage.Padding = new System.Windows.Forms.Padding(3);
			this.AnswerKeyPage.Size = new System.Drawing.Size(775, 561);
			this.AnswerKeyPage.TabIndex = 4;
			this.AnswerKeyPage.Text = "Answer Key";
			this.AnswerKeyPage.UseVisualStyleBackColor = true;
			// 
			// groupBox5
			// 
			this.groupBox5.Controls.Add(this.label5);
			this.groupBox5.Controls.Add(this.RemoveQuestionBtn);
			this.groupBox5.Controls.Add(this.AddQuestionBtn);
			this.groupBox5.Controls.Add(this.AnswerKeyDataGrid);
			this.groupBox5.Dock = System.Windows.Forms.DockStyle.Top;
			this.groupBox5.Location = new System.Drawing.Point(3, 3);
			this.groupBox5.Name = "groupBox5";
			this.groupBox5.Size = new System.Drawing.Size(769, 551);
			this.groupBox5.TabIndex = 3;
			this.groupBox5.TabStop = false;
			this.groupBox5.Text = "Edit the Answer Key:";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(258, 527);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(502, 24);
			this.label5.TabIndex = 3;
			this.label5.Text = "TIP: Select the row by clicking in the \'Number\' column then simply type the answe" +
    "r you want to choose.";
			// 
			// RemoveQuestionBtn
			// 
			this.RemoveQuestionBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.RemoveQuestionBtn.BackColor = System.Drawing.SystemColors.Control;
			this.RemoveQuestionBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.RemoveQuestionBtn.Image = global::alpha.Properties.Resources.key_delete;
			this.RemoveQuestionBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.RemoveQuestionBtn.Location = new System.Drawing.Point(132, 522);
			this.RemoveQuestionBtn.Name = "RemoveQuestionBtn";
			this.RemoveQuestionBtn.Size = new System.Drawing.Size(120, 24);
			this.RemoveQuestionBtn.TabIndex = 2;
			this.RemoveQuestionBtn.Text = "Remove Question";
			this.RemoveQuestionBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.RemoveQuestionBtn.UseVisualStyleBackColor = false;
			this.RemoveQuestionBtn.Click += new System.EventHandler(this.RemoveQuestionToAnswerKey);
			// 
			// AddQuestionBtn
			// 
			this.AddQuestionBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.AddQuestionBtn.BackColor = System.Drawing.SystemColors.Control;
			this.AddQuestionBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.AddQuestionBtn.Image = global::alpha.Properties.Resources.key_add;
			this.AddQuestionBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.AddQuestionBtn.Location = new System.Drawing.Point(6, 522);
			this.AddQuestionBtn.Name = "AddQuestionBtn";
			this.AddQuestionBtn.Size = new System.Drawing.Size(120, 24);
			this.AddQuestionBtn.TabIndex = 1;
			this.AddQuestionBtn.Text = "Add Question";
			this.AddQuestionBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.AddQuestionBtn.UseVisualStyleBackColor = false;
			this.AddQuestionBtn.Click += new System.EventHandler(this.AddQuestionToAnswerKey);
			// 
			// AnswerKeyDataGrid
			// 
			this.AnswerKeyDataGrid.AllowUserToAddRows = false;
			this.AnswerKeyDataGrid.AllowUserToDeleteRows = false;
			this.AnswerKeyDataGrid.AllowUserToResizeRows = false;
			this.AnswerKeyDataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.AnswerKeyDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.AnswerKeyDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.AnswerKeyDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Number,
            this.Answer});
			this.AnswerKeyDataGrid.Location = new System.Drawing.Point(6, 19);
			this.AnswerKeyDataGrid.Name = "AnswerKeyDataGrid";
			this.AnswerKeyDataGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.AnswerKeyDataGrid.Size = new System.Drawing.Size(754, 498);
			this.AnswerKeyDataGrid.TabIndex = 0;
			this.AnswerKeyDataGrid.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.AnswerKeyDataGrid_CellValueChanged);
			this.AnswerKeyDataGrid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AnswerKeyDataGrid_KeyDown);
			// 
			// Number
			// 
			this.Number.FillWeight = 50F;
			this.Number.HeaderText = "Number";
			this.Number.Name = "Number";
			this.Number.ReadOnly = true;
			// 
			// Answer
			// 
			this.Answer.HeaderText = "Answer";
			this.Answer.Name = "Answer";
			// 
			// GradebookTab
			// 
			this.GradebookTab.Controls.Add(this.groupBox2);
			this.GradebookTab.Controls.Add(this.panel1);
			this.GradebookTab.Location = new System.Drawing.Point(4, 4);
			this.GradebookTab.Name = "GradebookTab";
			this.GradebookTab.Padding = new System.Windows.Forms.Padding(3);
			this.GradebookTab.Size = new System.Drawing.Size(775, 561);
			this.GradebookTab.TabIndex = 1;
			this.GradebookTab.Text = "Gradebook";
			this.GradebookTab.UseVisualStyleBackColor = true;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.GradebookGrid);
			this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox2.Location = new System.Drawing.Point(3, 3);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(769, 526);
			this.groupBox2.TabIndex = 0;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Student Grades";
			// 
			// GradebookGrid
			// 
			this.GradebookGrid.AllowUserToAddRows = false;
			this.GradebookGrid.AllowUserToDeleteRows = false;
			this.GradebookGrid.AllowUserToResizeRows = false;
			this.GradebookGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.GradebookGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.GradebookGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.GradebookGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LastName,
            this.FirstName,
            this.Group,
            this.Correct,
            this.Total,
            this.Grade});
			this.GradebookGrid.Location = new System.Drawing.Point(6, 19);
			this.GradebookGrid.MultiSelect = false;
			this.GradebookGrid.Name = "GradebookGrid";
			this.GradebookGrid.ReadOnly = true;
			this.GradebookGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.GradebookGrid.Size = new System.Drawing.Size(757, 501);
			this.GradebookGrid.TabIndex = 0;
			// 
			// LastName
			// 
			this.LastName.FillWeight = 20F;
			this.LastName.HeaderText = "Last Name";
			this.LastName.Name = "LastName";
			this.LastName.ReadOnly = true;
			// 
			// FirstName
			// 
			this.FirstName.FillWeight = 20F;
			this.FirstName.HeaderText = "First Name";
			this.FirstName.Name = "FirstName";
			this.FirstName.ReadOnly = true;
			// 
			// Group
			// 
			this.Group.FillWeight = 20F;
			this.Group.HeaderText = "Class";
			this.Group.Name = "Group";
			this.Group.ReadOnly = true;
			// 
			// Correct
			// 
			this.Correct.FillWeight = 10F;
			this.Correct.HeaderText = "Correct";
			this.Correct.Name = "Correct";
			this.Correct.ReadOnly = true;
			// 
			// Total
			// 
			this.Total.FillWeight = 10F;
			this.Total.HeaderText = "Total";
			this.Total.Name = "Total";
			this.Total.ReadOnly = true;
			// 
			// Grade
			// 
			this.Grade.FillWeight = 10F;
			this.Grade.HeaderText = "Grade";
			this.Grade.Name = "Grade";
			this.Grade.ReadOnly = true;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.AddScanBtn);
			this.panel1.Controls.Add(this.DeleteStudentBtn);
			this.panel1.Controls.Add(this.EditStudentScanBtn);
			this.panel1.Controls.Add(this.MarkAllTestsBtn);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(3, 529);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(769, 29);
			this.panel1.TabIndex = 1;
			// 
			// AddScanBtn
			// 
			this.AddScanBtn.BackColor = System.Drawing.SystemColors.Control;
			this.AddScanBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.AddScanBtn.Image = global::alpha.Properties.Resources.picture_add;
			this.AddScanBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.AddScanBtn.Location = new System.Drawing.Point(3, 3);
			this.AddScanBtn.Name = "AddScanBtn";
			this.AddScanBtn.Size = new System.Drawing.Size(100, 24);
			this.AddScanBtn.TabIndex = 3;
			this.AddScanBtn.Text = "Add Scan...";
			this.AddScanBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.AddScanBtn.UseVisualStyleBackColor = false;
			this.AddScanBtn.Click += new System.EventHandler(this.AddScanBtn_Click);
			// 
			// DeleteStudentBtn
			// 
			this.DeleteStudentBtn.BackColor = System.Drawing.SystemColors.Control;
			this.DeleteStudentBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.DeleteStudentBtn.Image = global::alpha.Properties.Resources.picture_delete;
			this.DeleteStudentBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.DeleteStudentBtn.Location = new System.Drawing.Point(215, 3);
			this.DeleteStudentBtn.Name = "DeleteStudentBtn";
			this.DeleteStudentBtn.Size = new System.Drawing.Size(100, 24);
			this.DeleteStudentBtn.TabIndex = 2;
			this.DeleteStudentBtn.Text = "Delete Scan";
			this.DeleteStudentBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.DeleteStudentBtn.UseVisualStyleBackColor = false;
			this.DeleteStudentBtn.Click += new System.EventHandler(this.DeleteStudentBtn_Click);
			// 
			// EditStudentScanBtn
			// 
			this.EditStudentScanBtn.BackColor = System.Drawing.SystemColors.Control;
			this.EditStudentScanBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.EditStudentScanBtn.Image = global::alpha.Properties.Resources.picture_edit;
			this.EditStudentScanBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.EditStudentScanBtn.Location = new System.Drawing.Point(109, 3);
			this.EditStudentScanBtn.Name = "EditStudentScanBtn";
			this.EditStudentScanBtn.Size = new System.Drawing.Size(100, 24);
			this.EditStudentScanBtn.TabIndex = 1;
			this.EditStudentScanBtn.Text = "Edit Scan...";
			this.EditStudentScanBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.EditStudentScanBtn.UseVisualStyleBackColor = false;
			this.EditStudentScanBtn.Click += new System.EventHandler(this.EditStudentScanBtn_Click);
			// 
			// MarkAllTestsBtn
			// 
			this.MarkAllTestsBtn.BackColor = System.Drawing.SystemColors.Control;
			this.MarkAllTestsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.MarkAllTestsBtn.Image = global::alpha.Properties.Resources.layout_header;
			this.MarkAllTestsBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.MarkAllTestsBtn.Location = new System.Drawing.Point(585, 3);
			this.MarkAllTestsBtn.Name = "MarkAllTestsBtn";
			this.MarkAllTestsBtn.Size = new System.Drawing.Size(181, 24);
			this.MarkAllTestsBtn.TabIndex = 0;
			this.MarkAllTestsBtn.Text = "Mark Up All Scans...";
			this.MarkAllTestsBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.MarkAllTestsBtn.UseVisualStyleBackColor = false;
			this.MarkAllTestsBtn.Click += new System.EventHandler(this.MarkAllTestsBtn_Click);
			// 
			// AnalyticsPage
			// 
			this.AnalyticsPage.Controls.Add(this.groupBox3);
			this.AnalyticsPage.Location = new System.Drawing.Point(4, 4);
			this.AnalyticsPage.Name = "AnalyticsPage";
			this.AnalyticsPage.Padding = new System.Windows.Forms.Padding(3);
			this.AnalyticsPage.Size = new System.Drawing.Size(775, 561);
			this.AnalyticsPage.TabIndex = 2;
			this.AnalyticsPage.Text = "Analytics";
			this.AnalyticsPage.UseVisualStyleBackColor = true;
			// 
			// groupBox3
			// 
			this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox3.Controls.Add(this.AnaltyicsGrid);
			this.groupBox3.Location = new System.Drawing.Point(6, 6);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(763, 548);
			this.groupBox3.TabIndex = 0;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Analytics";
			// 
			// AnaltyicsGrid
			// 
			this.AnaltyicsGrid.AllowUserToAddRows = false;
			this.AnaltyicsGrid.AllowUserToDeleteRows = false;
			this.AnaltyicsGrid.AllowUserToResizeRows = false;
			this.AnaltyicsGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.AnaltyicsGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
			this.AnaltyicsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.AnaltyicsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Question,
            this.TotalWrong,
            this.TotalBlank,
            this.TotalCorrect,
            this.AnsweredA,
            this.AnsweredB,
            this.AnsweredC,
            this.AnsweredD,
            this.AnsweredE});
			this.AnaltyicsGrid.Location = new System.Drawing.Point(6, 19);
			this.AnaltyicsGrid.MultiSelect = false;
			this.AnaltyicsGrid.Name = "AnaltyicsGrid";
			this.AnaltyicsGrid.ReadOnly = true;
			this.AnaltyicsGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.AnaltyicsGrid.Size = new System.Drawing.Size(751, 523);
			this.AnaltyicsGrid.TabIndex = 0;
			// 
			// Question
			// 
			dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
			this.Question.DefaultCellStyle = dataGridViewCellStyle1;
			this.Question.Frozen = true;
			this.Question.HeaderText = "Question #";
			this.Question.Name = "Question";
			this.Question.ReadOnly = true;
			this.Question.ToolTipText = "Exam Question Number";
			this.Question.Width = 84;
			// 
			// TotalWrong
			// 
			dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
			this.TotalWrong.DefaultCellStyle = dataGridViewCellStyle2;
			this.TotalWrong.FillWeight = 90F;
			this.TotalWrong.HeaderText = "% Wrong";
			this.TotalWrong.Name = "TotalWrong";
			this.TotalWrong.ReadOnly = true;
			this.TotalWrong.ToolTipText = "Percent of Students who got this question wrong";
			this.TotalWrong.Width = 75;
			// 
			// TotalBlank
			// 
			dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
			this.TotalBlank.DefaultCellStyle = dataGridViewCellStyle3;
			this.TotalBlank.FillWeight = 90F;
			this.TotalBlank.HeaderText = "% Blank";
			this.TotalBlank.Name = "TotalBlank";
			this.TotalBlank.ReadOnly = true;
			this.TotalBlank.ToolTipText = "Percent of Students who left this question blank";
			this.TotalBlank.Width = 70;
			// 
			// TotalCorrect
			// 
			dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
			this.TotalCorrect.DefaultCellStyle = dataGridViewCellStyle4;
			this.TotalCorrect.FillWeight = 90F;
			this.TotalCorrect.HeaderText = "% Correct";
			this.TotalCorrect.Name = "TotalCorrect";
			this.TotalCorrect.ReadOnly = true;
			this.TotalCorrect.ToolTipText = "Percent of Students who got this question correct";
			this.TotalCorrect.Width = 77;
			// 
			// AnsweredA
			// 
			dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
			this.AnsweredA.DefaultCellStyle = dataGridViewCellStyle5;
			this.AnsweredA.FillWeight = 50F;
			this.AnsweredA.HeaderText = "A%";
			this.AnsweredA.Name = "AnsweredA";
			this.AnsweredA.ReadOnly = true;
			this.AnsweredA.ToolTipText = "Percent of Students who answered A for this question";
			this.AnsweredA.Width = 47;
			// 
			// AnsweredB
			// 
			dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
			this.AnsweredB.DefaultCellStyle = dataGridViewCellStyle6;
			this.AnsweredB.FillWeight = 50F;
			this.AnsweredB.HeaderText = "B%";
			this.AnsweredB.Name = "AnsweredB";
			this.AnsweredB.ReadOnly = true;
			this.AnsweredB.ToolTipText = "Percent of Students who answered B for this question";
			this.AnsweredB.Width = 47;
			// 
			// AnsweredC
			// 
			dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
			this.AnsweredC.DefaultCellStyle = dataGridViewCellStyle7;
			this.AnsweredC.FillWeight = 50F;
			this.AnsweredC.HeaderText = "C%";
			this.AnsweredC.Name = "AnsweredC";
			this.AnsweredC.ReadOnly = true;
			this.AnsweredC.ToolTipText = "Percent of Students who answered C for this question";
			this.AnsweredC.Width = 47;
			// 
			// AnsweredD
			// 
			dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
			this.AnsweredD.DefaultCellStyle = dataGridViewCellStyle8;
			this.AnsweredD.FillWeight = 50F;
			this.AnsweredD.HeaderText = "D%";
			this.AnsweredD.Name = "AnsweredD";
			this.AnsweredD.ReadOnly = true;
			this.AnsweredD.ToolTipText = "Percent of Students who answered D for this question";
			this.AnsweredD.Width = 48;
			// 
			// AnsweredE
			// 
			dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
			this.AnsweredE.DefaultCellStyle = dataGridViewCellStyle9;
			this.AnsweredE.FillWeight = 50F;
			this.AnsweredE.HeaderText = "E%";
			this.AnsweredE.Name = "AnsweredE";
			this.AnsweredE.ReadOnly = true;
			this.AnsweredE.ToolTipText = "Percent of Students who answered E for this question";
			this.AnsweredE.Width = 47;
			// 
			// FormEditorBtn
			// 
			this.FormEditorBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.FormEditorBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.FormEditorBtn.Image = global::alpha.Properties.Resources.application_form_edit;
			this.FormEditorBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.FormEditorBtn.Location = new System.Drawing.Point(142, 604);
			this.FormEditorBtn.Name = "FormEditorBtn";
			this.FormEditorBtn.Size = new System.Drawing.Size(124, 24);
			this.FormEditorBtn.TabIndex = 4;
			this.FormEditorBtn.Text = "Form Editor...";
			this.FormEditorBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.FormEditorBtn.UseVisualStyleBackColor = true;
			this.FormEditorBtn.Click += new System.EventHandler(this.FormEditorBtn_Click);
			// 
			// EditStudentsBtn
			// 
			this.EditStudentsBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.EditStudentsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.EditStudentsBtn.Image = global::alpha.Properties.Resources.group_edit;
			this.EditStudentsBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.EditStudentsBtn.Location = new System.Drawing.Point(12, 604);
			this.EditStudentsBtn.Name = "EditStudentsBtn";
			this.EditStudentsBtn.Size = new System.Drawing.Size(124, 24);
			this.EditStudentsBtn.TabIndex = 3;
			this.EditStudentsBtn.Text = "Edit Students...";
			this.EditStudentsBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.EditStudentsBtn.UseVisualStyleBackColor = true;
			this.EditStudentsBtn.Click += new System.EventHandler(this.EditStudentsBtn_Click);
			// 
			// SaveExamBtn
			// 
			this.SaveExamBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.SaveExamBtn.Enabled = false;
			this.SaveExamBtn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.SaveExamBtn.Image = global::alpha.Properties.Resources.disk;
			this.SaveExamBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.SaveExamBtn.Location = new System.Drawing.Point(685, 604);
			this.SaveExamBtn.Name = "SaveExamBtn";
			this.SaveExamBtn.Size = new System.Drawing.Size(110, 24);
			this.SaveExamBtn.TabIndex = 2;
			this.SaveExamBtn.Text = "Save Exam";
			this.SaveExamBtn.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
			this.SaveExamBtn.UseVisualStyleBackColor = true;
			this.SaveExamBtn.Click += new System.EventHandler(this.SaveExamBtn_Click);
			// 
			// ViewExamForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(807, 640);
			this.Controls.Add(this.FormEditorBtn);
			this.Controls.Add(this.EditStudentsBtn);
			this.Controls.Add(this.SaveExamBtn);
			this.Controls.Add(this.tabControl);
			this.DoubleBuffered = true;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "ViewExamForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Test Suite";
			this.tabControl.ResumeLayout(false);
			this.LoadExamPage.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.tableLayoutPanel2.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.EditDetailsPage.ResumeLayout(false);
			this.groupBox4.ResumeLayout(false);
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel1.PerformLayout();
			this.AnswerKeyPage.ResumeLayout(false);
			this.groupBox5.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.AnswerKeyDataGrid)).EndInit();
			this.GradebookTab.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GradebookGrid)).EndInit();
			this.panel1.ResumeLayout(false);
			this.AnalyticsPage.ResumeLayout(false);
			this.groupBox3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.AnaltyicsGrid)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage LoadExamPage;
        private System.Windows.Forms.TabPage GradebookTab;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox ExamListBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TabPage AnalyticsPage;
        private System.Windows.Forms.DataGridView GradebookGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn LastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn FirstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Group;
        private System.Windows.Forms.DataGridViewTextBoxColumn Correct;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.DataGridViewTextBoxColumn Grade;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView AnaltyicsGrid;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox FilterExamsBox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TabPage EditDetailsPage;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox AuthorEdit;
        private System.Windows.Forms.TextBox DescriptionEdit;
        private System.Windows.Forms.TextBox SubjectEdit;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox NameEdit;
        private System.Windows.Forms.Button SaveExamBtn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Question;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalWrong;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalBlank;
        private System.Windows.Forms.DataGridViewTextBoxColumn TotalCorrect;
        private System.Windows.Forms.DataGridViewTextBoxColumn AnsweredA;
        private System.Windows.Forms.DataGridViewTextBoxColumn AnsweredB;
        private System.Windows.Forms.DataGridViewTextBoxColumn AnsweredC;
        private System.Windows.Forms.DataGridViewTextBoxColumn AnsweredD;
        private System.Windows.Forms.DataGridViewTextBoxColumn AnsweredE;
        private System.Windows.Forms.TabPage AnswerKeyPage;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button RemoveQuestionBtn;
        private System.Windows.Forms.Button AddQuestionBtn;
        private System.Windows.Forms.DataGridView AnswerKeyDataGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn Number;
        private System.Windows.Forms.DataGridViewComboBoxColumn Answer;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button MarkAllTestsBtn;
        private System.Windows.Forms.Button EditStudentScanBtn;
        private System.Windows.Forms.Button DeleteStudentBtn;
		private System.Windows.Forms.Button AddScanBtn;
		private System.Windows.Forms.Button EditStudentsBtn;
		private System.Windows.Forms.Button FormEditorBtn;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
		private System.Windows.Forms.Button NewExamBtn;
		private System.Windows.Forms.Button LoadExamBtn;
	}
}
﻿namespace alpha.Dialogs
{
	partial class AddScanToExamForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		///// <summary>
		///// Clean up any resources being used.
		///// </summary>
		///// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		//protected override void Dispose(bool disposing)
		//{
		//	if (disposing && (components != null))
		//	{
		//		components.Dispose();
		//	}
		//	base.Dispose(disposing);
		//}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddScanToExamForm));
			this.splitContainerHorizontal = new System.Windows.Forms.SplitContainer();
			this.splitContainerVertical = new System.Windows.Forms.SplitContainer();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.ScanGrid = new System.Windows.Forms.DataGridView();
			this.Filename = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Student = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Score = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.State = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.StudentsGrid = new System.Windows.Forms.DataGridView();
			this.Group = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.FirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.LastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.label14 = new System.Windows.Forms.Label();
			this.StudentClassEdit = new System.Windows.Forms.TextBox();
			this.StudentLastNameEdit = new System.Windows.Forms.TextBox();
			this.StudentFirstNameEdit = new System.Windows.Forms.TextBox();
			this.label13 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.CancelBtn = new System.Windows.Forms.Button();
			this.AddScansBtn = new System.Windows.Forms.Button();
			this.toolStrip1 = new System.Windows.Forms.ToolStrip();
			this.AddFileBtn = new System.Windows.Forms.ToolStripButton();
			this.DeleteScanBtn = new System.Windows.Forms.ToolStripButton();
			this.ChangeScanBtn = new System.Windows.Forms.ToolStripButton();
			this.EditScanBtn = new System.Windows.Forms.ToolStripButton();
			this.ScanPreview = new alpha.Components.ImageViewer();
			((System.ComponentModel.ISupportInitialize)(this.splitContainerHorizontal)).BeginInit();
			this.splitContainerHorizontal.Panel1.SuspendLayout();
			this.splitContainerHorizontal.Panel2.SuspendLayout();
			this.splitContainerHorizontal.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainerVertical)).BeginInit();
			this.splitContainerVertical.Panel1.SuspendLayout();
			this.splitContainerVertical.Panel2.SuspendLayout();
			this.splitContainerVertical.SuspendLayout();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.ScanGrid)).BeginInit();
			this.groupBox2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.StudentsGrid)).BeginInit();
			this.groupBox3.SuspendLayout();
			this.groupBox4.SuspendLayout();
			this.panel1.SuspendLayout();
			this.toolStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// splitContainerHorizontal
			// 
			this.splitContainerHorizontal.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainerHorizontal.Location = new System.Drawing.Point(4, 4);
			this.splitContainerHorizontal.Name = "splitContainerHorizontal";
			// 
			// splitContainerHorizontal.Panel1
			// 
			this.splitContainerHorizontal.Panel1.Controls.Add(this.splitContainerVertical);
			// 
			// splitContainerHorizontal.Panel2
			// 
			this.splitContainerHorizontal.Panel2.Controls.Add(this.groupBox4);
			this.splitContainerHorizontal.Size = new System.Drawing.Size(819, 686);
			this.splitContainerHorizontal.SplitterDistance = 552;
			this.splitContainerHorizontal.TabIndex = 5;
			this.splitContainerHorizontal.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitContainerHorizontal_SplitterMoved);
			// 
			// splitContainerVertical
			// 
			this.splitContainerVertical.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainerVertical.Location = new System.Drawing.Point(0, 0);
			this.splitContainerVertical.Name = "splitContainerVertical";
			this.splitContainerVertical.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// splitContainerVertical.Panel1
			// 
			this.splitContainerVertical.Panel1.Controls.Add(this.groupBox1);
			// 
			// splitContainerVertical.Panel2
			// 
			this.splitContainerVertical.Panel2.Controls.Add(this.groupBox2);
			this.splitContainerVertical.Panel2.Controls.Add(this.groupBox3);
			this.splitContainerVertical.Size = new System.Drawing.Size(552, 686);
			this.splitContainerVertical.SplitterDistance = 344;
			this.splitContainerVertical.TabIndex = 0;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.ScanGrid);
			this.groupBox1.Controls.Add(this.toolStrip1);
			this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox1.Location = new System.Drawing.Point(0, 0);
			this.groupBox1.Margin = new System.Windows.Forms.Padding(6);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
			this.groupBox1.Size = new System.Drawing.Size(552, 344);
			this.groupBox1.TabIndex = 3;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Scans";
			// 
			// ScanGrid
			// 
			this.ScanGrid.AllowUserToAddRows = false;
			this.ScanGrid.AllowUserToDeleteRows = false;
			this.ScanGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.ScanGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.ScanGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Filename,
            this.Student,
            this.Score,
            this.State});
			this.ScanGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ScanGrid.Location = new System.Drawing.Point(4, 42);
			this.ScanGrid.MultiSelect = false;
			this.ScanGrid.Name = "ScanGrid";
			dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Gainsboro;
			dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.DarkCyan;
			this.ScanGrid.RowsDefaultCellStyle = dataGridViewCellStyle1;
			this.ScanGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.ScanGrid.Size = new System.Drawing.Size(544, 298);
			this.ScanGrid.TabIndex = 0;
			this.ScanGrid.SelectionChanged += new System.EventHandler(this.ScanGrid_SelectionChanged);
			this.ScanGrid.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ScanGrid_MouseDoubleClick);
			// 
			// Filename
			// 
			this.Filename.FillWeight = 50F;
			this.Filename.HeaderText = "FileName";
			this.Filename.Name = "Filename";
			this.Filename.ReadOnly = true;
			// 
			// Student
			// 
			this.Student.FillWeight = 50F;
			this.Student.HeaderText = "Student";
			this.Student.Name = "Student";
			this.Student.ReadOnly = true;
			// 
			// Score
			// 
			this.Score.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.Score.FillWeight = 12F;
			this.Score.HeaderText = "Score";
			this.Score.Name = "Score";
			// 
			// State
			// 
			this.State.FillWeight = 40F;
			this.State.HeaderText = "State";
			this.State.Name = "State";
			this.State.ReadOnly = true;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.StudentsGrid);
			this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox2.Location = new System.Drawing.Point(0, 93);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(552, 245);
			this.groupBox2.TabIndex = 6;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Preloaded Students";
			// 
			// StudentsGrid
			// 
			this.StudentsGrid.AllowUserToAddRows = false;
			this.StudentsGrid.AllowUserToDeleteRows = false;
			this.StudentsGrid.AllowUserToOrderColumns = true;
			this.StudentsGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
			this.StudentsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.StudentsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Group,
            this.FirstName,
            this.LastName});
			this.StudentsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.StudentsGrid.Location = new System.Drawing.Point(3, 16);
			this.StudentsGrid.MultiSelect = false;
			this.StudentsGrid.Name = "StudentsGrid";
			this.StudentsGrid.ReadOnly = true;
			this.StudentsGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.StudentsGrid.Size = new System.Drawing.Size(546, 226);
			this.StudentsGrid.TabIndex = 3;
			this.StudentsGrid.SelectionChanged += new System.EventHandler(this.StudentsGrid_SelectionChanged);
			// 
			// Group
			// 
			this.Group.HeaderText = "Group";
			this.Group.Name = "Group";
			this.Group.ReadOnly = true;
			// 
			// FirstName
			// 
			this.FirstName.HeaderText = "First Name";
			this.FirstName.Name = "FirstName";
			this.FirstName.ReadOnly = true;
			// 
			// LastName
			// 
			this.LastName.HeaderText = "Last Name";
			this.LastName.Name = "LastName";
			this.LastName.ReadOnly = true;
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.label14);
			this.groupBox3.Controls.Add(this.StudentClassEdit);
			this.groupBox3.Controls.Add(this.StudentLastNameEdit);
			this.groupBox3.Controls.Add(this.StudentFirstNameEdit);
			this.groupBox3.Controls.Add(this.label13);
			this.groupBox3.Controls.Add(this.label11);
			this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
			this.groupBox3.Location = new System.Drawing.Point(0, 0);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(552, 93);
			this.groupBox3.TabIndex = 5;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Selected Student (Can Modify Manually)";
			// 
			// label14
			// 
			this.label14.Location = new System.Drawing.Point(6, 68);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(75, 13);
			this.label14.TabIndex = 5;
			this.label14.Text = "Class:";
			// 
			// StudentClassEdit
			// 
			this.StudentClassEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.StudentClassEdit.Location = new System.Drawing.Point(103, 65);
			this.StudentClassEdit.Name = "StudentClassEdit";
			this.StudentClassEdit.Size = new System.Drawing.Size(443, 20);
			this.StudentClassEdit.TabIndex = 4;
			this.StudentClassEdit.TextChanged += new System.EventHandler(this.StudentEdits_TextChanged);
			this.StudentClassEdit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.StudentEdits_KeyDown);
			// 
			// StudentLastNameEdit
			// 
			this.StudentLastNameEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.StudentLastNameEdit.Location = new System.Drawing.Point(103, 39);
			this.StudentLastNameEdit.Name = "StudentLastNameEdit";
			this.StudentLastNameEdit.Size = new System.Drawing.Size(443, 20);
			this.StudentLastNameEdit.TabIndex = 3;
			this.StudentLastNameEdit.TextChanged += new System.EventHandler(this.StudentEdits_TextChanged);
			this.StudentLastNameEdit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.StudentEdits_KeyDown);
			// 
			// StudentFirstNameEdit
			// 
			this.StudentFirstNameEdit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.StudentFirstNameEdit.Location = new System.Drawing.Point(103, 13);
			this.StudentFirstNameEdit.Name = "StudentFirstNameEdit";
			this.StudentFirstNameEdit.Size = new System.Drawing.Size(443, 20);
			this.StudentFirstNameEdit.TabIndex = 2;
			this.StudentFirstNameEdit.TextChanged += new System.EventHandler(this.StudentEdits_TextChanged);
			this.StudentFirstNameEdit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.StudentEdits_KeyDown);
			// 
			// label13
			// 
			this.label13.Location = new System.Drawing.Point(6, 42);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(75, 13);
			this.label13.TabIndex = 1;
			this.label13.Text = "Last Name:";
			// 
			// label11
			// 
			this.label11.Location = new System.Drawing.Point(6, 16);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(75, 13);
			this.label11.TabIndex = 0;
			this.label11.Text = "First Name:";
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add(this.ScanPreview);
			this.groupBox4.Controls.Add(this.panel1);
			this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox4.Location = new System.Drawing.Point(0, 0);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(263, 686);
			this.groupBox4.TabIndex = 6;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Preview";
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.CancelBtn);
			this.panel1.Controls.Add(this.AddScansBtn);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(3, 630);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(257, 53);
			this.panel1.TabIndex = 0;
			// 
			// CancelBtn
			// 
			this.CancelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.CancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.CancelBtn.Location = new System.Drawing.Point(73, 27);
			this.CancelBtn.Name = "CancelBtn";
			this.CancelBtn.Size = new System.Drawing.Size(75, 23);
			this.CancelBtn.TabIndex = 1;
			this.CancelBtn.Text = "Cancel";
			this.CancelBtn.UseVisualStyleBackColor = true;
			// 
			// AddScansBtn
			// 
			this.AddScansBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.AddScansBtn.Location = new System.Drawing.Point(154, 27);
			this.AddScansBtn.Name = "AddScansBtn";
			this.AddScansBtn.Size = new System.Drawing.Size(100, 23);
			this.AddScansBtn.TabIndex = 0;
			this.AddScansBtn.Text = "Add These";
			this.AddScansBtn.UseVisualStyleBackColor = true;
			this.AddScansBtn.Click += new System.EventHandler(this.AddScansBtn_Click);
			// 
			// toolStrip1
			// 
			this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AddFileBtn,
            this.EditScanBtn,
            this.ChangeScanBtn,
            this.DeleteScanBtn});
			this.toolStrip1.Location = new System.Drawing.Point(4, 17);
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.Size = new System.Drawing.Size(544, 25);
			this.toolStrip1.TabIndex = 1;
			this.toolStrip1.Text = "toolStrip1";
			// 
			// AddFileBtn
			// 
			this.AddFileBtn.Image = global::alpha.Properties.Resources.picture_add;
			this.AddFileBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.AddFileBtn.Name = "AddFileBtn";
			this.AddFileBtn.Size = new System.Drawing.Size(79, 22);
			this.AddFileBtn.Text = "Add File...";
			this.AddFileBtn.Click += new System.EventHandler(this.AddFileBtn_Click);
			// 
			// DeleteScanBtn
			// 
			this.DeleteScanBtn.Image = global::alpha.Properties.Resources.picture_delete;
			this.DeleteScanBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.DeleteScanBtn.Name = "DeleteScanBtn";
			this.DeleteScanBtn.Size = new System.Drawing.Size(88, 22);
			this.DeleteScanBtn.Text = "Delete Scan";
			this.DeleteScanBtn.Click += new System.EventHandler(this.DeleteScanBtn_Click);
			// 
			// ChangeScanBtn
			// 
			this.ChangeScanBtn.Image = global::alpha.Properties.Resources.picture_go;
			this.ChangeScanBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.ChangeScanBtn.Name = "ChangeScanBtn";
			this.ChangeScanBtn.Size = new System.Drawing.Size(113, 22);
			this.ChangeScanBtn.Text = "Change Image...";
			this.ChangeScanBtn.ToolTipText = "Same Student, new Scan Image";
			// 
			// EditScanBtn
			// 
			this.EditScanBtn.Image = global::alpha.Properties.Resources.picture_edit;
			this.EditScanBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.EditScanBtn.Name = "EditScanBtn";
			this.EditScanBtn.Size = new System.Drawing.Size(84, 22);
			this.EditScanBtn.Text = "Edit Scan...";
			// 
			// ScanPreview
			// 
			this.ScanPreview.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ScanPreview.Image = null;
			this.ScanPreview.Location = new System.Drawing.Point(3, 16);
			this.ScanPreview.Name = "ScanPreview";
			this.ScanPreview.Size = new System.Drawing.Size(257, 614);
			this.ScanPreview.TabIndex = 1;
			// 
			// AddScanToExamForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(827, 694);
			this.Controls.Add(this.splitContainerHorizontal);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "AddScanToExamForm";
			this.Padding = new System.Windows.Forms.Padding(4);
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Select Scan Image(s)...";
			this.Shown += new System.EventHandler(this.AddScanToExamForm_Shown);
			this.splitContainerHorizontal.Panel1.ResumeLayout(false);
			this.splitContainerHorizontal.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainerHorizontal)).EndInit();
			this.splitContainerHorizontal.ResumeLayout(false);
			this.splitContainerVertical.Panel1.ResumeLayout(false);
			this.splitContainerVertical.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainerVertical)).EndInit();
			this.splitContainerVertical.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.ScanGrid)).EndInit();
			this.groupBox2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.StudentsGrid)).EndInit();
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			this.groupBox4.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.toolStrip1.ResumeLayout(false);
			this.toolStrip1.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.SplitContainer splitContainerHorizontal;
		private System.Windows.Forms.SplitContainer splitContainerVertical;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.DataGridView ScanGrid;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.DataGridView StudentsGrid;
		private System.Windows.Forms.DataGridViewTextBoxColumn Group;
		private System.Windows.Forms.DataGridViewTextBoxColumn FirstName;
		private System.Windows.Forms.DataGridViewTextBoxColumn LastName;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.TextBox StudentClassEdit;
		private System.Windows.Forms.TextBox StudentLastNameEdit;
		private System.Windows.Forms.TextBox StudentFirstNameEdit;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button CancelBtn;
		private System.Windows.Forms.Button AddScansBtn;
		private System.Windows.Forms.DataGridViewTextBoxColumn Filename;
		private System.Windows.Forms.DataGridViewTextBoxColumn Student;
		private System.Windows.Forms.DataGridViewTextBoxColumn Score;
		private System.Windows.Forms.DataGridViewTextBoxColumn State;
		private Components.ImageViewer ScanPreview;
		private System.Windows.Forms.ToolStrip toolStrip1;
		private System.Windows.Forms.ToolStripButton AddFileBtn;
		private System.Windows.Forms.ToolStripButton DeleteScanBtn;
		private System.Windows.Forms.ToolStripButton ChangeScanBtn;
		private System.Windows.Forms.ToolStripButton EditScanBtn;
	}
}
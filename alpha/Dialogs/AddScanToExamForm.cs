﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ScanReader;

namespace alpha.Dialogs
{	
	public partial class AddScanToExamForm : Form
	{
		// Private datatype
		private struct ScanType
		{
			public string Path;
			public Student student;
		}

		List<string> Files;			// These are the files we need to process
		List<ScanType> Scans;		// These are the scans we are loading
		StudentList StudentsList;	// List of Preloaded students
		Exam SelectedExam;
		bool bIgnoreEvents;         // When true, we're updating information, not changing it, so don't do anything on OnChanged events	

		public List<Student> ScansToAdd;	// When we've saved, this will contain the student structures to add

		public AddScanToExamForm(Exam exam)
		{
			InitializeComponent();

			SelectedExam = exam;
			Files = new List<string>();
			Scans = new List<ScanType>();
			ScansToAdd = null;

			LoadStudents();
		}
		private void LoadStudents()
		{
			StudentsList = Database.GetStudents();

			// Load the items
			StudentsGrid.Rows.Clear();
			foreach (var s in StudentsList.Students)
			{
				DataGridViewRow row = new DataGridViewRow();

				// Group Name
				var groupCell = new DataGridViewTextBoxCell();
				groupCell.Value = s.GroupName;
				row.Cells.Add(groupCell);

				// First Name
				var fnameCell = new DataGridViewTextBoxCell();
				fnameCell.Value = s.FirstName;
				row.Cells.Add(fnameCell);

				// First Name
				var lnameCell = new DataGridViewTextBoxCell();
				lnameCell.Value = s.LastName;
				row.Cells.Add(lnameCell);

				StudentsGrid.Rows.Add(row);
			}

			UpdateStudentGrid();
		}
		
		/// <summary>
		/// Updates the ScanGrid with the status of the scans
		/// </summary>
		private void BuildScanGrid()
		{
			// -- Delete the rows
			ScanGrid.Rows.Clear();

			// -- add a new row for each scan
			foreach (var scan in Scans)
			{
				DataGridViewRow row = new DataGridViewRow();

				// Filename
				DataGridViewTextBoxCell PathCell = new DataGridViewTextBoxCell();
				PathCell.Value = scan.Path;
				row.Cells.Add(PathCell);

				// Name
				DataGridViewTextBoxCell NameCell = new DataGridViewTextBoxCell();
				NameCell.Value = scan.student.FullName;
				row.Cells.Add(NameCell);

				// Score
				DataGridViewTextBoxCell ScoreCell = new DataGridViewTextBoxCell();
				ScoreCell.Value = (scan.student.CurrentScanState >= StudentScanState.LoadedButLowConfidence) ? scan.student.Score.PercentScoreString : string.Empty;
				row.Cells.Add(ScoreCell);

				// State
				DataGridViewTextBoxCell StateCell = new DataGridViewTextBoxCell();
				StateCell.Value = scan.student.CurrentScanState;
				row.Cells.Add(StateCell);

				ScanGrid.Rows.Add(row);
			}
			UpdateScanGrid();
		}

		/// <summary>
		/// Updates the Student name and Scan State for each scan
		/// </summary>
		private void UpdateScanGrid()
		{
			for (int i = 0; i < ScanGrid.Rows.Count; i++)
			{
				// Update Name
				ScanGrid.Rows[i].Cells[1].Value = Scans[i].student.FullName;

				// Update Score
				ScanGrid.Rows[i].Cells[2].Value = 
					(Scans[i].student.CurrentScanState >= StudentScanState.LoadedButLowConfidence) ? 
					Scans[i].student.Score.PercentScoreString : string.Empty;

				// Update State
				ScanGrid.Rows[i].Cells[3].Value = Scans[i].student.CurrentScanState;
				switch (Scans[i].student.CurrentScanState)
				{
					case StudentScanState.Loading:
						ScanGrid.Rows[i].Cells[3].Style.BackColor = Color.LightSkyBlue;
						ScanGrid.Rows[i].Cells[3].Style.SelectionForeColor = Color.LightSkyBlue;
						break;
					case StudentScanState.LoadingError:
						ScanGrid.Rows[i].Cells[3].Style.BackColor = Color.LightPink;
						ScanGrid.Rows[i].Cells[3].Style.SelectionForeColor = Color.Red;
						break;
					case StudentScanState.LoadedButLowConfidence:
						ScanGrid.Rows[i].Cells[3].Style.BackColor = Color.LightGoldenrodYellow;
						ScanGrid.Rows[i].Cells[3].Style.SelectionForeColor = Color.Goldenrod;
						break;
					case StudentScanState.LoadedOkay:
						ScanGrid.Rows[i].Cells[3].Style.BackColor = Color.LightGreen;
						ScanGrid.Rows[i].Cells[3].Style.SelectionForeColor = Color.Green;
						break;
					default:
						ScanGrid.Rows[i].Cells[3].Style.BackColor = ScanGrid.DefaultCellStyle.BackColor;
						ScanGrid.Rows[i].Cells[3].Style.SelectionForeColor = ScanGrid.DefaultCellStyle.SelectionForeColor;
						break;
				}
			}
		}

		/// <summary>
		/// Empties the Files array and creates the blank student objects BUT DOES NOT START THEM
		/// </summary>
		private void FilesToScans()
		{
			// Create the Scan Objects from files
			var lst = new List<string>(Files);  // Create copy so we can clear Files
			Files.Clear();

			// Create a student for each scan image
			foreach (string file in lst)
			{

				ScanType holder = new ScanType();
				holder.student = new Student();
				holder.Path = file;

				Scans.Add(holder);
			}

			// Update the Grid
			BuildScanGrid();
		}

		/// <summary>
		///  Selected Scan changed, so load the selected Student and preview the image
		/// </summary>
		private void ScanGrid_SelectionChanged(object sender, EventArgs e)
		{
			if (ScanGrid.SelectedCells.Count < 1 || 
				ScanGrid.SelectedCells[0].RowIndex < 0 ||
				ScanGrid.SelectedCells[0].RowIndex >= Scans.Count)
				return;

			bIgnoreEvents = true;

			// Update the student form the Scan array
			StudentFirstNameEdit.Text = Scans[ScanGrid.SelectedCells[0].RowIndex].student.FirstName;
			StudentLastNameEdit.Text = Scans[ScanGrid.SelectedCells[0].RowIndex].student.LastName;
			StudentClassEdit.Text = Scans[ScanGrid.SelectedCells[0].RowIndex].student.GroupName;

			// Reset Preloade student selection
			StudentsGrid.ClearSelection();

			// Load Scan Preview
			if (Scans[ScanGrid.SelectedCells[0].RowIndex].student != null &&
				Scans[ScanGrid.SelectedCells[0].RowIndex].student.Scan != null &&
				Scans[ScanGrid.SelectedCells[0].RowIndex].student.Scan.Loaded)
			{
				// Load the Image
				ScanPreview.Image = Scans[ScanGrid.SelectedCells[0].RowIndex].student.Scan.Image;

				// Zoom to Top Left (Maybe Name location?)
				var loc = (from obj in SelectedExam.Form.Objects where obj is ScanReader.ExamObjects.ExamWrittenEntry select obj.Location).First();

				if (loc.Width < 1 || loc.Height < 1)
					loc = new Rectangle(50, 50, 100, 100);  // Default

				ScanPreview.ImageBox.ZoomToRegion(loc);
			}
			else
				ScanPreview.Image = null;

			bIgnoreEvents = false;
			StudentFirstNameEdit.Focus();
		}

		/// <summary>
		/// Selection of a Preloaded student changed, so Update the selected student to the selected Preloade Stuedent
		/// </summary>
		private void StudentsGrid_SelectionChanged(object sender, EventArgs e)
		{
			if (bIgnoreEvents ||
				ScanGrid.SelectedCells.Count < 1 ||
				ScanGrid.SelectedCells[0].RowIndex < 0 ||
				ScanGrid.SelectedCells[0].RowIndex >= Scans.Count ||
				StudentsGrid.SelectedCells.Count < 1)
				return;

			bIgnoreEvents = true;

			// Update the Student Info
			StudentFirstNameEdit.Text = Scans[ScanGrid.SelectedCells[0].RowIndex].student.FirstName = 
				StudentsGrid.Rows[StudentsGrid.SelectedCells[0].RowIndex].Cells[1].Value.ToString();

			StudentLastNameEdit.Text = Scans[ScanGrid.SelectedCells[0].RowIndex].student.LastName =
				StudentsGrid.Rows[StudentsGrid.SelectedCells[0].RowIndex].Cells[2].Value.ToString();

			StudentClassEdit.Text = Scans[ScanGrid.SelectedCells[0].RowIndex].student.GroupName =
				StudentsGrid.Rows[StudentsGrid.SelectedCells[0].RowIndex].Cells[0].Value.ToString();

			UpdateStudentGrid();
			UpdateScanGrid();

			bIgnoreEvents = false;

		}

		/// <summary>
		/// Highlights the Students in the Preloaded list that have already been selected in the ScanGrid
		/// </summary>
		private void UpdateStudentGrid()
		{
			// List of students used in ScanGrid and the Exam
			var studentsUsed = from s in Scans
							   where s.student.FullName != string.Empty
							   select s.student;

			var examStudents = from s in SelectedExam.Students
							   select s;							   
						
			for (int i = 0; i < StudentsGrid.Rows.Count; i++)
			{
				Student s = new Student(StudentsGrid.Rows[i].Cells[1].Value.ToString(),
					StudentsGrid.Rows[i].Cells[2].Value.ToString(),
					StudentsGrid.Rows[i].Cells[0].Value.ToString());

				Color backColor;

				if (studentsUsed.Contains(s, new StudentComparer()))
					backColor = Color.LightYellow;
				else if (examStudents.Contains(s, new StudentComparer()))
					backColor = Color.Gainsboro;
				else
					backColor = StudentsGrid.DefaultCellStyle.BackColor;

				StudentsGrid.Rows[i].Cells[0].Style.BackColor = backColor;
				StudentsGrid.Rows[i].Cells[1].Style.BackColor = backColor;
				StudentsGrid.Rows[i].Cells[2].Style.BackColor = backColor;
			}
		}

		/// <summary>
		/// Student Manually edited, so update the UI and save changes
		/// </summary>
		private void StudentEdits_TextChanged(object sender, EventArgs e)
		{
			if (bIgnoreEvents ||
				ScanGrid.SelectedCells.Count < 1 ||
				ScanGrid.SelectedCells[0].RowIndex < 0 ||
				ScanGrid.SelectedCells[0].RowIndex >= Scans.Count)
				return;

			// Update the Student Info
			Scans[ScanGrid.SelectedCells[0].RowIndex].student.FirstName = StudentFirstNameEdit.Text;
			Scans[ScanGrid.SelectedCells[0].RowIndex].student.LastName = StudentLastNameEdit.Text;
			Scans[ScanGrid.SelectedCells[0].RowIndex].student.GroupName = StudentClassEdit.Text;

			UpdateScanGrid();
			UpdateStudentGrid();

			bIgnoreEvents = false;
		}

		/// <summary>
		/// Double Clicking on a Scan launches the Scan Editor
		/// </summary>
		private void ScanGrid_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			if (ScanGrid.SelectedCells.Count < 1 ||
				ScanGrid.SelectedCells[0].RowIndex < 0 ||
				ScanGrid.SelectedCells[0].RowIndex >= Scans.Count ||
				Scans[ScanGrid.SelectedCells[0].RowIndex].student.CurrentScanState == StudentScanState.NotLoaded)
				return;

			// Before we launch the editor, reload the Original Image
			Scans[ScanGrid.SelectedCells[0].RowIndex].student.Scan.ReloadForEditing(Scans[ScanGrid.SelectedCells[0].RowIndex].Path);

			// Create the Form
			using (GradeExamsForm frm = new GradeExamsForm(SelectedExam, Scans[ScanGrid.SelectedCells[0].RowIndex].student, false))
			{
				// Retain the changeable info
				var answers = new List<Answer>(Scans[ScanGrid.SelectedCells[0].RowIndex].student.Score.Answers);
				var correct = new List<bool>(Scans[ScanGrid.SelectedCells[0].RowIndex].student.Score.QuestionCorrect);

				if (frm.ShowDialog() != DialogResult.OK)
				{
					// Reset changes
					Scans[ScanGrid.SelectedCells[0].RowIndex].student.Score.Answers = new List<Answer>(answers);
					Scans[ScanGrid.SelectedCells[0].RowIndex].student.Score.QuestionCorrect = new List<bool>(correct);
				}

				UpdateScanGrid();
			}

			// Before we exit, Unload the original image again
			Scans[ScanGrid.SelectedCells[0].RowIndex].student.Scan.UnloadMemory();
		}

		// -- Image Loading Logic ------
		private void AddScanToExamForm_Shown(object sender, EventArgs e)
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Filter = "All Types|*.pdf;*.png;*.jpg;*.jpeg;|All Files (*.*)|*.*";
			dlg.Multiselect = true;

			// Quit this whole form if cancel
			if (dlg.ShowDialog() != DialogResult.OK)
			{
				this.Close();
				DialogResult = DialogResult.Cancel;
				return;
			}

			// Otherwise, load the files
			LoadScansFromFiles(dlg.FileNames);
		}
		private async void LoadScansFromFiles(string[] files)
		{
			// Don't start loading scans at index 0 if we've already loaded some scans
			int startIndex = Math.Max(Scans.Count, 0);

			// Start the async load of scans
			foreach (string file in files)
			{
				if (file.EndsWith(".pdf"))
					await LoadFromPDF(file);
				else
					LoadFromImage(file);
			}

			// All Files have been loaded (PDFs have been split), so move all the file strings to Student Objects, ready to be started
			FilesToScans();

			// Now, Process the list
			const int CONCURRENT_SCANS_TO_LOAD = 2;
			int nextIndex = startIndex;
			var Tasks = new List<Task<StudentScanState>>();
			while (nextIndex < (CONCURRENT_SCANS_TO_LOAD + startIndex) && nextIndex < Scans.Count)
			{
				Scans[nextIndex].student.ScanState = StudentScanState.Loading;
				UpdateScanGrid();
				Tasks.Add(LoadScanAsync(nextIndex));
				await Task.Delay(500);
				nextIndex++;
			}

			while (Tasks.Count > 0)
			{
				try
				{
					Task<StudentScanState> task = await Task.WhenAny(Tasks);
					Tasks.Remove(task);

					StudentScanState state = await task;

					// Update UI
					UpdateScanGrid();
				}
				catch (Exception exc) { Console.WriteLine(exc); }

				if (nextIndex < Scans.Count)
				{
					Scans[nextIndex].student.ScanState = StudentScanState.Loading;
					UpdateScanGrid();
					Tasks.Add(LoadScanAsync(nextIndex));
					nextIndex++;
				}
			}
		}
		private void LoadFromImage(string path)
		{
			// Copy file to TODO path
			string savePath = string.Format(@"ToDoScans\{0}", Path.GetFileName(path));
			string fileName = Path.GetFileNameWithoutExtension(path);
			string ext = Path.GetExtension(path);

			if (!Directory.GetFiles(@"ToDoScans\").Contains(savePath))
			{
				int loopBound = 0;
				while (loopBound < 100 && File.Exists(savePath))
					savePath = string.Format(@"ToDoScans\{0}_{1}.{2}", fileName, loopBound, ext);
				File.Copy(path, savePath);
			}

			Files.Add(savePath);
		}
		private async Task LoadFromPDF(string path)
		{
			// Setup the external command tool
			string xpdf = @"tools\xpdf\bin64\pdftopng.exe";
			string flags = "-q -r 600";
			string savePath = @"ToDoScans\";
			string fileName = Path.GetFileNameWithoutExtension(path);
			string command = string.Format("{0}  \"{1}\"  \"{2}{3}\"", flags, path, savePath, fileName);

			// Save files from before the extraction
			List<string> excludeFiles = getFilesInToDoPath(savePath);

			// Do the conversion and await the completion                
			Process proc = new Process();
			proc.StartInfo.FileName = xpdf;
			proc.StartInfo.Arguments = command;
			proc.StartInfo.UseShellExecute = false;
			proc.StartInfo.RedirectStandardOutput = true;
			proc.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
			proc.StartInfo.CreateNoWindow = true; //not diplay a windows

			await Task.Run(async () =>
			{
				await Task.Run(() => { proc.Start(); });
			});

			while (!proc.HasExited)
			{
				// Grab list of files now
				List<string> allFiles = getFilesInToDoPath(savePath);

				// Get list of new files from difference between lists
				string[] newFiles = (from f in allFiles where !excludeFiles.Contains(f) select f).ToArray();

				// Add those files to the TODO list
				Files.AddRange(newFiles);

				// Exclude the files we just found from future updates
				excludeFiles = allFiles;

				// Update the UI
				FilesToScans();

				// Don't poll too often
				await Task.Delay(100);
			}
		}
		private List<string> getFilesInToDoPath(string path = @"ToDoScans\")
		{
			List<string> files = new List<string>();

			string extentions = "png|jpg|jpeg|bmp";

			foreach (string ext in extentions.Split('|'))
				files.AddRange(Directory.GetFiles(path, string.Format("*.{0}", ext)));

			return files;
		}
		private Task<StudentScanState> LoadScanAsync(int ScanID)
		{
			Task<StudentScanState> tsk = new Task<StudentScanState>(() => {
				// Load the Scan
				var result = Scans[ScanID].student.LoadFromScanedImage(SelectedExam, (Bitmap)Image.FromFile(Scans[ScanID].Path));
				// ... Then Unload the massive Original Image
				Scans[ScanID].student.Scan.UnloadMemory();
				// And return the Result
				return result;
			});
			tsk.Start();
			return tsk;
		}

		// Ensure Splitter Minimum size for Right Panel
		private void splitContainerHorizontal_SplitterMoved(object sender, SplitterEventArgs e)
		{
			if (splitContainerHorizontal.Width - e.SplitX < 195)
			{
				splitContainerHorizontal.SplitterDistance = splitContainerHorizontal.Width - 195;
			}
		}

		// Done Button Clicked -- Before we exit, prepare the ScansToAdd structure
		private void AddScansBtn_Click(object sender, EventArgs e)
		{
			var lst = (from s in Scans select s.student).ToList();

			// Check For Errors
			string errorStr = string.Empty;
			foreach (Student s in lst)
			{
				if (string.IsNullOrWhiteSpace(s.FirstName) || string.IsNullOrWhiteSpace(s.LastName))
					errorStr = string.Format("{0}\nOn Scan #{1}: Student's First and Last Name cannot be empty to add these scans. Please select or enter a valid student", errorStr, lst.IndexOf(s) + 1);
				if (s.CurrentScanState < StudentScanState.LoadingError)
					errorStr = string.Format("{0}\nOn Scan #{1}: Scan isn't loaded.  Scans that don't load cannot be added.", errorStr, lst.IndexOf(s) + 1);
			}

			if (!string.IsNullOrWhiteSpace(errorStr))
			{
				// Show message and return
				MessageBox.Show(errorStr, "Cannot Add These Scans Yet!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				return;
			}

			ScansToAdd = lst;
			DialogResult = DialogResult.OK;
			Close();
		}

		// UI Scan Mangement
		private void DeleteScanBtn_Click(object sender, EventArgs e)
		{
			if (ScanGrid.SelectedCells.Count < 1 ||
				ScanGrid.SelectedCells[0].RowIndex < 0 ||
				ScanGrid.SelectedCells[0].RowIndex >= Scans.Count)
				return;

			Scans.RemoveAt(ScanGrid.SelectedCells[0].RowIndex);
			BuildScanGrid();
		}
		private void StudentEdits_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter)
			{
				if ((sender as TextBox) == StudentFirstNameEdit)
					StudentLastNameEdit.Focus();
				else
				{
					if (ScanGrid.SelectedCells.Count == 0)
						StudentClassEdit.Focus();
					else
					{
						int row = ScanGrid.SelectedCells[0].RowIndex;
						if (row + 1 >= ScanGrid.Rows.Count)
							row = -1;

						ScanGrid.ClearSelection();
						ScanGrid.Rows[row + 1].Selected = true;
					}
				}
			}
			// Doesn't work TODO Manually override tab
			else if (e.KeyCode == Keys.Tab &&
				((sender as TextBox) == StudentClassEdit))
			{
				int row;

				if (ScanGrid.SelectedCells.Count == 0)
					row = -1;
				else
					row = ScanGrid.SelectedCells[0].RowIndex;

				if (row + 1 >= ScanGrid.Rows.Count)
					row = -1;

				ScanGrid.ClearSelection();
				ScanGrid.Rows[row + 1].Selected = true;
			}
		}
		private void AddFileBtn_Click(object sender, EventArgs e)
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Filter = "All Types|*.pdf;*.png;*.jpg;*.jpeg;|All Files (*.*)|*.*";
			dlg.Multiselect = true;

			// Quit this whole form if cancel
			if (dlg.ShowDialog() != DialogResult.OK)
				return;

			// Otherwise, load the files
			LoadScansFromFiles(dlg.FileNames);
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}

			if (disposing && Scans?.Count > 0)
			{
				foreach (var s in Scans)
					s.student.Scan.Dispose();
			}

			if (disposing && ScansToAdd?.Count > 0)
			{
				foreach (var s in ScansToAdd)
					s.Scan.Dispose();
			}

			base.Dispose(disposing);
		}
	}

	class StudentComparer : IEqualityComparer<Student>
	{
		public bool Equals(Student a, Student b)
		{
			return
				a.LastName	== b.LastName &&
				a.FirstName == b.FirstName &&
				a.GroupName == b.GroupName;
		}

		public int GetHashCode(Student obj)
		{
			return (obj.LastName + obj.FirstName + obj.GroupName).GetHashCode();
		}
	}
}

﻿using ScanReader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace alpha
{
	public static class Database
	{
		public const string EXAMFORMEXTENTION = "efm";
		public const string EXAMEXTENTION = "exm";

		public static string StudentsFile = "Students.xml";
		public static string ExamsPath = "Exams\\";
		public static string ExamFormsPath = "Forms\\";


		#region -- Exams -- 
		public static List<string> GetExamNames()
		{
			List<string> names = new List<string>();
			var list = Directory.GetFiles(ExamsPath, string.Format("*.{0}", EXAMEXTENTION), SearchOption.AllDirectories);
			foreach (var e in list)
			{
				names.Add(Path.GetFileNameWithoutExtension(e));
			}
			return names;
		}
		public static Exam GetExam(string name)
		{
			string exmFile = string.Format("{0}\\{1}\\{1}.{2}", ExamsPath, name, EXAMEXTENTION);			
			return Exam.LoadFromFile(exmFile);
		}

		#endregion



		#region -- ExamForm --
		public static List<string> GetFormNames()
		{
			List<string> names = new List<string>();
			var list = Directory.GetFiles(ExamFormsPath, string.Concat("*.", EXAMFORMEXTENTION), SearchOption.AllDirectories);
			foreach (var e in list)
			{
				names.Add(Path.GetFileNameWithoutExtension(e));
			}
			return names;
		}
		public static ExamForm GetForm(string name)
		{
			return ExamForm.LoadFromFile(string.Format("{0}\\{1}.{2}", ExamFormsPath, name, EXAMFORMEXTENTION));
		}


		#endregion

		#region -- Students --
		public static StudentList GetStudents()
		{
			if (!File.Exists(StudentsFile))
				return new StudentList();

			return StudentList.Load(StudentsFile);
		}
		public static void SaveStudents(StudentList list)
		{
			list.Save(StudentsFile);
		}

		#endregion



	}
}

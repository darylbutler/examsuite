﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace alpha
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        // -- New Exam Creation -------------------------------------------------------------------
        private void NewExamBtn_Click(object sender, EventArgs e)
        {
            new Dialogs.NewExamForm().ShowDialog();
        }

        // -- Form Editor -------------------------------------------------------------------------
        private void FormEditorBtn_Click(object sender, EventArgs e)
        {
            new ExamCreation.DesignExamForm().ShowDialog();
        }

        // -- Grade Exams -------------------------------------------------------------------------
        private void AddBulkBtn_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Supported Images|*.png;*.jpg;*.bmp;*.jpeg|All Images (*.*)|*.*";
            dialog.InitialDirectory = @"ToDoScans\";
            dialog.Multiselect = true;

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                foreach (string f in dialog.FileNames)
                    GradeTheseListBox.Items.Add(f);
            }
        }
        private void GradeTheseListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            DeleteBulkBtn.Enabled = (GradeTheseListBox.SelectedIndex > -1) && (GradeTheseListBox.Items.Count > 0);
        }
        private void DeleteBulkBtn_Click(object sender, EventArgs e)
        {
            if (GradeTheseListBox.SelectedIndex < 0) return;

            GradeTheseListBox.Items.RemoveAt(GradeTheseListBox.SelectedIndex);
        }
        private void GradeMultipleBtn_Click(object sender, EventArgs e)
        {
            // For each in the list, Open a GradeExamsForm
            //   If that dialog is completed successfully, then remove the item from the list
            //   If the dialog is cancel, then exit this function
            //   If the dialog was completed with an Okay, then move to the next in the list

            //while (GradeTheseListBox.Items.Count > 0)
            //{
            //    GradeExamsForm frm = new GradeExamsForm(GradeTheseListBox.Items[0].ToString());
            //    var result = frm.ShowDialog();

            //    // If the dialog saved anything, then move processed file from list
            //    if (frm.TestWasGradedAndSaved)
            //        GradeTheseListBox.Items.RemoveAt(0);

            //    // Exit early if dialog was canceled
            //    if (result == DialogResult.Cancel)
            //        return;

            //    // Other wise, result was DialogResult.Okay, so keep going with the list
            //}
        }
        private void GradeOneBtn_Click(object sender, EventArgs e)
        {
            //OpenFileDialog dialog = new OpenFileDialog();
            //dialog.Filter = "Supported Images|*.png;*.jpg;*.bmp;*.jpeg|All Images (*.*)|*.*";
            //dialog.InitialDirectory = @"ToDoScans\";
            //dialog.Multiselect = false;

            //if (dialog.ShowDialog() == DialogResult.OK)
            //    new GradeExamsForm(dialog.FileName).ShowDialog();
        }

        // -- View Exams --------------------------------------------------------------------------
        private void ViewExamsBtn_Click(object sender, EventArgs e)
        {
            new Dialogs.ViewExamForm().ShowDialog();
        }

        // -- Edit Students -----------------------------------------------------------------------
        private void EditStudentsBtn_Click(object sender, EventArgs e)
        {
            new Dialogs.EditStudentsForm().ShowDialog();
        }

        // -- Show debug form ---------------------------------------------------------------------
        private void GradeTheseListBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.T && e.Shift)
                new Form1().Show();
        }


        // -- PDF ---------------------------------------------------------------------------------
        private async void ExtractFromPDFBtn_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Protable Document Format (*.pdf)|*.pdf|All Images (*.*)|*.*";
            dialog.Multiselect = false;

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                // This is a long task, change the mouse cursor
                GradeTheseListBox.Cursor = Cursors.WaitCursor;
                GradeMultipleBtn.Enabled = false;
                GradeOneBtn.Enabled = false;
                ExtractFromPDFBtn.Enabled = false;

                // Clear the box in anticipation for new images
                GradeTheseListBox.Items.Clear();                

                // Setup the external command tool
                string xpdf = @"tools\xpdf\bin64\pdftopng.exe";
                string flags = "-q -r 600";
                string savePath = @"ToDoScans\";
                string fileName = Path.GetFileNameWithoutExtension(dialog.FileName);
                string command = string.Format("{0}  \"{1}\"  \"{2}{3}\"", flags, dialog.FileName, savePath, fileName);

                // Save files from before the extraction
                List<string> excludeFiles = getFilesInToDoPath(savePath);

                // Do the conversion and await the completion                
                Process proc = new Process();
                proc.StartInfo.FileName = xpdf;
                proc.StartInfo.Arguments = command;
                proc.StartInfo.UseShellExecute = false;
                proc.StartInfo.RedirectStandardOutput = true;
                proc.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                proc.StartInfo.CreateNoWindow = true; //not diplay a windows

                await Task.Run(async () =>
                {
                    await Task.Run(() => { proc.Start(); });                    
                });
                
                while (!proc.HasExited)
                {
                    // Grab list of files now
                    List<string> allFiles = getFilesInToDoPath(savePath);

                    // Get list of new files from difference between lists
                    string[] newFiles = (from f in allFiles where !excludeFiles.Contains(f) select f).ToArray();

                    // Add those files to the TODO list
                    GradeTheseListBox.Items.AddRange(newFiles);

                    // Scroll to newest items
                    GradeTheseListBox.TopIndex = GradeTheseListBox.Items.Count - 1;
                    excludeFiles = allFiles;

                    await Task.Delay(100);
                }

                // Finished, reset cursor
                GradeTheseListBox.Cursor = Cursors.Default;             
                GradeMultipleBtn.Enabled = true;
                GradeOneBtn.Enabled = true;
                ExtractFromPDFBtn.Enabled = true;
            }
        }
        private List<string> getFilesInToDoPath(string path)
        {
            List<string> files = new List<string>();

            string extentions = "png|jpg|jpeg|bmp";

            foreach (string ext in extentions.Split('|'))
                files.AddRange(Directory.GetFiles(path, string.Format("*.{0}", ext)));

            return files;
        }
    }
}
